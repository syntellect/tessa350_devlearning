import { DeskiManager, CipherInfo } from './deskiManager';
import {
  ApplicationExtension,
  IApplicationExtensionMetadataContext,
  WorkspaceStorage,
  MetadataStorage
} from 'tessa';
import { Result } from 'tessa/platform';
import { MenuAction, showNotEmpty } from 'tessa/ui';
import { FileExtension, FileExtensionContext } from 'tessa/ui/files';
import { FileContainer, IFile } from 'tessa/files';
import {
  ValidationResultBuilder,
  ValidationResultType,
  ValidationResult
} from 'tessa/platform/validation';
import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
import { LocalizationManager } from 'tessa/localization';
import { FileViewModel, FileListViewModel } from 'tessa/ui/cards/controls';
import Platform from 'common/platform';

const InfoKey = '.deskiExtension';
// tslint:disable-next-line: no-any
const BASE_PATH = (window as any).__BASE_PATH__;
const APP_URL = BASE_PATH ? `${window.location.origin}/${BASE_PATH}` : window.location.origin;
const MASTER_KEYS_UPDATE_INTERVAL = 1000 * 60 * 60 * 4;

export class DeskiExtension extends ApplicationExtension {
  private _lastMasterKeysUpdate: number;

  public afterMetadataReceived(_context: IApplicationExtensionMetadataContext) {
    if (!DeskiManager.instance.deskiEnabled || Platform.isMobile()) {
      return;
    }

    (async () => {
      await DeskiManager.instance.checkDeski(true);
      const cipher: CipherInfo = MetadataStorage.instance.info.get('DeskiCipher');
      const result = await DeskiManager.instance.updateMasterKeys(cipher);
      MetadataStorage.instance.info.delete('DeskiCipher');
      if (!result.isSuccessful) {
        console.error(result.format());
        await showNotEmpty(result);
        return;
      }
      this._lastMasterKeysUpdate = Date.now();

      // каждые 5 минут проверяем время, которое прошло с последнего обновления мастер ключа
      // если прошло больше константы (4 часа), то обновляемся
      setInterval(async () => {
        const now = Date.now();
        if (now - this._lastMasterKeysUpdate >= MASTER_KEYS_UPDATE_INTERVAL) {
          const result = await DeskiManager.instance.updateMasterKeys();
          if (!result.isSuccessful) {
            console.error(result.format());
            return;
          }
          this._lastMasterKeysUpdate = now;
        }
      }, 300000);
    })();
  }
}

export class DeskiFileExtension extends FileExtension {
  public openingMenu(context: FileExtensionContext) {
    if (!DeskiManager.instance.deskiEnabled || Platform.isMobile()) {
      return;
    }

    const file = context.file.model;

    const editCollapsed = !file.permissions.canEdit;
    const previewIndex = context.actions.findIndex(x => x.name === 'Preview');

    context.actions.splice(
      previewIndex + 1,
      0,
      new MenuAction(
        'EditFile',
        '$UI_Controls_FilesControl_OpenForEdit',
        'ta icon-thin-002',
        async () => {
          await openFile(context.file, 'file', true);
        },
        null,
        editCollapsed
      ),
      new MenuAction(
        'EditFile',
        '$UI_Controls_FilesControl_OpenInFolderForEdit',
        'ta icon-thin-101',
        async () => {
          await openFile(context.file, 'folder', true);
        },
        null,
        editCollapsed
      ),
      new MenuAction(
        'EditFile',
        '$UI_Controls_FilesControl_OpenForRead',
        'ta icon-thin-008',
        async () => {
          await openFile(context.file, 'file', false);
        }
      ),
      new MenuAction(
        'EditFile',
        '$UI_Controls_FilesControl_OpenInFolderForRead',
        'ta icon-thin-101',
        async () => {
          await openFile(context.file, 'folder', false);
        },
        null,
        !editCollapsed
      )
    );

    // если файл был открыт на редактирование, то удаляем открытую версию из deski
    if (!!file.lastVersion.info[InfoKey]) {
      const revertMenu = context.actions.find(x => x.name === 'Revert');
      if (revertMenu) {
        // сначала выполняем обычный revert
        // если все прошло успешно, то взываем удаление контента в deski
        const closureAction = revertMenu.action;
        revertMenu.action = async e => {
          if (closureAction) {
            // костыль, чтобы понять, что основной revert прошел успешно
            // tslint:disable-next-line: no-any
            const result = ((await closureAction(e)) as any) as boolean;
            if (result && !file.wasModified && file.isDirty) {
              try {
                context.file.isLoading = true;
                file.isDirty = false;
                await removeFile(file);
              } finally {
                context.file.isLoading = false;
              }
            }
          }
        };
      }
    }
  }
}

export class DeskiUIExtension extends CardUIExtension {
  shouldExecute() {
    if (!DeskiManager.instance.deskiAvailable) {
      return false;
    }
    return true;
  }
  public initialized(context: ICardUIExtensionContext) {
    const fileControls = context.model.controlsBag.filter(
      x => x instanceof FileListViewModel
    ) as FileListViewModel[];
    for (let control of fileControls) {
      control.fileDoubleClickAction = async file => {
        const canEdit = file.model.permissions.canEdit;
        await openFile(file, 'file', canEdit);
      };
    }
  }
  public async saving(context: ICardUIExtensionContext) {
    let files = context.fileContainer.files;
    if (!files || files.length === 0) {
      return;
    }

    const editedFiles = files.filter(x => !!x.lastVersion.info[InfoKey]);
    if (editedFiles.length === 0) {
      return;
    }

    const savingResult = new ValidationResultBuilder();
    for (let file of editedFiles) {
      try {
        const { info: fileInfo, result: fileInfoResult } = await DeskiManager.instance.getFileInfo(
          APP_URL,
          file.lastVersion.id,
          true
        );
        if (fileInfoResult) {
          savingResult.add(fileInfoResult);
          continue;
        }

        if (!fileInfo) {
          savingResult.add(
            ValidationResultType.Error,
            `Can not find deski's FileInfo. File: ${file.lastVersion.id}`
          );
          continue;
        }

        if (fileInfo!.IsLocked) {
          savingResult.add(
            ValidationResultType.Error,
            LocalizationManager.instance.format('$UI_Cards_FileSaving_Locked', fileInfo.Name)
          );
          continue;
        }

        if (!fileInfo.IsModified) {
          await removeFile(file);
          continue;
        }

        const fileDataResult = await DeskiManager.instance.getFileData(APP_URL, fileInfo.ID);
        if (fileDataResult.result) {
          context.validationResult.add(fileDataResult.result);
          continue;
        }

        // не fileInfo.Name, т.к. файл может быть переименован
        file.replace(new File([fileDataResult.data!], file.name), true);

        file.isDirty = false;
        const cacheResult = await DeskiManager.instance.cacheModFileWithNewId(
          APP_URL,
          fileInfo.ID,
          file.lastVersion.id
        );
        if (!cacheResult.success) {
          savingResult.add(cacheResult.result);
        }
        await removeFile(file);
      } catch (error) {
        savingResult.add(ValidationResult.fromError(error));
      }
    }

    const result = savingResult.build();
    if (result.items.length > 0) {
      console.error(result.format());
    }
  }
  public async finalized(context: ICardUIExtensionContext) {
    await clearEditedFiles(context.fileContainer);
  }
}

async function openFile(
  fileVM: FileViewModel,
  mode: 'file' | 'folder',
  editable: boolean
): Promise<void> {
  if (!(await DeskiManager.instance.checkDeski())) {
    return;
  }

  const file = fileVM.model;

  if (editable) {
    file.lastVersion.info[InfoKey] = true;
  }

  let result = await DeskiManager.instance.setAppInfo(APP_URL, DeskiManager.instance.masterKeys);
  if (!result.success) {
    await showNotEmpty(result.result);
    return;
  }

  const contentInfo = await DeskiManager.instance.getContentInfo(APP_URL, file.lastVersion.id);
  if (contentInfo.result) {
    await showNotEmpty(contentInfo.result);
    return;
  }
  if (!contentInfo.isCached) {
    let content: Result<File>;
    if (file.lastVersion.content) {
      content = {
        data: file.lastVersion.content,
        validationResult: new ValidationResultBuilder().build()
      };
    } else {
      content = await file.source.getContent(file);
    }

    if (!content.data) {
      await showNotEmpty(
        ValidationResult.fromText('$Deski_Cant_Load_Content', ValidationResultType.Error)
      );
      return;
    }
    result = await DeskiManager.instance.cacheContent(
      APP_URL,
      file.lastVersion.id,
      file.lastVersion.name,
      content.data
    );
    if (!result.success) {
      await showNotEmpty(result.result);
      return;
    }
  }

  result = await DeskiManager.instance.openFile(APP_URL, file.lastVersion.id, mode, editable);
  if (!result.success) {
    await showNotEmpty(result.result);
    return;
  }

  if (editable) {
    file.isDirty = true;
  }
}

// при закрытии вкладки или обновлении страницы вручную надо удалить окрытые файлы
window.addEventListener('beforeunload', function() {
  if (!DeskiManager.instance.deskiAvailable) {
    return;
  }

  const cards = WorkspaceStorage.instance.cards;
  if (cards.size === 0) {
    return;
  }

  (async () => {
    for (let card of cards.values()) {
      if (card.editor.cardModel) {
        await clearEditedFiles(card.editor.cardModel.fileContainer);
      }
    }
  })();
});

const clearEditedFiles = async (fileContainer: FileContainer) => {
  const files = fileContainer.files;
  if (!files) {
    return;
  }

  const editedFiles = files.filter(x => !!x.lastVersion.info[InfoKey]);
  if (editedFiles.length === 0) {
    return;
  }

  for (let file of editedFiles) {
    try {
      await removeFile(file);
    } catch (error) {
      console.error(error);
    }
  }
};

const removeFile = async (file: IFile) => {
  await DeskiManager.instance.removeFile(APP_URL, file.lastVersion.id, true);
  delete file.lastVersion.info[InfoKey];
};
