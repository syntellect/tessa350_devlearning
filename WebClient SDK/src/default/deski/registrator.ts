import { ExtensionContainer, ExtensionStage } from 'tessa/extensions';

import { DeskiExtension, DeskiFileExtension, DeskiUIExtension } from './deskiExtension';

ExtensionContainer.instance.registerExtension({
  extension: DeskiExtension,
  stage: ExtensionStage.AfterPlatform
});
ExtensionContainer.instance.registerExtension({
  extension: DeskiFileExtension,
  stage: ExtensionStage.AfterPlatform
});
ExtensionContainer.instance.registerExtension({
  extension: DeskiUIExtension,
  stage: ExtensionStage.AfterPlatform
});
