import { ValidationResult, ValidationResultType } from 'tessa/platform/validation';
import { MetadataStorage } from 'tessa';
import { DeskiInfo, DeskiMetadata, getTypedFieldValue } from 'tessa/platform';
import { showLoadingOverlay, showConfirm, showDownloadDeskiDialog } from 'tessa/ui';
import { LocalizationManager } from 'tessa/localization';
import { IStorage } from 'tessa/platform/storage';
import { CardService, CardRequest } from 'tessa/cards/service';

const DESKI_SERVER_PATH = 'http://127.0.0.1';
let LOCK_DESKI_FIND = false;

export type DeskiFileInfo = {
  Name: string;
  Path: string;
  Modified: string;
  IsModified: boolean;
  IsLocked: boolean;
  Size: number;
  ID: string;
  Editable: boolean;
  App: string;
};

export type MasterKey = {
  Key: string;
  ExpiresAt: string;
};

export interface CipherInfo {
  Info: IStorage | null;
  LocalExpiryDates: string[];
  LocalPrivateKeys: string[];
}

export class DeskiManager {
  //#region ctor

  private constructor() {}

  //#endregion

  //#region instance

  private static _instance: DeskiManager;

  public static get instance(): DeskiManager {
    if (!DeskiManager._instance) {
      DeskiManager._instance = new DeskiManager();
    }
    return DeskiManager._instance;
  }

  //#endregion

  //#region props

  private _masterKeys: MasterKey[] = [];

  public get masterKeys(): ReadonlyArray<MasterKey> {
    return this._masterKeys;
  }

  //#endregion

  //#region methods

  public get deskiEnabled(): boolean {
    return MetadataStorage.instance.commonMetadata.deskiInfo.enabled;
  }

  public get deskiAvailable(): boolean {
    return !!(
      MetadataStorage.instance.commonMetadata.deskiInfo.enabled &&
      MetadataStorage.instance.commonMetadata.deskiInfo.port
    );
  }

  public getDeskiUrl(deskiPort?: number): string {
    return `${DESKI_SERVER_PATH}:${deskiPort ||
      MetadataStorage.instance.commonMetadata.deskiInfo.port ||
      0}`;
  }

  public async findDeski(
    delayTimeout: boolean = false
  ): Promise<{ port?: number; info?: DeskiInfo }> {
    if (LOCK_DESKI_FIND) {
      console.log("Another deski's extension is already searching for its host companion!");
      return {};
    }

    try {
      LOCK_DESKI_FIND = true;
      let port = 7711;
      const maxIterations = 20;
      let response;
      const shifts = stringToArray('TESSA');
      let deskiPort: number | undefined = 0;
      let info: DeskiInfo | undefined;

      for (let i = 0; port < 65535 && i < maxIterations; i++) {
        let timeout = !delayTimeout ? 1000 : i < 3 ? 1000 : 250;
        try {
          console.log(`Searching for Deski: port ${port}`);
          response = await (
            await fetchWithTimeout(timeout, fetch(`${DESKI_SERVER_PATH}:${port}/tessa-deski-info`))
          ).text();
          try {
            const resp: DeskiInfo = JSON.parse(response);
            if (resp && resp.ShortName === 'deski') {
              console.log(
                `Deski v${resp.VerMajor}.${resp.VerMinor} is found: ${DESKI_SERVER_PATH}:${port}`
              );
              deskiPort = port;
              info = resp;
              console.log(this.getDeskiUrl(deskiPort));
              return { port: deskiPort, info };
            } else {
              console.log(`Another app is listening at port ${port} (not Deski)`);
            }
          } catch (err) {
            console.log(`Another app is listening at port ${port} (not Deski)`);
          }
        } catch (error) {
          console.log(error);
        }
        port += shifts[i % 5];
      }

      if (!deskiPort) {
        console.log("Deski isn't found");
      }

      return { port: deskiPort, info };
    } finally {
      LOCK_DESKI_FIND = false;
    }
  }

  public async checkDeski(silent: boolean = false): Promise<boolean> {
    if (!MetadataStorage.instance.commonMetadata.deskiInfo.port) {
      if (silent) {
        const resp = await DeskiManager.instance.findDeski(false);
        if (resp.port) {
          MetadataStorage.instance.setCommonMetadataDeskiMeta(
            new DeskiMetadata({ enabled: true, ...resp })
          );
        }
        return !!MetadataStorage.instance.commonMetadata.deskiInfo.port;
      }

      const resp = await showLoadingOverlay(
        async () => await DeskiManager.instance.findDeski(true),
        LocalizationManager.instance.localize('$About_Web_Deski_LoadingCaption')
      );
      if (resp.port) {
        MetadataStorage.instance.setCommonMetadataDeskiMeta(
          new DeskiMetadata({ enabled: true, ...resp })
        );
      }

      // если мы не нашли дески, то предлагаем скачать его
      if (!LOCK_DESKI_FIND && !MetadataStorage.instance.commonMetadata.deskiInfo.port) {
        if (await showConfirm('$Deski_DownloadConfirmation')) {
          await showDownloadDeskiDialog();
          return false;
        }
      }
    }

    // если мы его нашли, то нужно проверить версию
    if (
      !Boolean(window.sessionStorage.getItem('DeskiUpdateRequest')) &&
      MetadataStorage.instance.commonMetadata.deskiInfo.port
    ) {
      const deskiVersionFromServer = MetadataStorage.instance.info.get('DeskiVersion');
      const deskiInfo = MetadataStorage.instance.commonMetadata.deskiInfo;
      if (
        deskiVersionFromServer &&
        deskiInfo.version &&
        deskiInfo.version !== deskiVersionFromServer
      ) {
        // предлагаем скачать новую версию
        window.sessionStorage.setItem('DeskiUpdateRequest', 'true');
        const updateDeskiResult = await showConfirm(
          LocalizationManager.instance.format(
            '$Deski_UpdateConfirmation',
            deskiInfo.version,
            deskiVersionFromServer
          )
        );
        if (updateDeskiResult) {
          await showDownloadDeskiDialog();
          return false;
        }
      }
    }

    return !!MetadataStorage.instance.commonMetadata.deskiInfo.port;
  }

  public async updateMasterKeys(cipher?: CipherInfo): Promise<ValidationResult> {
    if (!cipher) {
      const request = new CardRequest();
      request.requestType = '74b9cb3a-1a49-4624-baa2-1cc2d0232229'; // ResolveUserCipherInfo
      const response = await CardService.instance.request(request);
      if (!response.validationResult.isSuccessful) {
        return response.validationResult.build();
      }

      const cipherObject = response.info['UserCipherInfoObject'];
      if (!cipherObject) {
        return ValidationResult.fromText(
          'Can not find UserCipherInfoObject in response.',
          ValidationResultType.Error
        );
      }

      cipher = {
        Info: null,
        LocalExpiryDates: cipherObject['LocalExpiryDates'].map(x => getTypedFieldValue(x)),
        LocalPrivateKeys: cipherObject['LocalPrivateKeys'].map(x => getTypedFieldValue(x))
      };
    }

    const length = cipher.LocalPrivateKeys.length;
    const keys: MasterKey[] = [];
    for (let i = 0; i < length; i++) {
      keys.push({
        Key: cipher.LocalPrivateKeys[i],
        ExpiresAt: cipher.LocalExpiryDates[i]
      });
    }

    this._masterKeys = keys;

    return ValidationResult.empty;
  }

  //#endregion

  //#region service

  private getRequestOptions(opt?: RequestInit): RequestInit {
    const defaultOpt: RequestInit = {
      mode: 'cors'
    };
    return opt ? Object.assign({}, defaultOpt, opt) : defaultOpt;
  }

  public async setAppInfo(
    appUrl: string,
    keys: ReadonlyArray<MasterKey>
  ): Promise<{ success: true } | { success: false; result: ValidationResult }> {
    const deskiUrl = new URL(`${this.getDeskiUrl()}/api/setappinfo`);
    deskiUrl.searchParams.append('app', appUrl);

    const options = this.getRequestOptions({ method: 'POST' });

    // const formData = new FormData();
    // formData.append('BaseUrl', appUrl);
    // formData.append('KnownKeys', JSON.stringify(keys));
    // options.body = formData;

    options.body = JSON.stringify({
      BaseUrl: appUrl,
      KnownKeys: keys
    });

    const headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=utf-8');
    options.headers = headers;

    try {
      const response = await fetch(deskiUrl.href, options);
      if (!response.ok) {
        const result = await getJSONOrMessageFromResponse(response);
        return {
          success: false,
          result: ValidationResult.fromText(result.message, ValidationResultType.Error)
        };
      }

      return { success: true };
    } catch (e) {
      return {
        success: false,
        result: ValidationResult.fromError(e)
      };
    }
  }

  public async getContentInfo(
    appUrl: string,
    id: string
  ): Promise<{ isCached: boolean; result?: ValidationResult }> {
    const deskiUrl = new URL(`${this.getDeskiUrl()}/api/getcontentinfo`);
    deskiUrl.searchParams.append('app', appUrl);
    deskiUrl.searchParams.append('id', id);

    const options = this.getRequestOptions({ method: 'GET' });

    try {
      const response = await fetch(deskiUrl.href, options);
      const result = await getJSONOrMessageFromResponse(response);
      if (!response.ok) {
        return {
          isCached: false,
          result: ValidationResult.fromText(result.message, ValidationResultType.Error)
        };
      }

      return { isCached: result['IsCashed'] };
    } catch (e) {
      return { isCached: false, result: ValidationResult.fromError(e) };
    }
  }

  public async cacheContent(
    appUrl: string,
    id: string,
    name: string,
    content: File
  ): Promise<{ success: true } | { success: false; result: ValidationResult }> {
    const deskiUrl = new URL(`${this.getDeskiUrl()}/api/cachecontent`);
    deskiUrl.searchParams.append('app', appUrl);
    deskiUrl.searchParams.append('id', id);
    deskiUrl.searchParams.append('name', name);

    const options = this.getRequestOptions({ method: 'POST' });

    options.body = new Blob([content]);

    const headers = new Headers();
    headers.append('Content-Type', 'applcation/octet-stream');
    options.headers = headers;

    try {
      const response = await fetch(deskiUrl.href, options);
      if (!response.ok) {
        const result = await getJSONOrMessageFromResponse(response);
        return {
          success: false,
          result: ValidationResult.fromText(result.message, ValidationResultType.Error)
        };
      }

      return { success: true };
    } catch (e) {
      return {
        success: false,
        result: ValidationResult.fromError(e)
      };
    }
  }

  public async openFile(
    appUrl: string,
    id: string,
    mode: 'file' | 'folder',
    editable: boolean
  ): Promise<{ success: true } | { success: false; result: ValidationResult }> {
    const deskiUrl = new URL(`${this.getDeskiUrl()}/api/openfile`);
    deskiUrl.searchParams.append('app', appUrl);
    deskiUrl.searchParams.append('id', id);
    deskiUrl.searchParams.append('mode', mode);
    deskiUrl.searchParams.append('editable', String(editable));

    const options = this.getRequestOptions({ method: 'GET' });

    try {
      const response = await fetch(deskiUrl.href, options);
      if (!response.ok) {
        const result = await getJSONOrMessageFromResponse(response);
        return {
          success: false,
          result: ValidationResult.fromText(result.message, ValidationResultType.Error)
        };
      }

      return { success: true };
    } catch (e) {
      return {
        success: false,
        result: ValidationResult.fromError(e)
      };
    }
  }

  public async getFileInfo(
    appUrl: string,
    id: string,
    editable: boolean
  ): Promise<{ info?: DeskiFileInfo; result?: ValidationResult }> {
    const deskiUrl = new URL(`${this.getDeskiUrl()}/api/getfileinfo`);
    deskiUrl.searchParams.append('app', appUrl);
    deskiUrl.searchParams.append('id', id);
    deskiUrl.searchParams.append('editable', String(editable));

    const options = this.getRequestOptions({ method: 'GET' });

    try {
      const response = await fetch(deskiUrl.href, options);
      const result = await getJSONOrMessageFromResponse(response);
      if (!response.ok) {
        return {
          result: ValidationResult.fromText(result.message, ValidationResultType.Error)
        };
      }

      return { info: result };
    } catch (e) {
      return { result: ValidationResult.fromError(e) };
    }
  }

  public async getFileData(
    appUrl: string,
    id: string
  ): Promise<{ data?: Blob; result?: ValidationResult }> {
    const deskiUrl = new URL(`${this.getDeskiUrl()}/api/getfiledata`);
    deskiUrl.searchParams.append('app', appUrl);
    deskiUrl.searchParams.append('id', id);

    const options = this.getRequestOptions({ method: 'GET' });

    try {
      const response = await fetch(deskiUrl.href, options);
      if (!response.ok) {
        const result = await getJSONOrMessageFromResponse(response);
        return {
          result: ValidationResult.fromText(result.message, ValidationResultType.Error)
        };
      }

      const data = await response.blob();
      return { data };
    } catch (e) {
      return { result: ValidationResult.fromError(e) };
    }
  }

  public async cacheModFileWithNewId(
    appUrl: string,
    id: string,
    newId: string
  ): Promise<{ success: true } | { success: false; result: ValidationResult }> {
    const deskiUrl = new URL(`${this.getDeskiUrl()}/api/cachemodified`);
    deskiUrl.searchParams.append('app', appUrl);
    deskiUrl.searchParams.append('id', id);
    deskiUrl.searchParams.append('newid', newId);

    const options = this.getRequestOptions({ method: 'GET' });

    try {
      const response = await fetch(deskiUrl.href, options);
      if (!response.ok) {
        const result = await getJSONOrMessageFromResponse(response);
        return {
          success: false,
          result: ValidationResult.fromText(result.message, ValidationResultType.Error)
        };
      }

      return { success: true };
    } catch (e) {
      return {
        success: false,
        result: ValidationResult.fromError(e)
      };
    }
  }

  public async removeFile(
    appUrl: string,
    id: string,
    editable: boolean
  ): Promise<{ success: true } | { success: false; result: ValidationResult }> {
    const deskiUrl = new URL(`${this.getDeskiUrl()}/api/removefile`);
    deskiUrl.searchParams.append('app', appUrl);
    deskiUrl.searchParams.append('id', id);
    deskiUrl.searchParams.append('editable', String(editable));

    const options = this.getRequestOptions({ method: 'GET' });

    try {
      const response = await fetch(deskiUrl.href, options);
      if (!response.ok) {
        const result = await getJSONOrMessageFromResponse(response);
        return {
          success: false,
          result: ValidationResult.fromText(result.message, ValidationResultType.Error)
        };
      }

      return { success: true };
    } catch (e) {
      return {
        success: false,
        result: ValidationResult.fromError(e)
      };
    }
  }

  public async getContent(
    appUrl: string,
    id: string
  ): Promise<{ data?: Blob; result?: ValidationResult }> {
    const deskiUrl = new URL(`${this.getDeskiUrl()}/api/getcontent`);
    deskiUrl.searchParams.append('app', appUrl);
    deskiUrl.searchParams.append('id', id);

    const options = this.getRequestOptions({ method: 'GET' });

    try {
      const response = await fetch(deskiUrl.href, options);
      if (!response.ok) {
        const result = await getJSONOrMessageFromResponse(response);
        return {
          result: ValidationResult.fromText(result.message, ValidationResultType.Error)
        };
      }

      const data = await response.blob();
      return { data };
    } catch (e) {
      return { result: ValidationResult.fromError(e) };
    }
  }

  public async getOpenFiles(
    appUrl: string
  ): Promise<{
    files?: Array<DeskiFileInfo>;
    result?: ValidationResult;
  }> {
    const deskiUrl = new URL(`${this.getDeskiUrl()}/api/getopenfiles`);
    deskiUrl.searchParams.append('app', appUrl);

    const options = this.getRequestOptions({ method: 'GET' });

    try {
      const response = await fetch(deskiUrl.href, options);
      const result = await getJSONOrMessageFromResponse(response);
      if (!response.ok) {
        return {
          result: ValidationResult.fromText(result.message, ValidationResultType.Error)
        };
      }

      return { files: result };
    } catch (e) {
      return { result: ValidationResult.fromError(e) };
    }
  }

  //#endregion
}

// tslint:disable-next-line: no-any
function fetchWithTimeout(ms: number, promise: Promise<any>): Promise<any> {
  return new Promise((resolve, reject) => {
    let didTimeOut = false;
    const timeout = setTimeout(function() {
      didTimeOut = true;
      reject(new Error('Request timed out'));
    }, ms);

    promise
      .then(response => {
        // Clear the timeout as cleanup
        clearTimeout(timeout);
        if (!didTimeOut) {
          resolve(response);
        }
      })
      .catch(err => {
        clearTimeout(timeout);
        // Rejection already happened with setTimeout
        if (didTimeOut) return;
        // Reject with error
        reject(err);
      });
  });
}

function stringToArray(str: string) {
  const arr: number[] = [];
  for (let i = 0; i < str.length; i++) {
    arr.push(str.charCodeAt(i));
  }
  return arr;
}

async function getJSONOrMessageFromResponse(response: Response) {
  const contentType = response.headers.get('content-type');
  if (contentType && contentType.indexOf('application/json') !== -1) {
    return await response.json();
  }

  return {
    message: response.statusText
  };
}
