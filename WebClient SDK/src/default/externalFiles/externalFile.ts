import { TessaFile, FileCategory, FileType, IFileSource, FilePermissionsSealed, IFile } from 'tessa/files';

export class ExternalFile extends TessaFile {

  constructor(
    id: guid,
    name: string,
    category: FileCategory | null,
    type: FileType,
    source: IFileSource,
    permissions: FilePermissionsSealed | null,
    isLocal: boolean = true,
    origin: IFile | null = null,
    description: string = ''
  ) {
    super(
      id,
      name,
      category,
      type,
      source,
      permissions,
      isLocal,
      origin
    );

    this.description = description;
  }

  public description: string;

}