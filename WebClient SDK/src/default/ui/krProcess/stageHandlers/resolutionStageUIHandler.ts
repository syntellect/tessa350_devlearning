import {
  CardRow,
  CardFieldChangedEventArgs,
  CardRowState,
  CardRowStateChangedEventArgs
} from 'tessa/cards';
import { ICardModel, IControlViewModel } from 'tessa/ui/cards';
import { plainColumnName, sectionName } from 'tessa/workflow';
import {
  KrStageTypeUIHandler,
  IKrStageTypeUIHandlerContext,
  StageTypeHandlerDescriptor,
  resolutionDescriptor
} from 'tessa/workflow/krProcess';
import { Visibility, DotNetType, Guid } from 'tessa/platform';
import { ArrayStorage, CollectionChangedEventArgs } from 'tessa/platform/storage';

export class ResolutionStageUIHandler extends KrStageTypeUIHandler {
  //#region ctor

  constructor() {
    super();

    this._subscribedTo = new Set();
  }

  //#endregion

  //#region fields

  private _settings: CardRow | null;
  private _performers: ArrayStorage<CardRow> | null;
  private _card: ICardModel | null;
  private _controller: IControlViewModel | null;
  private _massCreation: IControlViewModel | null;
  private _majorPerformer: IControlViewModel | null;
  private _subscribedTo: Set<CardRow>;

  private readonly _krResolutionSettingsVirtual = 'KrResolutionSettingsVirtual';
  private readonly _krPerformersVirtual = 'KrPerformersVirtual';

  private readonly _controllerIdStr = plainColumnName(
    this._krResolutionSettingsVirtual,
    'ControllerID'
  );
  private readonly _controllerNameStr = plainColumnName(
    this._krResolutionSettingsVirtual,
    'ControllerName'
  );
  private readonly _plannedStr = plainColumnName(this._krResolutionSettingsVirtual, 'Planned');
  private readonly _durationInDaysStr = plainColumnName(
    this._krResolutionSettingsVirtual,
    'DurationInDays'
  );
  private readonly _withControlStr = plainColumnName(
    this._krResolutionSettingsVirtual,
    'WithControl'
  );
  private readonly _massCreationStr = plainColumnName(
    this._krResolutionSettingsVirtual,
    'MassCreation'
  );
  private readonly _majorPerformerStr = plainColumnName(
    this._krResolutionSettingsVirtual,
    'MajorPerformer'
  );
  private readonly _performersStr = sectionName(this._krPerformersVirtual);

  //#endregion

  //#region handlers

  public descriptors(): StageTypeHandlerDescriptor[] {
    return [resolutionDescriptor];
  }

  public initialize(context: IKrStageTypeUIHandlerContext) {
    this._settings = context.row;
    this._settings.fieldChanged.add(this.onSettingsFieldChanged);
    this._performers = context.cardModel.card.sections.get(this._performersStr)!.rows;
    this._performers.collectionChanged.add(this.onPerformersChanged);

    for (let performer of this._performers) {
      if (this.alivePerformer(performer)) {
        this._subscribedTo.add(performer);
        performer.stateChanged.add(this.onPerformerStateChanged);
      }
    }

    this._card = context.rowModel!;
    this._controller = this._card.controls.get('Controller')!;
    this._massCreation = this._card.controls.get('MassCreation')!;
    this._majorPerformer = this._card.controls.get('MajorPerformer')!;

    // tslint:disable-next-line: triple-equals
    if (this._settings.get(this._withControlStr) != undefined) {
      this._controller.captionVisibility = Visibility.Visible;
      this._controller.controlVisibility = Visibility.Visible;
    }

    // tslint:disable-next-line: triple-equals
    if (this._settings.get(this._massCreationStr) != undefined) {
      this._majorPerformer.controlVisibility = Visibility.Visible;
    }

    if (this._subscribedTo.size >= 2) {
      this._massCreation.controlVisibility = Visibility.Visible;
    }
  }

  public finalize(_context: IKrStageTypeUIHandlerContext) {
    this._settings!.fieldChanged.remove(this.onSettingsFieldChanged);
    this._settings = null;
    this._performers!.collectionChanged.remove(this.onPerformersChanged);
    this._performers = null;

    for (let performer of this._subscribedTo) {
      performer.stateChanged.remove(this.onPerformerStateChanged);
    }

    this._subscribedTo.clear();
    this._card = null;
    this._controller = null;
    this._massCreation = null;
    this._majorPerformer = null;
  }

  //#endregion

  //#region methods

  private onSettingsFieldChanged = (e: CardFieldChangedEventArgs) => {
    if (e.fieldName === this._plannedStr) {
      if (e.fieldValue) {
        this._settings!.set(this._durationInDaysStr, null);
      }
    } else if (e.fieldName === this._durationInDaysStr) {
      if (e.fieldValue) {
        this._settings!.set(this._plannedStr, null);
      }
    } else if (e.fieldName === this._withControlStr) {
      let visibility = Visibility.Collapsed;
      if (e.fieldValue) {
        visibility = Visibility.Visible;
      } else {
        this._settings!.set(this._controllerIdStr, null);
        this._settings!.set(this._controllerNameStr, null);
      }

      this._controller!.captionVisibility = visibility;
      this._controller!.controlVisibility = visibility;
    } else if (e.fieldName === this._massCreationStr) {
      let visibility = Visibility.Collapsed;
      if (e.fieldValue) {
        visibility = Visibility.Visible;
      } else {
        this._settings!.set(this._majorPerformerStr, false, DotNetType.Boolean);
      }

      this._controller!.controlVisibility = visibility;
    }
  };

  private onPerformerStateChanged = (e: CardRowStateChangedEventArgs) => {
    if (e.newState === CardRowState.Deleted) {
      this.performersChanged(CardRowState.Deleted, e.row);
    }

    if (e.oldState === CardRowState.Deleted) {
      this.performersChanged(CardRowState.Inserted, e.row);
    }
  };

  private onPerformersChanged = (e: CollectionChangedEventArgs<CardRow>) => {
    if (e.added.length > 0) {
      for (let performer of e.added) {
        this.performersChanged(CardRowState.Inserted, performer);
      }
    }

    if (e.removed.length > 0) {
      for (let performer of e.removed) {
        this.performersChanged(CardRowState.Deleted, performer);
      }
    }
  };

  private performersChanged = (action: CardRowState, performer: CardRow) => {
    if (action === CardRowState.Inserted) {
      if (!this._subscribedTo.has(performer)) {
        this._subscribedTo.add(performer);
        performer.stateChanged.add(this.onPerformerStateChanged);
      }
      // Действия могут производиться только в текущем диалоге, а значит,
      // всякий новодобавленный оказывается в текущем этапе. По этой причине
      // требуется наличие лишь одного исполняющего в таблице. Второй уже
      // добавлен, но его связь и прочие поля будут указаны позже.
      if (this._performers!.filter(x => this.alivePerformer(x)).length >= 1) {
        this.enableMassCreation(true);
      }
    } else if (action === CardRowState.Deleted) {
      this._subscribedTo.delete(performer);
      performer.stateChanged.remove(this.onPerformerStateChanged);

      if (this._performers!.filter(x => this.alivePerformer(x)).length < 2) {
        this.enableMassCreation(false);
      }
    }
  };

  private alivePerformer(performer: CardRow): boolean {
    if (performer.state === CardRowState.Deleted) {
      return false;
    }

    const stageRowId = performer.tryGet('StageRowID');
    // tslint:disable-next-line:triple-equals
    if (stageRowId == undefined) {
      return false;
    }

    return Guid.equals(stageRowId, this._settings!.get('RowID'));
  }

  private enableMassCreation(value: boolean) {
    this._settings!.set(this._massCreationStr, value, DotNetType.Boolean);
    // this._massCreation!.controlVisibility = value ? Visibility.Visible : Visibility.Collapsed;
  }

  //#endregion
}
