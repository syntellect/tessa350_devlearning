import {
  KrStageTypeUIHandler,
  IKrStageTypeUIHandlerContext,
  StageTypeHandlerDescriptor,
  dialogDescriptor
} from 'tessa/workflow/krProcess';
import { plainColumnName } from 'tessa/workflow';
import {
  ValidationResult,
  ValidationResultType,
  ValidationResultBuilder
} from 'tessa/platform/validation';
import { GridViewModel, GridRowEventArgs } from 'tessa/ui/cards/controls';
import { showNotEmpty } from 'tessa/ui';

// tslint:disable: triple-equals

export class DialogUIHandler extends KrStageTypeUIHandler {
  private _cardStoreModeId: string = plainColumnName(
    'KrDialogStageTypeSettingsVirtual',
    'CardStoreModeID'
  );
  private _openModeId: string = plainColumnName('KrDialogStageTypeSettingsVirtual', 'OpenModeID');
  private _dialogTypeId: string = plainColumnName(
    'KrDialogStageTypeSettingsVirtual',
    'DialogTypeID'
  );
  private _templateId: string = plainColumnName('KrDialogStageTypeSettingsVirtual', 'TemplateID');

  public descriptors(): StageTypeHandlerDescriptor[] {
    return [dialogDescriptor];
  }

  public validate(context: IKrStageTypeUIHandlerContext) {
    if (context.row.tryGet(this._cardStoreModeId) == undefined) {
      context.validationResult.add(
        ValidationResult.fromText(
          '$KrStages_Dialog_CardStoreModeNotSpecified',
          ValidationResultType.Error
        )
      );
    }

    if (context.row.tryGet(this._openModeId) == undefined) {
      context.validationResult.add(
        ValidationResult.fromText(
          '$KrStages_Dialog_CardOpenModeNotSpecified',
          ValidationResultType.Error
        )
      );
    }

    if (
      context.row.tryGet(this._dialogTypeId) == undefined &&
      context.row.tryGet(this._templateId) == undefined
    ) {
      context.validationResult.add(
        ValidationResult.fromText(
          '$KrStages_Dialog_TemplateAndTypeNotSpecified',
          ValidationResultType.Error
        )
      );
    }

    if (
      context.row.tryGet(this._dialogTypeId) != undefined &&
      context.row.tryGet(this._templateId) != undefined
    ) {
      context.validationResult.add(
        ValidationResult.fromText(
          '$KrStages_Dialog_TemplateAndTypeSelected',
          ValidationResultType.Error
        )
      );
    }
  }

  public initialize(context: IKrStageTypeUIHandlerContext) {
    const rowModel = context.rowModel;
    let grid: GridViewModel | null = null;
    if (rowModel && (grid = rowModel.controls.get('ButtonSettings') as GridViewModel)) {
      grid.rowEditorClosing.add(buttonSettings_RowClosing);
    }
  }

  public finalize(context: IKrStageTypeUIHandlerContext) {
    const rowModel = context.rowModel;
    let grid: GridViewModel | null = null;
    if (rowModel && (grid = rowModel.controls.get('ButtonSettings') as GridViewModel)) {
      grid.rowEditorClosing.remove(buttonSettings_RowClosing);
    }
  }
}

function buttonSettings_RowClosing(e: GridRowEventArgs) {
  const row = e.row;
  const validationResult = new ValidationResultBuilder();

  // tslint:disable-next-line:triple-equals
  if (row.tryGet('TypeID') == undefined) {
    validationResult.add(ValidationResult.fromError('$KrStages_Dialog_ButtonTypeIDNotSpecified'));
    e.cancel = true;
  }

  if (!row.tryGet('Caption')) {
    validationResult.add(ValidationResult.fromError('$KrStages_Dialog_ButtonCaptionNotSpecified'));
    e.cancel = true;
  }

  if (!row.tryGet('Name')) {
    validationResult.add(ValidationResult.fromError('$KrStages_Dialog_ButtonAliasNotSpecified'));
    e.cancel = true;
  }

  showNotEmpty(validationResult.build());
}
