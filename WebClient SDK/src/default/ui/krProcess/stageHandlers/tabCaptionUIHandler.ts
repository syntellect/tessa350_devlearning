import { TabContentIndicator } from '../../tabContentIndicator';
import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext } from 'tessa/workflow/krProcess';
import { TabControlViewModel } from 'tessa/ui/cards/controls';
import { MetadataStorage } from 'tessa';

// tslint:disable:triple-equals

export class TabCaptionUIHandler extends KrStageTypeUIHandler {

  private _indicator: TabContentIndicator;
  private _dispose: Function | null = null;

  public initialize(context: IKrStageTypeUIHandlerContext) {
    const control = context.rowModel!.controls.get('CSharpSourceTable');
    if (control && control instanceof TabControlViewModel) {
      const sectionMeta = MetadataStorage.instance.cardMetadata.getSectionByName('KrStagesVirtual');
      if (!sectionMeta) {
        return;
      }

      const fieldIds: [string, string][] = sectionMeta.columns.map(x => [x.id || '', x.name || '']);
      const storage = context.row;
      this._indicator = new TabContentIndicator(control, storage.getStorage(), fieldIds, true);
      this._indicator.update();
      this._dispose = context.row.fieldChanged.addWithDispose(this._indicator.fieldChangedAction);
    }
  }

  public finalize() {
    if (this._dispose) {
      this._dispose();
      this._dispose = null;
    }
  }

}