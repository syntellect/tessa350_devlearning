import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext, StageTypeHandlerDescriptor, universalTaskDescriptor } from 'tessa/workflow/krProcess';
import { showError } from 'tessa/ui';
import { GridViewModel, GridRowEventArgs, GridRowAction } from 'tessa/ui/cards/controls';
import { DotNetType, Guid } from 'tessa/platform';

// tslint:disable:triple-equals

export class KrUniversalTaskStageTypeUIHandler extends KrStageTypeUIHandler {

  public descriptors(): StageTypeHandlerDescriptor[] {
    return [universalTaskDescriptor];
  }

  public initialize(context: IKrStageTypeUIHandlerContext) {
    if (!context.rowModel) {
      return;
    }

    const grid = context.rowModel.controls.get('CompletionOptions') as GridViewModel;
    if (!grid) {
      return;
    }
    grid.rowInvoked.add(this.rowInvoked);
    grid.rowEditorClosing.add(this.rowClosing);
  }

  public finalize(context: IKrStageTypeUIHandlerContext) {
    if (!context.rowModel) {
      return;
    }

    const grid = context.rowModel.controls.get('CompletionOptions') as GridViewModel;
    if (!grid) {
      return;
    }
    grid.rowInvoked.remove(this.rowInvoked);
    grid.rowEditorClosing.remove(this.rowClosing);
  }

  private rowInvoked = (args: GridRowEventArgs) => {
    if (args.action == GridRowAction.Inserted) {
      args.row.set('OptionID', Guid.newGuid(), DotNetType.Guid);
    }
  }

  private rowClosing = async (args: GridRowEventArgs) => {
    if (!args.row.tryGet('Caption')) {
      await showError('$KrProcess_UniversalTask_CompletionOptionCaptionEmpty');
      args.cancel = true;
    }
  }

}