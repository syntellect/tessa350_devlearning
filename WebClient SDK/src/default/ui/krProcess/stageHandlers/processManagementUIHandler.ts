import { plainColumnName } from 'tessa/workflow';
import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext, StageTypeHandlerDescriptor,
  processManagementDescriptor } from 'tessa/workflow/krProcess';
import { IControlViewModel } from 'tessa/ui/cards';
import { DefaultFormSimpleViewModel } from 'tessa/ui/cards/forms';
import { TypedField, Visibility } from 'tessa/platform';
import { CardFieldChangedEventArgs } from 'tessa/cards';

// tslint:disable:triple-equals

export class ProcessManagementUIHandler extends KrStageTypeUIHandler {

  private _stageControl: IControlViewModel | null;
  private _groupControl: IControlViewModel | null;
  private _signalControl: IControlViewModel | null;
  private _modeId: string = plainColumnName('KrProcessManagementStageSettingsVirtual', 'ModeID');

  public descriptors(): StageTypeHandlerDescriptor[] {
    return [processManagementDescriptor];
  }

  public initialize(context: IKrStageTypeUIHandlerContext) {
    if (!context.rowModel) {
      return;
    }

    const form = context.rowModel.mainForm;
    if (!form || !(form instanceof DefaultFormSimpleViewModel)) {
      return;
    }

    this._stageControl = form.controls.get('StageRow')!;
    this._groupControl = form.controls.get('StageGroup')!;
    this._signalControl = form.controls.get('Signal')!;
    if (!this._stageControl || !this._groupControl || !this._signalControl) {
      return;
    }

    this.updateVisibility(context.row.tryGetField(this._modeId));
    context.row.fieldChanged.add(this.modeChanged);
  }

  public finalize(context: IKrStageTypeUIHandlerContext) {
    context.row.fieldChanged.remove(this.modeChanged);
  }

  private modeChanged = (args: CardFieldChangedEventArgs) => {
    if (args.fieldName !== this._modeId) {
      return;
    }

    this.updateVisibility(args.fieldTypedValue);
  }

  // tslint:disable-next-line:no-any
  private updateVisibility(field: TypedField | null | undefined) {
    this._stageControl!.controlVisibility = Visibility.Collapsed;
    this._groupControl!.controlVisibility = Visibility.Collapsed;
    this._signalControl!.controlVisibility = Visibility.Collapsed;

    if (field && field.$value === 0) { // stageMode
      this._stageControl!.controlVisibility = Visibility.Visible;
    }
    else if (field && field.$value === 1) { // groupMode
      this._groupControl!.controlVisibility = Visibility.Visible;
    }
    else if (field && field.$value === 5) { // signalMode
      this._signalControl!.controlVisibility = Visibility.Visible;
    }
  }

}