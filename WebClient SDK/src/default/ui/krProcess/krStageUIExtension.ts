import { StageGroup } from './stageGroup';
import { StageType } from './stageType';
import { StageSelectorViewModel } from './stageSelectorViewModel';
import { StageSelectorDialog } from './stageSelectorDialog';
import { designTimeCard, runtimeCard } from '../../workflow/krProcess/krUIHelper';
import { TabContentIndicator } from '../tabContentIndicator';
import { hasFlag, DotNetType, Visibility, Guid } from 'tessa/platform';
import { TessaViewRequest, ViewService, RequestParameterBuilder } from 'tessa/views';
import { RequestParameter, equalsCriteriaOperator } from 'tessa/views/metadata';
import { showError, showViewModelDialog, tryGetFromSettings, UIButton, MenuAction } from 'tessa/ui';
import {
  CardUIExtension,
  ICardUIExtensionContext,
  IBlockViewModel,
  ICardModel,
  IControlViewModel,
  IFormViewModel
} from 'tessa/ui/cards';
import { BlockViewModelBase } from 'tessa/ui/cards/blocks';
import {
  GridViewModel,
  GridRowAddingEventArgs,
  GridRowEventArgs,
  TabControlViewModel,
  ContainerViewModel,
  ControlViewModelBase,
  GridRowAction,
  GridRowValidationEventArgs,
  GridRowViewModel
} from 'tessa/ui/cards/controls';
import { LocalizationManager } from 'tessa/localization';
import {
  CardRow,
  CardRowState,
  CardSection,
  CardFieldChangedEventArgs,
  userKeyPrefix,
  Card
} from 'tessa/cards';
import {
  KrTypesCache,
  KrComponents,
  getKrComponentsByCard,
  PerformerUsageMode,
  KrToken,
  CanSkipStages
} from 'tessa/workflow';
import {
  KrStageTypeUIHandlerContext,
  KrStageTypeUIHandler,
  KrStageTypeFormatter,
  KrStageTypeFormatterContext
} from 'tessa/workflow/krProcess';
import { IExtensionExecutor, ExtensionContainer } from 'tessa/extensions';
import { userSession } from 'common/utility';
import { ValidationResult, ValidationResultType } from 'tessa/platform/validation';
import { MetadataStorage } from 'tessa';

export class KrStageUIExtension extends CardUIExtension {
  currentRow: CardRow | null;
  _dispose: Array<Function | null> = [];

  public initialized(context: ICardUIExtensionContext) {
    const cardModel = context.model;

    this.setTabIndication(cardModel, 'KrStageTemplates', 'CSharpSourceTable');
    this.setTabIndication(cardModel, 'KrStageGroups', 'CSharpSourceTableDesign');
    this.setTabIndication(cardModel, 'KrStageGroups', 'CSharpSourceTableRuntime');

    // Для шаблона этапов и вторички тоже выполняем расширение без проверки компонентов
    if (!designTimeCard(cardModel.cardType.id!)) {
      // KrStageTemplateTypeID
      const usedComponents = getKrComponentsByCard(cardModel.card, KrTypesCache.instance);
      // Выходим если нет согласования
      if (!hasFlag(usedComponents, KrComponents.Routes)) {
        return;
      }
    }

    const approvalStagesTable = cardModel.controls.get('ApprovalStagesTable') as GridViewModel;
    if (!approvalStagesTable) {
      return;
    }

    if (!designTimeCard(cardModel.cardType.id!)) {
      approvalStagesTable.leftButtons.push(
        UIButton.create({
          caption: '$CardTypes_Buttons_ActivateStage',
          buttonAction: () => this.activateStagesHandler(approvalStagesTable.selectedRows),
          isEnabled: () =>
            this.hasEnableActivateStageButton(cardModel.card, approvalStagesTable.selectedRows)
        })
      );

      approvalStagesTable.contextMenuGenerators.push(ctx => {
        ctx.menuActions.push(
          MenuAction.create({
            name: 'ActivateStage',
            caption: '$CardTypes_Buttons_ActivateStage',
            action: () => this.activateStagesHandler(approvalStagesTable.selectedRows),
            isCollapsed: this.hasEnableActivateStageButton(
              cardModel.card,
              approvalStagesTable.selectedRows
            )
          })
        );
      });

      // for (let approvalStageRow of approvalStagesTable.rows.filter(i => !!i.row.tryGet('Hidden'))) {
      //   approvalStageRow.cells.forEach(c => c.style.backgroundColor = 'rgba(220,220,220, 1)');
      // }
    }

    approvalStagesTable.rowAdding.add(this.addStageDialog);
    approvalStagesTable.rowInitializing.add(this.showSettingsBlock);
    approvalStagesTable.rowInvoked.add(this.bindUIHandlers);
    approvalStagesTable.rowInvoked.add(this.bindTimeFieldsRadio);
    approvalStagesTable.rowInvoked.add(args => (this.currentRow = args.row));
    approvalStagesTable.rowEditorClosing.add(this.handleFormatRow);
    approvalStagesTable.rowValidating.add(this.validateViaHandlers);
    approvalStagesTable.rowValidating.add(this.validateTimeLimit);
    approvalStagesTable.rowValidating.add(this.validatePerformers);
    approvalStagesTable.rowEditorClosed.add(this.unbindUIHandlers);
    approvalStagesTable.rowEditorClosed.add(this.unbindTimeFieldsRadio);
    approvalStagesTable.rowEditorClosed.add(() => (this.currentRow = null));

    const stagesSec = context.card.sections.tryGet('KrStagesVirtual');
    if (stagesSec) {
      for (let row of stagesSec.rows) {
        this.formatRow(row, cardModel, false);
      }
    }
  }

  public finalized() {
    for (let func of this._dispose) {
      if (func) {
        func();
      }
    }
    this._dispose.length = 0;
  }

  //#region fields

  private _krStageTypeUIHandlerExecutor: IExtensionExecutor | null;

  //#endregion

  //#region handlers

  private addStageDialog = async (args: GridRowAddingEventArgs) => {
    const card = args.cardModel.card;
    let group: StageGroup | null = null;

    const krStageTemplates = card.sections.tryGet('KrStageTemplates');
    const krSecondaryProcesses = card.sections.tryGet('KrSecondaryProcesses');
    const hasGroup = !!krStageTemplates || !!krSecondaryProcesses;
    if (hasGroup) {
      if (krStageTemplates) {
        const groupId = krStageTemplates.fields.tryGet('StageGroupID');
        if (groupId != null) {
          group = new StageGroup(groupId, krStageTemplates.fields.tryGet('StageGroupName')!, 0);
        }
      } else if (krSecondaryProcesses) {
        group = new StageGroup(card.id, krSecondaryProcesses.fields.tryGet('Name')!, 0);
      }
    }

    const commonInfo = card.sections.tryGet('DocumentCommonInfo');
    let type = commonInfo ? commonInfo.fields.tryGet('DocTypeID') : null;
    type = type ? type : card.typeId;

    const viewModel = new StageSelectorViewModel(group, card.id, type);
    viewModel.getGroupTypesFunc = async (
      groupVm: StageGroup | null,
      cardId: guid,
      typeId: guid
    ) => {
      if (hasGroup && !groupVm) {
        // группа не выбрана в шаблоне этапов
        return [];
      }

      if (groupVm) {
        return [groupVm];
      }

      const stageGroupsView = ViewService.instance.getByName('KrFilteredStageGroups')!;
      const request = new TessaViewRequest(stageGroupsView.metadata);

      const cardIdParam = new RequestParameterBuilder()
        .withMetadata(stageGroupsView.metadata.parameters.get('CardId')!)
        .addCriteria(equalsCriteriaOperator(), '', cardId)
        .asRequestParameter();
      request.values.push(cardIdParam);

      const typeIdParam = new RequestParameterBuilder()
        .withMetadata(stageGroupsView.metadata.parameters.get('TypeId')!)
        .addCriteria(equalsCriteriaOperator(), '', typeId)
        .asRequestParameter();
      request.values.push(typeIdParam);

      const result = await stageGroupsView.getData(request);
      return result.rows.map(x => new StageGroup(x[0], x[1], x[4]));
    };

    viewModel.getStageTypesFunc = async (groupType: guid, cardId: guid, typeId: guid) => {
      if (hasGroup && !group) {
        // группа не выбрана в шаблоне этапов
        return [];
      }

      const stageTypesView = ViewService.instance.getByName('KrFilteredStageTypes')!;
      const request = new TessaViewRequest(stageTypesView.metadata);

      if (designTimeCard(typeId)) {
        const isTemplateParam = new RequestParameterBuilder()
          .withMetadata(stageTypesView.metadata.parameters.get('IsTemplate')!)
          .addCriteria(equalsCriteriaOperator(), '', true)
          .asRequestParameter();
        request.values.push(isTemplateParam);
      }

      const param = new RequestParameterBuilder()
        .withMetadata(stageTypesView.metadata.parameters.get('StageGroupIDParam')!)
        .addCriteria(equalsCriteriaOperator(), '', groupType)
        .asRequestParameter();
      request.values.push(param);

      const cardIdParam = new RequestParameterBuilder()
        .withMetadata(stageTypesView.metadata.parameters.get('CardId')!)
        .addCriteria(equalsCriteriaOperator(), '', cardId)
        .asRequestParameter();
      request.values.push(cardIdParam);

      const typeIdMetadata = stageTypesView.metadata.parameters.get('TypeId')!;
      let typeIdParam: RequestParameter | null = null;
      if (designTimeCard(typeId)) {
        const typeRows = card.sections.get('KrStageTypes')!.rows;
        if (typeRows.length > 0) {
          const typeIdSet = new Set<guid>();

          for (let row of typeRows) {
            if (row.state !== CardRowState.Deleted) {
              const templateTypeId = row.get('TypeID');
              if (templateTypeId) {
                typeIdSet.add(templateTypeId);
              }
            }
          }

          if (typeIdSet.size > 0) {
            const typeIdBuilder = new RequestParameterBuilder().withMetadata(typeIdMetadata);

            for (let id of typeIdSet) {
              typeIdBuilder.addCriteria(equalsCriteriaOperator(), '', id);
            }

            typeIdParam = typeIdBuilder.asRequestParameter();
          }
        }
      } else {
        typeIdParam = new RequestParameterBuilder()
          .withMetadata(typeIdMetadata)
          .addCriteria(equalsCriteriaOperator(), '', typeId)
          .asRequestParameter();
      }

      if (typeIdParam) {
        request.values.push(typeIdParam);
      }

      const result = await stageTypesView.getData(request);
      return result.rows.map(x => new StageType(x[0], x[1], x[2]));
    };

    await viewModel.refresh();
    await viewModel.updateType();

    if (viewModel.groups.length === 0) {
      await showError('$UI_Error_NoAvailableStageGroups');
      args.cancel = true;
      return;
    }

    const result = (await showViewModelDialog(viewModel, StageSelectorDialog)) as {
      cancel: boolean;
      group: StageGroup | null;
      type: StageType | null;
    };

    if (!result.group || !result.type) {
      args.cancel = true;
      return;
    }

    args.row.set('StageGroupID', result.group.id, DotNetType.Guid);
    args.row.set('StageGroupName', result.group.name, DotNetType.String);
    args.row.set('StageGroupOrder', result.group.order, DotNetType.Int32);
    args.row.set('StageTypeID', result.type.id, DotNetType.Guid);
    args.row.set('StageTypeCaption', result.type.caption, DotNetType.String);
    args.row.set(
      'Name',
      LocalizationManager.instance.localize(result.type.defaultStage),
      DotNetType.String
    );

    if (!designTimeCard(args.cardModel.cardType.id!)) {
      const rows = args.rows;
      const groupId = result.group.id;
      const groupOrder = result.group.order;

      args.rowIndex = ((): number => {
        if (rows.length === 0) {
          return 0;
        }

        let rowIndex = 0;
        const getId = (): guid => rows[rowIndex].tryGet('StageGroupID', Guid.empty);
        const getOrder = (): number =>
          rows[rowIndex].tryGet('StageGroupOrder', Number.MAX_SAFE_INTEGER);
        const nestedStage = (): boolean => !!rows[rowIndex].tryGet(userKeyPrefix + 'NestedStage');

        const cnt = rows.length;

        // Достигаем начало требуемой группы
        while (
          rowIndex < cnt &&
          ((getId() !== groupId && getOrder() < groupOrder) || nestedStage())
        ) {
          rowIndex++;
        }

        // На нестеде тут мы быть не можем, т.к. пропустили возможные нестеды выше
        // Проверим, что мы в конце или на другой группе
        if (rows.length === rowIndex || (getId() !== groupId && getOrder() !== groupOrder)) {
          // Текущая группа последняя
          // В текущей группе нет этапов, просто добавляем на нужное место.
          return rowIndex;
        }

        const firstIndexInGroup = rowIndex;
        rowIndex++;

        // Спускаемся до конца группы
        while (
          rowIndex < cnt &&
          ((getId() === groupId && getOrder() === groupOrder) || nestedStage())
        ) {
          rowIndex++;
        }

        // Поднимаемся вверх до возможного места добавления
        let position = rowIndex;
        const sortedRows = rows.map(x => x).sort((a, b) => a.get('Order') - b.get('Order'));
        for (let i = rowIndex - 1; i >= firstIndexInGroup; i--) {
          const row = sortedRows[i];
          if (row.tryGet(userKeyPrefix + 'NestedStage')) {
            continue;
          }

          const posId = row.tryGet('BasedOnStageTemplateGroupPositionID');
          const orderChanged = row.tryGet('OrderChanged');
          if (
            // tslint:disable-next-line:triple-equals
            posId != undefined &&
            posId === 1 && // AtLast
            // tslint:disable-next-line:triple-equals
            orderChanged != undefined &&
            !orderChanged
          ) {
            position = i;
          }
        }

        return position;
      })();
    }
  };

  private showSettingsBlock = (args: GridRowEventArgs) => {
    if (!args.rowModel) {
      return;
    }

    const commonBlock = args.rowModel.blocks.get('StageCommonInfoBlock');
    if (commonBlock) {
      this.setOptionalControlVisibility(commonBlock, 'TimeLimitInput', args);
      this.setOptionalControlVisibility(commonBlock, 'PlannedInput', args);
      this.setOptionalControlVisibility(commonBlock, 'HiddenStageCheckbox', args);
      this.setOptionalControlVisibility(commonBlock, 'CanBeSkipCheckbox', args);
    }

    const performersBlock = args.rowModel.blocks.get('PerformersBlock');
    if (performersBlock) {
      const singleVisibility = this.setOptionalControlVisibility(
        performersBlock,
        'SinglePerformerEntryAC',
        args
      );
      const multipleVisibility = this.setOptionalControlVisibility(
        performersBlock,
        'MultiplePerformersTableAC',
        args
      );

      const sqlPerformersLinkBlock = args.rowModel.blocks.get('KrSqlPerformersLinkBlock');
      if (sqlPerformersLinkBlock && multipleVisibility === Visibility.Visible) {
        sqlPerformersLinkBlock.blockVisibility = Visibility.Visible;
      }

      if (singleVisibility === Visibility.Visible) {
        this.setControlCaption(performersBlock, 'SinglePerformerEntryAC', args);
      }
      if (multipleVisibility === Visibility.Visible) {
        this.setControlCaption(performersBlock, 'MultiplePerformersTableAC', args);
      }
    }

    const authorBlock = args.rowModel.blocks.get('AuthorBlock');
    if (authorBlock) {
      this.setOptionalControlVisibility(authorBlock, 'AuthorEntryAC', args);
    }

    const taskKindBlock = args.rowModel.blocks.get('TaskKindBlock');
    if (taskKindBlock) {
      this.setOptionalControlVisibility(taskKindBlock, 'TaskKindEntryAC', args);
    }

    const taskHistoryBlock = args.rowModel.blocks.get('KrTaskHistoryBlockAlias');
    if (taskHistoryBlock) {
      const v = this.setOptionalControlVisibility(
        taskHistoryBlock,
        'KrTaskHistoryGroupTypeControlAlias',
        args
      );
      this.setOptionalControlVisibility(
        taskHistoryBlock,
        'KrParentTaskHistoryGroupTypeControlAlias',
        args
      );
      this.setOptionalControlVisibility(
        taskHistoryBlock,
        'KrTaskHistoryGroupNewIterationControlAlias',
        args
      );
      // Если показывается контрол (а показываются они все по одному правилу), то показывается и заголовок
      taskHistoryBlock.captionVisibility = v !== null ? v : Visibility.Collapsed;
    }

    const settingsBlock = args.rowModel.blocks.get('SettingsBlock');
    if (settingsBlock) {
      for (let control of settingsBlock.controls) {
        const controlSettings = control.cardTypeControl.controlSettings;
        const stageHandlerID = tryGetFromSettings<guid>(
          controlSettings,
          'StageHandlerDescriptorIDSetting'
        );
        // tslint:disable-next-line:triple-equals
        if (stageHandlerID != undefined) {
          if (Guid.equals(args.row.get('StageTypeID'), stageHandlerID)) {
            control.controlVisibility = Visibility.Visible;
            this.setVisibilityViaTags(args.cardModel.cardType.id!, args.rowModel, control);
          } else {
            control.controlVisibility = Visibility.Collapsed;
          }
        }
      }
    }
  };

  private bindTimeFieldsRadio = async (args: GridRowEventArgs) => {
    args.row.fieldChanged.add(this.onTimeFieldChanged);
  };

  private unbindTimeFieldsRadio = async (args: GridRowEventArgs) => {
    args.row.fieldChanged.remove(this.onTimeFieldChanged);
  };

  onTimeFieldChanged = (args: CardFieldChangedEventArgs) => {
    if (args.fieldValue == null) {
      return;
    }

    switch (args.fieldName) {
      case 'Planned':
        this.currentRow!.set('TimeLimit', null);
        break;
      case 'TimeLimit':
        this.currentRow!.set('Planned', null);
        break;
    }
  };

  private bindUIHandlers = async (args: GridRowEventArgs) => {
    const context = this.getExecutionContext(args);
    if (!context) {
      return;
    }

    try {
      await this._krStageTypeUIHandlerExecutor!.executeAsync('initialize', context);
    } catch (err) {
      showError((err as Error).message);
    }
  };

  private validateViaHandlers = (args: GridRowValidationEventArgs) => {
    const context = this.getExecutionContext(args);
    if (!context) {
      return;
    }

    try {
      this._krStageTypeUIHandlerExecutor!.execute('validate', context);
    } catch (err) {
      showError((err as Error).message);
    }
  };

  private validateTimeLimit = async (args: GridRowValidationEventArgs) => {
    const row = args.row;
    const stageTypeId = row.tryGet('StageTypeID');
    const timeLimit = row.tryGet('TimeLimit');
    const planned = row.tryGet('Planned');
    const controls = args.rowModel!.controls;
    let control: IControlViewModel;
    let list: guid[];
    const checkField = (inputAlias: string) =>
      (control = controls.get(inputAlias)!) &&
      (list = tryGetFromSettings(
        control.cardTypeControl.controlSettings,
        'VisibleForTypesSetting',
        null
      )!) &&
      list.some(x => Guid.equals(x, stageTypeId));
    const checkTimeLimit = checkField('TimeLimitInput');
    const checkPlanned = checkField('PlannedInput');
    if (checkTimeLimit && checkPlanned) {
      if (timeLimit == null && planned == null) {
        args.validationResult.add(
          ValidationResult.fromText(
            '$UI_Error_TimeLimitOrPlannedNotSpecifiedWarn',
            ValidationResultType.Warning
          )
        );
      }
    } else if (checkTimeLimit && timeLimit == null) {
      args.validationResult.add(
        ValidationResult.fromText(
          '$UI_Error_TimeLimitNotSpecifiedWarn',
          ValidationResultType.Warning
        )
      );
    }
  };

  private validatePerformers = async (args: GridRowValidationEventArgs) => {
    const controls = args.rowModel!.controls;
    let mode: PerformerUsageMode;
    let control: IControlViewModel;
    if (
      (control = controls.get('SinglePerformerEntryAC')!) &&
      control.controlVisibility === Visibility.Visible
    ) {
      mode = PerformerUsageMode.Single;
    } else if (
      (control = controls.get('MultiplePerformersTableAC')!) &&
      control.controlVisibility === Visibility.Visible
    ) {
      mode = PerformerUsageMode.Multiple;
    } else {
      return;
    }

    const controlSettings = control.cardTypeControl.controlSettings;
    const list = tryGetFromSettings<guid[] | null>(
      controlSettings,
      'RequiredForTypesSetting',
      null
    );
    if (!list) {
      return;
    }

    const row = args.row;
    for (let id of list) {
      if (Guid.equals(id, row.get('StageTypeID'))) {
        let perfSec: CardSection;
        if (
          (mode === PerformerUsageMode.Single &&
            // tslint:disable-next-line:triple-equals
            row.get('KrSinglePerformerVirtual__PerformerID') == undefined) ||
          (mode === PerformerUsageMode.Multiple &&
            (perfSec = args.cardModel.card.sections.tryGet('KrPerformersVirtual_Synthetic')!) &&
            !perfSec.rows.some(
              x => Guid.equals(x.get('StageRowID'), row.rowId) && x.state !== CardRowState.Deleted
            ))
        ) {
          args.validationResult.add(
            ValidationResult.fromText(
              '$UI_Error_PerformerNotSpecifiedWarn',
              ValidationResultType.Warning
            )
          );
        }
        break;
      }
    }
  };

  private unbindUIHandlers = async (args: GridRowEventArgs) => {
    const context = this.getExecutionContext(args);
    if (!context) {
      return;
    }

    try {
      await this._krStageTypeUIHandlerExecutor!.executeAsync('finalize', context);
    } catch (err) {
      showError((err as Error).message);
    }
  };

  private getExecutionContext(
    args: GridRowEventArgs | GridRowValidationEventArgs
  ): KrStageTypeUIHandlerContext | null {
    if ('action' in args && args.action === GridRowAction.Deleting) {
      return null;
    }

    const stageTypeId = args.row.tryGet('StageTypeID');
    // tslint:disable-next-line:triple-equals
    if (stageTypeId == undefined) {
      return null;
    }

    if (!this._krStageTypeUIHandlerExecutor) {
      this._krStageTypeUIHandlerExecutor = ExtensionContainer.instance.resolveExecutor(
        KrStageTypeUIHandler
      );
    }

    if (this._krStageTypeUIHandlerExecutor.length === 0) {
      return null;
    }

    return new KrStageTypeUIHandlerContext(
      stageTypeId,
      'action' in args ? args.action : null,
      'control' in args ? args.control : null,
      args.row,
      args.rowModel,
      args.cardModel,
      'validationResult' in args ? args.validationResult : null
    );
  }

  private handleFormatRow = (args: GridRowEventArgs) => {
    this.formatRow(args.row, args.cardModel, true);
  };

  //#endregion

  //#region private methods

  private setTabIndication(cardModel: ICardModel, sectionName: string, controlName: string) {
    const section = cardModel.card.sections.tryGet(sectionName);
    if (section) {
      const sectionMeta = MetadataStorage.instance.cardMetadata.getSectionByName(sectionName);
      if (!sectionMeta) {
        return;
      }

      const fieldIds: [guid, string][] = sectionMeta.columns.map(x => [x.id || '', x.name || '']);
      const tabControl = cardModel.controls.get(controlName) as TabControlViewModel;
      const indicator = new TabContentIndicator(
        tabControl,
        section.fields.getStorage(),
        fieldIds,
        true
      );
      indicator.update();
      this._dispose.push(section.fields.fieldChanged.addWithDispose(indicator.fieldChangedAction));
    }
  }

  private setOptionalControlVisibility(
    block: IBlockViewModel,
    controlAlias: string,
    e: GridRowEventArgs
  ): Visibility | null {
    const inputControl = block.controls.find(x => x.name === controlAlias);
    if (!inputControl) {
      return null;
    }

    const controlSettings = inputControl.cardTypeControl.controlSettings;
    let list = tryGetFromSettings(controlSettings, 'VisibleForTypesSetting');
    if (!list || !Array.isArray(list)) {
      return null;
    }

    inputControl.controlVisibility = Visibility.Collapsed;
    for (let id of list) {
      if (Guid.equals(id, e.row.get('StageTypeID'))) {
        inputControl.controlVisibility = Visibility.Visible;
        break;
      }
    }

    inputControl.isRequired = false;
    list = tryGetFromSettings(controlSettings, 'RequiredForTypesSetting');
    if (list && Array.isArray(list)) {
      const stageTypeId = e.row.get('StageTypeID');
      for (let id of list) {
        if (Guid.equals(id, stageTypeId)) {
          inputControl.isRequired = true;
          break;
        }
      }
    }

    this.setControlSettings(e.cardModel.cardType.id!, inputControl);
    this.setVisibilityViaTags(e.cardModel.cardType.id!, e.rowModel!, inputControl);

    return inputControl.controlVisibility;
  }

  private setControlCaption(block: IBlockViewModel, controlAlias: string, e: GridRowEventArgs) {
    const inputControl = block.controls.find(x => x.name === controlAlias);
    if (!inputControl) {
      return;
    }

    const controlSettings = inputControl.cardTypeControl.controlSettings;
    const captions = tryGetFromSettings<{} | null>(controlSettings, 'ControlCaptionsSetting', null);
    if (!captions) {
      return;
    }

    const stageGroupId = e.row.get('StageTypeID');
    const caption = captions[stageGroupId];
    if (caption) {
      inputControl.caption = caption;
    }
  }

  private setVisibilityViaTags(
    typeId: guid,
    _rowModel: ICardModel,
    rootControl: IControlViewModel
  ) {
    const queue = new Array();
    if (rootControl instanceof TabControlViewModel) {
      this.enqueueTabs(rootControl, queue);
    } else if (rootControl instanceof ContainerViewModel) {
      this.enqueueForm(rootControl.form, queue);
    } else {
      return;
    }

    while (queue.length !== 0) {
      const item = queue.shift();
      if (item instanceof ControlViewModelBase) {
        this.setControlSettings(typeId, item);

        if (item instanceof TabControlViewModel) {
          this.enqueueTabs(item, queue);
        }
      } else if (item instanceof BlockViewModelBase) {
        this.setBlockControlsSettings(typeId, item);
        this.enqueueBlock(item, queue);
      }
    }
  }

  // tslint:disable-next-line:no-any
  private enqueueTabs(tabControl: TabControlViewModel, queue: any[]) {
    for (let tab of tabControl.tabs) {
      this.enqueueForm(tab, queue);
    }
  }

  // tslint:disable-next-line:no-any
  private enqueueForm(formViewModel: IFormViewModel, queue: any[]) {
    for (let blockViewModel of formViewModel.blocks) {
      queue.push(blockViewModel);
    }
  }

  // tslint:disable-next-line:no-any
  private enqueueBlock(blockViewModel: IBlockViewModel, queue: any[]) {
    for (let controlViewModel of blockViewModel.controls) {
      queue.push(controlViewModel);
    }
  }

  private setControlSettings(typeId: guid, controlViewModel: IControlViewModel) {
    if (controlViewModel.controlVisibility !== Visibility.Visible) {
      // Если контрол скрыт, нет смысла в нем копаться.
      return;
    }

    const controlSettings = controlViewModel.cardTypeControl.controlSettings;
    const tags = tryGetFromSettings(controlSettings, 'TagsListSetting');
    if (!tags || !Array.isArray(tags)) {
      return;
    }

    let hasRuntime = false;
    let hasDesign = false;
    let hasRuntimeReadonly = false;
    let hasDesignReadonly = false;

    for (let tag of tags) {
      switch (tag) {
        case 'Runtime':
          hasRuntime = true;
          break;
        case 'DesignTime':
          hasDesign = true;
          break;
        case 'RuntimeReadonly':
          hasRuntimeReadonly = true;
          break;
        case 'DesignTimeReadonly':
          hasDesignReadonly = true;
          break;
      }
    }

    if (!hasDesign && !hasRuntime) {
      controlViewModel.controlVisibility = Visibility.Visible;
    } else if (hasDesign && designTimeCard(typeId)) {
      controlViewModel.controlVisibility = Visibility.Visible;
    } else if (hasRuntime && runtimeCard(typeId)) {
      controlViewModel.controlVisibility = Visibility.Visible;
    } else {
      controlViewModel.controlVisibility = Visibility.Collapsed;
    }

    if (controlViewModel.controlVisibility === Visibility.Visible) {
      if (hasDesignReadonly && designTimeCard(typeId)) {
        controlViewModel.isReadOnly = true;
      } else if (hasRuntimeReadonly && runtimeCard(typeId)) {
        controlViewModel.isReadOnly = true;
      }
    }
  }

  private setBlockControlsSettings(typeId: guid, blockViewModel: IBlockViewModel) {
    if (blockViewModel.blockVisibility !== Visibility.Visible) {
      // Если блок скрыт, нет смысла в нем копаться.
      return;
    }

    const blockSettings = blockViewModel.cardTypeBlock.blockSettings;
    const tags = tryGetFromSettings(blockSettings, 'TagsListSetting');
    if (!tags || !Array.isArray(tags)) {
      return;
    }

    let hasRuntime = false;
    let hasDesign = false;

    for (let tag of tags) {
      switch (tag) {
        case 'Runtime':
          hasRuntime = true;
          break;
        case 'DesignTime':
          hasDesign = true;
          break;
      }
    }

    if (!hasDesign && !hasRuntime) {
      blockViewModel.blockVisibility = Visibility.Visible;
    } else if (hasDesign && designTimeCard(typeId)) {
      blockViewModel.blockVisibility = Visibility.Visible;
    } else if (hasRuntime && runtimeCard(typeId)) {
      blockViewModel.blockVisibility = Visibility.Visible;
    } else {
      blockViewModel.blockVisibility = Visibility.Collapsed;
    }
  }

  private formatRow(row: CardRow, cardModel: ICardModel, withChanges: boolean) {
    const stageTypeId = row.tryGet('StageTypeID');
    if (!stageTypeId) {
      return;
    }

    const extensionExecutor = ExtensionContainer.instance.resolveExecutor(KrStageTypeFormatter);
    if (extensionExecutor.length === 0) {
      return;
    }

    const info = {};
    const session = userSession;
    const ctx = new KrStageTypeFormatterContext(
      stageTypeId,
      session,
      info,
      cardModel.card,
      row,
      null
    );
    ctx.displayTimeLimit = row.tryGet('DisplayTimeLimit') || '';
    ctx.displayParticipants = row.tryGet('DisplayParticipants') || '';
    ctx.displaySettings = row.tryGet('DisplaySettings') || '';

    cardModel.executeInContext(() => extensionExecutor.execute('format', ctx));

    if (withChanges) {
      const displayTimeLimit = row.tryGet('DisplayTimeLimit');
      if (displayTimeLimit !== ctx.displayTimeLimit) {
        row.set('DisplayTimeLimit', ctx.displayTimeLimit, DotNetType.String);
      }
      const displayParticipants = row.tryGet('DisplayParticipants');
      if (displayParticipants !== ctx.displayParticipants) {
        row.set('DisplayParticipants', ctx.displayParticipants, DotNetType.String);
      }
      const displaySettings = row.tryGet('DisplaySettings');
      if (displaySettings !== ctx.displaySettings) {
        row.set('DisplaySettings', ctx.displaySettings, DotNetType.String);
      }
    } else {
      row.rawSet('DisplayTimeLimit', ctx.displayTimeLimit, DotNetType.String);
      row.rawSet('DisplayParticipants', ctx.displayParticipants, DotNetType.String);
      row.rawSet('DisplaySettings', ctx.displaySettings, DotNetType.String);
    }
  }

  private activateStagesHandler = (selectedRowStages: GridRowViewModel[]) => {
    for (let selectedRowStage of selectedRowStages) {
      selectedRowStage.row.set('Skip', false, DotNetType.Boolean);
      // selectedRowStage.cells.forEach(c => c.style.backgroundColor = 'transparent');
    }
  };

  private hasEnableActivateStageButton = (
    card: Card,
    selectedRows: GridRowViewModel[]
  ): boolean => {
    let token: KrToken | null = null;
    return (
      selectedRows.length > 0 &&
      !!(token = KrToken.tryGet(card.info)) &&
      token.hasPermission(CanSkipStages) &&
      selectedRows.every(i => !!i.row.tryGet('Skip'))
    );
  };

  //#endregion
}
