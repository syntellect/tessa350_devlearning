import { InfoAboutChanges } from '../../workflow/krCompilers/infoAboutChanges';
import { CardUIExtension, ICardUIExtensionContext, CardModelFlags } from 'tessa/ui/cards';
import { getKrComponentsByCard, KrTypesCache, KrComponents, KrToken, CanFullRecalcRoute } from 'tessa/workflow';
import { hasNotFlag, DotNetType, createTypedField } from 'tessa/platform';
import { GridViewModel } from 'tessa/ui/cards/controls';
import { Card, CardStoreMode } from 'tessa/cards';
import { tryGetFromInfo, UIButton } from 'tessa/ui';
import { IStorage } from 'tessa/platform/storage';

export class KrRecalcStagesUIExtension extends CardUIExtension {

  public initialized(context: ICardUIExtensionContext) {
    const cardModel = context.model;

    const usedComponents = getKrComponentsByCard(cardModel.card, KrTypesCache.instance);
    // Выходим если нет согласования
    if (hasNotFlag(usedComponents, KrComponents.Routes)) {
      return;
    }

    const approvalStagesTable = cardModel.controls.get('ApprovalStagesTable') as GridViewModel;
    let krToken: KrToken | null = null;
    if (approvalStagesTable
      && hasNotFlag(cardModel.flags, CardModelFlags.Disabled)
      && (krToken = KrToken.tryGet(context.card.info))
      && krToken.hasPermission(CanFullRecalcRoute)
    ) {
      const uiContext = context.uiContext;

      const hasStagePositions = KrRecalcStagesUIExtension.hasStagePositions(cardModel.card, false);
      const newCard = cardModel.card.storeMode === CardStoreMode.Insert;

      approvalStagesTable.leftButtons.push(
        new UIButton('$CardTypes_Buttons_RecalcApprovalStages', () => {
          const editor = uiContext.cardEditor;
          if (editor && !editor.operationInProgress) {
            const mode = hasStagePositions || newCard
              ? InfoAboutChanges.ChangesListToValidationResult
              : InfoAboutChanges.ChangesListToValidationResult | InfoAboutChanges.ChangesInHiddenStages;
            const info: IStorage = {
              '.Recalc': createTypedField(true, DotNetType.Boolean),
              '.InfoAboutChanges': createTypedField(mode, DotNetType.Int32)
            };
            editor.saveCard(uiContext, info);
          }
        })
      );
    }
  }

  private static hasStagePositions(card: Card, atLeastOne: boolean): boolean {
    const stagePositions = tryGetFromInfo(card.info, 'StagePositions');
    return stagePositions
      && Array.isArray(stagePositions)
      && (!atLeastOne || stagePositions.length > 0);
  }

}