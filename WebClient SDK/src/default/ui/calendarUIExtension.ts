import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
import { ButtonViewModel } from 'tessa/ui/cards/controls';
import { hasFlag, createTypedField, DotNetType } from 'tessa/platform';
import { CardPermissionFlags } from 'tessa/cards';
import { CardRequest, CardService } from 'tessa/cards/service';
import { showNotEmpty, UIContext, showMessage } from 'tessa/ui';
import { OperationService, OperationTypes } from 'tessa/platform/operations';

export class CalendarUIExtension extends CardUIExtension {
  public initialized(context: ICardUIExtensionContext) {
    CalendarUIExtension.attachCommandToButton(
      context,
      'ValidateCalendar',
      CalendarUIExtension.validateCalendarButtonAction
    );
    CalendarUIExtension.attachCommandToButton(
      context,
      'RebuildCalendar',
      CalendarUIExtension.rebuildCalendarButtonAction
    );
  }

  private static attachCommandToButton(
    context: ICardUIExtensionContext,
    buttonAlias: string,
    action: Function
  ) {
    const control = context.model.controls.get(buttonAlias);
    if (!control) {
      return;
    }

    const button = control as ButtonViewModel;
    if (
      !hasFlag(
        context.model.card.permissions.resolver.getCardPermissions(),
        CardPermissionFlags.AllowModify
      )
    ) {
      button.isReadOnly = true;
      return;
    }

    // tslint:disable-next-line:no-any
    button.onClick = action as any;
  }

  private static async validateCalendarButtonAction() {
    const request = new CardRequest();
    request.requestType = '3f75d47a-38fd-45f3-8d4a-39b0836f074d';

    const response = await CardService.instance.request(request);
    await showNotEmpty(response.validationResult.build(), '$UI_BusinessCalendar_ValidatedTitle');
  }

  private static async rebuildCalendarButtonAction() {
    const context = UIContext.current;
    const editor = context.cardEditor;

    if (editor && !editor.operationInProgress) {
      await editor.setOperationInProgress(async () => {
        const operationId = await OperationService.instance.create({
          typeId: OperationTypes.CalendarRebuild
        });

        const result = await editor.saveCard(context, {
          '.rebuildOperationGuid': createTypedField(operationId, DotNetType.Guid)
        });

        if (!result) {
          return;
        }

        let isActive = true;
        while (isActive) {
          await new Promise(resolve => setTimeout(resolve, 1000));
          isActive = await OperationService.instance.isAlive(operationId);
        }
      });

      await showMessage('$UI_BusinessCalendar_CalendarIsRebuiltNotification');
    }
  }
}
