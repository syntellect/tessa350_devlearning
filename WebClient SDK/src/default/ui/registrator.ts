import { ExtensionContainer, ExtensionStage } from 'tessa/extensions';

import { CalendarUIExtension } from './calendarUIExtension';
import { OutgoingPartnerUIExtension } from './outgoingPartnerUIExtension';
import { CarUIExtension } from './carUIExtension';
import { KrDocStateUIExtension } from './krDocStateUIExtension';
import { CreateAndSelectToolbarUIExtension } from './createAndSelectToolbarUIExtension';
import { KrVirtualFilesUIExtension } from './krVirtualFilesUIExtension';
import { KrPermissionsUIExtension } from './krPermissionsUIExtension';
import { KrExtendedPermissionsUIExtension } from './krExtendedPermissionsUIExtension';
import KrRoutesInWorkflowEngineUIExtension from './workflowEngine/krRoutesInWorkflowEngineUIExtension';
import { KrGetCycleFileInfoUIExtension } from './krGetCycleFileInfoUIExtension';

ExtensionContainer.instance.registerExtension({
  extension: KrGetCycleFileInfoUIExtension,
  stage: ExtensionStage.BeforePlatform
});
ExtensionContainer.instance.registerExtension({
  extension: CalendarUIExtension,
  stage: ExtensionStage.AfterPlatform
});
ExtensionContainer.instance.registerExtension({
  extension: OutgoingPartnerUIExtension,
  stage: ExtensionStage.AfterPlatform
});
ExtensionContainer.instance.registerExtension({
  extension: CarUIExtension,
  stage: ExtensionStage.AfterPlatform
});
ExtensionContainer.instance.registerExtension({
  extension: KrDocStateUIExtension,
  stage: ExtensionStage.AfterPlatform
});
ExtensionContainer.instance.registerExtension({
  extension: CreateAndSelectToolbarUIExtension,
  stage: ExtensionStage.AfterPlatform
});
ExtensionContainer.instance.registerExtension({
  extension: KrVirtualFilesUIExtension,
  stage: ExtensionStage.AfterPlatform
});
ExtensionContainer.instance.registerExtension({
  extension: KrPermissionsUIExtension,
  stage: ExtensionStage.AfterPlatform
});
ExtensionContainer.instance.registerExtension({
  extension: KrExtendedPermissionsUIExtension,
  stage: ExtensionStage.AfterPlatform
});
ExtensionContainer.instance.registerExtension({
  extension: KrRoutesInWorkflowEngineUIExtension,
  stage: ExtensionStage.AfterPlatform
});
