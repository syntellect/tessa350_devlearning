import { CardUIExtension, ICardUIExtensionContext, AdvancedCardDialogManager } from 'tessa/ui/cards';
import { FilePreviewViewModel, AutoCompleteEntryViewModel } from 'tessa/ui/cards/controls';
import { showLoadingOverlay } from 'tessa/ui';

export class CarUIExtension extends CardUIExtension {
  private _disposes: Function[] = [];
  public initialized(context: ICardUIExtensionContext) {
    if (context.card.typeId !== 'd0006e40-a342-4797-8d77-6501c4b7c4ac') { // CardTypeID
      return;
    }

    const cardModel = context.model;

    const driverControl = context.model.controls.get('DriverName2') as AutoCompleteEntryViewModel;
    if (driverControl) {
      driverControl.openRefAction = async () => {
        if (!driverControl.item
          // tslint:disable-next-line:triple-equals
          || driverControl.item.reference == undefined
        ) {
          return;
        }

        const info = {};
        info['.autocomplete'] = driverControl.getRefInfo();

        await cardModel.executeInContext(async context => {
          await showLoadingOverlay(async (splashResolve) => {
            await AdvancedCardDialogManager.instance.openCard({
              cardId: driverControl.item!.reference,
              context,
              info,
              splashResolve
            });
          });
        });
      };
    }

    const preview1 = context.model.controls.get('Preview1') as FilePreviewViewModel;
    const preview2 = context.model.controls.get('Preview2') as FilePreviewViewModel;
    if (!preview1 || !preview2) {
      return;
    }

    this._disposes.push(
      preview1.fileControlManager.pageChanged.addWithDispose((args: { page: number }) => {
        preview2.fileControlManager.setPageNumber(args.page);
      })!
    );

    // Использование апи выставления поворотов
    // this._disposes.push(
    //   preview1.fileControlManager.rotateChanged.addWithDispose(
    //     (args: { fileVersionId: string; page: number; angle: number }) => {
    //       const version = preview2.fileControlManager.fileVersion;
    //       if (!version) {
    //         return;
    //       }
    //       preview2.fileControlManager.setRotateAngle(
    //         version.id,
    //         preview2.fileControlManager.pageNumber,
    //         args.angle
    //       );
    //     }
    //   )!
    // );

    // Использование апи масштабирования
    // this._disposes.push(
    //   preview1.fileControlManager.scaleChanged.addWithDispose(
    //     (args: { fileVersionId: string; scale: ScaleOption; customScaleValue: number }) => {
    //       preview2.fileControlManager.setScale(args.scale, args.customScaleValue);
    //     }
    //   )!
    // );
  }

  public finalized() {
    for (let func of this._disposes) {
      if (func) {
        func();
      }
    }
    this._disposes.length = 0;
  }
}
