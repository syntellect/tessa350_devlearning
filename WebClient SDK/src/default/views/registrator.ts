import { ExtensionContainer, ExtensionStage } from 'tessa/extensions';

import { CreateCardExtension, CreateCardInitializeExtension } from './createCardExtension';
import { CustomFolderViewExtension, CustomFolderInitializeExtension } from './customFolderViewExtension';
import { TreeViewItemTestExtension } from './treeViewItemTestExtension';
import { ViewsContextMenuExtension } from './viewsContextMenuExtension';

ExtensionContainer.instance.registerExtension({
  extension: CreateCardExtension,
  stage: ExtensionStage.AfterPlatform,
  singleton: true
});
ExtensionContainer.instance.registerExtension({
  extension: CreateCardInitializeExtension,
  stage: ExtensionStage.AfterPlatform,
  singleton: true
});
ExtensionContainer.instance.registerExtension({
  extension: CustomFolderInitializeExtension,
  stage: ExtensionStage.AfterPlatform,
  singleton: true
});
ExtensionContainer.instance.registerExtension({
  extension: CustomFolderViewExtension,
  stage: ExtensionStage.AfterPlatform,
  singleton: true
});
ExtensionContainer.instance.registerExtension({
  extension: TreeViewItemTestExtension,
  stage: ExtensionStage.AfterPlatform,
  singleton: true
});
ExtensionContainer.instance.registerExtension({
  extension: ViewsContextMenuExtension,
  stage: ExtensionStage.AfterPlatform,
  singleton: true
});