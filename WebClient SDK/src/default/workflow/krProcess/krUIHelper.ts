import { UIContext } from 'tessa/ui';
import { createTypedField, DotNetType } from 'tessa/platform';
import { KrTypesCache, IKrType } from 'tessa/workflow';
import { Card } from 'tessa/cards';
import { ValidationResultBuilder, ValidationResult, ValidationResultType } from 'tessa/platform/validation';

export const CompiledCardTypes = [
  '2fa85bb3-bba4-4ab6-ba97-652106db96de', // KrStageTemplates
  '66cd517b-5423-43db-8374-f50ec0d967eb', // KrStageCommonMethods
  '9ce8e9f4-cbf0-4b5f-a569-b508b1fd4b3a', // KrStageGroup
  '61420fa1-cc1f-47cb-b0bb-4ea8ee77f51a'  // KrSecondaryProcess
];

export async function sendCompileRequest(compileFlag: string): Promise<void> {
  const context = UIContext.current;
  const editor = context.cardEditor;
  if (!editor || !editor.cardModel) {
    return;
  }

  if (editor.cardModel.hasChanges()) {
    const success = await editor.saveCard(context);
    if (!success) {
      return;
    }
  }

  await editor.saveCard(
    context,
    {
      [compileFlag]: createTypedField(true, DotNetType.Boolean)
    });
}

export function tryGetKrType(
  krTypesCache: KrTypesCache,
  card: Card,
  cardTypeId: guid,
  validationResult: ValidationResultBuilder | null = null
): IKrType | null {
  const krCardType = krTypesCache.cardTypes.find(x => x.id === cardTypeId);
  if (krCardType == null) {
    // карточка может не входить в типовое решение, тогда возвращается null
    // при этом нельзя кидать ошибку в ValidationResult, иначе любое действие с такой карточкой будет неудачным
    return null;
  }

  let result: IKrType = krCardType;
  if (krCardType.useDocTypes) {
    const section = card.sections.get('DocumentCommonInfo');
    if (section) {
      const value = section.fields.tryGetField('DocTypeID');
      if (value) {
        result = krTypesCache.docTypes.find(x => x.id === value.$value)!;
        if (!result) {
          if (validationResult) {
            validationResult.add(ValidationResult.fromText('$KrMessages_UnableToFindTypeWithID', ValidationResultType.Error));
          }

          return null;
        }
      } else {
        if (validationResult) {
          validationResult.add(ValidationResult.fromText('$KrMessages_UnableToFindTypeWithID', ValidationResultType.Error));
        }

        return null;
      }
    }
  }

  return result;
}

export function designTimeCard(typeId: guid): boolean {
  return typeId === '2fa85bb3-bba4-4ab6-ba97-652106db96de' // KrStageTemplates
    || typeId === '61420fa1-cc1f-47cb-b0bb-4ea8ee77f51a'; // KrSecondaryProcess
}

export function runtimeCard(typeId: guid): boolean {
  return !designTimeCard(typeId);
}