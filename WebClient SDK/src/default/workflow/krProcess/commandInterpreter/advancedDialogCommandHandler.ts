import { ClientCommandHandlerBase } from 'tessa/workflow/krProcess/clientCommandInterpreter';
import { IClientCommandHandlerContext } from 'tessa/workflow/krProcess';
import {
  CardButtonType,
  CardTaskCompletionOptionSettings,
  CardTaskDialogActionResult,
  systemKeyPrefix,
  Card,
  CardTaskDialogNewMethod,
  CardFileState
} from 'tessa/cards';
import { IUIContext, showConfirm, UIButton, UIContext, createCardEditorModel } from 'tessa/ui';
import {
  AdvancedCardDialogManager,
  CardToolbarAction,
  CardToolbarItemCommand,
  ICardEditorModel
} from 'tessa/ui/cards';
import { createTypedField, DotNetType, Guid } from 'tessa/platform';
import { IStorage } from 'tessa/platform/storage';
import { createCard, createFromTemplate } from 'tessa/ui/uiHost';
import { readFileContentAsBase64, IFile } from 'tessa/files';

export abstract class AdvancedDialogCommandHandler extends ClientCommandHandlerBase {
  async handle(context: IClientCommandHandlerContext): Promise<void> {
    const coSettings = this.prepareDialogCommand(context);
    if (!coSettings) {
      return;
    }

    // костыль
    // заворачиваем это все в setTimeout, т.к. мы хотим, чтобы вкладка сначала закрылась, если должна (completeDialogAsync->closeDialog)
    // после были удалены все лишние UIContext (showGlobalDialogAsync->contextInstance.dispose) и только потом запустился наш метод
    setTimeout(() => {
      const editor = UIContext.current.cardEditor;
      if (editor) {
        editor.info[systemKeyPrefix + 'CardEditorCompletionOptionSettings'] = coSettings;
        editor.info[systemKeyPrefix + 'CardEditorCompletionOptionSettingsOnButtonPressed'] = async (
          dialogCardEditor: ICardEditorModel,
          cos: CardTaskCompletionOptionSettings,
          buttonName: string,
          completeTask: boolean
        ) =>
          await this.completeDialogAsync(
            dialogCardEditor,
            editor,
            cos,
            buttonName,
            completeTask,
            context
          );
      } else {
        this.showGlobalDialogAsync(coSettings, context);
      }
    }, 100);
  }

  protected abstract prepareDialogCommand(
    context: IClientCommandHandlerContext
  ): CardTaskCompletionOptionSettings | null;

  protected abstract async completeDialogCoreAsync(
    actionResult: CardTaskDialogActionResult,
    context: IClientCommandHandlerContext,
    cardEditor: ICardEditorModel,
    parentCardEditor: ICardEditorModel | null
  );

  private async completeDialogAsync(
    dialogCardEditor: ICardEditorModel,
    parentCardEditor: ICardEditorModel | null,
    coSettings: CardTaskCompletionOptionSettings,
    buttonName: string,
    completeDialog: boolean,
    context: IClientCommandHandlerContext
  ) {
    const dialogCard = dialogCardEditor.cardModel!.card.clone();
    await this.prepareFilesForStore(dialogCard, dialogCardEditor.cardModel!.fileContainer.files);

    const actionResult = new CardTaskDialogActionResult();
    actionResult.mainCardId = Guid.empty;
    actionResult.pressedButtonName = buttonName;
    actionResult.storeMode = coSettings.storeMode;
    actionResult.keepFiles = coSettings.keepFiles;
    actionResult.completeDialog = completeDialog;
    actionResult.setDialogCard(dialogCard);

    const closeDialog = await this.completeDialogCoreAsync(
      actionResult,
      context,
      dialogCardEditor,
      parentCardEditor
    );
    if (closeDialog) {
      await dialogCardEditor.close();
    }
  }

  private async prepareFilesForStore(dialogCard: Card, files: readonly IFile[]) {
    for (let cardFile of dialogCard.files) {
      const file = files.find(x => x.id === cardFile.rowId);
      if (!file) {
        continue;
      }

      switch (cardFile.state) {
        case CardFileState.None:
        case CardFileState.Modified:
          // ничего не делаем
          // TODO DESKI: isDirty
          break;
        case CardFileState.Replaced:
        case CardFileState.ModifiedAndReplaced:
        case CardFileState.Inserted:
          if (!file.lastVersion.content) {
            await file.lastVersion.ensureContentLoaded();
          }
          const fileContent = await readFileContentAsBase64(file.lastVersion.content!)!;
          cardFile.info[systemKeyPrefix + 'FileContent'] = createTypedField(
            fileContent,
            DotNetType.String
          );
          cardFile.info[systemKeyPrefix + 'FileVersion'] = cardFile.lastVersion!.getStorage();
          break;
        case CardFileState.Deleted:
          break;
      }
    }
  }

  private async showGlobalDialogAsync(
    coSettings: CardTaskCompletionOptionSettings,
    context: IClientCommandHandlerContext
  ) {
    const info = coSettings.info;
    if (coSettings.preparedNewCard && coSettings.preparedNewCardSignature) {
      info[systemKeyPrefix + 'NewBilletCard'] = createTypedField(
        coSettings.preparedNewCard,
        DotNetType.Binary
      );
      info[systemKeyPrefix + 'NewBilletCardSignature'] = createTypedField(
        coSettings.preparedNewCardSignature,
        DotNetType.Binary
      );
    }
    info[systemKeyPrefix + 'StoreMode'] = createTypedField(coSettings.storeMode, DotNetType.Int32);

    const contextInstance = UIContext.create(
      new UIContext({
        cardEditor: createCardEditorModel(),
        actionOverridings: AdvancedCardDialogManager.instance.createUIContextActionOverridings()
      })
    );
    try {
      await this.createNewCard(coSettings, info, undefined, cardEditor => {
        cardEditor.context.info[systemKeyPrefix + 'DialogClosingAction'] = async (
          uiCtx: IUIContext,
          args: { cancel: boolean }
        ) => {
          if (uiCtx.cardEditor!.operationInProgress) {
            args.cancel = true;
          } else if (!uiCtx.cardEditor!.isClosed) {
            // TODO: check alt key
            const dialogResult = await showConfirm('$KrProcess_Dialog_ConfirmClose');
            if (!dialogResult) {
              args.cancel = true;
            }
          }
          return false;
        };
        this.prepareDialog(cardEditor, coSettings, context);
      });
    } finally {
      contextInstance.dispose();
    }
  }

  private prepareDialog(
    editor: ICardEditorModel,
    coSettings: CardTaskCompletionOptionSettings,
    context: IClientCommandHandlerContext
  ) {
    editor.statusBarIsVisible = false;
    if (coSettings.dialogName) {
      editor.dialogName = coSettings.dialogName;
    }

    editor.toolbar.clearItems();
    editor.bottomToolbar.clearItems();
    editor.bottomDialogButtons.length = 0;

    const getButtonAction = (name: string, cancel: boolean, completionDialog: boolean) => {
      return !cancel
        ? async _ =>
            await this.completeDialogAsync(
              editor,
              null,
              coSettings,
              name,
              completionDialog,
              context
            )
        : async _ => await editor.close();
    };

    for (let actionInfo of coSettings.buttons) {
      const name = actionInfo.name;
      const cancel = actionInfo.cancel;
      const completionDialog = actionInfo.completeDialog;
      let icon: string = '';
      if (actionInfo.icon) {
        icon = actionInfo.icon;
      }

      switch (actionInfo.cardButtonType) {
        case CardButtonType.BottomToolbarButton:
          const bottomActionAsync = getButtonAction(name, cancel, completionDialog);
          editor.bottomToolbar.addItem(
            new CardToolbarAction({
              name,
              icon: icon,
              command: bottomActionAsync as CardToolbarItemCommand,
              order: actionInfo.order,
              caption: actionInfo.caption
            })
          );
          break;
        case CardButtonType.ToolbarButton:
          const topActionAsync = getButtonAction(name, cancel, completionDialog);
          editor.toolbar.addItem(
            new CardToolbarAction({
              name,
              icon: icon,
              command: topActionAsync as CardToolbarItemCommand,
              order: actionInfo.order,
              caption: actionInfo.caption
            })
          );
          break;
        case CardButtonType.BottomDialogButton:
          editor.bottomDialogButtons.push(
            new UIButton(actionInfo.caption, getButtonAction(name, cancel, completionDialog))
          );
          break;
        default:
          throw new Error('CardButtonType out of range.');
      }
    }
  }

  private async createNewCard(
    completionOptionSettings: CardTaskCompletionOptionSettings,
    info?: IStorage,
    modifyCardAction?: (card: Card) => void,
    modifyEditorAction?: (editor: ICardEditorModel) => void
  ) {
    switch (completionOptionSettings.cardNewMethod) {
      case CardTaskDialogNewMethod.Default:
        await createCard({
          cardTypeId: completionOptionSettings.dialogTypeId,
          info: info,
          displayValue: completionOptionSettings.displayValue,
          cardModelModifierAction: modifyCardAction ? ctx => modifyCardAction(ctx.card) : undefined,
          cardEditorModifierAction: modifyEditorAction
            ? ctx => modifyEditorAction(ctx.editor)
            : undefined,
          context: UIContext.current
        });
        break;

      case CardTaskDialogNewMethod.Template:
        await createFromTemplate({
          templateId: completionOptionSettings.dialogTypeId,
          templateInfo: info,
          modifyCardAction,
          modifyEditorAction,
          context: UIContext.current
        });
        break;

      default:
        throw new Error('CardTaskDialogNewMethod out of range.');
    }
  }
}
