import { plainColumnName } from 'tessa/workflow';
import { KrStageTypeFormatter, IKrStageTypeFormatterContext, StageTypeHandlerDescriptor,
  approvalDescriptor } from 'tessa/workflow/krProcess';

export class KrApprovalStageTypeFormatter extends KrStageTypeFormatter {

  private _isParallel = plainColumnName('KrApprovalSettingsVirtual', 'IsParallel');

  public descriptors(): StageTypeHandlerDescriptor[] {
    return [approvalDescriptor];
  }

  public format(context: IKrStageTypeFormatterContext) {
    super.format(context);
    context.displaySettings = context.stageRow.tryGet(this._isParallel)
      ? '$UI_KrApproval_Parallel'
      : '$UI_KrApproval_Sequential';
  }

}