import { plainColumnName } from 'tessa/workflow';
import { KrStageTypeFormatter, IKrStageTypeFormatterContext, StageTypeHandlerDescriptor,
  notificationDescriptor } from 'tessa/workflow/krProcess';

export class NotificationStageTypeFormatter extends KrStageTypeFormatter {

  private _excludeDeputies = plainColumnName('KrNotificationSettingVirtual', 'ExcludeDeputies');
  private _excludeSubscribers = plainColumnName('KrNotificationSettingVirtual', 'ExcludeSubscribers');

  public descriptors(): StageTypeHandlerDescriptor[] {
    return [notificationDescriptor];
  }

  public format(context: IKrStageTypeFormatterContext) {
    super.format(context);

    const excludeDeputies = context.stageRow.get(this._excludeDeputies);
    const excludeSubscribers = context.stageRow.get(this._excludeSubscribers);

    context.displayTimeLimit = '';
    context.displaySettings = this.getDisplaySettings(excludeDeputies, excludeSubscribers);
  }

  private getDisplaySettings(
    excludeDeputies: boolean,
    excludeSubscribers: boolean
  ): string {
    let settings = '';
    if (excludeDeputies) {
      settings = '{$UI_KrNotification_ExcludeDeputies}';
    }
    if (excludeSubscribers) {
      if (excludeDeputies) {
        settings += '\n';
      }
      settings += '{$UI_KrNotification_ExcludeSubscribers}';
    }

    return settings;
  }

}