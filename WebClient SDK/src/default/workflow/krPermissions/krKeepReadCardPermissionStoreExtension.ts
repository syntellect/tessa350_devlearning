import { ICardStoreExtensionContext, CardStoreExtension } from 'tessa/cards/extensions';
import { UIContext } from 'tessa/ui';
import { ICardEditorModel, ICardModel, CardEditorOperationType } from 'tessa/ui/cards';
import { KrToken } from 'tessa/workflow';

export class KrKeepReadCardPermissionStoreExtension extends CardStoreExtension {

  public beforeRequest(context: ICardStoreExtensionContext) {
    const uiContext = UIContext.current;
    let editor!: ICardEditorModel;
    let model!: ICardModel;
    if ((editor = uiContext.cardEditor!)
      && (model = editor.cardModel!)
      // Флаг сохраняем только при открытии при сохранении карточки, рефреш карточки скидывает режим редактирования
      && (editor.currentOperationType === CardEditorOperationType.SaveAndRefresh
        || editor.currentOperationType === CardEditorOperationType.Open)
    ) {
      const token = KrToken.tryGet(model.card.info);
      if (token) {
        token.setInfo(context.request.info);
      }
    }
  }

}