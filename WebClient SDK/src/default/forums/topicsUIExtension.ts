import {
  CardUIExtension,
  ICardUIExtensionContext,
  ICardModel,
  CardSavingRequest,
  CardSavingMode
} from 'tessa/ui/cards';
import { ForumViewModel, ForumDialogManager } from 'tessa/ui/cards/controls';
import {
  showConfirm,
  showConfirmWithCancel,
  showMessage,
  IUIContext,
  UIContext,
  tryGetFromInfo
} from 'tessa/ui';
import { CardStoreMode } from 'tessa/cards';
import { DotNetType, createTypedField } from 'tessa/platform';
import { KrToken } from 'tessa/workflow';
import { IStorage } from 'tessa/platform/storage';
import {
  ViewParameterMetadata,
  equalsCriteriaOperator,
  isTrueCriteriaOperator,
  isFalseCriteriaOperator
} from 'tessa/views/metadata';
import { ParticipantTypes } from 'tessa/forums';
import { RequestParameterBuilder } from 'tessa/views';
import { showView } from 'tessa/ui/uiHost';

export class TopicsUIExtension extends CardUIExtension {
  //#region fields

  private _dispose: Function | null = null;

  //#endregion

  //#region CardUIExtension

  public initialized(context: ICardUIExtensionContext) {
    if (!context.validationResult.isSuccessful) {
      return;
    }

    const allForumControls = context.model.controlsBag.filter(
      x => x instanceof ForumViewModel
    ) as ForumViewModel[];
    for (let forumControl of allForumControls) {
      if (!forumControl.isLicenseValid) {
        continue;
      }

      if (context.model.inSpecialMode) {
        forumControl.isEnabledAddTopic = false;
        forumControl.isEnabledForumEmptyContextMenu = true;
      }

      forumControl.openParticipantsAction = this.getOpenParticipantsAction(context, forumControl);
      forumControl.checkAddTopicPermissionAction = this.getCheckAddTopicPermissionsAction(
        context,
        forumControl
      );
      forumControl.checkSuperModeratorPermissionAction = this.getCheckSuperModeratorPermissionAction(
        context,
        forumControl
      );

      const topicId = tryGetFromInfo<guid>(context.model.card.info, 'topicID');
      const topicTypeId = tryGetFromInfo<guid>(context.model.card.info, 'topicTypeID');
      if (topicId && topicTypeId && forumControl.topicTypeId === topicTypeId) {
        forumControl.showTopic(topicId);
        delete context.model.card.info['topicID'];
        delete context.model.card.info['topicTypeID'];
        return;
      } else {
        forumControl.showTopics();
      }
    }
  }

  public finalized() {
    if (this._dispose) {
      this._dispose();
      this._dispose = null;
    }
  }

  //#endregion

  //#region actions

  private getOpenParticipantsAction(context: ICardUIExtensionContext, _control: ForumViewModel) {
    const openParticipantsAction = async (
      topicId: guid,
      participantType: ParticipantTypes,
      isSuperModerator: boolean
    ): Promise<void> => {
      const paramTopicMeta = new ViewParameterMetadata();
      paramTopicMeta.alias = 'TopicID';
      const paramTopicId = new RequestParameterBuilder()
        .withMetadata(paramTopicMeta)
        .addCriteria(equalsCriteriaOperator(), 'topicID', topicId)
        .asRequestParameter();

      const paramCardMeta = new ViewParameterMetadata();
      paramCardMeta.alias = 'CardID';
      const paramCardId = new RequestParameterBuilder()
        .withMetadata(paramCardMeta)
        .addCriteria(equalsCriteriaOperator(), 'cardID', context.card.id)
        .asRequestParameter();

      const paramParticipantMeta = new ViewParameterMetadata();
      paramParticipantMeta.alias = 'ParticipantTypeID';
      const paramParticipantType = new RequestParameterBuilder()
        .withMetadata(paramParticipantMeta)
        .addCriteria(equalsCriteriaOperator(), 'participantType', participantType)
        .asRequestParameter();

      const paramIsSuperModeratorMeta = new ViewParameterMetadata();
      paramIsSuperModeratorMeta.alias = 'IsSuperModeratorMode';
      const paramIsSuperModerator = new RequestParameterBuilder()
        .withMetadata(paramIsSuperModeratorMeta)
        .addCriteria(isSuperModerator ? isTrueCriteriaOperator() : isFalseCriteriaOperator())
        .asRequestParameter();

      await showView({
        viewAlias: 'TopicParticipants',
        displayValue: '$Workplaces_User_TopicParticipants',
        parameters: [paramTopicId, paramCardId, paramParticipantType, paramIsSuperModerator],
        treeVisible: false
      });
    };

    return openParticipantsAction;
  }

  private getCheckAddTopicPermissionsAction(
    context: ICardUIExtensionContext,
    control: ForumViewModel
  ) {
    const checkAddTopicPermissionsAction = async (): Promise<void> => {
      await this.openMarkedCard(
        context.uiContext,
        'kr_calculate_addtopic_permissions',
        null, // Не требуем подтверждения дейтсвия если не было изменений
        async cardIsNew =>
          cardIsNew
            ? await (showConfirm('$KrTiles_EditModeConfirmation') ? true : null)
            : await showConfirmWithCancel('$KrTiles_EditModeConfirmation'),
        async () => await this.addTopicShowDialog(context, control)
      );
    };
    return checkAddTopicPermissionsAction;
  }

  private getCheckSuperModeratorPermissionAction(
    context: ICardUIExtensionContext,
    control: ForumViewModel
  ) {
    const checkSuperModeratorPermissionAction = async (): Promise<void> => {
      await this.openMarkedCard(
        context.uiContext,
        'kr_calculate_supermoderator_permissions',
        null, // Не требуем подтверждения дейтсвия если не было изменений
        async cardIsNew =>
          cardIsNew
            ? await (showConfirm('$KrTiles_EditModeConfirmation') ? true : null)
            : await showConfirmWithCancel('$KrTiles_EditModeConfirmation'),
        async () => await this.superModeratorPermissionsMessage(context, control)
      );
    };
    return checkSuperModeratorPermissionAction;
  }

  private addTopicShowDialog = async (
    context: ICardUIExtensionContext,
    control: ForumViewModel
  ) => {
    if (
      control.permissionsProvider.isEnableAddTopic(context.uiContext.cardEditor!.cardModel!.card)
    ) {
      await ForumDialogManager.instance.addTopicShowDialog(context.card.id, model =>
        control.modifyAddingTopic(model)
      );
      return true;
    } else {
      await showMessage('$Forum_Permission_NoPermissionToAddTopic');
      return false;
    }
  };

  private superModeratorPermissionsMessage = async (
    context: ICardUIExtensionContext,
    control: ForumViewModel
  ) => {
    if (
      control.permissionsProvider.isEnableSuperModeratorMode(
        context.uiContext.cardEditor!.cardModel!.card
      )
    ) {
      await showMessage('$Forum_Permission_SuperModeratorModeOn');
      return false;
    } else {
      await showMessage('$Forum_Permission_NoRequiredPermissions');
      return false;
    }
  };

  private async openMarkedCard(
    context: IUIContext,
    mark: string | null,
    proceedConfirmation: (() => Promise<boolean>) | null,
    proceedAndSaveCardConfirmation: ((cardIsNew: boolean) => Promise<boolean | null>) | null,
    continuationOnSuccessFunc: (() => Promise<boolean>) | null = null,
    getInfo: IStorage | null = null
  ): Promise<void> {
    const editor = context.cardEditor;
    let model: ICardModel;

    if (!editor || editor.operationInProgress || !(model = editor.cardModel!)) {
      return;
    }

    const cardIsNew = model.card.storeMode === CardStoreMode.Insert;
    const hasChanges = cardIsNew || model.hasChanges();
    let saveCardBeforeOpening: boolean | null = false;

    if (hasChanges && proceedAndSaveCardConfirmation) {
      saveCardBeforeOpening = await proceedAndSaveCardConfirmation(cardIsNew);
      // Если не указана функция подтверждения с вариантом отмены - сохраняем карточку
      // если есть подтверждение основного действия
    } else if (hasChanges && proceedConfirmation) {
      saveCardBeforeOpening = (await proceedConfirmation()) ? true : null;
      // Если в карточке не было изменений - не вызываем сохранения
    } else if (proceedConfirmation) {
      saveCardBeforeOpening = (await proceedConfirmation()) ? false : null;
      // Если не указана функция подтверждения и нет изменений - вызываем основное действие
      // без подтверждения и сохранения
    } else {
      saveCardBeforeOpening = false;
    }

    if (saveCardBeforeOpening === null) {
      return;
    }

    if (!getInfo) {
      getInfo = {};
    }

    if (mark) {
      getInfo[mark] = createTypedField(true, DotNetType.Boolean);
    }

    if (saveCardBeforeOpening) {
      const token = KrToken.tryGet(editor.info);
      KrToken.remove(editor.info);

      const saveSuccess = await editor.saveCard(
        context,
        {
          '.SaveWithPermissionsCalc': createTypedField(true, DotNetType.Boolean)
        },
        new CardSavingRequest(CardSavingMode.KeepPreviousCard)
      );

      if (!saveSuccess) {
        return;
      }

      if (token) {
        token.setInfo(getInfo);
      }
    }

    const cardId = model.card.id;
    const cardType = model.cardType;

    const sendTaskSucceeded = await editor.openCard({
      cardId,
      cardTypeId: cardType.id!,
      cardTypeName: cardType.name!,
      context,
      info: getInfo
    });

    if (sendTaskSucceeded) {
      editor.isUpdatedServer = true;
    } else if (cardIsNew || saveCardBeforeOpening) {
      // если карточка новая или была сохранена, а также не удалось выполнить mark-действие при открытии,
      // то у нас будет "висеть" карточка с некорректной версией;
      // её надо обновить, на этот раз без mark'и

      await editor.openCard({
        cardId,
        cardTypeId: cardType.id!,
        cardTypeName: cardType.name!,
        context
      });
    }

    if (!continuationOnSuccessFunc) {
      return;
    }

    const contextInstance = UIContext.create(context);
    try {
      await continuationOnSuccessFunc();
    } finally {
      contextInstance.dispose();
    }
  }

  //#endregion
}
