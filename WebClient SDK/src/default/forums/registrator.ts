import { ExtensionContainer, ExtensionStage } from 'tessa/extensions';

import { HideForumTabUIExtension } from './hideForumTabUIExtension';
import { TopicsUIExtension } from './topicsUIExtension';

ExtensionContainer.instance.registerExtension({
  extension: HideForumTabUIExtension,
  stage: ExtensionStage.AfterPlatform,
  order: 1
});
ExtensionContainer.instance.registerExtension({
  extension: TopicsUIExtension,
  stage: ExtensionStage.AfterPlatform,
  order: 2
});
