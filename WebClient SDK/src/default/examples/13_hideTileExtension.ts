import { TileExtension, ITileLocalExtensionContext } from 'tessa/ui/tiles';
import { UIContext } from 'tessa/ui';
import { ICardModel } from 'tessa/ui/cards';

/**
 * Скрываем тайл "Сохранить" для тестовых карточек
 */
export class HideTileExtension extends TileExtension {

  public initializingLocal(context: ITileLocalExtensionContext) {
    const panel = context.workspace.leftPanel;

    const saveTile = panel.tryGetTile('SaveCard');
    if (!saveTile) {
      return;
    }

    // скрываем тайл
    saveTile.evaluating.add(e => {
      const editor = UIContext.current.cardEditor;
      let cardModel: ICardModel;
      if (!editor
        || !(cardModel = editor.cardModel!)
        || cardModel.card.typeId !== '4bbd2f5e-6c65-41bd-a159-1a373355a26c'
      ) {
        return;
      }

      e.setIsEnabledWithCollapsing(e.currentTile, false);
    });
  }

}