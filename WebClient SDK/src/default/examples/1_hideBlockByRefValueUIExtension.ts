import { CardUIExtension, ICardUIExtensionContext, IBlockViewModel, IControlViewModel } from 'tessa/ui/cards';
import { Visibility, Guid } from 'tessa/platform';

/**
 * В зависимости от значения ссылочного поля карточки:
 * - скрываем\показываем элементы управления (блок, контрол)
 * - делаем рид-онли\редактируемыми элементы управления (контрол)
 *
 * Если поле "Валюта" не пустое, то блок с файлами становится видимым, контрол "Автор" скрывается,
 * а контрол "Контрагент" становится только для чтения.
 * Если поле "Валюта" пустое, то блок с файлами скрывается, контрол "Автор" становится видимым,
 * а контрол "Контрагент" становится редактируемым.
 */
export class HideBlockByRefValueUIExtension extends CardUIExtension {

  public initialized(context: ICardUIExtensionContext) {
    // если карточка не для тестов, то ничего не делаем
    if (!Guid.equals(context.card.typeId, '4bbd2f5e-6c65-41bd-a159-1a373355a26c')) {
      return;
    }

    // находим секцию
    const section = context.card.sections.tryGet('DocumentCommonInfo');
    // находим блок
    const block = context.model.blocks.get('Block5');
    // находим контролы
    const control1 = context.model.controls.get('AuthorControl');
    const control2 = context.model.controls.get('PartnerControl');
    if (!section
      || !block
      || !control1
      || !control2
    ) {
      return;
    }

    // смотрим есть ли какое-нибудь значение в поле CurrencyID
    const itemExists = !!section.fields.tryGet('CurrencyID');
    HideBlockByRefValueUIExtension.hideBlock(
      block,
      control1,
      control2,
      itemExists
    );

    // подписываемся на изменения полей в секции
    section.fields.fieldChanged.add(e => {
      // если было изменено другое поле, то никак не реагируем
      if (e.fieldName !== 'CurrencyID') {
        return;
      }

      HideBlockByRefValueUIExtension.hideBlock(
        block,
        control1,
        control2,
        !!e.fieldValue
      );
    });
  }

  private static hideBlock(
    block: IBlockViewModel,
    control1: IControlViewModel,
    control2: IControlViewModel,
    itemExists: boolean
  ) {
    if (itemExists) {
      // если значение есть, то мы показываем блок, скрываем первый контрол и делаем readOnly второй
      block.blockVisibility = Visibility.Visible;
      control1.controlVisibility = Visibility.Collapsed;
      control2.isReadOnly = true;
    } else {
      // если значения нет то мы скрываем блок, показываем первый контрол и делаем редактируемым второй
      block.blockVisibility = Visibility.Collapsed;
      control1.controlVisibility = Visibility.Visible;
      control2.isReadOnly = false;
    }
  }

}