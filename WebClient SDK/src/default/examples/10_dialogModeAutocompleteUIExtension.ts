import { runInAction } from 'mobx';
import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
import { Guid } from 'tessa/platform';
import { AutoCompleteEntryViewModel, AutoCompleteTableViewModel } from 'tessa/ui/cards/controls';

/**
 * Показываем выбранный автокомплит всегда в диалоговом окне.
 */
export class DialogModeAutocompleteUIExtension extends CardUIExtension {

  public initialized(context: ICardUIExtensionContext) {
    // если карточка не для тестов, то ничего не делаем
    if (!Guid.equals(context.card.typeId, '4bbd2f5e-6c65-41bd-a159-1a373355a26c')) {
      return;
    }

    const currencyControl = context.model.controls.get('CurrencyControl') as AutoCompleteEntryViewModel;
    if (currencyControl) {
      // runInAction нужен в том случае если мы пытаемся поменять observable поле,
      // для которого уже созданы observers
      runInAction(() => {
        currencyControl.alwaysShowInDialog = true;
      });
    }

    const linksControl = context.model.controls.get('LinksControl') as AutoCompleteTableViewModel;
    if (linksControl) {
      // runInAction нужен в том случае если мы пытаемся поменять observable поле,
      // для которого уже созданы observers
      runInAction(() => {
        linksControl.alwaysShowInDialog = true;
      });
    }
  }

}