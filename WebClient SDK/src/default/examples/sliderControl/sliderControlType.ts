import { CardControlType, CardControlTypeUsageMode, CardControlTypeFlags } from 'tessa/cards';

export const SliderControlType = new CardControlType(
  'c8bc8df5-c802-480a-9a18-1eab1f1f9e4a',
  'Slider',
  CardControlTypeUsageMode.Entry,
  CardControlTypeFlags.UseEverywhere
);