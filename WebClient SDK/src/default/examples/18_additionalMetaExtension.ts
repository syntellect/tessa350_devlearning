import { ApplicationExtension, IApplicationExtensionMetadataContext, MetadataStorage } from 'tessa';
import { IStorage } from 'tessa/platform/storage';
import { tryGetFromInfo } from 'tessa/ui';
import { TileExtension, ITileGlobalExtensionContext } from 'tessa/ui/tiles';

/*
  Пример:
  на серверной стороне добавляем необходмые данные в мету через ServerInitializationExtension
  на клинете достаем эти данные по ключу из меты и добавляем в MetadataStorage.info
  после инициализации приложения эти данные из MetadataStorage.info можно получить в любом расширении и использовать
*/

const AdditionalTilesKey = 'AdditionalTiles';

export class AdditionalMetaInitializationExtension extends ApplicationExtension {

  public afterMetadataReceived(context: IApplicationExtensionMetadataContext) {
    if (context.mainPartResponse) {
      const info = context.mainPartResponse.tryGetInfo();
      if (!info) {
        return;
      }

      const additionalTiles = tryGetFromInfo<IStorage[] | null>(info, AdditionalTilesKey, null);
      if (!additionalTiles) {
        return;
      }

      if (!MetadataStorage.instance.info.has(AdditionalTilesKey)) {
        MetadataStorage.instance.info.set(AdditionalTilesKey, additionalTiles);
      }
    }
  }

}

export class AdditionalMetaTileExtension extends TileExtension {

  public initializingGlobal(_context: ITileGlobalExtensionContext) {
    const additionalTiles: IStorage[] | null = MetadataStorage.instance.info.get(AdditionalTilesKey)
    if (!additionalTiles) {
      return;
    }

    for (let _tileInfo of additionalTiles) {
      // create tile
    }
  }

}