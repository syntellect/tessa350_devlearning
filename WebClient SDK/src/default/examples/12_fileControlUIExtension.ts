import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
import { Guid } from 'tessa/platform';
import { FileListViewModel } from 'tessa/ui/cards/controls';
import { showMessage } from 'tessa/ui/tessaDialog';

/**
 * В выбранном файловом контроле показываем только определенные файлы.
 * Разрешаем добавлять файлы только с определенным расширением.
 */
export class FileControlUIExtension extends CardUIExtension {
  public initialized(context: ICardUIExtensionContext) {
    // если не карточка "автомобиль", то ничего не делаем
    if (!Guid.equals(context.card.typeId, 'd0006e40-a342-4797-8d77-6501c4b7c4ac')) {
      return;
    }

    const imageFileControl = context.model.controls.get('ImageFilesControl') as FileListViewModel;
    if (!imageFileControl) {
      return;
    }

    // показываем файлы только с категорией Image
    for (let i = imageFileControl.files.length - 1; i > -1; i--) {
      const file = imageFileControl.files[i];
      if (!(file.model.category && file.model.category.caption === 'Image')) {
        imageFileControl.removeFile(file);
      }
    }

    // если добавляется файл с расширением отличным от 'txt', то прерываем добавление
    context.fileContainer.containerFileChanging.add(async e => {
      const file = e.added;
      if (file && file.getExtension() !== 'txt') {
        await showMessage(`Вы должны приложить файл только с расширением 'txt'.`);
        e.cancel = true;
      }
    });
  }
}
