import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
import { Guid, Visibility } from 'tessa/platform';
import { tryGetFromInfo } from 'tessa/ui';
import { CardSingletonCache } from 'tessa/cards';

/**
 * Скрывать\показывать вкладку карточки в зависимости:
 * - от наличия какого-то признака в info, пришедшем с сервера
 * - от значения какого-то справочника, загруженного в init-стриме
 * - от данных карточки
 */
export class HideFormUIExtension extends CardUIExtension {

  public initialized(context: ICardUIExtensionContext) {
    // если карточка не для тестов, то ничего не делаем
    if (!Guid.equals(context.card.typeId, '4bbd2f5e-6c65-41bd-a159-1a373355a26c')) {
      return;
    }

    const testForm = context.model.forms.find(x => x.name === 'TestTab');
    if (!testForm) {
      return;
    }

    // пытаемся найти флажок в info карточки
    const hideFromInfo = tryGetFromInfo(context.card.info, '__HideForm', false);

    // пытаемся найти флажок в справочнике
    let hideFromSettings = false;
    const settings = CardSingletonCache.instance.cards.get('KrSettings');
    if (settings) {
      const section = settings.sections.tryGet('KrSettings');
      if (section) {
        hideFromSettings = section.fields.get('HideCommentForApprove');
      }
    }

    // смотрим на флажок в данных карточки
    let hideFromCard = false;
    const additionalInfo = context.card.sections.tryGet('TEST_CarAdditionalInfo');
    if (additionalInfo) {
      hideFromCard = additionalInfo.fields.get('IsBaseColor');
    }

    const hideForm = hideFromInfo || hideFromSettings || hideFromCard;
    // скрываем или показываем вкладку
    testForm.visibility = hideForm ? Visibility.Hidden : Visibility.Visible;
  }

}