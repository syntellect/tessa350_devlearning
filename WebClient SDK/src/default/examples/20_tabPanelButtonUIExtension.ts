import { TabPanelUIExtension, ITabPanelUIExtensionContext, TabPanelButton,
  showLoadingOverlay } from 'tessa/ui';
import { openCard } from 'tessa/ui/uiHost';

export class CustomTabPanelButtonUIExtension extends TabPanelUIExtension {

  public initialize(context: ITabPanelUIExtensionContext) {
    // выводится слева и на мобилах выводтся в контекстном меню
    context.buttons.push(TabPanelButton.create({
      name: 'RoleDeputiesManagement',
      caption: '$UI_Tiles_RoleDeputiesManagement',
      buttonAction: async () => {
        // открываем виртуальную карточку "Мои замещения" для текущего пользователя
        await showLoadingOverlay(async (splashResolve) => {
          const editor = await openCard({
            cardTypeId: 'cb931209-2ad9-4370-bb3c-3172e61937ba', // RoleDeputiesManagementTypeID
            splashResolve
          });

          if (editor) {
            const workspaceInfo = '$UI_Tiles_Settings';
            if (editor.workspaceInfo !== workspaceInfo) {
              editor.workspaceInfo = workspaceInfo;
              editor.cardModelInitialized.add(async e => { e.workspaceInfo = workspaceInfo; });
            }
          }
        });
      }
    }));
  }

}