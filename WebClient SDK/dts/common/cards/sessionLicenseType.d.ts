export declare enum SessionLicenseType {
    /**
     * Лицензия не используется.
     */
    Unspecified = 0,
    /**
     * Конкурентная лицензия толстого клиента.
     */
    ConcurrentClient = 1,
    /**
     * Персональная лицензия толстого клиента.
     */
    PersonalClient = 2,
    /**
     * Конкурентная лицензия лёгкого клиента.
     */
    ConcurrentWeb = 3,
    /**
     * Персональная лицензия лёгкого клиента.
     */
    PersonalWeb = 4
}
