/**
 * Описывает устройства, от которых приходят действия
 */
export declare enum InteractionEventTypes {
    /**
       * Мышиное событие
       * @type {Number}
       */
    EVENT_TYPE_MOUSE = 0,
    /**
     * Таповое событие
     * @type {Number}
     */
    EVENT_TYPE_TOUCH = 1
}
