import { ApplicationExtension, IApplicationExtensionMetadataContext } from 'tessa';
import { FileExtension, FileExtensionContext } from 'tessa/ui/files';
import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
export declare class DeskiExtension extends ApplicationExtension {
    private _lastMasterKeysUpdate;
    afterMetadataReceived(_context: IApplicationExtensionMetadataContext): void;
}
export declare class DeskiFileExtension extends FileExtension {
    openingMenu(context: FileExtensionContext): void;
}
export declare class DeskiUIExtension extends CardUIExtension {
    shouldExecute(): boolean;
    initialized(context: ICardUIExtensionContext): void;
    saving(context: ICardUIExtensionContext): Promise<void>;
    finalized(context: ICardUIExtensionContext): Promise<void>;
}
