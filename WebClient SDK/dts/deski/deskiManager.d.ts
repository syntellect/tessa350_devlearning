import { ValidationResult } from 'tessa/platform/validation';
import { DeskiInfo } from 'tessa/platform';
import { IStorage } from 'tessa/platform/storage';
export declare type DeskiFileInfo = {
    Name: string;
    Path: string;
    Modified: string;
    IsModified: boolean;
    IsLocked: boolean;
    Size: number;
    ID: string;
    Editable: boolean;
    App: string;
};
export declare type MasterKey = {
    Key: string;
    ExpiresAt: string;
};
export interface CipherInfo {
    Info: IStorage | null;
    LocalExpiryDates: string[];
    LocalPrivateKeys: string[];
}
export declare class DeskiManager {
    private constructor();
    private static _instance;
    static get instance(): DeskiManager;
    private _masterKeys;
    get masterKeys(): ReadonlyArray<MasterKey>;
    get deskiEnabled(): boolean;
    get deskiAvailable(): boolean;
    getDeskiUrl(deskiPort?: number): string;
    findDeski(delayTimeout?: boolean): Promise<{
        port?: number;
        info?: DeskiInfo;
    }>;
    checkDeski(silent?: boolean): Promise<boolean>;
    updateMasterKeys(cipher?: CipherInfo): Promise<ValidationResult>;
    private getRequestOptions;
    setAppInfo(appUrl: string, keys: ReadonlyArray<MasterKey>): Promise<{
        success: true;
    } | {
        success: false;
        result: ValidationResult;
    }>;
    getContentInfo(appUrl: string, id: string): Promise<{
        isCached: boolean;
        result?: ValidationResult;
    }>;
    cacheContent(appUrl: string, id: string, name: string, content: File): Promise<{
        success: true;
    } | {
        success: false;
        result: ValidationResult;
    }>;
    openFile(appUrl: string, id: string, mode: 'file' | 'folder', editable: boolean): Promise<{
        success: true;
    } | {
        success: false;
        result: ValidationResult;
    }>;
    getFileInfo(appUrl: string, id: string, editable: boolean): Promise<{
        info?: DeskiFileInfo;
        result?: ValidationResult;
    }>;
    getFileData(appUrl: string, id: string): Promise<{
        data?: Blob;
        result?: ValidationResult;
    }>;
    cacheModFileWithNewId(appUrl: string, id: string, newId: string): Promise<{
        success: true;
    } | {
        success: false;
        result: ValidationResult;
    }>;
    removeFile(appUrl: string, id: string, editable: boolean): Promise<{
        success: true;
    } | {
        success: false;
        result: ValidationResult;
    }>;
    getContent(appUrl: string, id: string): Promise<{
        data?: Blob;
        result?: ValidationResult;
    }>;
    getOpenFiles(appUrl: string): Promise<{
        files?: Array<DeskiFileInfo>;
        result?: ValidationResult;
    }>;
}
