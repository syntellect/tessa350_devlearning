export declare const CACHE_NAME = "tessa-web-cache";
export declare function isMetaRequest(request: Request): boolean;
export declare function isApiRequest(request: Request): boolean;
export declare function isWallpaperRequest(request: Request): boolean;
export declare function isThemeImages(request: Request): boolean;
export declare function isLocosRequest(request: Request): boolean;
export declare function getLocoFromRequest(request: Request): string | null;
