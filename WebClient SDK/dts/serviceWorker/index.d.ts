declare global {
    interface Window {
        /**
         * ServiceWorkerGlobalScope
         *
         */
        skipWaiting: () => void;
        /**
         * ServiceWorkerGlobalScope
         *
         */
        clients: {
            matchAll: (options: {
                [key: string]: any;
            }) => Promise<{
                url: string;
            }[]>;
            claim: () => void;
        };
        registration: {
            scope: string;
        };
        __MULTIPLE_INSTANCES__: boolean;
    }
}
export {};
