import { ClientCommandHandlerBase } from 'tessa/workflow/krProcess/clientCommandInterpreter';
import { IClientCommandHandlerContext } from 'tessa/workflow/krProcess';
import { CardTaskCompletionOptionSettings, CardTaskDialogActionResult } from 'tessa/cards';
import { ICardEditorModel } from 'tessa/ui/cards';
export declare abstract class AdvancedDialogCommandHandler extends ClientCommandHandlerBase {
    handle(context: IClientCommandHandlerContext): Promise<void>;
    protected abstract prepareDialogCommand(context: IClientCommandHandlerContext): CardTaskCompletionOptionSettings | null;
    protected abstract completeDialogCoreAsync(actionResult: CardTaskDialogActionResult, context: IClientCommandHandlerContext, cardEditor: ICardEditorModel, parentCardEditor: ICardEditorModel | null): any;
    private completeDialogAsync;
    private prepareFilesForStore;
    private showGlobalDialogAsync;
    private prepareDialog;
    private createNewCard;
}
