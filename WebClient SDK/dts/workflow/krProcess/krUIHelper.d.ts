import { KrTypesCache, IKrType } from 'tessa/workflow';
import { Card } from 'tessa/cards';
import { ValidationResultBuilder } from 'tessa/platform/validation';
export declare const CompiledCardTypes: string[];
export declare function sendCompileRequest(compileFlag: string): Promise<void>;
export declare function tryGetKrType(krTypesCache: KrTypesCache, card: Card, cardTypeId: guid, validationResult?: ValidationResultBuilder | null): IKrType | null;
export declare function designTimeCard(typeId: guid): boolean;
export declare function runtimeCard(typeId: guid): boolean;
