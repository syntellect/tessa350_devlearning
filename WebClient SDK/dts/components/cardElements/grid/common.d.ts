import { GridUIBlockViewModel } from './models';
export declare const StyledTr: import("styled-components").StyledComponent<"tr", any, {}, never>;
export declare const blockHandle: (block: GridUIBlockViewModel) => (e: any) => void;
