import { CSSProperties } from 'react';
import { TextAlignProperty } from 'csstype';
import { MenuAction } from 'tessa/ui';
import { ColumnSortDirection } from 'tessa/ui/cards/controls/columnSortDirection';
export declare class GridUIRowModel {
    rowId: string;
    cells: GridUICellModel[];
    private _onClick?;
    private _onDoubleClick?;
    onToggle: (isToggleNeeded?: boolean) => void;
    isToggled: boolean;
    private _isSelected;
    private _isLastSelected;
    private setSelected;
    parentRowId?: string | null;
    parentBlockId: string | undefined;
    isParentToggled: () => boolean;
    getMenuActions?: (vm: GridUIViewModel, columnIndex?: number) => MenuAction[];
    vm: GridUIViewModel;
    /**
     * Необработанные данные с сервера для текущей строки
     *
     * @type {any[]}
     * @memberof GridUIRowModel
     */
    rawData: CellValue[] | undefined;
    menuActions: MenuAction[];
    onClickWrapper: (e: React.MouseEvent) => void;
    onMouseDown?: (e: React.MouseEvent) => void;
    constructor(args: IGridUIRowModelConstructor & IParentToggled & {
        vm: GridUIViewModel;
    });
    getContextMenu(e: React.MouseEvent, columnIndex?: number): MenuAction[];
    setToggle(): void;
    get isSelected(): boolean;
    set isSelected(value: boolean);
    get isLastSelected(): boolean;
    private _style?;
    get style(): CSSProperties;
    getColumnValue: (columnAlias: string) => any;
    getAppearencesSetting: () => never[];
    onClick: (e: import("react").MouseEvent<Element, MouseEvent> | import("react").KeyboardEvent<Element>, columnIndex?: number | undefined) => void;
    onDoubleClick: (e: import("react").MouseEvent<Element, MouseEvent> | import("react").KeyboardEvent<Element>, columnIndex?: number | undefined) => void;
    toolTip?: string;
}
export interface IGridUIRowModel {
    rowId: string;
    parentRowId?: string | null;
    parentBlockId?: string;
    cells: IGridUICellModel[];
    isToggled?: boolean;
    onClick?: (e: React.MouseEvent, columnIndex?: number) => void;
    onDoubleClick?: (e: React.MouseEvent, columnIndex?: number) => void;
    onMouseDown?: (e: React.MouseEvent) => void;
    getIsSelected: () => boolean;
    getIsLastSelected: () => boolean;
    setSelected: (value: boolean) => void;
    onToggle?: (isToggleNeeded: boolean) => void;
    getMenuActions?: (vm: GridUIViewModel, columnIndex?: number) => MenuAction[];
    style?: React.CSSProperties;
    rawData?: CellValue[];
    menuActions?: MenuAction[];
    toolTip?: string;
}
export interface IUIViewMetadata {
    appearance?: string | null;
}
export interface IGridUIRowModelConstructor extends IGridUIRowModel {
    cells: GridUICellModel[];
}
export interface IGridUICompatibleModal {
    canSelectMultipleItems: boolean;
    rows: ReadonlyArray<{
        rowId: string;
        isSelected: boolean;
    }>;
    selectedRows: ReadonlyArray<{
        rowId: string;
        isSelected: boolean;
    }>;
}
export declare class GridUICellModel {
    content: string | Function;
    className: string | undefined;
    private vmGetter;
    get vm(): GridUIRowModel;
    private _onClick?;
    private _onDoubleClick?;
    private _getIsSelected;
    private _setSelected;
    onClickWrapper: (e: React.MouseEvent) => void;
    _onMouseDown?: (e: React.MouseEvent) => void;
    private _index;
    constructor(args: IGridUICellModel & {
        vmGetter: () => GridUIRowModel;
        index: number;
    });
    private _style?;
    get style(): CSSProperties;
    getContextMenu(e: React.MouseEvent): MenuAction[];
    get isSelected(): boolean;
    set isSelected(value: boolean);
    onClick: (e: import("react").MouseEvent<Element, MouseEvent> | import("react").KeyboardEvent<Element>) => void;
    onDoubleClick: (e: import("react").MouseEvent<Element, MouseEvent> | import("react").KeyboardEvent<Element>) => void;
    onMouseDown: (e: import("react").MouseEvent<Element, MouseEvent>) => void;
    get index(): number;
    toolTip?: string;
}
export interface IGridUICellModel {
    content: string | Function;
    className?: string;
    style?: React.CSSProperties;
    toolTip?: string;
    onClick?: (e: React.MouseEvent | React.KeyboardEvent) => void;
    onDoubleClick?: (e: React.MouseEvent | React.KeyboardEvent) => void;
    onMouseDown?: (e: React.MouseEvent) => void;
    getIsSelected?: () => boolean;
    setSelected?: (value: boolean) => void;
}
export declare class GridUIHeaderCellModel {
    alias: string;
    caption: string;
    menuActions: MenuAction[];
    getMenuActions: ((vm: GridUIViewModel) => MenuAction[]) | undefined;
    onClick: ((e: React.MouseEvent) => void) | undefined;
    sortDirection: ColumnSortDirection;
    vm: GridUIViewModel;
    canSort: boolean;
    bold: boolean;
    alignment: TextAlignProperty;
    toolTip?: string;
    index: number;
    constructor(args: IGridUIHeaderCellModel & {
        vm: GridUIViewModel;
        index: number;
    });
    getContextMenu(): MenuAction[];
}
export interface IGridUIHeaderCellModel {
    alias: string;
    caption: string;
    menuActions?: MenuAction[];
    /**
     * Имеет приоритет над массивом
     */
    getMenuActions?: (vm: GridUIViewModel) => MenuAction[];
    sortDirection?: ColumnSortDirection;
    onClick?: (e: React.MouseEvent) => void;
    meta?: IUIColumnMetadata;
    canSort?: boolean;
    bold?: boolean;
    alignment?: TextAlignProperty;
    toolTip?: string;
}
export interface IUIColumnMetadata {
    appearance?: string | null;
}
export declare class GridUIViewModel {
    _rows: GridUIRowModel[];
    _headers: GridUIHeaderCellModel[];
    _blocks?: GridUIBlockViewModel[];
    rawData: IViewData | undefined;
    canSelectMultipleItems: boolean;
    cellSelectionMode: boolean;
    multiSelectEnabled: boolean;
    canReorderHeaders: boolean;
    dragableHeader?: GridUIHeaderCellModel;
    keyDownHandler?: (e: React.KeyboardEvent) => void;
    headerDropHandler?: (args: {
        source: IGridUIHeaderCellModel;
        target: IGridUIHeaderCellModel;
        sourceIndex: number;
        targetIndex: number;
    }) => void;
    constructor(args: {
        canSelectMultipleItems: boolean;
        cellSelectionMode?: boolean;
        multiSelectEnabled?: boolean;
        canReorderHeaders?: boolean;
        rows: IGridUIRowModel[];
        headers: IGridUIHeaderCellModel[];
        blocks?: IGridUIBlockViewModel[];
        rawData?: IViewData;
        keyDownHandler?: (e: React.KeyboardEvent) => void;
        headerDropHandler?: (args: {
            source: IGridUIHeaderCellModel;
            target: IGridUIHeaderCellModel;
            sourceIndex: number;
            targetIndex: number;
        }) => void;
    });
    private isParentToggled;
    get rows(): GridUIRowModel[];
    get headers(): GridUIHeaderCellModel[];
    get blocks(): GridUIBlockViewModel[];
    getColumnIndex: (columnAlias: string) => number;
    getOrderedRows(): GridUIRowModel[];
    handleKeyDown: (event: import("react").KeyboardEvent<Element>, isColumnOverflowed: (columnIndex: number) => boolean) => void;
}
export interface IGridUIBlockViewModel {
    id: string;
    parentBlockId: string | null;
    caption: string;
    count?: number;
    isToggled?: boolean;
    onToggle?: () => void;
}
export declare class GridUIBlockViewModel {
    id: string;
    parentBlockId: string | null;
    caption: string;
    count: number;
    private _isToggled;
    private _onToggle?;
    isParentToggled: () => boolean;
    constructor(args: IGridUIBlockViewModel & IParentToggled);
    get isToggled(): boolean;
    set isToggled(value: boolean);
    private setIsOpen;
}
export interface IParentToggled {
    isParentToggled(): boolean;
}
interface IViewData {
    rowcount: number;
    datatypes: string[];
    columns: string[];
    rows: CellValue[][];
}
declare type CellValue = any;
export {};
