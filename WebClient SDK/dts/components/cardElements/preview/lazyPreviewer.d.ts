import { Component } from 'react';
import { Result } from 'tessa/platform';
import { IFileVersion } from 'tessa/files';
import { ScaleOption } from 'tessa/ui/files';
export interface DefaultPreviewerProps {
    previewFileVersion: IFileVersion | null;
    previewExtension: string;
    previewFileName: string;
    l: (value: string) => string;
    previewPdfEnabled: boolean;
    tabMode: boolean;
    isPreviewTab: boolean;
    getFileContentFunc: (version: IFileVersion) => Promise<Result<File>>;
    rotateAngles: {
        [key: number]: number;
    };
    setRotateAngle: (fileVersionId: string, angle: number) => void;
    pageNumber: number;
    setPageNumber: (value: number) => void;
    scale: ScaleOption;
    customScaleValue: number;
    setScale: (value: ScaleOption, customScaleValue: number) => void;
}
export default class DefaultPreviewer extends Component<DefaultPreviewerProps> {
    render(): JSX.Element;
}
