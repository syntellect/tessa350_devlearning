import React from 'react';
import { IFileVersion } from 'tessa/files';
import { Result } from 'tessa/platform';
import { ScaleOption } from 'tessa/ui/files/interfaces';
export default class MyPdfViewer extends React.Component<MyPdfViewerProps, MyPdfViewerState> {
    timeout: any;
    isMouseClick: boolean;
    lastClientX: any;
    lastClientY: any;
    containerPreviewRef: any;
    cardNode: any;
    mainDiv: HTMLDivElement | null;
    constructor(props: MyPdfViewerProps);
    componentDidUpdate(prevProps: MyPdfViewerProps): void;
    componentDidMount(): void;
    componentWillUnmount(): void;
    onResize: () => void;
    onMouseDown: (e: any) => void;
    onMouseUp: () => void;
    onMouseMove: (e: any) => void;
    setWidth(forced?: boolean): void;
    getNewWidth(): any;
    getCardNode(): any;
    loadFile: (fileVersion: IFileVersion | null) => void;
    handleScaleChange: (e: any) => void;
    changeScale: (up: boolean) => void;
    onDocumentLoad: ({ numPages }: {
        numPages: any;
    }) => void;
    onDocumentError: (obj: any) => void;
    onPageLoad: ({ pageIndex, originalWidth }: {
        pageIndex: any;
        originalWidth: any;
    }) => void;
    changePage: (by: any) => void;
    getNextIndex: (index: number, total: number) => number;
    onInputPageBlur: (e: any) => void;
    onInputPageChange: (e: any) => void;
    onKeyDown: (event: any) => void;
    onWheel: (e: any) => void;
    setValue: (value: any) => void;
    setPageIndex: (pageIndex: any) => void;
    refHandler: (ref: any) => void;
    pageClickHandler: (page: number) => () => void;
    changeScaleHandler: (value: boolean) => () => void;
    handleRotateLeft: () => void;
    handleRotateRight: () => void;
    changeRotate: (value: number) => void;
    render(): JSX.Element;
}
export interface MyPdfViewerProps {
    previewFileVersion: IFileVersion | null;
    getFileContentFunc: (version: IFileVersion) => Promise<Result<File>>;
    l: (value: string) => string;
    tabMode: boolean;
    isPreviewTab: boolean;
    setRotateAngle: (fileVersionId: string, pageNumber: number, angle: number) => void;
    rotateAngles: {
        [key: number]: number;
    };
    pageNumber?: number;
    setPageNumber?: (value: number) => void;
    scale: ScaleOption;
    customScaleValue: number;
    setScale: (value: ScaleOption, customScaleValue: number) => void;
}
export interface MyPdfViewerState {
    pageIndex: number;
    pageNumber: number;
    total: number;
    width: number;
    file?: any;
    error?: string;
    isLoading?: any;
    originalWidth: number;
    scaleValue: ScaleOption;
    customScaleValue: number;
    inputPageValue: string;
    rotateAngle: number;
    rotateAngles: {
        [key: number]: number;
    };
}
