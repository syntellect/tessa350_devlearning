import React from 'react';
import { IFileVersion } from 'tessa/files';
import { Result } from 'tessa/platform';
export default class HtmlPreviewer extends React.Component<IHtmlPreviewerProps, {}> {
    iframeRef: HTMLIFrameElement | null;
    safeHtml: string | null;
    componentDidMount(): void;
    getFile(previewFileVersion: IFileVersion | null): void;
    refHandler: (ref: any) => any;
    /**
     * при переключении положения превью (лево/право) теряется контент - перерисовываем
     */
    componentDidUpdate(prevProps: IHtmlPreviewerProps): void;
    render(): JSX.Element;
}
export interface IHtmlPreviewerProps {
    previewFileVersion: IFileVersion | null;
    getFileContentFunc: (version: IFileVersion) => Promise<Result<File>>;
}
