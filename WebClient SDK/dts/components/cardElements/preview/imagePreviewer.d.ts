import React from 'react';
import { IFileVersion } from 'tessa/files';
import { Result } from 'tessa/platform';
export default class ImagePreviewer extends React.Component<IImagePreviewerProps, IImagePreviewerState> {
    state: {
        data: string;
    };
    componentDidMount(): void;
    getFile(previewFileVersion: IFileVersion | null): void;
    componentDidUpdate(prevProps: IImagePreviewerProps): void;
    render(): JSX.Element;
}
export interface IImagePreviewerProps {
    previewFileVersion: IFileVersion | null;
    getFileContentFunc: (version: IFileVersion) => Promise<Result<File>>;
}
export interface IImagePreviewerState {
    data?: string;
}
