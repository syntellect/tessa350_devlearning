import React from 'react';
import { IFileVersion } from 'tessa/files';
import { Result } from 'tessa/platform';
export default class TxtPreviewer extends React.Component<ITxtPreviewerProps, ITxtPreviewerState> {
    state: {
        text: string;
    };
    componentDidMount(): void;
    getFile(previewFileVersion: IFileVersion | null): void;
    componentDidUpdate(prevProps: ITxtPreviewerProps): void;
    render(): JSX.Element;
}
export interface ITxtPreviewerProps {
    previewFileVersion: IFileVersion | null;
    getFileContentFunc: (version: IFileVersion) => Promise<Result<File>>;
}
export interface ITxtPreviewerState {
    text?: string;
}
