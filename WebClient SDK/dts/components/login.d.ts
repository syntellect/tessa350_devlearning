import * as React from 'react';
import { Location } from 'history';
declare class Login extends React.Component<LoginProps, LoginState> {
    componentDidMount(): void;
    componentWillUnmount(): void;
    constructor(props: LoginProps);
    loginOnChange: (event: any) => void;
    passwordOnChange: (event: any) => void;
    handleAuth: (event: any) => void;
    handleWinAuth: (event: any) => void;
    handleSAMLAuth: (event: any) => void;
    render(): JSX.Element;
}
export interface LoginProps {
    location: Location<any>;
}
export interface LoginState {
    login?: string;
    password?: string;
    redirectTo?: string;
    autologin?: boolean;
}
export default Login;
