import React from 'react';
export interface LoadingOverlayProps {
    timeout?: number;
    skipDialogsPush?: boolean;
    progress?: number | null;
    progressDelay?: number;
    text?: string | null;
}
export interface LoadingOverlayState {
    isHidden: boolean;
    hideProgress: boolean;
}
export declare class LoadingOverlay extends React.Component<LoadingOverlayProps, LoadingOverlayState> {
    zIndex: number;
    timeoutId: any;
    progressDealyId: any;
    _overlayRef: any;
    constructor(props: LoadingOverlayProps);
    componentDidMount(): void;
    componentWillUnmount(): void;
    render(): JSX.Element;
    handleClick: (e: React.MouseEvent<Element, MouseEvent>) => void;
}
export declare class LoadingOverlayWithPortal extends React.Component<LoadingOverlayProps> {
    private el;
    constructor(props: LoadingOverlayProps);
    componentDidMount(): void;
    componentWillUnmount(): void;
    render(): React.ReactPortal;
}
export default LoadingOverlay;
