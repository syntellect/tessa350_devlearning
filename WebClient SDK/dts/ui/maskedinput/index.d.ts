import React from 'react';
interface MaskedInputProps extends React.HTMLProps<HTMLInputElement> {
    mask: string;
    formatCharacters?: object;
    placeholderChar?: string;
}
declare class MaskedInput extends React.Component<MaskedInputProps> {
    static defaultProps: {
        value: string;
    };
    private input;
    private mask;
    private hackedMaskUpdate;
    constructor(props: MaskedInputProps);
    _updatePattern(props: MaskedInputProps): void;
    _updateMaskSelection(): void;
    _updateInputSelection(): void;
    _onChange: (e: React.SyntheticEvent<HTMLInputElement, Event>) => void;
    _onKeyDown: (e: React.KeyboardEvent<HTMLInputElement>) => void;
    _onKeyPress: (e: React.KeyboardEvent<HTMLInputElement>) => void;
    _onPaste: (e: React.SyntheticEvent<HTMLInputElement, ClipboardEvent>) => void;
    _getDisplayValue(): any;
    _keyPressPropName(): "onBeforeInput" | "onKeyPress";
    _getEventHandlers(): {
        [x: string]: (e: React.SyntheticEvent<HTMLInputElement, Event>) => void;
        onChange: (e: React.SyntheticEvent<HTMLInputElement, Event>) => void;
        onKeyDown: (e: React.KeyboardEvent<HTMLInputElement>) => void;
        onPaste: (e: React.SyntheticEvent<HTMLInputElement, ClipboardEvent>) => void;
    };
    focus(): void;
    blur(): void;
    render(): JSX.Element;
}
export default MaskedInput;
