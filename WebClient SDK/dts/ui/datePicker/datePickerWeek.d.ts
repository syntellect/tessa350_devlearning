import * as React from 'react';
declare class DatePickerWeek extends React.Component<IDatePickerWeekProps, {}> {
    render(): JSX.Element;
}
export interface IDatePickerWeekProps {
    date: any;
    month: number;
    selectedDate?: object;
    onDaySelect: any;
}
export default DatePickerWeek;
