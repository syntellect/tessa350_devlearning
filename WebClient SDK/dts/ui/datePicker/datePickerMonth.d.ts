import * as React from 'react';
declare class DatePickerMonth extends React.Component<IDatePickerMonthProps, {}> {
    isWeekInMonth(startOfWeek: any): any;
    render(): JSX.Element;
}
export interface IDatePickerMonthProps {
    date: any;
    selectedDate?: object;
    onDaySelect: any;
}
export default DatePickerMonth;
