import * as React from 'react';
import Moment from 'moment';
import { MediaStyle } from '../mediaStyle';
declare class DatePicker extends React.Component<DatePickerProps, DatePickerState> {
    static defaultProps: {
        dateEnabled: boolean;
        timeEnabled: boolean;
    };
    constructor(props: DatePickerProps);
    shouldComponentUpdate(nextProps: DatePickerProps, nextState: DatePickerState): boolean;
    static getDerivedStateFromProps(props: DatePickerProps, state: DatePickerState): DatePickerState | null;
    static validateDate(date: Moment.Moment): Moment.Moment | null;
    getDateFormat(date: Moment.Moment): string | undefined;
    getDateFromText(value: any): Moment.Moment | null;
    updateDate(date: Moment.Moment | null): void;
    getInputFormatOptions(): "11.11.1111" | "11:11:11" | "11.11.1111 11:11:11" | undefined;
    focus(opt?: FocusOptions): void;
    handleCalendarButtonClick: (event: any) => void;
    handleClearButtonClick: (event: any) => void;
    handleCalendarDateSelect: (value: any) => void;
    handleInputFocus: (event: any) => void;
    handleInputBlur: (event: any) => void;
    handleInputValidate: (value: any) => any;
    handleKeyDown: (event: any) => void;
    renderCalendar(): JSX.Element | null;
    render(): JSX.Element;
}
export interface DatePickerProps {
    disabled?: boolean;
    className?: string;
    date?: any;
    dateEnabled?: boolean;
    timeEnabled?: boolean;
    onDateChange?: (date: Moment.Moment | null) => void;
    onChange?: any;
    onFocus?: any;
    onBlur?: any;
    onKeyDown?: any;
    hideClearButton?: boolean;
    id?: string;
    mediaStyle?: MediaStyle | null;
    style?: React.CSSProperties;
    title?: string;
    isInvalid?: boolean;
}
export interface DatePickerState {
    prevPropDate: any;
    isOpened: boolean;
    date: any;
}
export default DatePicker;
