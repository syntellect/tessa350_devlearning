import * as React from 'react';
import { GesturesInstance } from '../../gestures';
declare class DatePickerCalendar extends React.Component<IDatePickerCalendarProps, IDatePickerCalendarState> {
    swipeRightGesture: GesturesInstance;
    swipeLeftGesture: GesturesInstance;
    swipeLeft: any;
    swipeRight: any;
    static defaultProps: {
        onDaySelect: () => void;
    };
    constructor(props: any);
    componentDidMount(): void;
    componentWillUnmount(): void;
    increaseMonth: () => void;
    decreaseMonth: () => void;
    handlePrevButton: (event: any) => void;
    handleNextButton: (event: any) => void;
    handleSwipeRight: () => void;
    handleSwipeLeft: () => void;
    render(): JSX.Element;
}
export interface IDatePickerCalendarProps {
    selectedDate?: object;
    onDaySelect: any;
}
export interface IDatePickerCalendarState {
    date: any;
}
export default DatePickerCalendar;
