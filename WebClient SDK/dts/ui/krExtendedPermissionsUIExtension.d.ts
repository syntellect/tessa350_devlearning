import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
export declare class KrExtendedPermissionsUIExtension extends CardUIExtension {
    initialized(context: ICardUIExtensionContext): void;
    private modifyTask;
}
