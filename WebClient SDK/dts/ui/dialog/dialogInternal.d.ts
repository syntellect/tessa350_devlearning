import * as React from 'react';
declare class DialogInternal extends React.Component<DialogInternalProps> {
    layerIndex: number;
    overlayClick: boolean;
    static defaultProps: {
        isAutoSize: boolean;
        isOverlayHidden: boolean;
        closeByEsc: boolean;
        okByEnter: boolean;
        autofocus: boolean;
    };
    windowRef: React.RefObject<HTMLDivElement>;
    layoutRef: React.RefObject<HTMLDivElement>;
    constructor(props: DialogInternalProps);
    componentDidMount(): void;
    componentWillUnmount(): void;
    handleOverlayClick: (event: any) => void;
    handleKeyDown: (event: React.KeyboardEvent<Element>) => void;
    render(): JSX.Element;
}
export interface DialogInternalProps {
    children?: any;
    className?: string;
    style?: object;
    isAutoSize?: boolean;
    isOverlayHidden?: boolean;
    onCloseRequest?: any;
    onClick?: any;
    closeByEsc?: boolean;
    okByEnter?: boolean;
    onKeyDown?: any;
    autofocus?: boolean;
    [key: string]: any;
}
export default DialogInternal;
