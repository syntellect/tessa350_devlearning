import * as React from 'react';
declare class DialogContainer extends React.Component<IDialogContainerProps, {}> {
    render(): JSX.Element;
}
export interface IDialogContainerProps {
    children?: any;
    className?: string;
    [key: string]: any;
}
export default DialogContainer;
