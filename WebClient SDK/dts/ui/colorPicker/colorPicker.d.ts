import React from 'react';
import { InjectedColorProps } from 'react-color';
export interface ColorPickerProps extends InjectedColorProps {
    className?: string;
    style?: React.CSSProperties;
}
declare const _default: React.ComponentClass<ColorPickerProps & import("react-color/lib/components/common/ColorWrap").ExportedColorProps, any>;
export default _default;
