import * as React from 'react';
import { MediaStyle } from '../mediaStyle';
declare class Autocomplete extends React.Component<AutocompleteProps, AutocompleteState> {
    debounceItemsRequest: any;
    inputCaretPos: number;
    input: any;
    comboBoxButton: any;
    dialogInput: any;
    dialogAutocomplete: any;
    autocomplete: any;
    dotsButton: any;
    get isMobileMode(): boolean;
    static defaultProps: {
        items: never[];
        isAutocompleteMode: boolean;
        isComboBoxMode: boolean;
        isTable: boolean;
        alwaysShowInDialog: boolean;
        isSelectMode: boolean;
    };
    constructor(props: AutocompleteProps);
    shouldComponentUpdate(nextProps: AutocompleteProps, nextState: AutocompleteState): boolean;
    static getDerivedStateFromProps(props: AutocompleteProps, state: AutocompleteState): AutocompleteState | null;
    componentDidUpdate(): void;
    getCurrentItems(items: any): any;
    getDropdownItems(text?: null): void;
    addToCurrentItems(index?: number | null): void;
    deleteFromCurrentItems(index?: number | null): void;
    openDropdown(type: any): void;
    closeDropdown(): void;
    openDialog(): void;
    closeDialog(): void;
    getCaretPos(openDirection: any): number;
    getDropdownRootElement(): any;
    getContextRootElement(): any;
    getButton(isDialog: any, props: any): JSX.Element;
    getNextFocusedDropdownItemIndex(keyCode: any, currentIndex: any, itemsLength: any): number;
    getCurrentInput(): any;
    getCurrentContainer(): any;
    focus(opt?: FocusOptions): void;
    blur(): void;
    select(): void;
    checkUniqueItem(text: any, items: any): boolean;
    getSelectableItemsIndex(): number[];
    isNotReference(currentItems: any, index: any): boolean;
    setCurrentRowId(index: any, item: any): void;
    isContextMenuOpen(isDeleteAllowed: any, isReferenceAllowed: any): boolean;
    /**
     * Обработчик нажатия клавиш.
     * @param {object} event Событие нажатия клавиши.
     */
    handleKeyDown: (event: React.KeyboardEvent<Element>) => void;
    handleClick: (event: any) => void;
    /**
     * Обработчик нажатия кнопки combox.
     * @param {object} event Событие нажатия кнопки.
     */
    handleComboBoxButtonClick: () => void;
    handleDotsButtonClick: () => void;
    /**
     * Обработчик ввода.
     * @param {string} text Введенный текст.
     */
    handleInputChange: (text: any) => void;
    /**
     * Обработчик получения фокуса.
     */
    handleInputFocus: () => void;
    /**
     * Обработчик потери фокуса.
     */
    handleInputBlur: () => void;
    /**
     * Обработчик выбора объекта из выпадающей подсказки.
     * @param {object} index Индекс выбранного объекта.
     */
    handleDropdownItemSelect: (index: any) => void;
    /**
     * Обработчик получения фокуса объекта из выпадающей подсказки.
     * @param {object} index Индекс выбранного объекта.
     */
    handleDropdownItemFocus: (index: any) => void;
    /**
     * Обработчик выбора объекта из текущей коллекции.
     * @param {object} index Индекс выбранного объекта.
     */
    handleCurrentItemSelect: (index: any, item: any) => void;
    /**
     * Обработчик клика за границей выпадющих элементов.
     */
    handleOutsideDropdownClick: () => void;
    handleOutsideContextClick: () => void;
    render(): JSX.Element;
    renderDialog(): JSX.Element | null;
    renderButtons(isDialog?: boolean): JSX.Element | null;
}
export interface AutocompleteDropDownItemsCollection {
    additionalLabelText: string | null;
    items: {
        text: string | null | undefined;
        fields: any[];
    }[];
}
export interface AutocompleteProps {
    items?: {
        text: string;
    }[];
    dropdownItems: AutocompleteDropDownItemsCollection;
    disabled?: boolean;
    className?: string;
    dialogProps?: {
        other: any;
        className: string;
    };
    isAutocompleteMode?: boolean;
    isComboBoxMode?: boolean;
    isTable?: boolean;
    onAddItem: any;
    onDeleteItem: any;
    onDropdownItemsRequest: any;
    onTextChange?: any;
    onReferenceItem?: any;
    alwaysShowInDialog?: boolean;
    onDotsModeToggle?: any;
    hideSelectorButton?: boolean;
    onItemSelect?: any;
    isSelectMode?: boolean;
    isLineBreak?: boolean;
    isManualInput?: boolean;
    comboIsLoading?: boolean;
    onKeyDown?: any;
    onFocus?: any;
    onBlur?: any;
    mediaStyle?: MediaStyle | null;
    style?: React.CSSProperties;
    title?: string;
}
export interface AutocompleteState {
    mode: string | null;
    text?: string | null;
    currentItems?: {
        text: string;
    }[];
    isDropdownOpened?: string;
    isDialogOpened: boolean;
    focusedDropdownItemIndex: number;
    selectedCurrentItemIndex: number;
    selectedCurrentItemIndexForSelectMode: number;
    prevPropItems: {
        text: string;
    }[] | null;
}
export default Autocomplete;
