import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext, StageTypeHandlerDescriptor } from 'tessa/workflow/krProcess';
export declare class ApprovalUIHandler extends KrStageTypeUIHandler {
    private _settings;
    private _returnIfNotApprovedFlagControl;
    private _returnAfterApprovalFlagControl;
    descriptors(): StageTypeHandlerDescriptor[];
    initialize(context: IKrStageTypeUIHandlerContext): void;
    finalize(): void;
    private onSettingsFieldChanged;
    private getKind;
    private advisoryConfigureFields;
    private notReturnEditConfigureFields;
}
