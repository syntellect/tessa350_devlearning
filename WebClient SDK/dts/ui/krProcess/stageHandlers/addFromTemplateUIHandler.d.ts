import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext, StageTypeHandlerDescriptor } from 'tessa/workflow/krProcess';
export declare class AddFromTemplateUIHandler extends KrStageTypeUIHandler {
    private _templateId;
    descriptors(): StageTypeHandlerDescriptor[];
    validate(context: IKrStageTypeUIHandlerContext): void;
}
