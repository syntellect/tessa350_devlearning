import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext, StageTypeHandlerDescriptor } from 'tessa/workflow/krProcess';
export declare class DialogUIHandler extends KrStageTypeUIHandler {
    private _cardStoreModeId;
    private _openModeId;
    private _dialogTypeId;
    private _templateId;
    descriptors(): StageTypeHandlerDescriptor[];
    validate(context: IKrStageTypeUIHandlerContext): void;
    initialize(context: IKrStageTypeUIHandlerContext): void;
    finalize(context: IKrStageTypeUIHandlerContext): void;
}
