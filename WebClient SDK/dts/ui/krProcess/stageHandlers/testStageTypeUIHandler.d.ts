import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext } from 'tessa/workflow/krProcess';
export declare class TestStageTypeUIHandler extends KrStageTypeUIHandler {
    initialize(context: IKrStageTypeUIHandlerContext): void;
}
