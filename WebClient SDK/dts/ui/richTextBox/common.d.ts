import { ContentState, EditorState } from 'draft-js';
export interface RichBoxStyleGroup {
    fontSize: RichBoxStyle;
    color: RichBoxStyle;
    backgroundColor: RichBoxStyle;
}
export interface RichBoxStyle {
    [key: string]: any;
}
export declare enum RichTextBoxAttachmentType {
    File = "File",
    Link = "Link"
}
export declare type RichTextBoxAttachmentFile = {
    id: guid;
    type: RichTextBoxAttachmentType.File;
    caption: string;
    data: File;
};
export declare type LinkInfo = {
    caption: string;
    link: string;
};
export declare type RichTextBoxAttachmentLink = {
    id: guid;
    type: RichTextBoxAttachmentType.Link;
} & LinkInfo;
export declare type RichTextBoxAttachment = RichTextBoxAttachmentFile | RichTextBoxAttachmentLink;
export declare const getEditorStateFromHTML: (html?: string | null | undefined) => ContentState;
export declare const getHTMLFromEditorState: (editorState: EditorState) => string;
