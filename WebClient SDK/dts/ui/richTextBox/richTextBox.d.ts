import React from 'react';
import { Editor, EditorProps, EditorState } from 'draft-js';
import { RichTextBoxAttachment, LinkInfo } from './common';
export interface RichTextBoxProps extends Omit<EditorProps, 'editorState' | 'onChange'> {
    html?: string;
    autoFocus?: boolean;
    showFocus?: boolean;
    hasReadOnlyMode?: boolean;
    readOnlyMode?: boolean;
    dropDownDirectionUp?: boolean;
    onChange?(html: string): void;
    onChangeReadOnlyMode?(readOnlyMode: boolean): void;
    onChangeState?(state: EditorState): EditorState;
    hasAttachments?: boolean;
    attachments?: ReadonlyArray<RichTextBoxAttachment>;
    showFileDialog?(): Promise<readonly File[] | File | null>;
    showLinkDialog?(): Promise<LinkInfo | null>;
    onAddAttachments?(attachments: RichTextBoxAttachment[]): void;
    onRemoveAttachments?(attachment: RichTextBoxAttachment): void;
}
export declare type RichTextBoxRef = Editor;
export declare const RichTextBox: React.ForwardRefExoticComponent<RichTextBoxProps & React.RefAttributes<Editor>>;
export default RichTextBox;
