import { FunctionComponent } from 'react';
import { RichTextBoxAttachment, LinkInfo } from './common';
export interface RichTextBoxAttachmentsToolbarProps {
    showFileDialog(): Promise<readonly File[] | File | null>;
    showLinkDialog(): Promise<LinkInfo | null>;
    onAddAttachments(attachments: RichTextBoxAttachment[]): void;
    title?: string;
}
export declare const RichTextBoxAttachmentsToolbar: FunctionComponent<RichTextBoxAttachmentsToolbarProps>;
