import { FC } from 'react';
import { RichTextBoxAttachment } from './common';
export interface RichTextBoxAttachmentsFooterProps {
    attachments: ReadonlyArray<RichTextBoxAttachment>;
    onRemoveAttachments(attachment: RichTextBoxAttachment): void;
}
export declare const RichTextBoxAttachmentsFooter: FC<RichTextBoxAttachmentsFooterProps>;
