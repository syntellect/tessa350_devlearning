import { FC } from 'react';
import { EditorState, DraftInlineStyle } from 'draft-js';
import { RichBoxStyleGroup, RichTextBoxAttachment, LinkInfo } from './common';
export interface RichTextBoxToolbarProps {
    editorState: EditorState;
    setEditorState(state: EditorState): any;
    inlineStyle: DraftInlineStyle;
    styles: RichBoxStyleGroup;
    blockType: string;
    readOnly?: boolean;
    readOnlyMode?: boolean;
    hasReadOnlyMode?: boolean;
    onChangeReadOnlyMode(): any;
    dropDownDirectionUp?: boolean;
    hasReadonlyMode?: boolean;
    showFileDialog?(): Promise<readonly File[] | File | null>;
    showLinkDialog?(): Promise<LinkInfo | null>;
    onAddAttachments?(attachments: RichTextBoxAttachment[]): void;
}
export declare const RichTextBoxToolbar: FC<RichTextBoxToolbarProps>;
