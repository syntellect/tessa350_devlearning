import { FunctionComponent } from 'react';
import { EditorState } from 'draft-js';
import { RichBoxStyle } from './common';
export interface RichTextBoxColorPickerToolbarProps {
    editorState: EditorState;
    setEditorState: (editorState: EditorState) => void;
    styles: RichBoxStyle;
    icon: string;
    defaultColor: string;
    dropDownDirectionUp: boolean;
    title?: string;
}
export declare const RichTextBoxColorPickerToolbar: FunctionComponent<RichTextBoxColorPickerToolbarProps>;
