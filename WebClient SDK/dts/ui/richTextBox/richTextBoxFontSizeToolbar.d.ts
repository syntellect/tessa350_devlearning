import { FunctionComponent } from 'react';
import { EditorState } from 'draft-js';
import { RichBoxStyle } from './common';
export interface RichTextBoxFontSizeToolbarProps {
    editorState: EditorState;
    setEditorState: (editorState: EditorState) => void;
    styles: RichBoxStyle;
    dropDownDirectionUp: boolean;
    title?: string;
}
export declare const RichTextBoxFontSizeToolbar: FunctionComponent<RichTextBoxFontSizeToolbarProps>;
