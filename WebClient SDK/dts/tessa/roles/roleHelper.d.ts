export declare class RoleHelper {
    static readonly staticRoleTypeId: guid;
    static readonly departmentRoleTypeId: guid;
    static readonly personalRoleTypeId: guid;
    static readonly dynamicRoleTypeId: guid;
    static readonly contextRoleTypeId: guid;
    static readonly metaRoleTypeId: guid;
    static readonly taskRoleTypeId: guid;
    static readonly generatorTypeId: guid;
    static readonly personalRoleSatelliteTypeId: guid;
    static readonly roleDeputiesManagementTypeId: guid;
    static readonly staticRoleTypeName: string;
    static readonly departmentRoleTypeName: string;
    static readonly personalRoleTypeName: string;
    static readonly dynamicRoleTypeName: string;
    static readonly contextRoleTypeName: string;
    static readonly metaRoleTypeName: string;
    static readonly taskRoleTypeName: string;
    static readonly generatorTypeName: string;
    static readonly personalRoleSatelliteTypeName: string;
    static readonly roleDeputiesManagementTypeName: string;
}
