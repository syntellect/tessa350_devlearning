import { CardNewResponse } from './cardNewResponse';
import { CardMetadataSealed } from 'tessa/cards/metadata';
import { CardRow } from 'tessa/cards/cardRow';
import { MapStorage } from 'tessa/platform/storage';
export declare class CardNewStrategy {
    private constructor();
    private static _instance;
    static get instance(): CardNewStrategy;
    createResponse(cardTypeId: guid, cardMetadata: CardMetadataSealed): CardNewResponse;
    private newEntry;
    private newTable;
    createSectionRows(cardTypeId: guid, cardMetadata: CardMetadataSealed): MapStorage<CardRow>;
    setSessionInfo(newResponse: CardNewResponse, userId: guid, userName: string): void;
}
