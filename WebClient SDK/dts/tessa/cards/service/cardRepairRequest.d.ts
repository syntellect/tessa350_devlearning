import { CardInfoStorageObject } from 'tessa/cards/cardInfoStorageObject';
import { IStorage } from 'tessa/platform/storage';
import { Card } from 'tessa/cards/card';
import { CardNewMode } from 'tessa/cards/cardNewMode';
export declare class CardRepairRequest extends CardInfoStorageObject {
    constructor(storage?: IStorage);
    static readonly newModeKey: string;
    static readonly notifyFieldsUpdatedKey: string;
    static readonly cardKey: string;
    static readonly cardBSONKey: string;
    static readonly convertFromBSONBeforeRequestKey: string;
    static readonly convertFromBSONAfterRequestKey: string;
    get newMode(): CardNewMode;
    set newMode(value: CardNewMode);
    get notifyFieldsUpdated(): boolean;
    set notifyFieldsUpdated(value: boolean);
    get card(): Card;
    set card(value: Card);
    get cardBSON(): string;
    set cardBSON(value: string);
    get convertFromBSONBeforeRequest(): boolean;
    set convertFromBSONBeforeRequest(value: boolean);
    get convertFromBSONAfterRequest(): boolean;
    set convertFromBSONAfterRequest(value: boolean);
    tryGetCard(): Card | null | undefined;
}
