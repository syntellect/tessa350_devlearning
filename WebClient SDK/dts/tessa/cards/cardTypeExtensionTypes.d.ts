import { CardTypeExtensionType } from './cardTypeExtensionType';
export declare class CardTypeExtensionTypes {
    static sortRows: CardTypeExtensionType;
    static cleanupFields: CardTypeExtensionType;
    static makeReadonly: CardTypeExtensionType;
    static hideTabs: CardTypeExtensionType;
}
