import { NumberContext } from './numberContext';
import { IUIContext } from 'tessa/ui';
export declare class NumberDirector {
    constructor(executeInContext: (action: (context: IUIContext) => void) => void);
    private readonly _executeInContext;
    reserveNumber(context: NumberContext): Promise<boolean>;
    releaseNumber(context: NumberContext): Promise<boolean>;
    private getCardRequest;
    private processControlRequest;
}
