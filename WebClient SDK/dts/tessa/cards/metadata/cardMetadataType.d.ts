import { CardMetadataRuntimeType } from './cardMetadataRuntimeType';
import { CardSerializableObject } from 'tessa/cards/cardSerializableObject';
import { DbType } from 'tessa/platform/dbType';
import { DotNetType } from 'tessa/platform/dotNetType';
export interface CardMetadataTypeSealed {
    readonly dataType: DbType | null;
    readonly type: CardMetadataRuntimeType;
    readonly dotNetType: DotNetType;
    seal<T = CardMetadataTypeSealed>(): T;
}
/**
 * Тип, определяющий представление данных в карточке.
 */
export declare class CardMetadataType extends CardSerializableObject {
    constructor();
    dataType: DbType | null;
    get type(): CardMetadataRuntimeType;
    get dotNetType(): DotNetType;
    seal<T = CardMetadataTypeSealed>(): T;
}
