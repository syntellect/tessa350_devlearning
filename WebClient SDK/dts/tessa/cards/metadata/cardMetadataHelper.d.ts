import { CardMetadataRuntimeType } from './cardMetadataRuntimeType';
import { DbType } from 'tessa/platform/dbType';
import { DotNetType } from 'tessa/platform/dotNetType';
export declare function getMetadataRuntimeTypeFromDbType(dbType: DbType): CardMetadataRuntimeType;
export declare function getDotNetTypeFromRuntimeType(runtimeType: CardMetadataRuntimeType): DotNetType;
export declare function getDotNetTypeFromFromDbType(dbType: DbType): DotNetType;
export declare function getDefaultValueForRuntimeType(type: DotNetType): any;
