import { FileCategory } from './fileCategory';
import { FileType } from './fileType';
import { FilePermissions } from './filePermissions';
import { IStorage } from 'tessa/platform/storage';
import { IFile } from 'tessa/files';
export interface IFileCreationToken {
    id: guid | null;
    name: string;
    category: FileCategory | null;
    type: FileType;
    isLocal: boolean;
    permissions: FilePermissions;
    options: IStorage;
    requestInfo: IStorage;
    size: number;
    set(file: IFile): any;
}
export declare class FileCreationToken implements IFileCreationToken {
    constructor();
    id: guid | null;
    name: string;
    category: FileCategory | null;
    type: FileType;
    isLocal: boolean;
    permissions: FilePermissions;
    options: IStorage;
    requestInfo: IStorage;
    size: number;
    set(file: IFile): void;
}
