import { IFile } from './file';
import { IFileVersion } from './fileVersion';
import { ValidationResult } from 'tessa/platform/validation';
import { Card } from 'tessa/cards';
export interface FileRelation {
    hasData: boolean;
    fileIdIndexRelation: {
        [key: string]: number;
    };
    fileIdVersionIdRelation: {
        [key: string]: string;
    };
    files: ReadonlyArray<File>;
}
export declare function createFileRelation(card: Card, files: ReadonlyArray<IFile>): FileRelation;
export declare function checkCanDownloadFilesAndShowMessages(operationFiles: IFile[] | IFileVersion[]): Promise<boolean>;
export declare function checkCanDownloadFile(file: IFile | IFileVersion): ValidationResult;
/**
 * В IE и мобильном сафари в FileAPI не доступа к конструктору File.
 * Используем этот грязный хак.
 */
export declare function createFakeFile(blob: Blob, name?: string, lastModifiedDate?: Date): File;
export declare function readFileContentAsBase64(data: File): Promise<string | null>;
