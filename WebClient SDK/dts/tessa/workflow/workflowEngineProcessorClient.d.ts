import { IWorkflowEngineProcessResult, WorkflowEngineProcessResult } from './workflowEngineProcessResult';
import { IWorkflowEngineProcessRequest } from './workflowEngineProcessStorageRequest';
import { IWorkflowEngineSignal } from './signals';
import { IStorage } from 'tessa/platform/storage';
export interface IWorkflowEngineProcessorClient {
    processSignalAsync: (request: IWorkflowEngineProcessRequest, requestSignature?: string | null, additionalInfo?: IStorage | null) => Promise<IWorkflowEngineProcessResult>;
    sendAsyncSignalAsync: (signal: IWorkflowEngineSignal, nodeInstanceId: guid, processInstanceId: guid, lockProcess: boolean, processDigest?: string | null) => Promise<void>;
    sendSignalAsync: (processInstanceId: guid, signal: IWorkflowEngineSignal, nodeId?: guid | null, nodeInstanceId?: guid | null) => Promise<IWorkflowEngineProcessResult>;
}
export declare class WorkflowEngineProcessorClient implements IWorkflowEngineProcessorClient {
    processSignalAsync(request: IWorkflowEngineProcessRequest, requestSignature?: string | null, additionalInfo?: IStorage | null): Promise<IWorkflowEngineProcessResult>;
    sendAsyncSignalAsync(signal: IWorkflowEngineSignal, nodeInstanceId: guid, processInstanceId: guid, lockProcess: boolean, processDigest?: string | null): Promise<void>;
    sendSignalAsync(processInstanceId: guid, signal: IWorkflowEngineSignal, nodeId?: guid | null, nodeInstanceId?: guid | null): Promise<WorkflowEngineProcessResult>;
}
