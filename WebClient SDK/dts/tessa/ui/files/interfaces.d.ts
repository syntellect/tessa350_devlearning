import { IExtension } from 'tessa/extensions';
import { MenuAction } from 'tessa/ui/menuAction';
import { FileContainer, IFileVersion, IFile } from 'tessa/files';
import { FileViewModel, FileSortingDirection, FileListCategoryFilter, FileListTypeFilter, FileControlCancelEventArgs, FileControlEventArgs, FileListGroupSorting, FileListVersionsDialogViewModel, ReadonlyFileGroupViewModel } from 'tessa/ui/cards/controls';
import { FileGrouping } from 'tessa/ui/cards/controls/fileList/grouping';
import { FileSorting } from 'tessa/ui/cards/controls/fileList/sorting';
import { FileFiltering } from 'tessa/ui/cards/controls/fileList/filtering';
import { IControlState, ICardModel, IControlViewModel } from 'tessa/ui/cards';
import { EventHandler, Result } from 'tessa/platform';
import { IStorage } from 'tessa/platform/storage';
export interface IFileControl extends IControlViewModel {
    readonly model: ICardModel;
    fileContainer: FileContainer;
    readonly files: ReadonlyArray<FileViewModel>;
    readonly filteredFiles: ReadonlyArray<FileViewModel>;
    readonly groupings: ReadonlyArray<FileGrouping>;
    readonly sortings: ReadonlyArray<FileSorting>;
    readonly actions: ReadonlyArray<MenuAction>;
    readonly fileActions: ReadonlyArray<MenuAction>;
    readonly versionActions: ReadonlyArray<MenuAction>;
    availableGroupings: ReadonlyArray<FileGrouping>;
    availableSortings: ReadonlyArray<FileSorting>;
    availableActions: ReadonlyArray<MenuAction>;
    availableFileActions: ReadonlyArray<MenuAction>;
    availableVersionActions: ReadonlyArray<MenuAction>;
    readonly groups: ReadonlyMap<string, ReadonlyFileGroupViewModel> | null;
    readonly isFileExists: boolean;
    selectedSorting: FileSorting | null;
    selectedSortDirection: FileSortingDirection;
    selectedGrouping: FileGrouping | null;
    groupsExpanded: boolean;
    selectedFiltering: FileFiltering | null;
    isCategoriesEnabled: boolean;
    isManualCategoriesCreationDisabled: boolean;
    isNullCategoryCreationDisabled: boolean;
    isPreservingCategoriesOrder: boolean;
    isIgnoreExistingCategories: boolean;
    categoryFilter: FileListCategoryFilter | null;
    typeFilter: FileListTypeFilter | null;
    groupSorting: FileListGroupSorting | null;
    readonly cryptoProPluginEnabled: boolean;
    getState(): IControlState;
    setState(state: IControlState): boolean;
    handleDropFiles(contents: ReadonlyArray<File>): Promise<void>;
    readonly containerFileAdding: EventHandler<(args: FileControlCancelEventArgs) => void>;
    readonly containerFileAdded: EventHandler<(args: FileControlEventArgs) => void>;
    readonly containerFileRemoving: EventHandler<(args: FileControlCancelEventArgs) => void>;
    readonly containerFileRemoved: EventHandler<(args: FileControlEventArgs) => void>;
    manager: IFileControlManager | null;
    info: IStorage;
    addFile(file: IFile): FileViewModel;
    removeFile(file: IFile | FileViewModel): any;
}
export interface IFileControlExtension extends IExtension {
    initializing(context: IFileControlExtensionContext): any;
    openingMenu(context: IFileControlExtensionContext): any;
}
export interface IFileExtension extends IExtension {
    openingMenu(context: IFileExtensionContext): any;
}
export interface IFileVersionExtension extends IExtension {
    openingMenu(context: IFileVersionExtensionContext): any;
}
export interface IFileExtensionContextBase {
    readonly control: IFileControl;
    readonly actions: MenuAction[];
    readonly info: IStorage;
}
export interface IFileControlExtensionContext extends IFileExtensionContextBase {
    readonly groupings: FileGrouping[];
    readonly sortings: FileSorting[];
}
export interface IFileExtensionContext extends IFileExtensionContextBase {
    readonly file: FileViewModel;
    readonly files: ReadonlyArray<FileViewModel>;
}
export interface IFileVersionExtensionContext extends IFileExtensionContextBase {
    readonly file: FileViewModel;
    readonly version: IFileVersion;
    readonly versions: ReadonlyArray<IFileVersion>;
    readonly dialog: FileListVersionsDialogViewModel;
}
/**
 * Объект, управляющий контролами <see cref="IFileControl"/>.
 */
export interface IFileControlManager {
    showPreview: (file: IFile) => void;
    fileVersion: IFileVersion | null;
    fileName: string | null;
    fileExtension: string | null;
    previewPdfEnabled: boolean;
    handleFileContentLoading: (version: IFileVersion) => Promise<Result<File>>;
    pageNumber: number;
    pageChanged: EventHandler<(args: {
        fileVersionId: string;
        page: number;
    }) => void>;
    setPageNumber: (value: number) => void;
    reset: () => void;
    filesAngles: {
        [key: string]: {
            [key: string]: number;
        };
    };
    fileAngles: {
        [key: string]: number;
    };
    setRotateAngle: (fileVersionId: string, page: number, angle: number) => void;
    rotateChanged: EventHandler<(args: {
        fileVersionId: string;
        page: number;
        angle: number;
    }) => void>;
    scale: ScaleOption;
    customScaleValue: number;
    setScale: (value: ScaleOption, customScaleValue: number) => void;
    scaleChanged: EventHandler<(args: {
        fileVersionId: string;
        scale: ScaleOption;
        customScaleValue: number;
    }) => void>;
}
/**
 * Элемент управления для области предпросмотра.
 */
export interface IFilePreviewControl {
    attach: (fileControl: IFileControl) => void;
}
export declare enum ScaleOption {
    auto = 0,
    actual = 1,
    custom = 2,
    _50 = 50,
    _100 = 100,
    _200 = 200,
    _400 = 400,
    _800 = 800
}
