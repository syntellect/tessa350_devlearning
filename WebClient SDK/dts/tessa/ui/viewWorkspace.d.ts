import { WorkspaceModel } from './workspaceModel';
import { IWorkplaceViewModel } from 'tessa/ui/views';
export declare class ViewWorkspace extends WorkspaceModel {
    constructor(workplace: IWorkplaceViewModel);
    readonly workplace: IWorkplaceViewModel;
    get isCloseable(): boolean;
    get workspaceName(): string;
    get workspaceInfo(): string;
    activate(): void;
    deactivate(): void;
    close(_force?: boolean): Promise<boolean>;
    getRoute(): string;
}
