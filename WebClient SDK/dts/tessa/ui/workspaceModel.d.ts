import { ITileWorkspace } from 'tessa/ui/tiles';
import { IExtensionExecutor } from 'tessa/extensions';
import { EventHandler } from 'tessa/platform';
export interface WorkspaceModelClosedArgs {
    workspace: WorkspaceModel;
}
export declare class WorkspaceModel {
    constructor(id: guid);
    protected _tileWorkspsace: ITileWorkspace;
    protected _tileExecutor: IExtensionExecutor | null;
    readonly id: guid;
    readonly uiId: guid;
    get tileWorkspsace(): ITileWorkspace;
    get tileExecutor(): IExtensionExecutor;
    get isCloseable(): boolean;
    get workspaceName(): string;
    get workspaceInfo(): string;
    activate(): void;
    deactivate(): void;
    close(_force?: boolean): Promise<boolean>;
    getRoute(): string;
    readonly onClosed: EventHandler<(args: WorkspaceModelClosedArgs) => void>;
}
