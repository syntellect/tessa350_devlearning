import { WorkspaceModel } from './workspaceModel';
import { ICardEditorModel } from 'tessa/ui/cards/interfaces';
export declare class CardWorkspace extends WorkspaceModel {
    constructor(id: guid, editor: ICardEditorModel);
    readonly editor: ICardEditorModel;
    get isCloseable(): boolean;
    get workspaceName(): string;
    get workspaceInfo(): string;
    getRoute(): string;
    close(force?: boolean): Promise<boolean>;
    private onModelSet;
    private onCardEditorModelClosed;
}
