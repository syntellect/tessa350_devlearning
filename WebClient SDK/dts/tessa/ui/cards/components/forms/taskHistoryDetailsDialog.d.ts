import * as React from 'react';
import { TaskHistoryItemViewModel } from '../../tasks/taskHistoryItemViewModel';
export interface TaskHistoryDetailsDialogProps {
    viewModel: TaskHistoryItemViewModel;
    onClose: () => void;
}
export declare class TaskHistoryDetailsDialog extends React.Component<TaskHistoryDetailsDialogProps> {
    render(): JSX.Element;
    private renderDetails;
    private localize;
    private handleClose;
    private handleMiddleClick;
}
