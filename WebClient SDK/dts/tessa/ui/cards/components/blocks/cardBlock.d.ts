import * as React from 'react';
import { IBlockViewModel } from 'tessa/ui/cards/interfaces';
export interface CardBlockProps {
    viewModel: IBlockViewModel;
}
export interface CardBlockState {
    prevViewModel: IBlockViewModel | null;
    blockComponent: Function | null;
}
export declare class CardBlock extends React.Component<CardBlockProps, CardBlockState> {
    constructor(props: CardBlockProps);
    static getDerivedStateFromProps(props: CardBlockProps, state: CardBlockState): CardBlockState | null;
    render(): JSX.Element | null;
}
