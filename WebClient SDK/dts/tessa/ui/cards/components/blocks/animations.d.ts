export declare function collapseSection(element: HTMLDivElement, callback?: Function): void;
export declare function expandSection(element: HTMLDivElement, callback?: Function): void;
