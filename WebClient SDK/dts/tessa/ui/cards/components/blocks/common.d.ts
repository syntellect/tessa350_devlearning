/// <reference types="react" />
import { IBlockViewModel } from 'tessa/ui/cards/interfaces';
export declare const handleCollapsed: (element: HTMLDivElement | null, oldCollapsed: boolean | null, newCollapsed: boolean, callback?: Function | undefined) => void;
export declare const captionClickHandler: (viewModel: IBlockViewModel, controlsRef: import("react").RefObject<HTMLDivElement>) => () => void;
export declare const AnimatedComponent: (component: any) => import("styled-components").StyledComponent<any, any, object, string | number | symbol>;
export declare const getCollapsedAttribute: (element: HTMLDivElement | null) => boolean | null;
