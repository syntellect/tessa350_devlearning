import { Visibility } from 'tessa/platform/visibility';
export declare const StyledCaption: import("styled-components").StyledComponent<"div", any, IBlockCaption, never>;
declare type IBlockCaption = {
    visibility: Visibility;
};
export {};
