import * as React from 'react';
import { IControlViewModel } from 'tessa/ui/cards/interfaces';
export interface CardControlProps {
    viewModel: IControlViewModel;
}
export interface CardControlState {
    prevViewModel: IControlViewModel | null;
    controlComponent: Function | null;
}
export declare class CardControl extends React.Component<CardControlProps, CardControlState> {
    constructor(props: CardControlProps);
    static getDerivedStateFromProps(props: CardControlProps, state: CardControlState): CardControlState | null;
    render(): JSX.Element | null;
}
