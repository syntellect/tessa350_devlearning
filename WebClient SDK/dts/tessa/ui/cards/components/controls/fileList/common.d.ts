import { SignatureType, SignatureProfile } from 'tessa/cards';
import { FileSignatureState } from 'tessa/files';
export declare const getTypeProfileText: (type: SignatureType, profile: SignatureProfile) => string;
export declare const getSignatureTypeText: (type: SignatureType) => "$Enum_SignatureTypes_None" | "$Enum_SignatureTypes_CAdES" | "Unknown";
export declare const getSignatureProfileText: (profile: SignatureProfile) => "$Enum_Signature_Profiles_T" | "$Enum_Signature_Profiles_X_Type1" | "$Enum_Signature_Profiles_X_Type2" | "$Enum_Signature_Profiles_A" | "Unknown" | "$Enum_Signature_Profiles_None" | "$Enum_Signature_Profiles_BES" | "$Enum_Signature_Profiles_C" | "$Enum_Signature_Profiles_EPES" | "$Enum_Signature_Profiles_XL" | "$Enum_Signature_Profiles_XL_Type1" | "$Enum_Signature_Profiles_XL_Type2";
export declare const getSignatureIntegrityText: (state: FileSignatureState) => "$Enum_Signature_Integrity_NotChecked" | "$Enum_Signature_Integrity_Checked" | "$Enum_Signature_Integrity_Failed" | "$Enum_Signature_Integrity_CheckedWithWarning";
