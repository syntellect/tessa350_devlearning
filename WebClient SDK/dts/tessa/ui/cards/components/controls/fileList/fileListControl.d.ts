import * as React from 'react';
import { ControlProps } from '../controlProps';
import { IControlViewModel } from 'tessa/ui/cards/interfaces';
export declare class FileListControl extends React.Component<ControlProps<IControlViewModel>> {
    render(): JSX.Element | null;
    private renderFilterPlank;
    private renderStubCaption;
    private renderItems;
    private renderFiles;
    private renderGroups;
    private handleDrop;
    private handleGroupClick;
    private handleEmptyClick;
    private handleOnContextMenu;
}
