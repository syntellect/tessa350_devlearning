import * as React from 'react';
export declare class GridControlPaging extends React.Component<IGridControlPaging, {}> {
    _currentPage: number | undefined;
    moveBackward: () => void;
    moveForward: () => void;
    fakeInputRef: HTMLSpanElement | null;
    inputRef: HTMLInputElement | null;
    constructor(props: IGridControlPaging, context: any);
    componentDidUpdate(prevProps: IGridControlPaging): void;
    setCurrentPage: (page: number | undefined) => void;
    fakeInputRefHandler: (ref: any) => any;
    inputRefHandler: (ref: any) => any;
    onInputPageBlur: (e: any) => void;
    onInputPageChange: (e: any) => void;
    onKeyDown: (event: any) => void;
    setValue: (value: any) => void;
    isValidValue: (value: any) => boolean;
    render(): JSX.Element | null;
}
export interface IGridControlPaging {
    movePage: (isForward: boolean) => void;
    setPage: (page: number | undefined) => void;
    isEnabled: boolean;
    pages: number;
    currentPage: number;
}
