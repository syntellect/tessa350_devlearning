import * as React from 'react';
import { GridViewModel } from 'tessa/ui/cards/controls/grid';
export interface GridControlButtonsProps {
    viewModel: GridViewModel;
}
export declare class GridControlButtons extends React.Component<GridControlButtonsProps> {
    render(): JSX.Element;
}
