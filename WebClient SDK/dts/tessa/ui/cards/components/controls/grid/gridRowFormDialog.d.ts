import * as React from 'react';
import { GridViewModel } from 'tessa/ui/cards/controls/grid';
export interface GridRowFormDialogProps {
    viewModel: GridViewModel;
}
export declare class GridRowFormDialog extends React.Component<GridRowFormDialogProps> {
    render(): JSX.Element | null;
    private handleCloseForm;
    private handleCloseFormWithSave;
    private handleCloseByContext;
    private handleCloseFormWithNoValidation;
}
