import * as React from 'react';
import { UIButton } from 'tessa/ui/uiButton';
import { ForumPager } from 'tessa/ui/cards/controls';
export interface ForumHeaderProps {
    title: string;
    leftButtons: UIButton[];
    rightButtons: UIButton[];
    pager?: ForumPager;
}
export declare class ForumHeader extends React.Component<ForumHeaderProps> {
    render(): JSX.Element;
    private renderButtons;
    private renderPager;
    private handleCurrentPageChanged;
}
