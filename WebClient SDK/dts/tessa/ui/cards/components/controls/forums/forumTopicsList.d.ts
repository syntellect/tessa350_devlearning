import * as React from 'react';
import { TopicListViewModel } from 'tessa/ui/cards/controls';
export interface ForumTopicsListProps {
    viewModel: TopicListViewModel;
}
export declare class ForumTopicsList extends React.Component<ForumTopicsListProps> {
    render(): JSX.Element;
}
