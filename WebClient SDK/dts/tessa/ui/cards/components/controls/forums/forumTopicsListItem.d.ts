import * as React from 'react';
import { TopicViewModel } from 'tessa/ui/cards/controls';
export interface ForumTopicsListItemProps {
    viewModel: TopicViewModel;
}
export declare class ForumTopicsListItem extends React.Component<ForumTopicsListItemProps> {
    private _clickWrapper;
    render(): JSX.Element;
    private renderMessages;
    private handleClick;
    private handleDoubleClick;
}
