import React, { FunctionComponent } from 'react';
import { ControlProps } from '../controlProps';
import { RichTextBoxViewModel } from 'tessa/ui/cards/controls';
export interface RichTextBoxProps extends ControlProps<RichTextBoxViewModel> {
}
export declare const RichTextBox: FunctionComponent<RichTextBoxProps>;
export declare class RichTextBoxStub extends React.Component<RichTextBoxProps> {
    private _bodyRef;
    componentDidMount(): void;
    render(): JSX.Element;
}
export default RichTextBox;
