import { ICardEditorModel } from './interfaces';
import { CardTaskCompletionOptionSettings } from 'tessa/cards';
import { IValidationResultBuilder } from 'tessa/platform/validation';
export declare class CardTaskDialogContext {
    readonly parentEditor: ICardEditorModel;
    readonly cardId: guid;
    readonly taskId: guid;
    readonly taskTypeId: guid;
    readonly completionOptionSettings: CardTaskCompletionOptionSettings;
    readonly validationResult: IValidationResultBuilder;
    readonly onButtonPressed: CardTaskDialogOnButtonPressedFunc | null;
    constructor(parentEditor: ICardEditorModel, cardId: guid, taskId: guid, taskTypeId: guid, completionOptionSettings: CardTaskCompletionOptionSettings, validationResult: IValidationResultBuilder, onButtonPressed: CardTaskDialogOnButtonPressedFunc | null);
}
export declare type CardTaskDialogOnButtonPressedFunc = (dialogCardEditor: ICardEditorModel, settings: CardTaskCompletionOptionSettings, buttonName: string, cancel: boolean) => Promise<void>;
