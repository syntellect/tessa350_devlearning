export declare enum TaskGroupingType {
    SpecialFirst = 0,
    Default = 1,
    Author = 2,
    SpecialLast = 9999
}
