export declare enum TaskWorkspaceState {
    Initial = 0,
    DefaultForm = 1,
    OptionForm = 2,
    PostponeForm = 3,
    ReturnFromPostponed = 4,
    Locked = 5,
    LockedForAuthor = 6,
    UnlockedForAuthor = 7
}
