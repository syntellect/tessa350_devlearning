export declare class TaskHistoryTagViewModel {
    text: string;
    icon: string;
    tooltip: string;
    handler: Function | undefined;
    constructor(text: string, icon?: string, tooltip?: string, handler?: Function);
}
