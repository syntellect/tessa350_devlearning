import { TaskActionType } from './taskActionType';
import { TaskGroupingType } from './taskGroupingType';
import { ICardModel } from 'tessa/ui/cards/interfaces';
import { CardMetadataCompletionOption } from 'tessa/cards/metadata';
export declare abstract class TaskAction {
    constructor(caption: string, action: Function, type?: TaskActionType, groupingType?: TaskGroupingType, completionOption?: CardMetadataCompletionOption | null);
    caption: string;
    readonly command: Function;
    readonly type: TaskActionType;
    readonly groupingType: TaskGroupingType;
    readonly completionOption: CardMetadataCompletionOption | null;
}
export declare class TaskActionViewModel extends TaskAction {
    constructor(caption: string, action: Function, type?: TaskActionType, groupingType?: TaskGroupingType, completionOption?: CardMetadataCompletionOption | null, model?: ICardModel | null);
}
export declare class TaskAdditionalActionViewModel extends TaskAction {
    constructor(caption: string, action: Function, type?: TaskActionType, groupingType?: TaskGroupingType, completionOption?: CardMetadataCompletionOption | null, model?: ICardModel | null);
}
