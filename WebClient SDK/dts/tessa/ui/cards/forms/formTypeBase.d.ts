import { IFormType } from '../formTypeRegistry';
import { ICardModel, IFormViewModel } from '../interfaces';
import { CardTypeForm, CardTypeControl } from 'tessa/cards/types';
/**
 * Базовый класс для типа формы, используемой в автоматическом UI карточки по умолчанию.
 */
export declare abstract class FormTypeBase implements IFormType {
    protected abstract createFormCore(form: CardTypeForm, parentControl: CardTypeControl | null, model: ICardModel): IFormViewModel;
    createForm(form: CardTypeForm, parentControl: CardTypeControl | null, model: ICardModel): IFormViewModel;
}
