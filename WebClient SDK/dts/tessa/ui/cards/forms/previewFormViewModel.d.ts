import { IFormViewModel, IBlockViewModel, ICardModel, IFormState } from '../interfaces';
import { CardTypeFormSealed } from 'tessa/cards/types';
import { SupportUnloadingViewModel } from 'tessa/ui/cards/supportUnloadingViewModel';
import { IValidationResultBuilder } from 'tessa/platform/validation';
import { PreviewManager } from 'tessa/ui/cards/previewManager';
import { Visibility, EventHandler } from 'tessa/platform';
import { CardFilePreviewPosition } from 'tessa/cards';
export declare class PreviewFormViewModel extends SupportUnloadingViewModel implements IFormViewModel {
    private static _sideAtom;
    private static _widthAtom;
    constructor(model: ICardModel);
    protected _blocks: ReadonlyArray<IBlockViewModel>;
    protected _tabCaption: string | null;
    protected _blockMargin: string | null;
    protected _isForceTabMode: boolean;
    protected _contentClass: string;
    protected _visibility: Visibility;
    readonly cardModel: ICardModel;
    readonly previewManager: PreviewManager;
    readonly componentId: guid;
    readonly cardTypeForm: CardTypeFormSealed;
    get blocks(): ReadonlyArray<IBlockViewModel>;
    get name(): string | null;
    get isEmpty(): boolean;
    get tabCaption(): string | null;
    set tabCaption(value: string | null);
    get blockMargin(): string | null;
    set blockMargin(value: string | null);
    get isForceTabMode(): boolean;
    set isForceTabMode(value: boolean);
    get headerClass(): string;
    get contentClass(): string;
    get isEnabled(): boolean;
    set isEnabled(value: boolean);
    get hasFileControl(): boolean;
    get filePreviewIsDisabled(): boolean;
    get visibility(): Visibility;
    set visibility(value: Visibility);
    getIsTabMode(): boolean;
    getState(): IFormState;
    setState(state: IFormState): boolean;
    close(): boolean;
    static setFilePreviewPosition(value?: CardFilePreviewPosition): void;
    static get filePreviewPosition(): CardFilePreviewPosition;
    static setFilePreviewWidth(value: number): void;
    static get filePreviewWidth(): number;
    onUnloading(validationResult: IValidationResultBuilder): void;
    readonly closed: EventHandler<() => void>;
}
export declare class PreviewFormViewModelState implements IFormState {
    constructor(form: PreviewFormViewModel);
    readonly isForceTabMode: boolean;
    readonly visibility: Visibility;
    apply(form: PreviewFormViewModel): boolean;
}
export interface PreviewFormViewModelSettings {
    previewSide?: CardFilePreviewPosition;
    previewWidth?: number;
}
