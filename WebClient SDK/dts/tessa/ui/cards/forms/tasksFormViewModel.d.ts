import { IFormViewModel, IBlockViewModel, ICardModel, IFormState } from '../interfaces';
import { CardTypeFormSealed } from 'tessa/cards/types';
import { SupportUnloadingViewModel } from 'tessa/ui/cards/supportUnloadingViewModel';
import { IValidationResultBuilder } from 'tessa/platform/validation';
import { Visibility, EventHandler } from 'tessa/platform';
export declare class TasksFormViewModel extends SupportUnloadingViewModel implements IFormViewModel {
    constructor(model: ICardModel);
    protected _blocks: ReadonlyArray<IBlockViewModel>;
    protected _tabCaption: string | null;
    protected _blockMargin: string | null;
    protected _headerClass: string;
    protected _contentClass: string;
    protected _visibility: Visibility;
    readonly cardModel: ICardModel;
    readonly componentId: guid;
    readonly cardTypeForm: CardTypeFormSealed;
    get blocks(): ReadonlyArray<IBlockViewModel>;
    get name(): string | null;
    get isEmpty(): boolean;
    get tabCaption(): string | null;
    set tabCaption(value: string | null);
    get blockMargin(): string | null;
    set blockMargin(value: string | null);
    get headerClass(): string;
    get contentClass(): string;
    get hasFileControl(): boolean;
    get filePreviewIsDisabled(): boolean;
    get visibility(): Visibility;
    set visibility(value: Visibility);
    getIsTabMode(): boolean;
    getState(): IFormState;
    setState(state: IFormState): boolean;
    close(): boolean;
    onUnloading(validationResult: IValidationResultBuilder): void;
    readonly closed: EventHandler<() => void>;
}
export declare class TasksFormViewModelState implements IFormState {
    constructor(form: TasksFormViewModel);
    readonly isForceTabMode: boolean;
    readonly visibility: Visibility;
    apply(form: TasksFormViewModel): boolean;
}
