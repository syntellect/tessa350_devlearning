import { CardModelFlags } from './cardModelFlags';
import { CardModelTableInfo } from './cardModelTableInfo';
import { RowEditingType, CardRowFormContext } from './cardRowFormContext';
import { ICardModel, IFormViewModel, IBlockViewModel, IControlViewModel } from './interfaces';
import { PreviewManager } from './previewManager';
import { CardSavingRequest } from './cardSavingRequest';
import { Card, CardRow, CardTask, ICAdESProvider } from 'tessa/cards';
import { CardMetadataSealed, CardMetadataBinder } from 'tessa/cards/metadata';
import { CardTypeSealed } from 'tessa/cards/types';
import { FileContainer } from 'tessa/files';
import { IUIContext } from 'tessa/ui/uiContext';
import { ValidationResult } from 'tessa/platform/validation';
import { TaskViewModel, TaskHistoryViewModel } from 'tessa/ui/cards/tasks';
import { PreviewFormViewModel } from 'tessa/ui/cards/forms';
export declare class CardModel implements ICardModel {
    constructor(card: Card, sectionRows: Map<string, CardRow>, cardMetadata: CardMetadataSealed);
    constructor(card: Card, sectionRows: ReadonlyMap<string, CardRow>, cardMetadata: CardMetadataSealed, cardTask: CardTask | null, cardTypeId: guid, generalMetadata: CardMetadataSealed, binder: CardMetadataBinder, forms: IFormViewModel[], blocks: ReadonlyMap<string, IBlockViewModel>, controls: ReadonlyMap<string, IControlViewModel>, table: CardModelTableInfo, tableStack: CardModelTableInfo[], formBag: ReadonlyArray<IFormViewModel>, blockBag: ReadonlyArray<IBlockViewModel>, controlBag: ReadonlyArray<IControlViewModel>, fileContainer: FileContainer, contextExecutor: (action: (context: IUIContext) => void) => void, info: any, isRowForm: boolean, hasActiveValidation: boolean);
    private _tableStack;
    private _isRowForm;
    private _rowFormContext;
    private _contextExecutor;
    private _savingFunc;
    private _isClosed;
    private _digest;
    private _lastRequest;
    readonly componentId: guid;
    readonly card: Card;
    readonly sectionRows: ReadonlyMap<string, CardRow>;
    readonly cardMetadata: CardMetadataSealed;
    readonly generalMetadata: CardMetadataSealed;
    readonly binder: CardMetadataBinder;
    readonly cardType: CardTypeSealed;
    parentModel: ICardModel | null;
    cardTask: CardTask | null;
    mainForm: IFormViewModel | null;
    readonly forms: ReadonlyArray<IFormViewModel>;
    readonly formsBag: ReadonlyArray<IFormViewModel>;
    readonly blocks: ReadonlyMap<string, IBlockViewModel>;
    readonly blocksBag: ReadonlyArray<IBlockViewModel>;
    readonly controls: ReadonlyMap<string, IControlViewModel>;
    readonly controlsBag: ReadonlyArray<IControlViewModel>;
    table: CardModelTableInfo | null;
    get tableStack(): ReadonlyArray<CardModelTableInfo>;
    hasActiveValidation: boolean;
    flags: CardModelFlags;
    readonly info: any;
    currentRow: ICardModel | null;
    get rowFormContext(): CardRowFormContext | null;
    fileContainer: FileContainer;
    readonly previewManager: PreviewManager;
    readonly edsProvider: ICAdESProvider;
    get inSpecialMode(): boolean;
    get digest(): string;
    set digest(value: string);
    stateIsInitialized: boolean;
    contextIsInitialized: boolean;
    get lastRequest(): CardSavingRequest | null;
    closingRequest: CardSavingRequest | null;
    setActiveValidation(validation?: boolean): void;
    close(): Promise<void>;
    createEmptyRow(sectionName: string): CardRow;
    createForRow(sectionName: string, rowIndex: number): ICardModel;
    createRowFormContext(editingType: RowEditingType, initFunc: (context: CardRowFormContext) => Promise<boolean>, closeFunc: (context: CardRowFormContext) => Promise<boolean>): Promise<CardRowFormContext>;
    setContextExecutor(executor: (action: (context: IUIContext) => void) => void): void;
    executeInContext: (action: (context: IUIContext) => void) => Promise<void>;
    setSavingFunc(action: (request?: CardSavingRequest) => Promise<ValidationResult>): void;
    saveAsync: (request?: CardSavingRequest | undefined) => Promise<ValidationResult>;
    hasChanges(ignoreForceChanges?: boolean): boolean;
    tryGetPreviewTab(): PreviewFormViewModel | null;
    tryGetTasks(): TaskViewModel[] | null;
    tryGetTaskHistory(): TaskHistoryViewModel | null;
    clearBags(): void;
}
