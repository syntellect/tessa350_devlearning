import { ICardModel } from '../interfaces';
import { ITessaView } from 'tessa/views';
import { RequestParameter } from 'tessa/views/metadata';
export declare enum ViewMapColumnType {
    CardColumn = 0,
    CardID = 1,
    CardType = 2,
    CardTypeAlias = 3,
    CurrentUser = 4,
    ConstantValue = 5
}
export declare function getViewMapColumnTypeCaption(columnType: ViewMapColumnType): "$UI_Cards_TypesEditor_CardColumnViewMap" | "$UI_Cards_TypesEditor_CardIDViewMap" | "$UI_Cards_TypesEditor_CardTypeViewMap" | "$UI_Cards_TypesEditor_CardTypeAliasViewMap" | "$UI_Cards_TypesEditor_CurrentUserViewMap" | "$UI_Cards_TypesEditor_ConstantValueViewMap";
export declare function addRequestParameters(viewMapping: any[] | null, model: ICardModel, view?: ITessaView | null): RequestParameter[] | null;
