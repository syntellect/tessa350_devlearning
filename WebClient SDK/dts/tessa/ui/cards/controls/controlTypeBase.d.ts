import { IControlType } from '../controlTypeRegistry';
import { ICardModel, IControlViewModel } from '../interfaces';
import { CardTypeForm, CardTypeBlock, CardTypeControl } from 'tessa/cards/types';
export declare abstract class ControlTypeBase implements IControlType {
    protected abstract createControlCore(control: CardTypeControl, block: CardTypeBlock, form: CardTypeForm, parentControl: CardTypeControl | null, model: ICardModel): IControlViewModel;
    createControl(control: CardTypeControl, block: CardTypeBlock, form: CardTypeForm, parentControl: CardTypeControl | null, model: ICardModel): IControlViewModel;
}
