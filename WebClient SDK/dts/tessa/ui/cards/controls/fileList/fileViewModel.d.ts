import { FileTagViewModel } from './fileTagViewModel';
import { IFile } from 'tessa/files';
import { Delegate } from 'tessa/platform';
export declare class FileViewModel {
    constructor(file: IFile, collection: FileViewModel[]);
    private _isLoading;
    private _tag;
    private _collection;
    readonly model: IFile;
    readonly id: guid;
    readonly captionDelegate: Delegate<(viewModel: FileViewModel) => string>;
    readonly tooltipDelegate: Delegate<(viewModel: FileViewModel) => string>;
    get caption(): string;
    canDownload: boolean;
    get isLoading(): boolean;
    set isLoading(value: boolean);
    get isModified(): boolean;
    get tag(): FileTagViewModel | null;
    set tag(value: FileTagViewModel | null);
    get toolTip(): string;
    get collection(): FileViewModel[];
    dispose(): void;
    private getDefaultCaption;
    private getDefaultToolTip;
}
