import { ControlViewModelBase } from './controlViewModelBase';
import { ICardModel, ControlKeyDownEventArgs } from '../interfaces';
import { CardTypeEntryControl } from 'tessa/cards/types';
import { EventHandler } from 'tessa/platform';
import { ValidationResultBuilder } from 'tessa/platform/validation';
/**
 * Модель представления для элемента управления, позволяющего форматировать текст.
 */
export declare class RichTextBoxViewModel extends ControlViewModelBase {
    constructor(control: CardTypeEntryControl, model: ICardModel);
    private _fields;
    private _defaultFieldName;
    private _isReadOnlyMode;
    private _fieldDisposer;
    private _text;
    get text(): string | null;
    set text(value: string | null);
    get isReadOnlyMode(): boolean;
    set isReadOnlyMode(value: boolean);
    get error(): string | null;
    get hasEmptyValue(): boolean;
    private getDataFromFiled;
    private setDataToField;
    readonly keyDown: EventHandler<(args: ControlKeyDownEventArgs) => void>;
    onUnloading(validationResult: ValidationResultBuilder): void;
}
