import { ControlViewModelBase } from './controlViewModelBase';
import { ICardModel, ControlKeyDownEventArgs } from '../interfaces';
import { CardTypeEntryControl } from 'tessa/cards/types';
import { TextBoxMode } from 'tessa/ui/cards/controls';
import { EventHandler } from 'tessa/platform';
import { MediaStyle } from 'ui/mediaStyle';
import { AvalonTextBoxFontType } from 'tessa/cards/avalonTextBoxFontType';
import { SyntaxHighlighting } from 'tessa/cards/syntaxHighlighting';
/**
 * Модель представления для элемента управления, выполняющего ввод текстовых строк.
 */
export declare class TextBoxViewModel extends ControlViewModelBase {
    constructor(control: CardTypeEntryControl, model: ICardModel);
    private _fields;
    private _fieldNames;
    private _defaultFieldName;
    private _displayFormat;
    /**
     * Максимальная длина введённой текстовой строки.
     */
    readonly maxLength: number;
    /**
     * Минимальное количество строк.
     */
    readonly minRows: number;
    /**
     * Максимальное количество строк.
     */
    readonly maxRows: number;
    /**
     * Способ отображения элемента управления.
     */
    readonly textBoxMode: TextBoxMode;
    readonly avalonFontType: AvalonTextBoxFontType;
    readonly avalonShowLineNumbers: boolean;
    readonly avalonSyntaxType: SyntaxHighlighting;
    /**
     * Текстовое представление введённой строки.
     * Для элементов управления, доступных только для чтения,
     * строка возвращается в локализованном виде.
     */
    get text(): string;
    set text(value: string);
    /**
     * Формат поля. Применяется только в случае, когда контрол доступен только для чтения.
     * Значение null или пустая строка определяют отсутствие дополнительного форматирования.
     */
    get displayFormat(): string | null;
    set displayFormat(value: string | null);
    get error(): string | null;
    get hasEmptyValue(): boolean;
    private getFormatedText;
    getControlStyle(): MediaStyle | null;
    readonly keyDown: EventHandler<(args: ControlKeyDownEventArgs) => void>;
}
