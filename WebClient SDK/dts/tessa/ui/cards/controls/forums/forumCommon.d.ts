import { ForumItemViewModel } from './forumItemViewModel';
import { MenuAction } from 'tessa/ui/menuAction';
export interface IAttachmentMenuContext {
    item: ForumItemViewModel;
    menuActions: MenuAction[];
}
export declare function convertFromCustomStyle(customStyle: string, elem: Element): void;
