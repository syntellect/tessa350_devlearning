import { TopicEditorViewModel } from './topicEditorViewModel';
import { TopicListViewModel } from './topicListViewModel';
import { TopicViewModel } from './topicViewModel';
import { ForumPermissionsProvider } from './forumPermissionsProvider';
import { EmptyForumViewModel } from './emptyForumViewModel';
import { ControlViewModelBase } from '../controlViewModelBase';
import { ICardModel, IControlState } from '../../interfaces';
import { TopicModel, MessageModel, ParticipantTypes } from 'tessa/forums';
import { ArrayStorage } from 'tessa/platform/storage';
import { IValidationResultBuilder } from 'tessa/platform/validation';
import { CardTypeControl } from 'tessa/cards/types';
import { EventHandler } from 'tessa/platform';
export interface ContentChangedEventArgs {
    forum: ForumViewModel;
    content: ForumViewModelContentType | null;
}
export declare type ForumViewModelContentType = EmptyForumViewModel | TopicEditorViewModel | TopicListViewModel;
export declare class ForumViewModel extends ControlViewModelBase {
    constructor(control: CardTypeControl, model: ICardModel);
    private _model;
    private _content;
    private _stubMessage;
    private _isLicenseValid;
    private _isEnableSuperModeratorMode;
    private _isEnabledAddTopic;
    private _isEnabledForumEmptyContextMenu;
    private _satelliteId;
    private _krToken;
    readonly permissionsProvider: ForumPermissionsProvider;
    private _topicTypeId;
    get cardModel(): ICardModel;
    get content(): ForumViewModelContentType | null;
    get stubMessage(): string | null;
    set stubMessage(value: string | null);
    get isLicenseValid(): boolean;
    ratioContentWidth: number;
    get isEnableSuperModeratorMode(): boolean;
    set isEnableSuperModeratorMode(value: boolean);
    get isEnabledAddTopic(): boolean;
    set isEnabledAddTopic(value: boolean);
    get isEnabledForumEmptyContextMenu(): boolean;
    set isEnabledForumEmptyContextMenu(value: boolean);
    get topicTypeId(): guid;
    set topicTypeId(value: guid);
    private setContent;
    refresh(topicId?: guid | null, lastReadMessageTime?: string | null, needLoadTopics?: boolean, topics?: ArrayStorage<TopicModel> | null): Promise<void>;
    refreshTopics(needLoadTopics: boolean, topics?: ArrayStorage<TopicModel> | null): Promise<void>;
    refreshTopic(topicId: guid, lastReadMessageTime?: string | null, page?: number, newestMessage?: boolean): Promise<void>;
    showTopics(): Promise<void>;
    showTopic(topicId: guid): any;
    showTopic(viewModel: TopicViewModel): any;
    tryGetTopics(): TopicListViewModel | null;
    tryGetTopicEditor(): TopicEditorViewModel | null;
    sendMessage(topicId: guid, message: MessageModel, getfileContent: (id: guid) => File | null): Promise<void>;
    private updateLastReadMessageTimeTopic;
    private getLastReadMessageTimeTopic;
    getSatelliteId(): Promise<guid | null>;
    addOnContentChangedAndInvoke(func: (args: ContentChangedEventArgs) => void): (() => void) | null;
    getState(): IControlState;
    setState(state: IControlState): boolean;
    openParticipantsAction: (topicId: guid, participantType: ParticipantTypes, isSuperModerator: boolean) => Promise<void>;
    checkAddTopicPermissionAction: () => Promise<void>;
    checkSuperModeratorPermissionAction: () => Promise<void>;
    modifyAddingTopic: (model: TopicModel) => void;
    readonly onContentChanged: EventHandler<(args: ContentChangedEventArgs) => void>;
    protected onUnloading(_validationResult: IValidationResultBuilder): void;
}
export declare class ForumViewModelState {
    constructor(topicId: guid | null);
    private _topicId;
    apply(control: ForumViewModel): boolean;
}
