import { TopicViewModelBase } from './topicViewModelBase';
import { ForumViewModel } from './forumViewModel';
import { TopicModel } from 'tessa/forums';
export declare class TopicViewModel extends TopicViewModelBase {
    constructor(forumViewModel: ForumViewModel, model: TopicModel);
    private createMessage;
    tryGetClientPageNumber(): number;
}
