import { TopicModel } from 'tessa/forums';
export declare class ForumDialogManager {
    private constructor();
    private static _instance;
    static get instance(): ForumDialogManager;
    private _topic;
    private _participants;
    private _participantReadOnly;
    private _participantModeratorMode;
    private _isSuccessAddPartisipants;
    addTopicShowDialog(cardId: guid, modifyAddingTopic?: (model: TopicModel) => void): Promise<TopicModel | null>;
    private disableCheckBox;
    private addParticipantsShowDialogWithoutTopicId;
    private getParticipants;
    private getParticipantsList;
    private saveAddTopic;
    private saveAddParticipants;
}
