import { MessageViewModelBase } from './messageViewModelBase';
import { ForumViewModel } from './forumViewModel';
import { ForumAvatarViewModel } from './forumAvatarViewModel';
import { ForumItemViewModel } from './forumItemViewModel';
import { TopicViewModelBase } from './topicViewModelBase';
import { MessageModel } from 'tessa/forums';
export declare class MessageViewModel extends MessageViewModelBase {
    constructor(forumViewModel: ForumViewModel, topic: TopicViewModelBase, model: MessageModel);
    private _authorName;
    private _created;
    private _avatar;
    private _items;
    private _innerItems;
    get authorName(): string;
    set authorName(value: string);
    get created(): string;
    set created(value: string);
    get avatar(): ForumAvatarViewModel;
    set avatar(value: ForumAvatarViewModel);
    get items(): ForumItemViewModel[];
    get innerItems(): ForumItemViewModel[];
    private initAttachments;
}
