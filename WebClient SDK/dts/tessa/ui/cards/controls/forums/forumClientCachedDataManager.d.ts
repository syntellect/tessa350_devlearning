import { UserStatusModel } from 'tessa/forums';
export declare class ForumClientCachedDataManager {
    private constructor();
    private static _instance;
    static get instance(): ForumClientCachedDataManager;
    private _readTopicIdList;
    get readTopicIdList(): Map<guid, UserStatusModel>;
    addReadedTopicId(topicId: guid, lastReadMessageTime: string): void;
    addTopicId(topicId: guid): void;
}
