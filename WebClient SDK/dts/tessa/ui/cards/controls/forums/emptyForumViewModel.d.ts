import { ForumItemViewModel } from './forumItemViewModel';
import { ForumViewModel } from './forumViewModel';
import { MenuAction } from 'tessa/ui/menuAction';
export declare class EmptyForumViewModel {
    private _forumViewModel;
    constructor(_forumViewModel: ForumViewModel);
    getAttachmentContextMenu(_item: ForumItemViewModel): MenuAction[];
    dispose(): void;
    addTopic(): Promise<void>;
    superModerator(): Promise<void>;
}
