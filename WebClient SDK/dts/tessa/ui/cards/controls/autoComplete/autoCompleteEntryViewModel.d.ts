import { AutoCompleteValueEventArgs } from './autoCompleteCommon';
import { AutoCompleteEntryDataSource } from './autoCompleteEntryDataSource';
import { IAutoCompleteItem } from './autoCompleteItem';
import { IAutoCompletePopupItem } from './autoCompletePopupItem';
import { ControlViewModelBase } from '../controlViewModelBase';
import { ICardModel, ControlKeyDownEventArgs } from '../../interfaces';
import { CardTypeEntryControl } from 'tessa/cards/types';
import { EventHandler } from 'tessa/platform/eventHandler';
import { ValidationResultBuilder } from 'tessa/platform/validation';
import { IStorage } from 'tessa/platform/storage';
/**
 * Модель представления для контрола "Ссылка".
 */
export declare class AutoCompleteEntryViewModel extends ControlViewModelBase {
    constructor(control: CardTypeEntryControl, model: ICardModel);
    private _cardModel;
    private _referenceSection;
    private _referenceColumn;
    private _refSection;
    private _view;
    private _viewComboBox;
    private _viewMapping;
    private _popupItems;
    private _selectedItem;
    private _hasSelectionAction;
    private _comboIsLoading;
    private _comboIsLoadingTimer;
    isAllowOpenRefs: boolean;
    readonly hideSelectorButton: boolean;
    readonly comboBoxMode: boolean;
    readonly itemsSource: AutoCompleteEntryDataSource;
    get item(): IAutoCompleteItem | null;
    get popupItems(): ReadonlyArray<IAutoCompletePopupItem>;
    get popupDisplayIndexes(): ReadonlyArray<number> | null;
    alwaysShowInDialog: boolean;
    get error(): string | null;
    get hasEmptyValue(): boolean;
    get selectedItem(): IAutoCompleteItem | null;
    set selectedItem(value: IAutoCompleteItem | null);
    get hasSelectionAction(): boolean;
    set hasSelectionAction(value: boolean);
    readonly manualInput: boolean;
    get comboIsLoading(): boolean;
    setItem(item: IAutoCompletePopupItem | null): void;
    setItemFromViews(): Promise<void>;
    deleteItem(): void;
    findItems(filter: string | null): Promise<ReadonlyArray<IAutoCompletePopupItem>>;
    getRefInfo(): IStorage;
    openRefAction: () => Promise<void>;
    readonly valueSet: EventHandler<(args: AutoCompleteValueEventArgs<AutoCompleteEntryViewModel>) => void>;
    readonly valueDeleted: EventHandler<(args: AutoCompleteValueEventArgs<AutoCompleteEntryViewModel>) => void>;
    readonly keyDown: EventHandler<(args: ControlKeyDownEventArgs) => void>;
    onUnloading(validationResult: ValidationResultBuilder): void;
}
