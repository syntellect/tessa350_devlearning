import { AutoCompleteValueEventArgs } from './autoCompleteCommon';
import { AutoCompleteTableDataSource } from './autoCompleteTableDataSource';
import { IAutoCompleteItem } from './autoCompleteItem';
import { IAutoCompletePopupItem } from './autoCompletePopupItem';
import { ControlViewModelBase } from '../controlViewModelBase';
import { ICardModel, ControlKeyDownEventArgs } from '../../interfaces';
import { CardTypeTableControl } from 'tessa/cards/types';
import { ValidationResultBuilder } from 'tessa/platform/validation';
import { EventHandler } from 'tessa/platform/eventHandler';
import { IStorage } from 'tessa/platform/storage';
import { ISelectFromViewContext } from 'tessa/ui/views/selectFromViewContext';
/**
 * Модель представления для контрола "Список".
 */
export declare class AutoCompleteTableViewModel extends ControlViewModelBase {
    constructor(control: CardTypeTableControl, model: ICardModel);
    private _cardModel;
    private _referenceSection;
    private _referenceColumn;
    private _refSection;
    private _view;
    private _viewMapping;
    private _popupItems;
    private _itemsKeepAliveDisposer;
    private _selectedItem;
    private _hasSelectionAction;
    isAllowOpenRefs: boolean;
    readonly hideSelectorButton: boolean;
    readonly hasLineBreak: boolean;
    readonly itemsSource: AutoCompleteTableDataSource;
    get items(): ReadonlyArray<IAutoCompleteItem>;
    get popupItems(): ReadonlyArray<IAutoCompletePopupItem>;
    get popupDisplayIndexes(): ReadonlyArray<number> | null;
    alwaysShowInDialog: boolean;
    get error(): string | null;
    get hasEmptyValue(): boolean;
    get selectedItem(): IAutoCompleteItem | null;
    set selectedItem(value: IAutoCompleteItem | null);
    get hasSelectionAction(): boolean;
    set hasSelectionAction(value: boolean);
    readonly manualInput: boolean;
    setItem(item: IAutoCompletePopupItem | null): void;
    setItemFromViews(): Promise<void>;
    selectFromView: (context: ISelectFromViewContext) => Promise<void>;
    deleteItem(item: IAutoCompleteItem): void;
    findItems(filter: string | null): Promise<ReadonlyArray<IAutoCompletePopupItem>>;
    getRefInfo(item: IAutoCompleteItem): IStorage;
    openRefAction(index: number): void;
    readonly valueSet: EventHandler<(args: AutoCompleteValueEventArgs<AutoCompleteTableViewModel>) => void>;
    readonly valueDeleted: EventHandler<(args: AutoCompleteValueEventArgs<AutoCompleteTableViewModel>) => void>;
    readonly keyDown: EventHandler<(args: ControlKeyDownEventArgs) => void>;
    onUnloading(validationResult: ValidationResultBuilder): void;
}
