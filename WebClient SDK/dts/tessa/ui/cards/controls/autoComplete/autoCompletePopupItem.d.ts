export interface IAutoCompletePopupItem {
    fields: any[];
    displayFields: string[];
    dateTypes: number[];
    columns: string[];
}
export declare class AutoCompletePopupItem implements IAutoCompletePopupItem {
    constructor(fields: any[], dateTypes?: number[], columns?: string[]);
    fields: any[];
    displayFields: string[];
    dateTypes: number[];
    columns: string[];
    private generateDisplayFields;
}
