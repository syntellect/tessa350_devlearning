import { GridColumnViewModel } from './gridColumnViewModel';
import { GridRowViewModel } from './gridRowViewModel';
import { GridCellViewModel } from './gridCellViewModel';
import { ControlViewModelBase } from '../controlViewModelBase';
import { ControlKeyDownEventArgs, ICardModel } from '../../interfaces';
import { CardTypeTableControl } from 'tessa/cards/types';
import { CardRow } from 'tessa/cards';
import { Visibility } from 'tessa/platform';
import { EventHandler } from 'tessa/platform/eventHandler';
import { ValidationResultBuilder } from 'tessa/platform/validation';
import { UIButton } from 'tessa/ui/uiButton';
import { ColumnSortDirection } from '../columnSortDirection';
import { MenuAction } from 'tessa/ui/menuAction';
import { MediaStyle } from 'ui/mediaStyle';
import { GridCellFormatContext } from './gridCellFormatContext';
export declare class GridViewModel extends ControlViewModelBase implements IFilterableGrid {
    constructor(control: CardTypeTableControl, model: ICardModel);
    private _cardModel;
    private _control;
    private _form;
    private _sectionId;
    private _sectionName;
    private _referenceColumnNames;
    private _orderColumnName;
    private _rows;
    private _rowModels;
    private _isRowFormOpened;
    private _rowsDisposer;
    private _childSectionNamesAndParentColumns;
    private _canSelectMultipleItems;
    private _sortingColumn;
    private _sortDirection;
    private _selectionState;
    private _multiSelectButtonEnabled;
    private _columns;
    get cardModel(): ICardModel;
    get sectionId(): string;
    get columns(): ReadonlyArray<GridColumnViewModel>;
    get rows(): ReadonlyArray<GridRowViewModel>;
    get sortingColumn(): number | undefined;
    setSortingColumn(value: number | undefined, descendingByDefault?: boolean): void;
    get sortDirection(): ColumnSortDirection;
    set sortDirection(value: ColumnSortDirection);
    get selectedRow(): GridRowViewModel | null;
    get selectedRows(): GridRowViewModel[];
    get isRowFormOpened(): boolean;
    readonly leftButtons: UIButton[];
    readonly rightButtons: UIButton[];
    searchText: string;
    get canSelectMultipleItems(): boolean;
    readonly contextMenuGenerators: ((ctx: CardRowMenuContext) => void)[];
    get multiSelectButtonEnabled(): boolean;
    set multiSelectButtonEnabled(value: boolean);
    readonly addButton: UIButton;
    readonly removeButton: UIButton;
    readonly moveUpButton: UIButton;
    readonly moveDownButton: UIButton;
    readonly multiSelectButton: UIButton;
    private searchFilterFunc;
    private getColumns;
    private rowVisibilityFilter;
    getAddRemoveButtonsVisibility(): Visibility;
    getMoveButtonsVisibility(): Visibility;
    isCanAddRow(): boolean;
    addRow(): Promise<void>;
    isCanRemoveRow(): boolean;
    removeRow(): Promise<void>;
    isCanMoveSelectedRowsUp(): boolean;
    moveSelectedRowsUp(): void;
    isCanMoveSelectedRowsDown(): boolean;
    moveSelectedRowsDown(): void;
    canSetSearchText(): boolean;
    setSearchText(text: string): void;
    editRow(rowViewModel: GridRowViewModel | null, columnIndex?: number): Promise<void>;
    private editRowInternal;
    private restoreRowsAfterAddingCancel;
    private validateRow;
    private validateRowWithValidators;
    private modifyValidationContext;
    private getChildSectionNamesAndParentColumns;
    private setPermissionsForNewRow;
    private removePermissionsForRow;
    getRowContextMenu: (rowViewModel: GridRowViewModel, columnIndex: number) => readonly MenuAction[];
    private defaultContextMenuGenerator;
    private addDefaultKeyDownHandler;
    getCaptionStyle(): MediaStyle | null;
    getControlStyle(): MediaStyle | null;
    changeColumnOrderByTarget(sourceIndex: number, targetIndex: number): void;
    private _cellFormatFunc;
    get gridCellFormatFunc(): ((context: GridCellFormatContext) => any) | null;
    set gridCellFormatFunc(value: ((context: GridCellFormatContext) => any) | null);
    readonly rowAdding: EventHandler<(args: GridRowAddingEventArgs) => void>;
    readonly rowInvoked: EventHandler<(args: GridRowEventArgs) => void>;
    readonly rowInitializing: EventHandler<(args: GridRowEventArgs) => void>;
    readonly rowInitialized: EventHandler<(args: GridRowEventArgs) => void>;
    readonly rowEditorClosing: EventHandler<(args: GridRowEventArgs) => void>;
    readonly rowEditorClosed: EventHandler<(args: GridRowEventArgs) => void>;
    readonly rowValidating: EventHandler<(args: GridRowValidationEventArgs) => void>;
    readonly keyDown: EventHandler<(args: ControlKeyDownEventArgs) => void>;
    onUnloading(validationResult: ValidationResultBuilder): void;
}
export declare enum GridRowAction {
    Inserted = 0,
    Opening = 1,
    Deleting = 2
}
export interface GridRowAddingEventArgs {
    readonly row: CardRow;
    readonly rows: ReadonlyArray<CardRow>;
    rowIndex: number;
    readonly control: GridViewModel;
    readonly cardModel: ICardModel;
    cancel: boolean;
}
export interface GridRowEventArgs {
    readonly action: GridRowAction;
    readonly control: GridViewModel;
    readonly row: CardRow;
    readonly rowModel: ICardModel | null;
    readonly cardModel: ICardModel;
    cancel: boolean;
    readonly cell: GridCellViewModel | null;
    readonly columnIndex: number;
}
export interface GridRowValidationEventArgs {
    readonly row: CardRow;
    readonly rowModel: ICardModel;
    readonly cardModel: ICardModel;
    readonly validationResult: ValidationResultBuilder;
    readonly control: GridViewModel;
}
export interface IFilterableGrid {
    canSetSearchText: () => boolean;
    setSearchText: (text: string) => void;
}
export interface CardRowMenuContext {
    readonly row: GridRowViewModel;
    readonly control: GridViewModel;
    readonly menuActions: MenuAction[];
    readonly cell: GridCellViewModel | null;
    readonly columnIndex: number;
}
