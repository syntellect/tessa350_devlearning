import * as React from 'react';
import { ValidationResult, ValidationResultItem } from 'tessa/platform/validation';
export interface ValidationResultDialogProps {
    result: ValidationResult;
    caption?: string;
    onClose: () => void;
}
export interface ValidationResultDialogState {
    currentItem: ValidationResultItem | null;
}
export declare class ValidationResultDialog extends React.Component<ValidationResultDialogProps, ValidationResultDialogState> {
    constructor(props: ValidationResultDialogProps);
    private _clipboardRef;
    private _clipboard;
    private _locManager;
    componentWillUnmount(): void;
    render(): JSX.Element;
    private renderCommon;
    private renderDetails;
    private getCaption;
    private localize;
    private handleClose;
    private handleShowDetails;
    private handleCloseDetails;
    private handleCopy;
}
