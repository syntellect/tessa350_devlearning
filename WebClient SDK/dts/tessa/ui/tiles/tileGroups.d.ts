export declare enum TileGroups {
    Platform = -1,
    Top = 100,
    Views = 200,
    Cards = 300,
    KrButtons = 400,
    WorkflowButtons = 500,
    Settings = 600,
    Bottom = 700,
    NoGroup
}
export declare function getTileGroupName(group: TileGroups): string;
