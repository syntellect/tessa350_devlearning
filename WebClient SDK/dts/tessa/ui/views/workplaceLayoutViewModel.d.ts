import { IViewContext } from './viewContext';
import { IReactRefsProvider, ReactRef } from '../reactRefProvider';
export declare class WorkplaceLayoutViewModel implements IReactRefsProvider {
    constructor(parent?: WorkplaceLayoutViewModel | null, _splitPosition?: number | null, // TODO
    _secondChildSize?: number | null);
    private _firstChild;
    private _secondChild;
    private _innerContent;
    private _caption;
    readonly parent: WorkplaceLayoutViewModel | null;
    get firstChild(): WorkplaceLayoutViewModel | null;
    set firstChild(value: WorkplaceLayoutViewModel | null);
    get secondChild(): WorkplaceLayoutViewModel | null;
    set secondChild(value: WorkplaceLayoutViewModel | null);
    get innerContent(): any | null;
    set innerContent(value: any | null);
    get caption(): string;
    set caption(value: string);
    selectionAction: (context: IViewContext | null) => void;
    get isSelectionListener(): boolean;
    getReactRefs(): ReactRef[];
}
