/// <reference types="react" />
import { IBaseContentItem } from './content';
import { IViewParameters } from './parameters';
import { IWorkplaceViewModel } from './workplaceViewModel';
import { IViewContext } from './viewContext';
import { DoubleClickAction } from './doubleClickInfo';
import { ISelectionState } from './selectionState';
import { PagingContext } from './pagingContext';
import { SelectAction } from './selectFromViewContext';
import { IViewSorting } from './viewSorting';
import { IReactRefProvider, ReactRef } from '../reactRefProvider';
import { MenuAction } from '../menuAction';
import { IUIContext } from '../uiContext';
import { IExtensionExecutor } from 'tessa/extensions';
import { DataNodeMetadataSealed } from 'tessa/views/workplaces';
import { SortingColumn } from 'tessa/views/sortingColumn';
import { DbType } from 'tessa/platform';
import { ViewParameterMetadataSealed, ViewMetadataSealed } from 'tessa/views/metadata';
import { RequestParameter } from 'tessa/views/metadata/requestParameter';
import { ViewSelectionMode } from 'tessa/views/metadata/viewSelectionMode';
import { ITessaViewResult, ITessaViewRequest, Paging, ITessaView } from 'tessa/views';
export interface BuildContentDelegate {
    (model: IWorkplaceViewComponent, content: Map<string, IBaseContentItem>, contentFactories: Map<string, (c: IWorkplaceViewComponent) => IBaseContentItem | null>): void;
}
export interface ConvertParameterValueDelegate {
    (model: IWorkplaceViewComponent, sourceValue: any, metadata: ViewParameterMetadataSealed): any;
}
export interface DataColumnConverterDelegate {
    (model: IWorkplaceViewComponent, columnName: string, columnDataType: DbType): [string, DbType];
}
export interface DataColumnsConverterDelegate {
    (model: IWorkplaceViewComponent, columnNames: string[], columnDataTypes: DbType[]): [string, DbType][];
}
export interface DataRowConverterDelegate {
    (model: IWorkplaceViewComponent, columns: Map<string, DbType>, data: any): Map<string, any>;
}
export interface DataRowsConverterDelegate {
    (model: IWorkplaceViewComponent, columns: Map<string, DbType>, objects: any[]): Map<string, any>[];
}
export interface GetDataDelegate {
    (model: IWorkplaceViewComponent, request: ITessaViewRequest | null): Promise<ITessaViewResult | null>;
}
export interface GetPageLimitDelegate {
    (model: IWorkplaceViewComponent): number;
}
export interface GetPagingModeDelegate {
    (model: IWorkplaceViewComponent): Paging;
}
export interface GetRequestDelegate {
    (model: IWorkplaceViewComponent, parameters: RequestParameter[], setupActions: Array<(c: IWorkplaceViewComponent, r: ITessaViewRequest) => void>): ITessaViewRequest | null;
}
export interface GetViewDelegate {
    (model: IWorkplaceViewComponent): ITessaView | null;
}
export interface GetViewMetadataDelegate {
    (model: IWorkplaceViewComponent): ViewMetadataSealed | null;
}
export interface GetWorkplaceViewMetadataDelegate {
    (model: IWorkplaceViewComponent): DataNodeMetadataSealed | null;
}
export interface SetRequestPagingParametersDelegate {
    (model: IWorkplaceViewComponent, parameters: RequestParameter[], pagingContext: PagingContext): void;
}
export interface SelectFromViewDelegate extends SelectAction {
}
export interface IViewContextMenuContext {
    readonly viewContext: IViewContext;
    readonly menuActions: MenuAction[];
    readonly uiContextExecutor: (action: (context: IUIContext) => void) => void;
}
export interface IWorkplaceViewComponent extends IViewContext, IReactRefProvider {
    readonly uiId: guid;
    readonly extensionExecutor: IExtensionExecutor;
    readonly viewContext: IViewContext;
    readonly actualRowCount: number;
    calculatedRowCount: number;
    canAutoFocus: boolean;
    quickSearchEnabled: boolean;
    readonly content: ReadonlyMap<string, IBaseContentItem>;
    readonly contentByArea: ReadonlyMap<number, IBaseContentItem[]>;
    readonly contentFactories: Map<string, (c: IWorkplaceViewComponent) => IBaseContentItem | null>;
    currentPage: number;
    readonly dataNodeMetadata: DataNodeMetadataSealed;
    doubleClickAction: DoubleClickAction;
    readonly isCounterAvailable: boolean;
    isDataLoading: boolean;
    optionalPagingStatus: boolean;
    pageCount: number;
    pageCountStatus: boolean;
    readonly pageLimit: number;
    readonly pagingMode: Paging;
    readonly parametersSetName: string;
    readonly rowCounterVisible: boolean;
    readonly selectionState: ISelectionState;
    readonly sortingColumns: ReadonlyArray<SortingColumn>;
    readonly hasNextPage: boolean;
    readonly hasPreviousPage: boolean;
    readonly contextMenuGenerators: ((ctx: IViewContextMenuContext) => void)[];
    multiSelectEnabled: boolean;
    buildContent: BuildContentDelegate;
    convertParameterValue: ConvertParameterValueDelegate;
    dataColumnConverter: DataColumnConverterDelegate;
    dataColumnsConverter: DataColumnsConverterDelegate;
    dataRowConverter: DataRowConverterDelegate;
    dataRowsConverter: DataRowsConverterDelegate;
    getData: GetDataDelegate;
    getPageLimit: GetPageLimitDelegate;
    getPagingMode: GetPagingModeDelegate;
    getRequest: GetRequestDelegate;
    getView: GetViewDelegate;
    getViewMetadata: GetViewMetadataDelegate;
    getWorkplaceViewMetadata: GetWorkplaceViewMetadataDelegate;
    setRequestPagingParameters: SetRequestPagingParametersDelegate;
    selectAction: SelectFromViewDelegate | null;
    initialize(): any;
    dispose(): any;
    attach(observer: IViewContext): any;
    detach(observer: IViewContext): any;
    refresh(): Promise<void>;
    canRefresh(): boolean;
    setTableVisibleColumnOrdering(func: () => ReadonlyArray<string>): any;
}
export declare class WorkplaceViewComponent implements IWorkplaceViewComponent {
    constructor(workplace: IWorkplaceViewModel, masterContext: IWorkplaceViewComponent | null, parametersFactory: (c: IWorkplaceViewComponent) => {
        parameters: IViewParameters;
        needDispose: boolean;
    }, sortingFactory: (m: ViewMetadataSealed | null) => IViewSorting);
    private _masterContext;
    private _parametersFactory;
    private _sortingFactory;
    private _calculatedRowCount;
    private _quickSearchEnabled;
    private _observers;
    private _resultColumns;
    private _content;
    private _contentByArea;
    private _currentPage;
    private _data;
    private _isDataLoading;
    private _optionalPagingStatus;
    private _pageCount;
    private _pageCountStatus;
    private _viewParameters;
    private _viewParametersNeedDispose;
    private _refSection;
    private _sorting;
    private _selection;
    private _hasNextPage;
    private _existedLinks;
    private _selectionReaction;
    private _parameterReaction;
    private _reactRef;
    private _multiSelectEnabled;
    private _tableVisibleColumnOrderingFunc;
    get children(): ReadonlyArray<IViewContext>;
    get columns(): ReadonlyMap<string, DbType>;
    get data(): ReadonlyArray<ReadonlyMap<string, any>> | null;
    get id(): guid;
    get multiSelect(): boolean;
    get parameters(): IViewParameters;
    get parentContext(): IViewContext | null;
    get refSection(): string;
    set refSection(value: string);
    get selectedCellValue(): any | null;
    get selectedColumn(): string | null;
    get selectedRow(): ReadonlyMap<string, any> | null;
    get selectedRows(): ReadonlyArray<ReadonlyMap<string, any>> | null;
    get selectionMode(): ViewSelectionMode;
    get view(): ITessaView | null;
    readonly workplace: IWorkplaceViewModel;
    readonly uiId: string;
    readonly extensionExecutor: IExtensionExecutor;
    get actualRowCount(): number;
    get calculatedRowCount(): number;
    set calculatedRowCount(value: number);
    canAutoFocus: boolean;
    get quickSearchEnabled(): boolean;
    set quickSearchEnabled(value: boolean);
    get content(): ReadonlyMap<string, IBaseContentItem>;
    get contentByArea(): ReadonlyMap<number, IBaseContentItem[]>;
    readonly contentFactories: Map<string, (c: IWorkplaceViewComponent) => IBaseContentItem>;
    get currentPage(): number;
    set currentPage(value: number);
    get dataNodeMetadata(): DataNodeMetadataSealed;
    doubleClickAction: DoubleClickAction;
    get isCounterAvailable(): boolean;
    get isDataLoading(): boolean;
    set isDataLoading(value: boolean);
    get optionalPagingStatus(): boolean;
    set optionalPagingStatus(value: boolean);
    get pageCount(): number;
    set pageCount(value: number);
    get pageCountStatus(): boolean;
    set pageCountStatus(value: boolean);
    get pageLimit(): number;
    get pagingMode(): Paging;
    get parametersSetName(): string;
    get rowCounterVisible(): boolean;
    get selectionState(): ISelectionState;
    get sortingColumns(): ReadonlyArray<SortingColumn>;
    get viewContext(): IViewContext;
    get hasNextPage(): boolean;
    get hasPreviousPage(): boolean;
    readonly contextMenuGenerators: ((ctx: IViewContextMenuContext) => void)[];
    get multiSelectEnabled(): boolean;
    set multiSelectEnabled(value: boolean);
    buildContent: BuildContentDelegate;
    convertParameterValue: ConvertParameterValueDelegate;
    dataColumnConverter: DataColumnConverterDelegate;
    dataColumnsConverter: DataColumnsConverterDelegate;
    dataRowConverter: DataRowConverterDelegate;
    dataRowsConverter: DataRowsConverterDelegate;
    getData: GetDataDelegate;
    getPageLimit: GetPageLimitDelegate;
    getPagingMode: GetPagingModeDelegate;
    getRequest: GetRequestDelegate;
    getView: GetViewDelegate;
    getViewMetadata: GetViewMetadataDelegate;
    getWorkplaceViewMetadata: GetWorkplaceViewMetadataDelegate;
    setRequestPagingParameters: SetRequestPagingParametersDelegate;
    selectAction: SelectFromViewDelegate | null;
    refreshView(): Promise<void>;
    canRefreshView(): boolean;
    filterView(): Promise<void>;
    canFilterView(): boolean;
    clearFilterView(): void;
    canClearFilterView(): boolean;
    getSortedColumns(): ReadonlyArray<SortingColumn>;
    sortColumn(column: string, addOrInverse: boolean, descendingByDefault: boolean): void;
    getTableVisibleColumnOrdering(): ReadonlyArray<string>;
    inCellSelectionMode(): boolean;
    inSelectionMode(): boolean;
    initialize(): void;
    dispose(): void;
    private initializeSelection;
    private initializeParameters;
    private initializeSorting;
    private initializePaging;
    private initializeContent;
    private updateParameters;
    private updateParameterLinkValues;
    private updateColumnLinkValues;
    private getLinkedColumnInfo;
    private updatePageCount;
    private updatePageCountVisibility;
    private updateQuickSearchAvailability;
    private updateData;
    private tryGetFirstVisibleColumnName;
    private load;
    private cancelLoad;
    private loadAndCancel;
    private resetData;
    private getSetupRequestActions;
    private getCurrentState;
    private restoreState;
    attach(observer: IViewContext): void;
    detach(observer: IViewContext): void;
    refresh(): Promise<void>;
    canRefresh(): boolean;
    private initializeMasterSelectionReaction;
    private initializeParameterReaction;
    setReactRef<R = any>(ref: React.RefObject<R> | null): void;
    getReactRef(): ReactRef | null;
    setTableVisibleColumnOrdering(func: () => ReadonlyArray<string>): void;
}
