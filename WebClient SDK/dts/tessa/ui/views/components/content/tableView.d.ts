import * as React from 'react';
import { TableGridViewModel } from '../../content/tableGridViewModel';
export interface TableViewProps {
    viewModel: TableGridViewModel;
}
export declare class TableView extends React.Component<TableViewProps> {
    private _gridUIVM;
    render(): JSX.Element;
    private renderRowCounter;
    private getHeaders;
    private getRows;
    private getBlocks;
    private convertFromSortDirection;
    private handleHeaderDrop;
}
