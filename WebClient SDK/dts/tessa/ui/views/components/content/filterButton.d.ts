import * as React from 'react';
import { FilterButtonViewModel } from '../../content/filterButtonViewModel';
export interface FilterButtonProps {
    viewModel: FilterButtonViewModel;
}
export declare class FilterButton extends React.Component<FilterButtonProps> {
    render(): JSX.Element | null;
    private handleClick;
}
