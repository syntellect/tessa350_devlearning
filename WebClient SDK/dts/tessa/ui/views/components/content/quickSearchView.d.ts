import * as React from 'react';
import { QuickSearchViewModel } from '../../content/quickSearchViewModel';
export interface QuickSearchViewProps {
    viewModel: QuickSearchViewModel;
}
export declare class QuickSearchView extends React.Component<QuickSearchViewProps> {
    private _mainRef;
    constructor(props: QuickSearchViewProps);
    componentDidMount(): void;
    componentWillUnmount(): void;
    componentDidUpdate(prevProps: QuickSearchViewProps): void;
    render(): JSX.Element | null;
    focus(opt?: FocusOptions): void;
    private handleSearch;
    private handleFocusControlWhenDataWasLoaded;
}
