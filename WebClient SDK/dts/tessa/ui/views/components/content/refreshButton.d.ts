import * as React from 'react';
import { RefreshButtonViewModel } from '../../content/refreshButtonViewModel';
export interface RefreshButtonProps {
    viewModel: RefreshButtonViewModel;
}
export declare class RefreshButton extends React.Component<RefreshButtonProps> {
    render(): JSX.Element;
    private handleClick;
}
