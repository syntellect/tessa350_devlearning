import * as React from 'react';
export interface DynamicContentComponentProps {
    viewModel: any;
}
export interface DynamicContentComponentState {
    prevViewModel: any | null;
    component: Function | null;
}
export declare class DynamicContentComponent extends React.Component<DynamicContentComponentProps, DynamicContentComponentState> {
    constructor(props: DynamicContentComponentProps);
    static getDerivedStateFromProps(props: DynamicContentComponentProps, state: DynamicContentComponentState): DynamicContentComponentState | null;
    render(): JSX.Element | null;
}
