import * as React from 'react';
export declare class Workplace extends React.Component {
    private _mouseClicked;
    private _treeWidth;
    private _treeRef;
    private _resizerRef;
    private _viewRef;
    private _cachedId;
    componentDidMount(): void;
    componentWillUnmount(): void;
    render(): JSX.Element | null;
    private handleMouseUp;
    private handleMouseDown;
    private handleMouseMove;
    private handleOnContextMenu;
}
