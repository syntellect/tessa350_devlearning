import { IExtension } from 'tessa/extensions';
import { IStorage } from 'tessa/platform/storage';
export interface IWorkplaceExtension<T> extends IExtension {
    settingsStorage: IStorage;
    getExtensionName(): string;
    initializeSettings(model: T): any;
    initialize(model: T): any;
    initialized(model: T): any;
}
