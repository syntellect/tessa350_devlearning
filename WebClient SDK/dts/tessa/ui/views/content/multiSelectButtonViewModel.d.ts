import { BaseContentItem } from './baseContentItem';
import { ContentPlaceArea } from './contentPlaceArea';
import { IWorkplaceViewComponent } from '../workplaceViewComponent';
import { Visibility } from 'tessa/platform';
export declare class MultiSelectButtonViewModel extends BaseContentItem {
    constructor(viewComponent: IWorkplaceViewComponent, area?: ContentPlaceArea, order?: number);
    private _toolTip;
    get toolTip(): string;
    set toolTip(value: string);
    private _visibility;
    get isLoading(): boolean;
    get multiSelectEnabled(): boolean;
    get visibility(): Visibility;
    set visibility(value: Visibility);
    changeMultiSelectEnabled(): void;
}
