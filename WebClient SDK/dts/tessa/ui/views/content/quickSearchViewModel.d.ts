/// <reference types="react" />
import { BaseContentItem } from './baseContentItem';
import { ContentPlaceArea } from './contentPlaceArea';
import { IWorkplaceViewComponent } from '../workplaceViewComponent';
import { Visibility } from 'tessa/platform';
export declare class QuickSearchViewModel extends BaseContentItem {
    constructor(viewComponent: IWorkplaceViewComponent, area?: ContentPlaceArea, order?: number);
    private _searchText;
    private _visibility;
    private _reactComponentRef;
    get quickSearchEnabled(): boolean;
    set quickSearchEnabled(value: boolean);
    get searchText(): string;
    set searchText(value: string);
    get visibility(): Visibility;
    set visibility(value: Visibility);
    get isLoading(): boolean;
    focusControlWhenDataWasLoaded: boolean;
    dispose(): void;
    bindReactComponentRef(ref: React.RefObject<any>): void;
    unbindReactComponentRef(): void;
    focus(opt?: FocusOptions): void;
    search(): void;
}
