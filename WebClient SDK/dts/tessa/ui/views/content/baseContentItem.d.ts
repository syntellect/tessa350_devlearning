import { ContentPlaceArea } from './contentPlaceArea';
import { IWorkplaceViewComponent } from '../workplaceViewComponent';
export interface IBaseContentItem {
    readonly area: ContentPlaceArea;
    readonly order: number;
    initiailize(): any;
    dispose(): any;
}
export declare abstract class BaseContentItem implements IBaseContentItem {
    constructor(viewComponent: IWorkplaceViewComponent, area: ContentPlaceArea, order: number);
    readonly viewComponent: IWorkplaceViewComponent;
    readonly area: ContentPlaceArea;
    readonly order: number;
    initiailize(): void;
    dispose(): void;
}
