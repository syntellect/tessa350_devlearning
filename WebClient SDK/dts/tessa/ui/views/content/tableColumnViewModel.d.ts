import { ViewColumnMetadataSealed, ViewReferenceMetadataSealed, ViewMetadataSealed } from 'tessa/views/metadata';
import { DbType } from 'tessa/platform';
import { SortDirection } from 'tessa/views';
import { MenuAction } from 'tessa/ui/menuAction';
export declare class TableColumnViewModel {
    constructor(metadata: ViewColumnMetadataSealed | null, columnName: string, dataType: DbType, canSort: boolean, sortAction?: (((column: string, addOrInverse: boolean, descendingByDefault: boolean) => void)) | null, getContextMenu?: ((column: TableColumnViewModel) => ReadonlyArray<MenuAction>) | null);
    private _header;
    private _canSort;
    private _sortDirection;
    private _visibility;
    private _sortAction;
    private _getContextMenu;
    private _toolTip;
    get canGrouping(): boolean;
    get canSort(): boolean;
    set canSort(value: boolean);
    get header(): string;
    set header(value: string);
    readonly columnName: string;
    readonly dataType: DbType;
    isReferencedColumn: boolean;
    metadata: ViewColumnMetadataSealed | null;
    referenceMetadata: ViewReferenceMetadataSealed | null;
    viewMetadata: ViewMetadataSealed | null;
    get sortDirection(): SortDirection | null;
    set sortDirection(value: SortDirection | null);
    get visibility(): boolean;
    set visibility(value: boolean);
    get toolTip(): string;
    set toolTip(value: string);
    sortColumn(addOrInverse?: boolean, descendingByDefault?: boolean): void;
    getContextMenu(): ReadonlyArray<MenuAction>;
}
