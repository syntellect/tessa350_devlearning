import { BaseContentItem } from './baseContentItem';
import { ContentPlaceArea } from './contentPlaceArea';
import { IWorkplaceViewComponent } from '../workplaceViewComponent';
import { Visibility } from 'tessa/platform';
import { SortDirection } from 'tessa/views/sortingColumn';
export interface SortButtonColumn {
    alias: string;
    caption: string;
    sortDirection: SortDirection | null;
}
export declare class SortButtonViewModel extends BaseContentItem {
    constructor(viewComponent: IWorkplaceViewComponent, area?: ContentPlaceArea, order?: number);
    private _toolTip;
    get toolTip(): string;
    set toolTip(value: string);
    private _visibility;
    private _columnsCache;
    private _columns;
    private _columnReaction;
    get visibility(): Visibility;
    set visibility(value: Visibility);
    initiailize(): void;
    dispose(): void;
    get isLoading(): boolean;
    private initColumns;
    getSortingColumns(): SortButtonColumn[];
    sortColumn(column: SortButtonColumn, addOrInverse?: boolean, descendingByDefault?: boolean): void;
    private isColumnsEquals;
}
