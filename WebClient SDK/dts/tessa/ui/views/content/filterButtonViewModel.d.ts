import { BaseContentItem } from './baseContentItem';
import { ContentPlaceArea } from './contentPlaceArea';
import { IWorkplaceViewComponent } from '../workplaceViewComponent';
import { IViewParameters } from '../parameters';
import { Visibility } from 'tessa/platform';
import { ViewMetadataSealed } from 'tessa/views/metadata';
export declare class FilterButtonViewModel extends BaseContentItem {
    constructor(viewComponent: IWorkplaceViewComponent, parameters: IViewParameters, viewMetadata: ViewMetadataSealed, area?: ContentPlaceArea, order?: number);
    private _toolTip;
    get toolTip(): string;
    set toolTip(value: string);
    private _visibility;
    readonly parameters: IViewParameters;
    readonly viewMetadata: ViewMetadataSealed;
    get visibility(): Visibility;
    set visibility(value: Visibility);
    get isLoading(): boolean;
    openFilter(): Promise<void>;
}
