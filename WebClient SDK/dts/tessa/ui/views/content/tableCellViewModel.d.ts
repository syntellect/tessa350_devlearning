/// <reference types="react" />
import { TableRowViewModel } from './tableRowViewModel';
import { TableColumnViewModel } from './tableColumnViewModel';
export declare class TableCellViewModel {
    readonly row: TableRowViewModel;
    readonly column: TableColumnViewModel;
    readonly value: any;
    readonly appearance: string | null;
    constructor(row: TableRowViewModel, column: TableColumnViewModel, value: any, appearance: string | null);
    private _convertedValue;
    private _toolTip;
    private _style;
    readonly maxLength: number;
    get convertedValue(): any;
    get isSelected(): boolean;
    get toolTip(): string;
    get style(): React.CSSProperties;
    set style(value: React.CSSProperties);
    private convertValue;
    private sliceValue;
    selectCell(isSelected?: boolean): void;
}
