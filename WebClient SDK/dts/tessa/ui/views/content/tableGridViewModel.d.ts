import { BaseContentItem } from './baseContentItem';
import { ContentPlaceArea } from './contentPlaceArea';
import { TableColumnViewModel } from './tableColumnViewModel';
import { TableRowViewModel } from './tableRowViewModel';
import { TableBlockViewModel } from './tableBlockViewModel';
import { IWorkplaceViewComponent } from '../workplaceViewComponent';
import { ViewColumnMetadataSealed } from 'tessa/views/metadata';
import { MenuAction } from 'tessa/ui/menuAction';
import { ColumnSettings } from 'tessa/views/workplaces/columnSettings';
export interface ITableGridMenuContext {
    readonly tableGrid: TableGridViewModel;
    readonly column: TableColumnViewModel | null;
    readonly menuActions: MenuAction[];
}
export declare class TableGridMenuContext implements ITableGridMenuContext {
    constructor(tableGrid: TableGridViewModel, column: TableColumnViewModel);
    readonly tableGrid: TableGridViewModel;
    readonly column: TableColumnViewModel | null;
    readonly menuActions: MenuAction[];
}
export declare class TableGridViewModel extends BaseContentItem {
    constructor(viewComponent: IWorkplaceViewComponent, area?: ContentPlaceArea, order?: number);
    private _columnsCache;
    private _columns;
    private _columnReaction;
    private _columnSortReaction;
    private _rows;
    private _rowsReaction;
    private _columnSettings;
    private _groupingColumn;
    private _blocks;
    private _blocksCache;
    get columns(): ReadonlyArray<TableColumnViewModel>;
    get rows(): ReadonlyArray<TableRowViewModel>;
    get blocks(): ReadonlyArray<TableBlockViewModel>;
    get multiSelect(): boolean;
    get inCellSelectionMode(): boolean;
    readonly contextMenuGenerators: ((ctx: ITableGridMenuContext) => void)[];
    get groupingColumn(): ViewColumnMetadataSealed | null;
    set groupingColumn(value: ViewColumnMetadataSealed | null);
    get columnSettings(): ColumnSettings;
    initiailize(): void;
    dispose(): void;
    private initColumns;
    private isColumnsEquals;
    private initRows;
    private initBlocks;
    private createRow;
    private createCell;
    private setSorting;
    private getColumnContextMenu;
    private getRowContextMenu;
    private executeInViewContext;
    private arrangeColumns;
    changeColumnOrderByTarget(sourceIndex: number, targetIndex: number): void;
    rebuild(): void;
}
