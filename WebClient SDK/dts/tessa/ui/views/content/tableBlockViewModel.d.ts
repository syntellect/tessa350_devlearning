export declare class TableBlockViewModel {
    readonly id: string;
    readonly caption: string;
    readonly parentBlockId: string | null;
    constructor(id: string, caption: string, parentBlockId?: string | null);
    private _isToggled;
    private _count;
    get isToggled(): boolean;
    set isToggled(value: boolean);
    get count(): number;
    set count(value: number);
    get text(): string;
    toggle(): void;
}
