import { BaseContentItem } from './baseContentItem';
import { ContentPlaceArea } from './contentPlaceArea';
import { IWorkplaceViewComponent } from '../workplaceViewComponent';
export declare class RefreshButtonViewModel extends BaseContentItem {
    constructor(viewComponent: IWorkplaceViewComponent, area?: ContentPlaceArea, order?: number);
    private _toolTip;
    get toolTip(): string;
    set toolTip(value: string);
    get isLoading(): boolean;
    refresh(): void;
}
