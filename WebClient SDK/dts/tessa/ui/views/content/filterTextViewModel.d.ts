import { BaseContentItem } from './baseContentItem';
import { ContentPlaceArea } from './contentPlaceArea';
import { IWorkplaceViewComponent } from '../workplaceViewComponent';
import { IViewParameters } from '../parameters';
import { Visibility } from 'tessa/platform';
import { ViewMetadataSealed } from 'tessa/views/metadata';
export declare class FilterTextViewModel extends BaseContentItem {
    constructor(viewComponent: IWorkplaceViewComponent, parameters: IViewParameters, viewMetadata: ViewMetadataSealed, area?: ContentPlaceArea, order?: number);
    readonly parameters: IViewParameters;
    readonly viewMetadata: ViewMetadataSealed;
    get visibility(): Visibility;
    get canOpenFilter(): boolean;
    get canClearFilter(): boolean;
    get isLoading(): boolean;
    openFilter(): Promise<void>;
    clearFilter(): void;
}
