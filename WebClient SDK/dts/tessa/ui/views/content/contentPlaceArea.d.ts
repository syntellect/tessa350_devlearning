export declare enum ContentPlaceArea {
    ToolBarPanel = 0,
    ContextPanel = 1,
    ContentPanel = 2
}
