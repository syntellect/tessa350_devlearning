declare type LinkInfo = {
    caption: string;
    link: string;
};
export declare function showLinkDialog(): Promise<LinkInfo | null>;
export {};
