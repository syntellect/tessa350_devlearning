import { IApplicationExtensionContext, IApplicationExtensionMetadataContext } from './applicationExtensionContext';
import { IExtension } from './extensions';
export interface IApplicationExtension extends IExtension {
    initialize(context: IApplicationExtensionContext): any;
    afterMetadataReceived(context: IApplicationExtensionContext): any;
    finalize(context: IApplicationExtensionContext): any;
}
export declare class ApplicationExtension implements IApplicationExtension {
    static readonly type = "ApplicationExtension";
    shouldExecute(_context: any): boolean;
    initialize(_context: IApplicationExtensionContext): void;
    afterMetadataReceived(_context: IApplicationExtensionMetadataContext): void;
    finalize(_context: IApplicationExtensionContext): void;
}
