import { ValidationStorageObject } from 'tessa/platform/validation';
import { IStorage } from 'tessa/platform/storage';
export declare class ForumSettingsModel extends ValidationStorageObject {
    constructor(storage?: IStorage);
    static readonly contentRatioKey: string;
    get contentRatio(): number | null;
    set contentRatio(value: number | null);
}
