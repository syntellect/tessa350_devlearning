export declare enum MessageTypes {
    Default = 0,
    AddUser = 1,
    RemoveUser = 2,
    AddRoles = 3,
    RemoveRoles = 4,
    LeaveUser = 5
}
export declare enum AttachmentTypes {
    File = 0,
    Link = 1,
    InnerItem = 2
}
export declare enum OutsideTypes {
    Other = 0,
    First = 1,
    Last = 2
}
export declare enum ParticipantTypes {
    Participant = 0,
    Moderator = 1,
    SuperModerator = 2,
    ParticipantFromRole = 3
}
export declare enum AttachmentStoreMode {
    Add = 0,
    Delete = 1,
    NotChanged = 2,
    Changed = 3
}
