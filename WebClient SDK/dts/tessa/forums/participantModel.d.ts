import { ParticipantTypes } from './enums';
import { ValidationStorageObject } from 'tessa/platform/validation';
import { IStorage } from 'tessa/platform/storage';
export declare class ParticipantModel extends ValidationStorageObject {
    constructor(storage?: IStorage);
    static readonly topicIdKey: string;
    static readonly userIdKey: string;
    static readonly userNameKey: string;
    static readonly readOnlyKey: string;
    static readonly subscribedKey: string;
    static readonly typeIdKey: string;
    get topicId(): guid;
    set topicId(value: guid);
    get userId(): guid | null;
    set userId(value: guid | null);
    get userName(): string | null;
    set userName(value: string | null);
    get readOnly(): boolean;
    set readOnly(value: boolean);
    get subscribed(): boolean;
    set subscribed(value: boolean);
    get typeId(): ParticipantTypes;
    set typeId(value: ParticipantTypes);
}
