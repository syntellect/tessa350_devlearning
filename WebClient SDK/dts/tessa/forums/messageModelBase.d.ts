import { MessageBody } from './messageBody';
import { ValidationStorageObject } from 'tessa/platform/validation';
import { IStorage } from 'tessa/platform/storage';
export declare class MessageModelBase extends ValidationStorageObject {
    constructor(storage?: IStorage);
    static readonly idKey: string;
    static readonly bodyKey: string;
    static readonly authorIdKey: string;
    static readonly authorNameKey: string;
    static readonly topicIdKey: string;
    get id(): guid;
    set id(value: guid);
    get body(): MessageBody;
    set body(value: MessageBody);
    get authorId(): string | null;
    set authorId(value: string | null);
    get authorName(): string | null;
    set authorName(value: string | null);
    get topicId(): guid;
    set topicId(value: guid);
    tryGetBody(): MessageBody | null | undefined;
    getMessageText(): string | null;
}
