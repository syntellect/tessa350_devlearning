declare const ShortMessagesСount = 3;
declare const MessagesInTopicСount = 3;
declare const TopicsCount = 3;
declare const AllTopicsCount = 10;
declare const FromDate: () => string;
declare const MinDateTime: () => string;
declare const MessagesCountInTopicControl = 30;
export { ShortMessagesСount, MessagesInTopicСount, TopicsCount, AllTopicsCount, FromDate, MinDateTime, MessagesCountInTopicControl };
