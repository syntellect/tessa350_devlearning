export interface IExtension {
    shouldExecute(context: any): any;
}
