import { ValidationResultBuilder } from 'tessa/platform/validation';
import { IStorage } from 'tessa/platform/storage';
export interface TessaRequestInit extends RequestInit {
    credentials: 'same-origin';
    headers: Headers;
}
export declare class ApiService {
    private constructor();
    private static _instance;
    static get instance(): ApiService;
    private _servicePath;
    initialize(path: string): void;
    getDefaultOptions(): TessaRequestInit;
    setDefaultXHROptions(xhr: XMLHttpRequest): XMLHttpRequest;
    getURL(url?: string): string;
    get(url: string, options?: RequestInit | null): Promise<any>;
    post(url: string, options?: RequestInit | null): Promise<any>;
    fetch<T = Response>(url: string, options?: RequestInit | null, getResult?: (reponse: Response) => Promise<T>): Promise<T>;
    convertFromBSON(bson: string, validationResult?: ValidationResultBuilder | null): Promise<IStorage | null>;
    convertToBSON(storage: IStorage, validationResult?: ValidationResultBuilder | null): Promise<string>;
}
