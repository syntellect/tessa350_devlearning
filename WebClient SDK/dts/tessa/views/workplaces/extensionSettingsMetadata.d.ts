import { IStorage } from 'tessa/platform/storage';
export interface IExtensionSettingsMetadata {
    data: string | null;
    storage: IStorage | null;
    seal<T = ExtensionSettingsMetadataSealed>(): T;
}
export interface ExtensionSettingsMetadataSealed {
    readonly data: string | null;
    readonly storage: IStorage | null;
    seal<T = ExtensionSettingsMetadataSealed>(): T;
}
export declare class ExtensionSettingsMetadata implements IExtensionSettingsMetadata {
    constructor();
    data: string | null;
    storage: IStorage | null;
    seal<T = ExtensionSettingsMetadataSealed>(): T;
}
