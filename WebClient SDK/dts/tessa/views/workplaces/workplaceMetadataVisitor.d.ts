import { IWorkplaceComponentMetadata, WorkplaceMetadataComponentSealed } from './workplaceMetadataComponent';
export interface IWorkplaceMetadataVisitor {
    visitEnter(component: IWorkplaceComponentMetadata | WorkplaceMetadataComponentSealed): any;
    visitLeave(component: IWorkplaceComponentMetadata | WorkplaceMetadataComponentSealed): any;
}
