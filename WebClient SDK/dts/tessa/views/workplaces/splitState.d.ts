export declare enum SplitState {
    UnSplit = 0,
    HorizontalSplit = 1,
    VerticalSplit = 2
}
