import { ITessaView } from './tessaView';
import { ViewMetadataSealed } from './metadata/viewMetadata';
export interface IViewService {
    allViews: ReadonlyArray<ITessaView>;
    getByName(name: string): ITessaView | null;
    getByNames(names: string[]): ReadonlyArray<ITessaView>;
    getByReferences(referenceName: string): ReadonlyArray<ITessaView>;
    initializeViews(serverViews: ViewMetadataSealed[]): any;
}
export declare class ViewService implements IViewService {
    private constructor();
    private static _instance;
    static get instance(): IViewService;
    private _allViews;
    private _parametersProvider;
    get allViews(): ReadonlyArray<ITessaView>;
    getByName(name: string): ITessaView | null;
    getByNames(names: string[]): ReadonlyArray<ITessaView>;
    getByReferences(referenceName: string): ReadonlyArray<ITessaView>;
    initializeViews(serverViews: ViewMetadataSealed[]): void;
}
