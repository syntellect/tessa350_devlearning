import { DbType } from 'tessa/platform';
export interface IViewColumnMetadata {
    alias: string;
    appearance: string | null;
    caption: string | null;
    convertToLocal: boolean;
    dataType: DbType | null;
    disableGrouping: boolean;
    hidden: boolean;
    localizable: boolean;
    maxLength: number;
    sortBy: string | null;
    schemeType: string | null;
    schemeTypeName: string | null;
    schemeTypeString: string | null;
    seal<T = ViewColumnMetadataSealed>(): T;
}
export interface ViewColumnMetadataSealed {
    readonly alias: string;
    readonly appearance: string | null;
    readonly caption: string | null;
    readonly convertToLocal: boolean;
    readonly dataType: DbType | null;
    readonly disableGrouping: boolean;
    readonly hidden: boolean;
    readonly localizable: boolean;
    readonly maxLength: number;
    readonly sortBy: string | null;
    readonly schemeType: string | null;
    readonly schemeTypeName: string | null;
    readonly schemeTypeString: string | null;
    seal<T = ViewColumnMetadataSealed>(): T;
}
export declare class ViewColumnMetadata implements IViewColumnMetadata {
    constructor();
    alias: string;
    appearance: string | null;
    caption: string | null;
    convertToLocal: boolean;
    dataType: DbType | null;
    disableGrouping: boolean;
    hidden: boolean;
    localizable: boolean;
    maxLength: number;
    sortBy: string | null;
    schemeType: string | null;
    schemeTypeName: string | null;
    schemeTypeString: string | null;
    seal<T = ViewColumnMetadataSealed>(): T;
}
