import { AutoCompleteInfo, AutoCompleteInfoSealed } from './autoCompleteInfo';
import { DropDownInfo, DropDownInfoSealed } from './dropDownInfo';
import { DbType } from 'tessa/platform/dbType';
export interface IViewParameterMetadata {
    alias: string;
    allowedOperands: string[] | null;
    autoCompleteInfo: AutoCompleteInfo | null;
    caption: string | null;
    convertToUtc: boolean;
    disallowedOperands: string[] | null;
    dropDownInfo: DropDownInfo | null;
    hidden: boolean;
    hideAutoCompleteButton: boolean;
    multiple: boolean;
    refSection: string | null;
    sourceViews: string[] | null;
    schemeType: string | null;
    schemeTypeName: string | null;
    schemeTypeString: string | null;
    dataType: DbType | null;
    seal<T = ViewParameterMetadataSealed>(): T;
}
export interface ViewParameterMetadataSealed {
    readonly alias: string;
    readonly allowedOperands: ReadonlyArray<string> | null;
    readonly autoCompleteInfo: AutoCompleteInfoSealed | null;
    readonly caption: string | null;
    readonly convertToUtc: boolean;
    readonly disallowedOperands: ReadonlyArray<string> | null;
    readonly dropDownInfo: DropDownInfoSealed | null;
    readonly hidden: boolean;
    readonly hideAutoCompleteButton: boolean;
    readonly multiple: boolean;
    readonly refSection: string | null;
    readonly sourceViews: ReadonlyArray<string> | null;
    readonly schemeType: string | null;
    readonly schemeTypeName: string | null;
    readonly schemeTypeString: string | null;
    readonly dataType: DbType | null;
    seal<T = ViewParameterMetadataSealed>(): T;
}
export declare class ViewParameterMetadata implements IViewParameterMetadata {
    constructor();
    alias: string;
    allowedOperands: string[] | null;
    autoCompleteInfo: AutoCompleteInfo | null;
    caption: string | null;
    convertToUtc: boolean;
    disallowedOperands: string[] | null;
    dropDownInfo: DropDownInfo | null;
    hidden: boolean;
    hideAutoCompleteButton: boolean;
    multiple: boolean;
    refSection: string | null;
    sourceViews: string[] | null;
    schemeType: string | null;
    schemeTypeName: string | null;
    schemeTypeString: string | null;
    dataType: DbType | null;
    seal<T = ViewParameterMetadataSealed>(): T;
}
