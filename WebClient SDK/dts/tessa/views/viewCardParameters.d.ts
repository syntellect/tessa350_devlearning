import { RequestParameter } from './metadata';
export declare class ViewCardParameters {
    readonly currentCardId: string;
    readonly crrentCardIdName: string;
    readonly currentCardTypeId: string;
    readonly currentCardTypeIdName: string;
    getCardIdParameter(cardId: guid, hidden?: boolean, readOnly?: boolean): RequestParameter;
    getCardTypeIdParameter(cardTypeId: guid, hidden?: boolean, readOnly?: boolean): RequestParameter;
    provideCurrentCardIdParameter(parameters: RequestParameter[], cardId: guid): void;
    provideCurrentCardTypeIdParameter(parameters: RequestParameter[], cardTypeId: guid): void;
}
