import { DbType } from 'tessa/platform/dbType';
export interface ITessaViewResult {
    columns: string[];
    dataTypes: DbType[];
    schemeTypes: string[];
    result: Map<string, any>;
    rowCount: number;
    rows: any[];
    hasTimeOut: boolean;
    getColumnIndex(columnName: string): number;
}
export declare class TessaViewResult implements ITessaViewResult {
    constructor();
    readonly columnsKey = "columns";
    readonly dataTypesKey = "datatypes";
    readonly rowCountKey = "rowcount";
    readonly rowsKey = "rows";
    readonly schemeTypesKey = "schemetypes";
    get columns(): string[];
    set columns(value: string[]);
    get dataTypes(): DbType[];
    set dataTypes(value: DbType[]);
    hasTimeOut: boolean;
    result: Map<string, any>;
    get rowCount(): number;
    set rowCount(value: number);
    get rows(): any[];
    set rows(value: any[]);
    get schemeTypes(): string[];
    set schemeTypes(value: string[]);
    getColumnIndex(columnName: string): number;
}
