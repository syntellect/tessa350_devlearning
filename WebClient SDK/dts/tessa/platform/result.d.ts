import { ValidationResult } from 'tessa/platform/validation';
export interface Result<T> {
    data: T | null;
    validationResult: ValidationResult;
}
