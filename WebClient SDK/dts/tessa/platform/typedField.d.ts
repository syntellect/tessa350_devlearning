import { DotNetType } from './dotNetType';
export declare type TypedField<T extends DotNetType = DotNetType, V = any> = {
    $type: T;
    $value: V;
};
export declare function isTypedField(object: any): object is TypedField;
export declare function createTypedField<T extends DotNetType, V>(value: V, type: T): TypedField<T, V>;
export declare function getTypedFieldValue(field: TypedField | null): any;
export declare function getTypedOrNormalValue(field: TypedField | any | null): any;
export declare function getGuidEmpty(): TypedField<DotNetType.Guid, string>;
export declare function getZero(): TypedField<DotNetType.Int32, number>;
export declare function getSystemUserId(): TypedField<DotNetType.Guid, string>;
export declare function getSystemUserName(): TypedField<DotNetType.String, string>;
export declare function isDecimalField(field: TypedField | null): boolean;
