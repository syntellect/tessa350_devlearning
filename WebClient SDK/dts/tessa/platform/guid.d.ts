export declare const Guid: {
    empty: string;
    equals: (a: string | null | undefined, b: string | null | undefined) => boolean;
    newGuid: () => string;
    isValid: (id: string | null | undefined) => boolean;
    isEmpty: (id: string | null | undefined) => boolean;
};
