export interface IStorageCleanable {
    clean(): any;
}
export declare function isIStorageCleanable(object: any): boolean;
