import { IStorage } from 'tessa/platform/storage';
export declare class ConfigurationMetadata {
    private storage;
    constructor(storage?: IStorage);
    get flags(): number;
    get isUnknown(): boolean;
    get buildVersion(): string;
    get buildName(): string;
    get buildDate(): string;
    get description(): string;
    get modified(): string;
    get modifiedByID(): string;
    get modifiedByName(): string;
    get version(): number;
    get info(): IStorage;
}
