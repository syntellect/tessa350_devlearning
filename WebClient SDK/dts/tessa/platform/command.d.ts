export declare type CommandFunc = (...args: any[]) => any;
export declare class Command {
    private _atom;
    private _executing;
    private _func;
    constructor(command?: CommandFunc | null);
    get executing(): boolean;
    get func(): CommandFunc | null;
    set func(value: CommandFunc | null);
    execute(...args: any[]): Promise<any>;
}
