import { ViewPlaceholderInfo } from './viewPlaceholderInfo';
export interface CreateInfoFunc {
    (viewAlias: string): ViewPlaceholderInfo | null;
}
export declare class ViewPlaceholderContext {
    constructor(createInfoFunc?: CreateInfoFunc | null);
    private _items;
    private _createInfoFunc;
    private _defaultViewAlias;
    get defaultViewAlias(): string | null;
    set defaultViewAlias(value: string | null);
    private createInfo;
    get(viewAlias: string): ViewPlaceholderInfo;
    tryGet(viewAlias: string): ViewPlaceholderInfo | null | undefined;
    remove(viewAlias: string): void;
    reset(): void;
}
