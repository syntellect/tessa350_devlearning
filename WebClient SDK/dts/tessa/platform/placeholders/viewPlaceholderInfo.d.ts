import { SortingColumn } from 'tessa/views';
import { RequestParameter } from 'tessa/views/metadata';
import { SerializationTransform } from 'tessa/platform/serialization';
export declare function asJSONString(transformAction: () => SerializationTransform): () => SerializationTransform;
export declare class ViewPlaceholderInfo {
    constructor();
    private _currentPage;
    private _pageLimit;
    parameters: RequestParameter[];
    sortingColumns: SortingColumn[];
    get currentPage(): number;
    set currentPage(value: number);
    get pageLimit(): number;
    set pageLimit(value: number);
}
