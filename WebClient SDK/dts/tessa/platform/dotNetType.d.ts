export declare enum DotNetType {
    Object = "System.Object",
    String = "System.String",
    Boolean = "System.Boolean",
    Int32 = "System.Int32",
    Int64 = "System.Int64",
    Guid = "System.Guid",
    Double = "System.Double",
    Decimal = "System.Decimal",
    DateTime = "System.DateTime",
    DateTimeOffset = "System.DateTimeOffset",
    Binary = "System.Byte[]"
}
