import { ConfigurationMetadata } from 'tessa/platform/configurationMetadata';
import { ServiceWorkerMetadata, DeskiMetadata } from 'tessa/platform';
export interface CommonMetadata {
    cryptoProPluginEnabled: boolean;
    maxWallpaperSizeB: number;
    previewPdfEnabled: boolean;
    maxFileSizeB: number;
    denyFileDownload: boolean;
    createBasedOnTypes: ReadonlyArray<guid>;
    configuration: ConfigurationMetadata;
    serviceWorkerInfo: ServiceWorkerMetadata;
    deskiInfo: DeskiMetadata;
}
