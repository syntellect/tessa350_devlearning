import { ValidationResultItem } from './validationResultItem';
import { ValidationResultType } from './validationResultType';
import { ValidationLevel } from './validationLevel';
export declare class ValidationResult {
    constructor(items: ValidationResultItem[]);
    readonly items: ReadonlyArray<ValidationResultItem>;
    get hasErrors(): boolean;
    get hasWarnings(): boolean;
    get hasInfo(): boolean;
    get isSuccessful(): boolean;
    static get empty(): ValidationResult;
    format(): string;
    toString(level?: ValidationLevel): string;
    static fromError(error: Error | any, warning?: boolean): ValidationResult;
    static fromText(text: string, type?: ValidationResultType): ValidationResult;
}
