import { ValidationResultType } from './validationResultType';
import { ValidationLevel } from './validationLevel';
export interface IValidationResultItem {
    readonly type: ValidationResultType;
    readonly message: string;
    readonly fieldName: string;
    readonly objectName: string;
    readonly objectType: string;
    readonly details: string;
}
export declare class ValidationResultItem implements IValidationResultItem {
    constructor(args: {
        type: ValidationResultType;
        message: string;
        fieldName?: string;
        objectName?: string;
        objectType?: string;
        details?: string;
    });
    readonly type: ValidationResultType;
    readonly message: string;
    readonly fieldName: string;
    readonly objectName: string;
    readonly objectType: string;
    readonly details: string;
    toString(level?: ValidationLevel): string;
    private static getMessage;
}
