import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
export declare class TableControlDoubleClickUIExtension extends CardUIExtension {
    initialized(context: ICardUIExtensionContext): void;
}
