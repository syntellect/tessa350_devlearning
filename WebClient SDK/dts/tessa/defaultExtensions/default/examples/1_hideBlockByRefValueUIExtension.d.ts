import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
/**
 * В зависимости от значения ссылочного поля карточки:
 * - скрываем\показываем элементы управления (блок, контрол)
 * - делаем рид-онли\редактируемыми элементы управления (контрол)
 *
 * Если поле "Валюта" не пустое, то блок с файлами становится видимым, контрол "Автор" скрывается,
 * а контрол "Контрагент" становится только для чтения.
 * Если поле "Валюта" пустое, то блок с файлами скрывается, контрол "Автор" становится видимым,
 * а контрол "Контрагент" становится редактируемым.
 */
export declare class HideBlockByRefValueUIExtension extends CardUIExtension {
    initialized(context: ICardUIExtensionContext): void;
    private static hideBlock;
}
