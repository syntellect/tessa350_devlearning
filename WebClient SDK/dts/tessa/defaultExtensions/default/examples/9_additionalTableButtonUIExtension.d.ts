import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
/**
 * Добавляем дополнительную кнопку справа под контролом таблицы.
 * При нажатии на кнопку, мы копируем выбранную строку и добавляем в секцию.
 */
export declare class AdditionalTableButtonUIExtension extends CardUIExtension {
    private _disposer;
    initialized(context: ICardUIExtensionContext): void;
    finalized(): void;
}
