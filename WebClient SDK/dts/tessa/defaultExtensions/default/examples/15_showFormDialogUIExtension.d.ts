import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
/**
 * Показываем диалог с формой
 */
export declare class ShowFormDialogUIExtension extends CardUIExtension {
    initialized(context: ICardUIExtensionContext): void;
    private static showFormDialog;
}
