import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
/**
 * Скрывать\показывать какой-то элемент управления в задании в зависимости от данных задания.
 *
 * В задаче при варианте заверешения отправки на исполнение проверяем комментарий задания.
 * Если комментарий есть, то скрываем контрол "Вернуть на роль".
 * Если комментария нет, то показываем контрол "Вернуть на роль".
 */
export declare class HideTaskBlockUIExtension extends CardUIExtension {
    initialized(context: ICardUIExtensionContext): void;
    private static modifyWorkspace;
}
