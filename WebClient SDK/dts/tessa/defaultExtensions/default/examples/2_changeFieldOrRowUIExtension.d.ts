import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
/**
 * В зависимости от значения флажка на форме карточки менять другие поля карточки:
 * - в текстовое поле
 * - добавлять строчку в коллекционную секцию
 *
 * Когда жмакают галку "Базовый цвет", то:
 * - если галка установлена, то значение поля "Цвет" меняется на "Это базовый цвет"
 * - если галка установлена, то в секцию "Исполнители" добавляется новый сотрудник "Admin"
 * - если галка не установлена, то занчение поля "Цвет" меняется на "Это другой, не базовый цвет"
 */
export declare class ChangeFieldOrRowUIExtension extends CardUIExtension {
    initialized(context: ICardUIExtensionContext): void;
}
