import { ApplicationExtension, IApplicationExtensionMetadataContext } from 'tessa';
import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
export declare class CustomThemePropApplicationExtension extends ApplicationExtension {
    afterMetadataReceived(_context: IApplicationExtensionMetadataContext): void;
}
export declare class CustomThemePropUIExtension extends CardUIExtension {
    initialized(context: ICardUIExtensionContext): void;
    private changeCaptionStyle;
}
