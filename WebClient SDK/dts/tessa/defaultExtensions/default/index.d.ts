import './bundleRegistrator';
import './cards/registrator';
import './deski/registrator';
import './documents/registrator';
import './eds/registrator';
import './externalFiles/registrator';
import './files/registrator';
import './forums/registrator';
import './tiles/registrator';
import './ui/krProcess/registrator';
import './ui/registrator';
import './views/registrator';
import './workflow/krPermissions/registrator';
import './workflow/krProcess/registrator';
import './workflow/wf/registrator';
import './workplaces/registrator';
/**
 * uncomment for examples extensions
 */
