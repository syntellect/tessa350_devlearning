import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext, StageTypeHandlerDescriptor } from 'tessa/workflow/krProcess';
export declare class KrUniversalTaskStageTypeUIHandler extends KrStageTypeUIHandler {
    descriptors(): StageTypeHandlerDescriptor[];
    initialize(context: IKrStageTypeUIHandlerContext): void;
    finalize(context: IKrStageTypeUIHandlerContext): void;
    private rowInvoked;
    private rowClosing;
}
