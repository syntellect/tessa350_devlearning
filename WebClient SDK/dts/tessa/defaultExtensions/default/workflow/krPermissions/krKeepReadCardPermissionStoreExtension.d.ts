import { ICardStoreExtensionContext, CardStoreExtension } from 'tessa/cards/extensions';
export declare class KrKeepReadCardPermissionStoreExtension extends CardStoreExtension {
    beforeRequest(context: ICardStoreExtensionContext): void;
}
