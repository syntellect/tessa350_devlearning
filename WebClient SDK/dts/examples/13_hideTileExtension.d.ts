import { TileExtension, ITileLocalExtensionContext } from 'tessa/ui/tiles';
/**
 * Скрываем тайл "Сохранить" для тестовых карточек
 */
export declare class HideTileExtension extends TileExtension {
    initializingLocal(context: ITileLocalExtensionContext): void;
}
