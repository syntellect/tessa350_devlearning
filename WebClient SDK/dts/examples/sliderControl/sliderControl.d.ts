import * as React from 'react';
import { SliderViewModel } from './sliderViewModel';
import { ControlProps } from 'tessa/ui/cards/components/controls';
export declare class SliderControl extends React.Component<ControlProps<SliderViewModel>> {
    private _inputRef;
    constructor(props: ControlProps<SliderViewModel>);
    componentDidMount(): void;
    componentWillUnmount(): void;
    componentDidUpdate(prevProps: ControlProps<SliderViewModel>): void;
    render(): JSX.Element | null;
    focus(opt?: FocusOptions): void;
    private handleChange;
    private handleFocus;
    private handleBlur;
}
export declare const StyledInput: any;
