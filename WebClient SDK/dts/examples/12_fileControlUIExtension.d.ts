import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
/**
 * В выбранном файловом контроле показываем только определенные файлы.
 * Разрешаем добавлять файлы только с определенным расширением.
 */
export declare class FileControlUIExtension extends CardUIExtension {
    initialized(context: ICardUIExtensionContext): void;
}
