import { CardStoreExtension, ICardStoreExtensionContext } from 'tessa/cards/extensions';
import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
export declare class CloseCardOnCompleteTaskStoreExtension extends CardStoreExtension {
    private _taskComplete;
    beforeRequest(context: ICardStoreExtensionContext): void;
    afterRequest(context: ICardStoreExtensionContext): void;
}
export declare class CloseCardOnCompleteTaskUIExtension extends CardUIExtension {
    reopening(context: ICardUIExtensionContext): void;
}
