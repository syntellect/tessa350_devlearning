import { TessaFile, FileCategory, FileType, IFileSource, FilePermissionsSealed, IFile } from 'tessa/files';
export declare class ExternalFile extends TessaFile {
    constructor(id: guid, name: string, category: FileCategory | null, type: FileType, source: IFileSource, permissions: FilePermissionsSealed | null, isLocal?: boolean, origin?: IFile | null, description?: string);
    description: string;
}
