# О репозитории
Этот репозиторий используется для обучения разработчиков на базе платформы [Tessa 3.5.0](https://mytessa.ru/docs/ReleaseNotes.html)

# Ссылки
[Discord](https://discord.gg/K3Epycs)
[Site](https://mytessa.ru)
[Docs](https://mytessa.ru/system/docs/)