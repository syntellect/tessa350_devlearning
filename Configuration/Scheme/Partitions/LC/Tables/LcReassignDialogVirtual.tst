﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="7cbebd3f-f8c0-4b69-8713-f0eeda3b0c67" ID="8132806a-ca55-4dbe-959e-7d3319c586ae" Name="LcReassignDialogVirtual" Group="LC" IsVirtual="true" InstanceType="Cards" ContentType="Entries">
	<Description>Параметры переназначения задания.</Description>
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="8132806a-ca55-00be-2000-0d3319c586ae" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="8132806a-ca55-01be-4000-0d3319c586ae" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemeComplexColumn ID="9961a0dc-358a-4464-a666-88a7490fe3d8" Name="Card" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<Description>Карточка содержащая переназначаемое задание.</Description>
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="9961a0dc-358a-0064-4000-08a7490fe3d8" Name="CardID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
		<SchemePhysicalColumn ID="0eade842-2381-41bb-a10a-332d4366a18b" Name="CardDescription" Type="String(250) Null" />
	</SchemeComplexColumn>
	<SchemeComplexColumn ID="0664b327-1263-4c60-8db3-b4051577f5a9" Name="Task" Type="Reference(Typified) Not Null" ReferencedTable="5bfa9936-bb5a-4e8f-89a9-180bfd8f75f8">
		<Description>Переназначаемое задание.</Description>
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="0664b327-1263-0060-4000-04051577f5a9" Name="TaskID" Type="Guid Not Null" ReferencedColumn="5bfa9936-bb5a-008f-3100-080bfd8f75f8" />
		<SchemeReferencingColumn ID="a6fb18dd-1a67-4688-9ee3-1c765cfc9d70" Name="TaskDigest" Type="String(4000) Null" ReferencedColumn="653735ee-cb40-402e-a233-ec908031dfe3" />
	</SchemeComplexColumn>
	<SchemeComplexColumn ID="67bd551c-4533-4268-a195-5dd1f9e5d966" Name="ReassignTo" Type="Reference(Typified) Not Null" ReferencedTable="81f6010b-9641-4aa5-8897-b8e8603fbf4b">
		<Description>Роль на которую должно быть переназначено задание.</Description>
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="67bd551c-4533-0068-4000-0dd1f9e5d966" Name="ReassignToID" Type="Guid Not Null" ReferencedColumn="81f6010b-9641-01a5-4000-08e8603fbf4b" />
		<SchemeReferencingColumn ID="4de59135-b195-4bb0-b1cf-2616529296cf" Name="ReassignToName" Type="String(128) Not Null" ReferencedColumn="616d6b2e-35d5-424d-846b-618eb25962d0" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn ID="ce53fd26-2b83-4ba7-85cf-c45a5e11abd7" Name="Comment" Type="String(4000) Null">
		<Description>Комментарий указываемый при переназначении.</Description>
	</SchemePhysicalColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="8132806a-ca55-00be-5000-0d3319c586ae" Name="pk_LcReassignDialogVirtual" IsClustered="true">
		<SchemeIndexedColumn Column="8132806a-ca55-01be-4000-0d3319c586ae" />
	</SchemePrimaryKey>
</SchemeTable>