﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="7cbebd3f-f8c0-4b69-8713-f0eeda3b0c67" ID="5ec22560-9247-4152-8f4f-881b7ec074e4" Name="AdditionalApprovers" Group="LC" InstanceType="Cards" ContentType="Collections">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="5ec22560-9247-0052-2000-081b7ec074e4" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="5ec22560-9247-0152-4000-081b7ec074e4" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="5ec22560-9247-0052-3100-081b7ec074e4" Name="RowID" Type="Guid Not Null" />
	<SchemeComplexColumn ID="f9a7991d-c677-4663-9580-d136a918e038" Name="User" Type="Reference(Typified) Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="f9a7991d-c677-0063-4000-0136a918e038" Name="UserID" Type="Guid Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="3decd4a2-34bf-45ed-883e-6a5c7564f07a" Name="UserName" Type="String(128) Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="5ec22560-9247-0052-5000-081b7ec074e4" Name="pk_AdditionalApprovers">
		<SchemeIndexedColumn Column="5ec22560-9247-0052-3100-081b7ec074e4" />
	</SchemePrimaryKey>
	<SchemeIndex IsSystem="true" IsPermanent="true" IsSealed="true" ID="5ec22560-9247-0052-7000-081b7ec074e4" Name="idx_AdditionalApprovers_ID" IsClustered="true">
		<SchemeIndexedColumn Column="5ec22560-9247-0152-4000-081b7ec074e4" />
	</SchemeIndex>
</SchemeTable>