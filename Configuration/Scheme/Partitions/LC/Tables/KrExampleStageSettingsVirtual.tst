﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="7cbebd3f-f8c0-4b69-8713-f0eeda3b0c67" ID="4a3da8dc-6b5e-4528-842b-c1fc5a296a5a" Name="KrExampleStageSettingsVirtual" Group="KrStageTypes" IsVirtual="true" InstanceType="Cards" ContentType="Entries">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="4a3da8dc-6b5e-0028-2000-01fc5a296a5a" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="4a3da8dc-6b5e-0128-4000-01fc5a296a5a" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn ID="c4bda777-1b64-4f30-b818-9f2c0b26b7ae" Name="Digest" Type="String(Max) Null" />
	<SchemePhysicalColumn ID="1aaddf30-a783-4ec3-8126-d10c35e7955d" Name="Script" Type="String(Max) Null" />
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="4a3da8dc-6b5e-0028-5000-01fc5a296a5a" Name="pk_KrExampleStageSettingsVirtual" IsClustered="true">
		<SchemeIndexedColumn Column="4a3da8dc-6b5e-0128-4000-01fc5a296a5a" />
	</SchemePrimaryKey>
</SchemeTable>