﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="7cbebd3f-f8c0-4b69-8713-f0eeda3b0c67" ID="1f39a447-09a2-49c6-ae9e-00c9c136e733" Name="Approvers" Group="LC" InstanceType="Cards" ContentType="Collections">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="1f39a447-09a2-00c6-2000-00c9c136e733" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="1f39a447-09a2-01c6-4000-00c9c136e733" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="1f39a447-09a2-00c6-3100-00c9c136e733" Name="RowID" Type="Guid Not Null" />
	<SchemeComplexColumn ID="4ed886e5-faf5-411a-9e7d-e8fe9155578a" Name="User" Type="Reference(Typified) Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="4ed886e5-faf5-001a-4000-08fe9155578a" Name="UserID" Type="Guid Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="18f1276c-4602-468a-885d-3e17a207e477" Name="UserName" Type="String(128) Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="1f39a447-09a2-00c6-5000-00c9c136e733" Name="pk_Approvers">
		<SchemeIndexedColumn Column="1f39a447-09a2-00c6-3100-00c9c136e733" />
	</SchemePrimaryKey>
	<SchemeIndex IsSystem="true" IsPermanent="true" IsSealed="true" ID="1f39a447-09a2-00c6-7000-00c9c136e733" Name="idx_Approvers_ID" IsClustered="true">
		<SchemeIndexedColumn Column="1f39a447-09a2-01c6-4000-00c9c136e733" />
	</SchemeIndex>
</SchemeTable>