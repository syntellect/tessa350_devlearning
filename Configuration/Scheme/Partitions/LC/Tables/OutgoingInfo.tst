﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="7cbebd3f-f8c0-4b69-8713-f0eeda3b0c67" ID="1cf8ee1f-0d2e-47a7-a4a0-7c690cf46013" Name="OutgoingInfo" Group="LC" InstanceType="Cards" ContentType="Entries">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="1cf8ee1f-0d2e-00a7-2000-0c690cf46013" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="1cf8ee1f-0d2e-01a7-4000-0c690cf46013" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn ID="6f7cb801-0694-4bed-ace8-d7b268555444" Name="IsImportant" Type="Boolean Not Null">
		<SchemeDefaultConstraint IsPermanent="true" ID="da7351ac-ea77-4943-9844-b8d25a619bd9" Name="df_OutgoingInfo_IsImportant" Value="false" />
	</SchemePhysicalColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="1cf8ee1f-0d2e-00a7-5000-0c690cf46013" Name="pk_OutgoingInfo" IsClustered="true">
		<SchemeIndexedColumn Column="1cf8ee1f-0d2e-01a7-4000-0c690cf46013" />
	</SchemePrimaryKey>
</SchemeTable>