﻿#tessa_exchange_format(Version:1, CreatorName:System, CreationTime:2020-01-01T00\:00\:00) {
	#exchange_view(RowID:2c0b6a4a-8759-43d1-b23c-0c64f365d343, Alias:KrTypesForDialogs, Caption:KrTypesForDialogs, ModifiedById:11111111-1111-1111-1111-111111111111, ModifiedByName:System, FormatVersion:2, ModifiedDateTime:01.01.2020 0\:00\:00, GroupName:Kr Wf) {
		#metadata {
			#view(DefaultSortColumn: TypeCaption, DefaultSortDirection: asc, Paging: always, RowCountSubset: Count, QuickSearchParam: Caption)
			#column(Alias: TypeID, Hidden: true, Type: $Types.ID)
			#column(Alias: TypeCaption, Caption: $Views_Types_Name, Type: $Types.Caption, Localizable: true, SortBy: t.LocalizedCaption)
			#column(Alias: TypeName, Caption: $Views_Types_Alias, Type: $Types.Name, SortBy: t.TypeName)
			#column(Alias: IsDocTypeCaption, Caption: $Views_KrTypes_Type, Type: String, Localizable: true)
			#column(Alias: State, Caption: $Views_KrTypes_State, Type: String, Localizable: true)
			#column(Alias: ParentType, Caption: $Views_KrTypes_ParentType, Type: $KrSettingsCardTypes.CardTypeCaption, Localizable: true)
			#column(Alias: LocalizedCaption, Hidden: true, Type: String)
			#column(Alias: rn, Hidden: true, Type: Int64)
			#param(Alias: Caption, Caption: $Views_Types_Name_Param, Type: String, Multiple: true)
			#param(Alias: Name, Caption: $Views_Types_Alias_Param, Multiple: true, Type: $Types.Name)
			#param(Alias: NameOrCaption, Caption: NameOrCaption, Hidden: true, Multiple: true, Type: String)
			#reference(ColPrefix: Type, RefSection: KrTypesForDialogs, DisplayValueColumn: TypeCaption, IsCard: false, OpenOnDoubleClick: false)
			#subset(Alias: Count)
		}
		#description {
			В общем случае копирует логику представления Types\, однако по набору параметров и логике отбора адаптировано для использования в системе маршрутов.
			Если бы эти особенности встраивались в оригинальное Types\, то имели бы место сложные вложенные \#if.
		}
		#ms_query {
			SELECT
				[t].*
			FROM \(
				SELECT
					\#if\(Normal\) \{
					[t].[TypeID]\,
					[t].[TypeCaption]\,
					[t].[TypeName]\,
					[t].[IsDocTypeCaption]\,
					[t].[State]\,
					[t].[ParentType]\,
					\#if\(request.SortedBy\("TypeCaption"\)\) \{
					[t].[LocalizedCaption]\,
					\}
					row_number\(\) OVER \(ORDER BY \#order_by\)		AS [rn]
					\}
					\#if\(Count\) \{
					count\(*\) AS [cnt]
					\}
				FROM \(
					SELECT
						\#if\(Normal\) \{
						[t].[ID]								AS [TypeID]\,
						[t].[Caption]							AS [TypeCaption]\,
						[t].[Name]								AS [TypeName]\,
						N'$Views_KrTypes_CardType_Sql'			AS [IsDocTypeCaption]\,
						N'$Views_KrTypes_CardTypeDoesntUseStandardSolution_Sql'
																AS [State]\,
						N''										AS [ParentType]\,
						\#if\(request.SortedBy\("TypeCaption"\)\) \{
						[t].[LocalizedCaption]\,
						\}
						\}
						[t].[TypeInstance]
					FROM \(
						SELECT
							\#if\(Normal\) \{
							[t].[ID]\,
							[t].[Caption]\,
							[t].[Name]\,
							\#if\(request.SortedBy\("TypeCaption"\)\) \{
							[lCaption].[Value] AS [LocalizedCaption]\,
							\}
							\}
							-- запрос выполняется примерно в 2 раза быстрее\, если флаги сначала вернуть в SELECT во внутреннем запросе\, а затем проверить в WHERE во внешнем запросе
							[t].[Definition].value\('\(/cardType/@type\)[1]'\, 'int'\)				AS [TypeInstance]
						FROM [Types] AS [t] WITH \(NOLOCK\)
						LEFT JOIN [KrDocType] AS [dt] WITH\(NOLOCK\) ON [t].[ID] = [dt].[CardTypeID]
						\#if\(Caption || Normal && request.SortedBy\("TypeCaption"\)\) \{
						CROSS APPLY [Localization]\([t].[Caption]\, \#param\(locale\)\) AS [lCaption]
						\}
						WHERE 1 = 1
							\#param\(Name\, [t].[Name]\)
							\#param\(Caption\, [lCaption].[Value]\)
							and \(1=1 \#param\(NameOrCaption\, "t"."Name"\) or 1=1 \#param\(NameOrCaption\, "lCaption"."Value"\)\)
							and [dt].[ID] is null
						\) AS [t]
					WHERE
						[t].[TypeInstance] = 0		-- тип карточки\, а не файла или задания\#
					
					UNION ALL
					
					SELECT
						\#if\(Normal\) \{
						[t].[ID]							AS [TypeID]\,
						[t].[Title]							AS [TypeCaption]\,
						[t].[CardTypeName]					AS [TypeName]\,
						N'$Views_KrTypes_DocType_Sql'		AS [IsDocTypeCaption]\,
						CASE [kr].[UseDocTypes]
							WHEN 0 THEN N'$Views_KrTypes_ParentCardUseDocTypes_Sql'
							WHEN 1 THEN N'$Views_KrTypes_SolutionParentDocType_Sql'
						END									AS [State]\,
						[kr].[CardTypeCaption]				AS [ParentType]\,
						\#if\(request.SortedBy\("TypeCaption"\)\) \{
						[lTitle].[Value]					AS [LocalizedCaption]\,
						\}
						\}
						0									AS [TypeInstance]
					FROM [KrDocType] AS [t] WITH \(NOLOCK\)
					INNER JOIN [KrSettingsCardTypes] AS [kr] WITH \(NOLOCK\)
						ON [kr].[CardTypeID] = [t].[CardTypeID]
					\#if\(Caption || Normal && request.SortedBy\("TypeCaption"\)\) \{
					CROSS APPLY [Localization]\([t].[Title]\, \#param\(locale\)\) AS [lTitle]
					\}
					WHERE 1 = 1
						\#param\(Name\, [t].[CardTypeName]\)
						\#param\(Caption\, [lTitle].[Value]\)
						and \(1=1 \#param\(NameOrCaption\, "t"."CardTypeName"\) or 1=1 \#param\(NameOrCaption\, "lTitle"."Value"\)\)
					\) AS [t]
				\) AS [t]
			\#if\(PageOffset\) \{
			WHERE [t].[rn] >= \#param\(PageOffset\) AND [t].[rn] < \(\#param\(PageOffset\) + \#param\(PageLimit\)\)
			\}
			\#if\(Normal\) \{
			ORDER BY [t].[rn]
			\}
		}
		#pg_query {
			SELECT
				"t".*
			FROM \(
				SELECT
					\#if\(Normal\) \{
					"t"."TypeID"\,
					"t"."TypeCaption"\,
					"t"."TypeName"\,
					"t"."IsDocTypeCaption"\,
					"t"."State"\,
					"t"."ParentType"\,
					\#if\(request.SortedBy\("TypeCaption"\)\) \{
					"t"."LocalizedCaption"\,
					\}
					0\:\:int8										AS "rn"
					\}
					\#if\(Count\) \{
					count\(*\) AS "cnt"
					\}
				FROM \(
					SELECT
						\#if\(Normal\) \{
						"t"."ID"								AS "TypeID"\,
						"t"."Caption"							AS "TypeCaption"\,
						"t"."Name"								AS "TypeName"\,
						'$Views_KrTypes_CardType_Sql'			AS "IsDocTypeCaption"\,
						'$Views_KrTypes_CardTypeDoesntUseStandardSolution_Sql'
																AS "State"\,
						''										AS "ParentType"\,
						\#if\(request.SortedBy\("TypeCaption"\)\) \{
						"t"."LocalizedCaption"\,
						\}
						\}
						"t"."TypeInstance"
					FROM \(
						SELECT
							\#if\(Normal\) \{
							"t"."ID"\,
							"t"."Caption"\,
							"t"."Name"\,
							\#if\(request.SortedBy\("TypeCaption"\)\) \{
							"lCaption"."Value" AS "LocalizedCaption"\,
							\}
							\}
							-- запрос выполняется примерно в 2 раза быстрее\, если флаги сначала вернуть в SELECT во внутреннем запросе\, а затем проверить в WHERE во внешнем запросе
							\(\(xpath\('/cardType/@type'\, "t"."Definition"\)\:\:text[]\)\:\:int[]\)[1]	AS "TypeInstance"
						FROM "Types" AS "t"
						LEFT JOIN "KrDocType" AS "dt" ON "t"."ID" = "dt"."CardTypeID"
						\#if\(Caption || Normal && request.SortedBy\("TypeCaption"\)\) \{
						CROSS JOIN "Localization"\("t"."Caption"\, \#param\(locale\)\) AS "lCaption"
						\}
						WHERE true
							\#param\(Name\, "t"."Name"\)
							\#param\(Caption\, "lCaption"."Value"\)
							and \(true \#param\(NameOrCaption\, "t"."Name"\) or true \#param\(NameOrCaption\, "lCaption"."Value"\)\)
							and "dt"."ID" is null
						\) AS "t"
					WHERE
						"t"."TypeInstance" = 0		-- тип карточки\, а не файла или задания\#
					
					UNION ALL
					
					SELECT
						\#if\(Normal\) \{
						"t"."ID"							AS "TypeID"\,
						"t"."Title"							AS "TypeCaption"\,
			            "t"."CardTypeName"					AS "TypeName"\,
						'$Views_KrTypes_DocType_Sql'		AS "IsDocTypeCaption"\,
						CASE "kr"."UseDocTypes"
							WHEN false THEN '$Views_KrTypes_ParentCardUseDocTypes_Sql'
							WHEN true THEN '$Views_KrTypes_SolutionParentDocType_Sql'
						END									AS "State"\,
						"kr"."CardTypeCaption"				AS "ParentType"\,
						\#if\(request.SortedBy\("TypeCaption"\)\) \{
						"lTitle"."Value"					AS "LocalizedCaption"\,
						\}
						\}
						0									AS "TypeInstance"
					FROM "KrDocType" AS "t"
					INNER JOIN "KrSettingsCardTypes" AS "kr"
						ON "kr"."CardTypeID" = "t"."CardTypeID"
					\#if\(Caption || Normal && request.SortedBy\("TypeCaption"\)\) \{
					CROSS JOIN "Localization"\("t"."Title"\, \#param\(locale\)\) AS "lTitle"
					\}
					WHERE true
						\#param\(Name\, "t"."Name"\)
						\#param\(Caption\, "lTitle"."Value"\)
						and \(true \#param\(NameOrCaption\, "t"."CardTypeName"\) or true \#param\(NameOrCaption\, "lTitle"."Value"\)\)
					\) AS "t"
				\) AS "t"
			\#if\(Normal\) \{
			ORDER BY \#order_by
			\}
			\#if\(PageOffset\) \{
			OFFSET \#param\(PageOffset\) - 1 LIMIT \#param\(PageLimit\)
			\}
		}
		#role(RoleID:7ff52dc0-ff6a-4c9d-ba25-b562c370004d, ViewID:2c0b6a4a-8759-43d1-b23c-0c64f365d343) 
	}
}