﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.ComponentModel;
using Tessa.Json;
using Tessa.Platform;
using Tessa.Platform.ConsoleApps;
using Tessa.Platform.IO;
using Tessa.Platform.Json;
using Tessa.Platform.Storage;

namespace Tessa.Extensions.Default.Console.ConvertConfiguration
{
    public sealed class Operation :
        ConsoleOperation<OperationContext>
    {
        #region ConversionItem Private Class

        private sealed class ConversionItem
        {
            public ConversionItem(string oldPath, string newPath, object obj)
            {
                this.OldPath = oldPath;
                this.NewPath = newPath;
                this.Object = obj;
            }

            public string OldPath { get; }

            public string NewPath { get; }

            public object Object { get; }
        }

        #endregion

        #region Constructors

        public Operation(
            ConsoleSessionManager sessionManager,
            IConsoleLogger logger)
            : base(logger, sessionManager)
        {
        }

        #endregion

        #region Private Methods

        private static async Task ConvertCardAsync(string oldPath, string newPath, bool downgrade, CancellationToken cancellationToken = default)
        {
            await using FileStream sourceStream = FileHelper.OpenRead(oldPath, synchronousOnly: true);
            await using FileStream targetStream = FileHelper.Create(newPath);
            if (downgrade)
            {
                await ConvertCardToBinaryAsync(sourceStream, targetStream, cancellationToken);
            }
            else
            {
                await ConvertCardToJsonAsync(sourceStream, targetStream, cancellationToken);
            }
        }

        private static async Task ConvertCardToJsonAsync(Stream sourceStream, Stream targetStream, CancellationToken cancellationToken = default)
        {
            var cardReader = new CardReader(sourceStream);
            CardHeader header = await cardReader.ReadHeaderAsync(cancellationToken);
            CardStoreRequest request = await cardReader.ReadCardStoreRequestAsync(cancellationToken);

            var result = new List<object> { request.GetStorage() };

            foreach (CardHeaderFile headerFile in header.GetOrderedFiles())
            {
                await using SubStream contentStream = cardReader.ReadStream(headerFile.Size);
                byte[] bytes = await contentStream.ReadAllBytesAsync(cancellationToken);

                result.Add(
                    new Dictionary<string, object>(StringComparer.Ordinal)
                    {
                        { CardComponentHelper.ContentFileIDKey, headerFile.ID },
                        { CardComponentHelper.ContentFileSizeKey, headerFile.Size },
                        { CardComponentHelper.ContentFileDataKey, bytes },
                    });
            }

            await using var writer = new StreamWriter(targetStream, Encoding.UTF8, FileHelper.DefaultBufferSize, leaveOpen: true) { NewLine = "\n" };
            using var jsonWriter = new JsonTextWriter(writer) { Formatting = Formatting.Indented };
            TessaSerializer.JsonTyped.Serialize(jsonWriter, result);
        }

        private static async Task ConvertCardToBinaryAsync(Stream sourceStream, Stream targetStream, CancellationToken cancellationToken = default)
        {
            using var reader = new StreamReader(sourceStream, Encoding.UTF8, true, FileHelper.DefaultBufferSize, true);
            using var jsonReader = new JsonTextReader(reader);
            var storage = TessaSerializer.JsonTyped.Deserialize<List<object>>(jsonReader);
            if (storage.Count == 0
                || !(storage[0] is Dictionary<string, object> requestStorage))
            {
                // это такая же критичная ошибка, как невозможность прочитать из стрима, поэтому кидаем исключение
                throw new InvalidOperationException($"Can't read card as {CardFileFormat.Json} format from specified stream.");
            }

            var storeRequest = new CardStoreRequest(requestStorage);

            var header = new CardHeader();
            var fileStreams = new Dictionary<Guid?, Func<Stream>>();

            for (int i = 1; i < storage.Count; i++)
            {
                if (storage[i] is Dictionary<string, object> fileStorage)
                {
                    Guid? rowID = fileStorage.TryGet<Guid?>(CardComponentHelper.ContentFileIDKey);
                    long? size = fileStorage.TryGet<long?>(CardComponentHelper.ContentFileSizeKey);
                    byte[] content = fileStorage.TryGet<byte[]>(CardComponentHelper.ContentFileDataKey);

                    if (rowID.HasValue && size.HasValue && content != null)
                    {
                        CardHeaderFile file = header.Files.Add(rowID.Value);
                        file.Size = size.Value;
                        file.Order = header.Files.Count - 1;

                        fileStreams.Add(rowID, () => new MemoryStream(content));
                    }
                }
            }

            var writer = new CardWriter(targetStream);
            await writer.WriteAsync(header, cancellationToken);
            await writer.WriteAsync(storeRequest, cancellationToken);

            foreach (CardHeaderFile headerFile in header.GetOrderedFiles())
            {
                await writer.WriteAsync(fileStreams[headerFile.ID](), cancellationToken);
            }
        }

        private async Task<ConversionItem> ReadForConvertAsync(string extension, string filePath)
        {
            switch (extension)
            {
                case ".tct":
                    await this.Logger.InfoAsync("Reading type from: \"{0}\"", filePath);

                    var cardType = new CardType();
                    await using (FileStream fileStream = FileHelper.OpenRead(filePath, synchronousOnly: true))
                    {
                        cardType.DeserializeFromXml(fileStream);
                    }

                    string newTypeFilePath = ChangeExtension(filePath, ".tct", ".jtype");
                    return new ConversionItem(filePath, newTypeFilePath, cardType);

                case ".cardlib":
                    await this.Logger.InfoAsync("Reading card library from: \"{0}\"", filePath);

                    var cardLibrary = new CardLibrary();
                    await using (FileStream fileStream = FileHelper.OpenRead(filePath, synchronousOnly: true))
                    {
                        cardLibrary.DeserializeFromXml(fileStream);
                    }

                    return new ConversionItem(filePath, filePath, cardLibrary);

                case ".card":
                    await this.Logger.InfoAsync("Card is pending to convert: \"{0}\"", filePath);
                    string newCardTypePath = ChangeExtension(filePath, ".card", ".jcard");
                    return new ConversionItem(filePath, newCardTypePath, new Card());

                default:
                    return null;
            }
        }

        private async Task<ConversionItem> ReadForDowngradeAsync(string extension, string filePath, CancellationToken cancellationToken = default)
        {
            switch (extension)
            {
                case ".jtype":
                    await this.Logger.InfoAsync("Reading type from: \"{0}\"", filePath);

                    string text = await File.ReadAllTextAsync(filePath, cancellationToken);
                    var cardType = new CardType();
                    cardType.DeserializeFromJson(text);

                    string newTypeFilePath = ChangeExtension(filePath, ".jtype", ".tct");
                    return new ConversionItem(filePath, newTypeFilePath, cardType);

                case ".cardlib":
                    await this.Logger.InfoAsync("Reading card library from: \"{0}\"", filePath);

                    var cardLibrary = new CardLibrary();
                    await using (FileStream fileStream = FileHelper.OpenRead(filePath, synchronousOnly: true))
                    {
                        cardLibrary.DeserializeFromXml(fileStream);
                    }

                    return new ConversionItem(filePath, filePath, cardLibrary);

                case ".jcard":
                    await this.Logger.InfoAsync("Card is pending to convert: \"{0}\"", filePath);
                    string newCardTypePath = ChangeExtension(filePath, ".jcard", ".card");
                    return new ConversionItem(filePath, newCardTypePath, new Card());

                default:
                    return null;
            }
        }

        private static string ChangeExtension(string path, string oldExtension, string newExtension) =>
            path?.EndsWith(oldExtension, StringComparison.OrdinalIgnoreCase) == true
                ? path.Substring(0, path.Length - oldExtension.Length) + newExtension
                : path;

        private static void ConvertCardLibraryItems(CardLibrary cardLibrary, bool downgrade)
        {
            foreach (CardLibraryItem item in cardLibrary.Items)
            {
                item.Path = downgrade
                    ? ChangeExtension(item.Path, ".jcard", ".card")
                    : ChangeExtension(item.Path, ".card", ".jcard");
            }
        }

        #endregion

        #region Base Overrides

        public override async Task<int> ExecuteAsync(OperationContext context, CancellationToken cancellationToken = default)
        {
            await this.Logger.InfoAsync("Converting configuration from: \"{0}\"", context.Source);

            try
            {
                var items = new List<ConversionItem>();
                foreach (string filePath in DefaultConsoleHelper.GetSourceFiles(context.Source, "*.*", throwIfNotFound: false))
                {
                    string extension = Path.GetExtension(filePath)?.ToLowerInvariant();
                    ConversionItem item;

                    try
                    {
                        switch (context.ConversionMode)
                        {
                            case ConversionMode.Upgrade:
                                item = await this.ReadForConvertAsync(extension, filePath);
                                break;

                            case ConversionMode.Downgrade:
                                item = await this.ReadForDowngradeAsync(extension, filePath, cancellationToken);
                                break;

                            case ConversionMode.LF:
                            case ConversionMode.CRLF:
                                switch (extension)
                                {
                                    case ".cardlib": // библиотека карточек в xml
                                    case ".jcard": // карточка в json
                                    case ".json": // произвольный текстовый json, например, app.json
                                    case ".jtype": // тип карточки в json
                                    case ".sql": // sql-скрипт с процедурой, функцией или миграцией
                                    case ".tct": // тип карточки в xml
                                    case ".tll": // библиотека локализации в xml
                                    case ".tpf": // функция схемы в xml
                                    case ".tpm": // миграция схемы в xml
                                    case ".tpp": // процедура схемы в xml
                                    case ".tsd": // база данных схемы в xml
                                    case ".tsp": // библиотека схемы в xml
                                    case ".tst": // таблица схемы в xml
                                    case ".txt": // текстовые файлы вида readme.txt
                                    case ".view": // представление в exchange format
                                    case ".workplace": // рабочее место в exchange format
                                    case ".xml": // произвольный текстовый xml, например, extensions.xml
                                        // только наши текстовые файлы, не трогаем бинарные .card, и другие файлы (например, файлы реестра .reg)
                                        item = new ConversionItem(filePath, filePath, null);
                                        break;

                                    default:
                                        item = null;
                                        break;
                                }

                                break;

                            default:
                                throw new ArgumentOutOfRangeException(nameof(ConversionMode), context.ConversionMode, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        await this.Logger.LogExceptionAsync($"Error when loading file \"{filePath}\"", ex);
                        item = null;
                    }

                    if (item != null)
                    {
                        items.Add(item);
                    }
                }

                if (items.Count > 0)
                {
                    await this.Logger.InfoAsync("Converting configuration files ({0})", items.Count);

                    foreach (ConversionItem item in items)
                    {
                        try
                        {
                            switch (item.Object)
                            {
                                case null:
                                    // преобразование переводов строк
                                    string text = await File.ReadAllTextAsync(item.OldPath, Encoding.UTF8, cancellationToken);
                                    string newText = context.ConversionMode == ConversionMode.LF
                                        ? text.NormalizeLineEndingsUnixStyle()
                                        : text.NormalizeLineEndingsWindowsStyle();

                                    if (!string.Equals(text, newText, StringComparison.Ordinal))
                                    {
                                        await File.WriteAllTextAsync(item.NewPath, newText, Encoding.UTF8, cancellationToken);
                                        await this.Logger.InfoAsync("Line endings are converted: \"{0}\"", item.NewPath);
                                    }

                                    break;

                                case CardType cardType:
                                    string typeText = context.ConversionMode == ConversionMode.Downgrade ? cardType.SerializeToXml() : cardType.SerializeToJson(indented: true);
                                    await File.WriteAllTextAsync(item.NewPath, typeText, Encoding.UTF8, cancellationToken);
                                    await this.Logger.InfoAsync("Type is converted: \"{0}\"", item.NewPath);
                                    break;

                                case CardLibrary cardLibrary:
                                    ConvertCardLibraryItems(cardLibrary, context.ConversionMode == ConversionMode.Downgrade);
                                    string libraryText = cardLibrary.SerializeToXml();
                                    await File.WriteAllTextAsync(item.NewPath, libraryText, Encoding.UTF8, cancellationToken);
                                    await this.Logger.InfoAsync("Card library is converted: \"{0}\"", item.NewPath);
                                    break;

                                case Card _:
                                    await ConvertCardAsync(item.OldPath, item.NewPath, context.ConversionMode == ConversionMode.Downgrade, cancellationToken);
                                    await this.Logger.InfoAsync("Card is converted: \"{0}\"", item.NewPath);
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            await this.Logger.LogExceptionAsync($"Error when converting file \"{item.OldPath}\"", ex);
                        }

                        if (item.NewPath != item.OldPath)
                        {
                            FileHelper.DeleteFileSafe(item.OldPath);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                await this.Logger.LogExceptionAsync("Error converting configuration", e);
                return -1;
            }

            await this.Logger.InfoAsync("Configuration is converted successfully");
            return 0;
        }

        #endregion
    }
}