﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MailKit.Net.Pop3;
using MimeKit;
using MimeKit.Text;
using NLog;
using Tessa.Extensions.Default.Chronos.Notices;
using Tessa.Extensions.Default.Server.Notices;
using Tessa.Platform;
using Tessa.Platform.IO;

namespace Tessa.Extensions.Default.Chronos.Workflow
{
    public sealed class Pop3MailReceiver : IMailReceiver
    {
        #region Fields

        private readonly Func<IMessageProcessor> getProcessorFunc;

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Constructors

        public Pop3MailReceiver(Func<IMessageProcessor> getProcessorFunc)
        {
            // получаем функцию Func<IMessageProcessor>, чтобы зависимости Unity (всякие кэши метаинфы)
            // не инициализировались раньше времени
            this.getProcessorFunc = getProcessorFunc;
        }

        #endregion

        #region Properties

        public bool StopRequested => this.StopRequestedFunc != null && this.StopRequestedFunc();

        public Pop3ImapSettings Settings { get; set; }

        #endregion

        #region IMailReceiver Methods

        public Func<bool> StopRequestedFunc { get; set; }


        public async Task ReceiveMessagesAsync(CancellationToken cancellationToken = default)
        {
            logger.Trace("Loading messages.");

            using var client = new Pop3Client();

            // ReSharper disable PossibleInvalidOperationException
            await client.ConnectAsync(this.Settings.Host, (int) this.Settings.Port, this.Settings.UseSsl ?? false, cancellationToken);
            // ReSharper restore PossibleInvalidOperationException

            try
            {
                // Authenticate ourselves towards the server
                await client.AuthenticateAsync(this.Settings.User, this.Settings.Password, cancellationToken);

                if (this.StopRequested)
                {
                    return;
                }

                // Get the number of messages in the inbox
                int messageCount = await client.GetMessageCountAsync(cancellationToken);

                if (messageCount == 0)
                {
                    logger.Trace("There are no messages to process.");
                    return;
                }

                // Messages are numbered in the interval: [1, messageCount]
                // Ergo: message numbers are 1-based.
                // Most servers give the latest message the highest number
                IMessageProcessor processor = null;
                for (int messageIndex = 0; messageIndex < messageCount; messageIndex++)
                {
                    if (this.StopRequested)
                    {
                        return;
                    }

                    MimeMessage message = await client.GetMessageAsync(messageIndex, cancellationToken);
                    
                    string from = message.From.Mailboxes.FirstOrDefault()?.Address;
                    string subject = message.Subject ?? string.Empty;
                    logger.Trace("Message loaded. From: {0}, Subject: {1}.", from, subject);

                    if (this.StopRequested)
                    {
                        return;
                    }

                    try
                    {
                        string messageText = null;

                        string plainText = message.GetTextBody(TextFormat.Plain);
                        if (plainText != null)
                        {
                            messageText = plainText;
                        }
                        else
                        {
                            string htmlText = message.GetTextBody(TextFormat.Html);
                            if (htmlText != null)
                            {
                                string html = htmlText;
                                messageText = FormattingHelper.ExtractPlainTextFromHtml(html);
                            }
                        }

                        logger.Trace("Finding user and setting user session.");

                        processor ??= this.getProcessorFunc();

                        var attachments = new List<NoticeAttachment>();
                        foreach (MimeEntity entity in message.Attachments)
                        {
                            if (entity is MimePart part)
                            {
                                await using MemoryStream stream = StreamHelper.AcquireMemoryStream(capacity: 80000);
                                await part.Content.DecodeToAsync(stream, cancellationToken);
                                attachments.Add(new NoticeAttachment { Name = part.FileName, Data = stream.ToArray() });
                            }
                        }

                        await processor.ProcessMessageAsync(
                            new NoticeMessage
                            {
                                From = from,
                                Subject = subject,
                                Body = messageText ?? string.Empty,
                                Attachments = attachments.ToArray(),
                            },
                            cancellationToken);
                    }
                    catch (Exception ex)
                    {
                        logger.LogException(ex);
                    }
                    finally
                    {
                        await client.DeleteMessageAsync(messageIndex, cancellationToken);
                        logger.Trace("Message is deleted. Subject: '{0}'", subject);
                    }
                }
            }
            finally
            {
                await client.DisconnectAsync(true, cancellationToken);
            }
        }

        #endregion
    }
}