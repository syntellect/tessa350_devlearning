﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Tessa.BusinessCalendar;
using Tessa.Cards;
using Tessa.Cards.Caching;
using Tessa.Extensions.Default.Shared;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Extensions.Default.Shared.Workflow.WorkflowEngine;
using Tessa.Notices;
using Tessa.Platform;
using Tessa.Platform.Collections;
using Tessa.Platform.Data;
using Tessa.Platform.Placeholders;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Tessa.Roles;
using Tessa.Scheme;
using Tessa.Workflow;
using Tessa.Workflow.Actions;
using Tessa.Workflow.Bindings;
using Tessa.Workflow.Compilation;
using Tessa.Workflow.Helpful;
using Tessa.Workflow.Signals;
using Tessa.Workflow.Storage;

using Unity;

using static Tessa.Extensions.Default.Shared.Workflow.KrProcess.KrConstants;
using static Tessa.Extensions.Default.Shared.Workflow.WorkflowEngine.WorkflowConstants;

namespace Tessa.Extensions.Default.Server.Workflow.WorkflowEngine
{
    /// <summary>
    /// Обработчик действия <see cref="KrDescriptors.KrSigningDescriptor"/>.
    /// </summary>
    public sealed class KrSigningAction : KrWorkflowMultiTaskActionBase
    {
        #region Fields

        /// <summary>
        /// Имя ключа по которму в <see cref="CardInfoStorageObject.Info"/> хранится признак показывающий, что не требуется дополнительно обрабатывать значение <see cref="CardTask.Result"/> в <see cref="GetResultAsync(IWorkflowEngineContext, CardTask)"/>. Тип значения: <see cref="bool"/>.
        /// </summary>
        private const string isProcessedTaskResultKey = CardHelper.SystemKeyPrefix + "IsProcessedTaskResult";

        private const string mainSectionName = KrSigningActionVirtual.SectionName;

        /// <summary>
        /// Идентификатор типа задания подписания.
        /// </summary>
        private static readonly Guid taskTypeID = DefaultTaskTypes.KrSigningTypeID;

        /// <summary>
        /// Значение по умолчанию для параметра "Длительность, рабочие дни". Данное значение используется только, если в схеме не указано значение по умолчанию для поля <see cref="KrSigningActionVirtual.Period"/>.
        /// </summary>
        private const double periodInDaysDefaultValue = 1d;

        /// <summary>
        /// Значение по умолчанию для параметра "Длительность, рабочие дни". Данное значение используется только, если в схеме не указано значение по умолчанию для поля <see cref="KrWeEditInterjectOptionsVirtual.Period"/>.
        /// </summary>
        private const double revisionPeriodInDaysDefaultValue = 1d;

        /// <summary>
        /// Массив типов обрабатываемых сигналов.
        /// </summary>
        private static readonly string[] signalTypes = new string[]
        {
            WorkflowSignalTypes.CompleteTask,
            WorkflowSignalTypes.DeleteTask,
            WorkflowSignalTypes.UpdateTask,
        };

        /// <summary>
        /// Состояние документа при обработке действия.
        /// </summary>
        private readonly static KrState activeState = KrState.Signing;

        /// <summary>
        /// Состояние документа при положительном завершении обработки действия.
        /// </summary>
        private readonly static KrState positiveState = KrState.Signed;

        /// <summary>
        /// Состояние документа при отрицательном завершении обработки действия.
        /// </summary>
        private readonly static KrState negativeState = KrState.Declined;

        /// <summary>
        /// Идентификатор варианта завершения действия при положительном завершении обработки.
        /// </summary>
        private readonly static Guid positiveActionCompletionOptionID = ActionCompletionOptions.Signed;

        /// <summary>
        /// Идентификатор варианта завершения действия при отрицательном завершении обработки.
        /// </summary>
        private readonly static Guid negativeActionCompletionOptionID = ActionCompletionOptions.Declined;

        private readonly IRoleRepository roleRepository;
        private readonly IWorkflowBindingParser bindingParser;
        private readonly IWorkflowBindingExecutor bindingExecutor;
        private readonly ICardCache cardCache;
        private readonly Func<IPlaceholderManager> getPlaceholderManagerFunc;

        #endregion

        #region Constructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="KrSigningAction"/>.
        /// </summary>
        public KrSigningAction(
            IRoleRepository roleRepository,
            [Dependency(NotificationManagerNames.DeferredWithoutTransaction)] INotificationManager notificationManager,
            IWorkflowEngineCardsScope cardsScope,
            [Dependency(CardRepositoryNames.ExtendedWithoutTransaction)] ICardRepository cardRepository,
            [Dependency(CardRepositoryNames.DefaultWithoutTransaction)] ICardRepository cardRepositoryDef,
            ICardServerPermissionsProvider serverPermissionsProvider,
            ISignatureProvider signatureProvider,
            Func<ICardTaskCompletionOptionSettingsBuilder> ctcBuilderFactory,
            ICardFileManager cardFileManager,
            IWorkflowBindingParser bindingParser,
            IWorkflowBindingExecutor bindingExecutor,
            IWorkflowEngineCardRequestExtender requestExtender,
            IBusinessCalendarService calendarService,
            ICardCache cardCache,
            Func<IPlaceholderManager> getPlaceholderManagerFunc)
            : base(
                  descriptor: KrDescriptors.KrSigningDescriptor,
                  notificationManager: notificationManager,
                  cardsScope: cardsScope,
                  cardRepository: cardRepository,
                  cardRepositoryDef: cardRepositoryDef,
                  serverPermissionsProvider: serverPermissionsProvider,
                  signatureProvider: signatureProvider,
                  ctcBuilderFactory: ctcBuilderFactory,
                  cardFileManager: cardFileManager,
                  requestExtender: requestExtender,
                  calendarService: calendarService)
        {
            this.roleRepository = roleRepository;
            this.bindingParser = bindingParser;
            this.bindingExecutor = bindingExecutor;
            this.cardCache = cardCache;
            this.getPlaceholderManagerFunc = getPlaceholderManagerFunc;
        }

        #endregion

        #region Base Overrides

        /// <inheritdoc/>
        public override bool Compile(
            IWorkflowCompilationSyntaxTreeBuilder builder,
            WorkflowActionStorage action)
        {
            if (base.Compile(builder, action))
            {
                this.CompileEvents(builder, action);
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public override void PrepareForExecute(WorkflowActionStateStorage actionState, IWorkflowEngineContext context)
        {
            base.PrepareForExecute(actionState, context);
            actionState.Hash.Remove(KrSigningActionOptionsVirtual.SectionName);
        }

        /// <inheritdoc/>
        /// <remarks>
        /// Схема выполнения действия:<para/>
        /// 1. Получаем список идентификаторов обрабатываемых действием заданий;<para/>
        /// 2. Если задания есть, то очищаем список переходов;<para/>
        /// 3. Если тип сигнала - default, то:<para/>
        /// 3.1. Очищаем список переходов.<para/>
        /// 3.1. Если заданий нет, то:<para/>
        /// 3.1.1 Есть исполнители, то создаем задание и все необходимые подписки;<para/>
        /// 3.1.2. Нет исполнителей, то завершаем действие с вариантом завершения <see cref="positiveActionCompletionOptionID"/>;
        /// 3.2. Если задания есть, игнорируем создание задания;<para/>
        /// 4. Если тип сигнала из списка обрабатываемых типов сигналов, то:<para/>
        /// 4.1. Если заданий нет, то игнорируем;<para/>
        /// 4.2. Если задания есть, то для каждого задания обрабатываем сигнал.<para/>
        /// </remarks>
        protected override async Task ExecuteAsync(IWorkflowEngineContext context, IWorkflowEngineCompiled scriptObject)
        {
            var taskIDList = GetProcessingTaskIDList(context);
            var hasTaskIDList = taskIDList?.Any() == true;
            var signalType = context.Signal.Type;

            if (hasTaskIDList)
            {
                context.Links.Clear();
            }

            using (this.CreateTasksContext(context))
            {
                if (signalType == WorkflowSignalTypes.Default)
                {
                    context.Links.Clear();

                    if (!hasTaskIDList)
                    {
                        var sqlPerformerScript = await context.GetAsync<string>(
                            mainSectionName,
                            KrSigningActionVirtual.SqlPerformersScript);

                        var sqlPerformers = await this.GetSqlPerformers(sqlPerformerScript, context);
                        if (!context.ValidationResult.IsSuccessful())
                        {
                            return;
                        }

                        var performersRows = await context.GetAllRowsAsync(KrWeRolesVirtual.SectionName) ?? Enumerable.Empty<Dictionary<string, object>>();
                        var performers = WorkflowHelper.CombinePerformers(
                            performersRows.Select(i => new RoleEntryStorage(
                                WorkflowEngineHelper.Get<Guid>(i, Names.Table_RowID),
                                WorkflowEngineHelper.Get<Guid>(i, KrWeRolesVirtual.Role, Names.Table_ID),
                                WorkflowEngineHelper.Get<string>(i, KrWeRolesVirtual.Role, Table_Field_Name))),
                            sqlPerformers,
                            SqlApproverRoleID);

                        WorkflowEngineHelper.Set(
                            context.ActionInstance.Hash,
                            performers.Select(i => i.GetStorage()).ToArray(),
                            NamesKeys.RoleList);

                        context.ActionInstance.Hash[NamesKeys.IsNegativeActionResult] = BooleanBoxes.False;

                        if (!performers.Any())
                        {
                            context.ValidationResult.AddError(this, WorkflowHelper.GetValidatePerformerNotSpecifiedMessage(context.ActionTemplate, context.NodeTemplate));
                            return;
                        }

                        if (await context.GetAsync<bool?>(mainSectionName, KrSigningActionVirtual.ChangeStateOnStart) == true)
                        {
                            await this.SetStateIDAsync(
                                context,
                                activeState,
                                context.CancellationToken);
                        }

                        WorkflowHelper.SetCurrentPerformerIndex(context.ActionInstance.Hash, 0);

                        if (await context.GetAsync<bool?>(mainSectionName, KrSigningActionVirtual.IsParallel) == true)
                        {
                            await this.SendSigningTaskAsync(context, scriptObject, performers);
                        }
                        else
                        {
                            await this.SendSigningTaskAsync(context, scriptObject, performers.Take(1));
                        }

                        foreach (var newSignalType in signalTypes)
                        {
                            this.CreateSubscription(context, newSignalType);
                        }

                        this.SubscribeOnEvents(context);
                    }
                }
                else if (hasTaskIDList)
                {
                    switch (signalType)
                    {
                        case WorkflowSignalTypes.CompleteTask:
                            foreach (var taskID in taskIDList.Cast<Guid>().ToArray())
                            {
                                await this.CompleteTaskAsync(context, scriptObject, taskID);
                            }
                            break;

                        case WorkflowSignalTypes.DeleteTask:
                            foreach (var taskID in taskIDList.Cast<Guid>().ToArray())
                            {
                                if (await this.DeleteTaskAsync(context, taskID))
                                {
                                    taskIDList.Remove(taskID);
                                }
                            }
                            break;

                        case WorkflowSignalTypes.UpdateTask:
                            foreach (var taskID in taskIDList.Cast<Guid>())
                            {
                                await this.UpdateTaskAsync(context, taskID);
                            }
                            break;

                        default:
                            foreach (var taskID in taskIDList.Cast<Guid>())
                            {
                                await this.PerformEvent(context, scriptObject, taskID);
                            }
                            break;
                    }
                }
            }
        }

        #endregion

        #region WorkflowTaskActionBase Overrides

        /// <inheritdoc/>
        protected override async Task<string> GetResultAsync(
            IWorkflowEngineContext context,
            CardTask task)
        {
            if (task.Info.TryGet<bool>(isProcessedTaskResultKey))
            {
                return task.Result;
            }

            if (task.TypeID == taskTypeID)
            {
                return
                    await this.GetWithPlaceholdersAsync(
                        context,
                        await context.GetAsync<string>(mainSectionName, KrSigningActionVirtual.Result),
                        task);
            }

            return task.Result;
        }

        /// <inheritdoc/>
        protected override async Task CompleteTaskCoreAsync(
            IWorkflowEngineContext context,
            CardTask task,
            Guid taskOptionID,
            IWorkflowEngineCompiled scriptObject)
        {
            var oldTaskResult = task.Result;
            Guid? actionOptionID;
            using (this.CreateTasksContext(context))
            {
                actionOptionID = await this.CompleteTaskPredefinedActionProcessingAsync(context, scriptObject, task, taskOptionID);
            }

            await this.CompleteTaskScriptAsync(
                context,
                task,
                taskOptionID,
                scriptObject,
                (!string.IsNullOrEmpty(oldTaskResult) || string.IsNullOrEmpty(task.Result))
                && string.Equals(oldTaskResult, task.Result));

            if (actionOptionID.HasValue
                && !context.Cancel)
            {
                await this.CompleteActionAsync(context, scriptObject, actionOptionID.Value);
            }

            context.Cancel = false;

            if (task.State == CardRowState.Deleted)
            {
                GetProcessingTaskIDList(context)?.Remove(task.RowID);
            }
        }

        /// <inheritdoc/>
        protected override async Task DelegateTaskCoreAsync(
            IWorkflowEngineContext context,
            IWorkflowEngineCompiled scriptObject,
            CardTask originalTask,
            CardTask delegatedTask)
        {
            await this.CompleteSubtasksAsync(context, scriptObject, originalTask.RowID);

            AddNewProcessingTaskID(context, delegatedTask.RowID);
            GetProcessingTaskIDList(context).Remove(originalTask.RowID);
            await this.AddToHistoryAsync(context, delegatedTask.RowID, WorkflowHelper.GetProcessCycle(context.ProcessInstance.Hash));
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Обрабатывает завершение заданий в соответсвии с предопределённой логикой обработки.
        /// </summary>
        /// <param name="context">Контекст обработчика процесса.</param>
        /// <param name="scriptObject">Объект, предоставляющий доступ к скриптам действия.</param>
        /// <param name="task">Завершаемое задание.</param>
        /// <param name="taskOptionID">Идентификатор варианта завершения с которым завершается задание. Может отличаться от <see cref="CardTask.OptionID"/> из <paramref name="task"/>, если он был изменён в скрипте, обрабатывающим завершение задания.</param>
        /// <returns>Идентификатор варианта завершения действия, для которого должны быть обработаны связи выполняющиеся после завершения обработки этого действия или значение по умолчанию для типа, если действие не завершается.</returns>
        private async Task<Guid?> CompleteTaskPredefinedActionProcessingAsync(
            IWorkflowEngineContext context,
            IWorkflowEngineCompiled scriptObject,
            CardTask task,
            Guid taskOptionID)
        {
            Guid? actionOptionID = default;
            if (task.TypeID == taskTypeID)
            {
                if (taskOptionID == DefaultCompletionOptions.Sign)
                {
                    actionOptionID = await this.CompleteSigningTaskAsync(context, scriptObject, task, false);
                }
                else if (taskOptionID == DefaultCompletionOptions.Decline)
                {
                    actionOptionID = await this.CompleteSigningTaskAsync(context, scriptObject, task, true);
                }
                else if (taskOptionID == DefaultCompletionOptions.Delegate)
                {
                    await this.DelegateTaskAsync(
                        context,
                        scriptObject,
                        task,
                        await context.GetAsync<string>(mainSectionName, KrApprovalActionVirtual.Digest),
                        await context.GetAsync<Guid?>(mainSectionName, KrApprovalActionVirtual.Kind, Names.Table_ID),
                        await context.GetAsync<string>(mainSectionName, KrApprovalActionVirtual.Kind, Table_Field_Caption));
                }
            }
            else if (task.TypeID == DefaultTaskTypes.KrEditInterjectTypeID)
            {
                if (taskOptionID == DefaultCompletionOptions.Continue)
                {
                    actionOptionID = positiveActionCompletionOptionID;
                }
            }

            if (taskOptionID == DefaultCompletionOptions.RequestComments)
            {
                var answerTask = await this.SendRequestCommentTaskAsync(
                    context,
                    this.roleRepository,
                    task);

                if (answerTask is null)
                {
                    return default;
                }

                await this.AddToHistoryAsync(context, answerTask.RowID, WorkflowHelper.GetProcessCycle(context.ProcessInstance.Hash));
                AddNewProcessingTaskID(context, answerTask.RowID);
            }

            await this.RequestCommentTaskCompleteAsync(
                context,
                task,
                taskOptionID);

            return actionOptionID;
        }

        /// <summary>
        /// Асинхронно отправляет задание на указанную коллекцию ролей.
        /// </summary>
        /// <param name="context">Контекст обработчика процесса.</param>
        /// <param name="scriptObject">Объект, предоставляющий доступ к скриптам действия.</param>
        /// <param name="roles">Коллекция ролей на которые должны быть отправлены задания.</param>
        /// <returns>Асинхронная задача.</returns>
        private async Task SendSigningTaskAsync(IWorkflowEngineContext context, IWorkflowEngineCompiled scriptObject, IEnumerable<RoleEntryStorage> roles)
        {
            var digest = await context.GetAsync<string>(mainSectionName, KrSigningActionVirtual.Digest);

            var periodInDays = (double?)(await context.GetAsync<object>(mainSectionName, KrSigningActionVirtual.Period)
                ?? (await context.CardMetadata.GetSectionsAsync(context.CancellationToken))[mainSectionName]
                        .Columns[KrSigningActionVirtual.Period]
                        .DefaultValue)
                ?? periodInDaysDefaultValue;
            var planned = await context.GetAsync<DateTime?>(mainSectionName, KrSigningActionVirtual.Planned);

            var parentTaskRowID = context.Signal.As<WorkflowEngineTaskSignal>().ParentTaskRowID;
            var authorID = await context.GetAsync<Guid?>(mainSectionName, KrSigningActionVirtual.Author, Names.Table_ID);

            authorID = await context.GetAuthorIDAsync(roleRepository, authorID);
            if (!authorID.HasValue)
            {
                return;
            }

            var kindID = await context.GetAsync<Guid?>(mainSectionName, KrSigningActionVirtual.Kind, Names.Table_ID);
            var kindCaption = await context.GetAsync<string>(mainSectionName, KrSigningActionVirtual.Kind, Table_Field_Caption);

            digest = await this.GetWithPlaceholdersAsync(
                context,
                digest,
                context.Task);

            foreach (var performer in roles)
            {
                var cardTask =
                    await context.SendTaskAsync(
                        taskTypeID,
                        digest,
                        planned,
                        (int?)Math.Round(periodInDays * TimeZonesHelper.QuantsInDay),
                        performer.ID,
                        performer.Name,
                        parentRowID: parentTaskRowID,
                        cancellationToken: context.CancellationToken);

                if (cardTask is null
                    || !context.ValidationResult.IsSuccessful())
                {
                    return;
                }

                context.ValidationResult.Add(WorkflowHelper.SetTaskKind(cardTask, kindID, kindCaption, this));

                if (!context.ValidationResult.IsSuccessful())
                {
                    return;
                }

                if (await context.GetAsync<bool?>(mainSectionName, KrSigningActionVirtual.CanEditCard) == true)
                {
                    cardTask.Settings[NamesKeys.CanEditCard] = BooleanBoxes.True;
                }

                if (await context.GetAsync<bool?>(mainSectionName, KrSigningActionVirtual.CanEditAnyFiles) == true)
                {
                    cardTask.Settings[NamesKeys.CanEditAnyFiles] = BooleanBoxes.True;
                }

                context.ActionInstance.Hash[NamesKeys.EditInterjectAuthorID] = cardTask.AuthorID = authorID;

                this.AddTaskToNextContextTasks(context, cardTask);
                AddNewProcessingTaskID(context, cardTask.RowID);
                await this.AddActiveTaskAsync(context, cardTask.RowID);
                await this.AddToHistoryAsync(context, cardTask.RowID, WorkflowHelper.GetProcessCycle(context.ProcessInstance.Hash));

                if (scriptObject != null)
                {
                    await scriptObject.ExecuteActionAsync(
                        KrWorkflowActionMethods.KrSigningInitMethod.MethodName,
                        cardTask,
                        cardTask.Card.DynamicEntries,
                        cardTask.Card.DynamicTables);
                }

                await this.SendStartTaskNotificationAsync(
                    context,
                    scriptObject,
                    cardTask,
                    mainSectionName,
                    methodName: KrWorkflowActionMethods.KrSigningStartNotificationMethod.MethodName);
            }
        }

        /// <summary>
        /// Обрабатывает завершение задания согласования.
        /// </summary>
        /// <param name="context">Контекст обработки процесса в WorkflowEngine.</param>
        /// <param name="scriptObject">Объект, предоставляющий доступ к скриптам действия.</param>
        /// <param name="task">Завершаемое задание.</param>
        /// <param name="isNagativeResult">Значение <see langword="true"/>, если задание задание было завершено отказом, иначе - <see langword="false"/>.</param>
        /// <returns>Идентификатор варианта завершения действия, для которого должны быть обработаны связи выполняющиеся после завершения обработки этого действия или значение по умолчанию для типа, если действие не завершается.</returns>
        private async Task<Guid?> CompleteSigningTaskAsync(
            IWorkflowEngineContext context,
            IWorkflowEngineCompiled scriptObject,
            CardTask task,
            bool isNagativeResult)
        {
            await this.TryRemoveActiveTaskAsync(context, task.RowID);
            await this.CompleteSubtasksAsync(context, scriptObject, task.RowID);

            var sCard = await context.TryGetKrSatelliteCardAsync(
                context.CancellationToken);

            if (sCard is null)
            {
                return default;
            }

            WorkflowHelper.AppendApprovalInfoUserCompleteTask(
                sCard.Sections,
                context.Session.User,
                task,
                isNagativeResult);

            if (isNagativeResult)
            {
                context.ActionInstance.Hash[NamesKeys.IsNegativeActionResult] = BooleanBoxes.True;
            }

            if (isNagativeResult
                || !(await this.cardCache.Cards
                    .GetAsync(KrSettings.Name, context.CancellationToken))
                    .Sections[KrSettings.Name].Fields.Get<bool>(KrSettings.HideCommentForApprove))
            {
                var comment = task.Card.Sections[KrTask.Name].Fields.TryGet<string>(KrTask.Comment);

                if (!string.IsNullOrWhiteSpace(comment))
                {
                    if (this.bindingParser.IsBinding(comment))
                    {
                        comment = await this.bindingExecutor.GetAsync<string>(context, comment);
                    }

                    task.Result = await this.GetWithPlaceholdersAsync(
                        context,
                        comment,
                        task);
                }
            }

            var currentPerformerIndex = WorkflowHelper.CurrentPerformerIndexIncrenent(context.ActionInstance.Hash);

            // Завершено последнее задание?
            if (currentPerformerIndex == context.ActionInstance.Hash.Get<IList<object>>(NamesKeys.RoleList).Count)
            {
                if (context.ActionInstance.Hash.Get<bool>(NamesKeys.IsNegativeActionResult))
                {
                    await this.NegativeResultProcessingAsync(context);
                    return negativeActionCompletionOptionID;
                }

                await this.PositiveResultProcessingAsync(context, scriptObject);
                return positiveActionCompletionOptionID;
            }
            else
            {
                if (await context.GetAsync<bool?>(mainSectionName, KrSigningActionVirtual.IsParallel) != true)
                {
                    if (isNagativeResult)
                    {
                        await this.NegativeResultProcessingAsync(context);

                        if (await context.GetAsync<bool?>(mainSectionName, KrSigningActionVirtual.ExpectAllSigners) != true)
                        {
                            return negativeActionCompletionOptionID;
                        }
                    }

                    await this.SendSigningTaskAsync(
                        context,
                        scriptObject,
                        context.ActionInstance.Hash
                            .Get<IList<object>>(NamesKeys.RoleList)
                            .Select(i => new RoleEntryStorage((Dictionary<string, object>)i))
                            .Skip(currentPerformerIndex)
                            .Take(1));

                    context.Cancel = true;
                }
            }

            return default;
        }

        /// <summary>
        /// Обрабатывает положительный вариант завершения основного задания.
        /// </summary>
        /// <param name="context">Контекст обработки процесса в WorkflowEngine.</param>
        /// <param name="scriptObject">Объект, предоставляющий доступ к скриптам действия.</param>
        /// <returns>Асинхронная задача.</returns>
        private async Task PositiveResultProcessingAsync(
            IWorkflowEngineContext context,
            IWorkflowEngineCompiled scriptObject)
        {
            if (await context.GetAsync<bool?>(mainSectionName, KrSigningActionVirtual.ReturnWhenApproved) == true)
            {
                await this.SendEditInterjectTaskAsync(context, scriptObject);

                context.Cancel = true;
            }
        }

        /// <summary>
        /// Асинхронно отправляет задание доработки автором.
        /// </summary>
        /// <param name="context">Контекст обработки процесса в WorkflowEngine.</param>
        /// <param name="scriptObject">Объект, предоставляющий доступ к скриптам действия.</param>
        /// <returns>Асинхронная задача.</returns>
        private async Task SendEditInterjectTaskAsync(
            IWorkflowEngineContext context,
            IWorkflowEngineCompiled scriptObject)
        {
            var cardTask = await context.SendEditInterjectTaskAsync(
                this.getPlaceholderManagerFunc(),
                this.roleRepository,
                context.ActionInstance.Hash.Get<Guid>(NamesKeys.EditInterjectAuthorID),
                revisionPeriodInDaysDefaultValue);

            if (cardTask is null)
            {
                return;
            }

            this.AddTaskToNextContextTasks(context, cardTask);
            AddNewProcessingTaskID(context, cardTask.RowID);
            await this.AddActiveTaskAsync(context, cardTask.RowID);
            await this.AddToHistoryAsync(context, cardTask.RowID, WorkflowHelper.GetProcessCycle(context.ProcessInstance.Hash));

            if (scriptObject != null)
            {
                await scriptObject.ExecuteActionAsync(
                    KrWorkflowActionMethods.EditInterjectTaskInitMethod.MethodName,
                    cardTask,
                    cardTask.Card.DynamicEntries,
                    cardTask.Card.DynamicTables);
            }

            await this.SendStartTaskNotificationAsync(
                context,
                scriptObject,
                cardTask,
                KrWeEditInterjectOptionsVirtual.SectionName,
                methodName: KrWorkflowActionMethods.EditInterjectTaskStartNotificationMethod.MethodName);
        }

        /// <summary>
        /// Обрабатывает отрицательный вариант завершения основного задания.
        /// </summary>
        /// <param name="context">Контекст обработки процесса в WorkflowEngine.</param>
        /// <returns>Асинхронная задача.</returns>
        private async Task NegativeResultProcessingAsync(
            IWorkflowEngineContext context)
        {
            var taskHistoryItemRowID = Guid.NewGuid();
            await this.AddTaskHistoryByTaskAsync(
                context,
                DefaultTaskTypes.KrRebuildTypeID,
                DefaultCompletionOptions.RebuildDocument,
                default,
                modifyAction: item => item.RowID = taskHistoryItemRowID);

            await this.AddToHistoryAsync(context, taskHistoryItemRowID, WorkflowHelper.GetProcessCycle(context.ProcessInstance.Hash));
        }

        /// <summary>
        /// Асинхронно отзывает дочерние задания.
        /// </summary>
        /// <param name="context">Контекст обработки процесса в WorkflowEngine.</param>
        /// <param name="scriptObject">Объект, предоставляющий доступ к скриптам действия.</param>
        /// <param name="taskRowID">Идентификатор родительского задания.</param>
        /// <returns>Асинхронная задача.</returns>
        private Task CompleteSubtasksAsync(
            IWorkflowEngineContext context,
            IWorkflowEngineCompiled scriptObject,
            Guid taskRowID)
        {
            return this.CompleteSubtasksAsync(
                context,
                scriptObject,
                taskRowID,
                new[]
                {
                    DefaultTaskTypes.KrRequestCommentTypeID
                },
                async (task) =>
                {
                    task.OptionID = DefaultCompletionOptions.Cancel;
                    task.Result = "$ApprovalHistory_ParentTaskIsCompleted";
                    task.Info[isProcessedTaskResultKey] = BooleanBoxes.True;

                    await this.TryRemoveActiveTaskAsync(context, task.RowID);
                });
        }

        /// <summary>
        /// Асинхронно обрабатывает скрипт выполняющийся при завершении задания и отправляет уведомления.
        /// </summary>
        /// <param name="context">Контекст обработчика процесса.</param>
        /// <param name="task">Завершаемое задание.</param>
        /// <param name="optionID">Идентификатор варианта завершения с которым завершается задание. Может отличаться от <see cref="CardTask.OptionID"/> из <paramref name="task"/>, если он был изменён в скрипте, обрабатывающим завершение задания.</param>
        /// <param name="scriptObject">Объект, предоставляющий доступ к скриптам действия.</param>
        /// <param name="isSetResult">Значение <see langword="true"/>, если результат завершения задания определяется настройкамим обработки варианта завершения задания, иначе - <see langword="false"/>.</param>
        /// <returns>Асинхронная задача.</returns>
        private async Task CompleteTaskScriptAsync(
            IWorkflowEngineContext context,
            CardTask task,
            Guid optionID,
            IWorkflowEngineCompiled scriptObject,
            bool isSetResult)
        {
            var optionRows = await context.GetAllRowsAsync(context.ActionTemplate.Hash, KrSigningActionOptionsVirtual.SectionName);

            if (optionRows?.Any() == true)
            {
                var taskTypeID = task.TypeID;
                var processRows = optionRows
                    .Cast<Dictionary<string, object>>()
                    .Where(x =>
                        WorkflowEngineHelper.Get<Guid?>(
                            x,
                            ActionSeveralTaskTypesOptionsBase.TaskType,
                            Names.Table_ID) == taskTypeID
                        && WorkflowEngineHelper.Get<Guid?>(
                            x,
                            ActionOptionsBase.Option,
                            Names.Table_ID) == optionID);

                foreach (var processRow in processRows)
                {
                    if (isSetResult)
                    {
                        var completionResult = processRow.TryGet<string>(KrSigningActionOptionsVirtual.Result);
                        if (!string.IsNullOrWhiteSpace(completionResult))
                        {
                            if (this.bindingParser.IsBinding(completionResult))
                            {
                                completionResult = await this.bindingExecutor.GetAsync<string>(context, completionResult);
                            }

                            task.Result = await this.GetWithPlaceholdersAsync(
                                context,
                                completionResult,
                                task);
                        }
                    }

                    var info = new WorkflowTaskNotificationInfo(
                        context,
                        processRow,
                        ActionNotificationRolesBase.Option,
                        KrSigningActionNotificationRolesVirtual.SectionName);

                    if (scriptObject != null)
                    {
                        await scriptObject.ExecuteActionAsync(
                            KrWorkflowActionMethods.KrSigningOptionMethod.GetMethodName(processRow),
                            task,
                            task.Card.DynamicEntries, task.Card.DynamicTables,
                            info);
                    }

                    await this.SendCompleteTaskNotificationAsync(
                        context,
                        scriptObject,
                        task,
                        info);
                }
            }
        }

        /// <summary>
        /// Асинхронно обрабатывает завершение действия.
        /// </summary>
        /// <param name="context">Контекст обработчика процесса.</param>
        /// <param name="scriptObject">Объект, предоставляющий доступ к скриптам действия.</param>
        /// <param name="actionOptionID">Идентификатор варианта завершения действия.</param>
        /// <returns>Асинхронная задача.</returns>
        private async Task CompleteActionAsync(
            IWorkflowEngineContext context,
            IWorkflowEngineCompiled scriptObject,
            Guid actionOptionID)
        {
            if (await context.GetAsync<bool?>(mainSectionName, KrSigningActionVirtual.ChangeStateOnEnd) == true)
            {
                await this.SetStateIDAsync(
                    context,
                    actionOptionID == positiveActionCompletionOptionID ? positiveState : negativeState,
                    context.CancellationToken);
            }

            var optionActionRows = await context.GetAllRowsAsync(context.ActionTemplate.Hash, KrSigningActionOptionsActionVirtual.SectionName);

            if (optionActionRows?.Any() == true)
            {
                var processRows = optionActionRows
                    .Where(x => WorkflowEngineHelper.Get<Guid?>(
                        x, ActionOptionsActionBase.ActionOption, Names.Table_ID) == actionOptionID);

                HashSet<Guid> linksForPerforming = new HashSet<Guid>();

                foreach (var processRow in processRows)
                {
                    var info = new WorkflowTaskNotificationInfo(
                        context,
                        processRow,
                        KrSigningActionNotificationActionRolesVirtual.Option,
                        KrSigningActionNotificationActionRolesVirtual.SectionName);

                    if (scriptObject != null)
                    {
                        await scriptObject.ExecuteActionAsync(
                            KrWorkflowActionMethods.KrSigningActionOptionActionMethod.GetMethodName(info.Row),
                            info);
                    }

                    await this.SendCompleteActionNotificationAsync(
                        context,
                        scriptObject,
                        info,
                        KrWorkflowActionMethods.KrSigningCompleteActionNotificationMethod.GetMethodName(info.Row));

                    var linkRows = context.ActionTemplate.Hash.TryGet<List<object>>(KrSigningActionOptionLinksVirtual.SectionName);
                    var rowID = WorkflowEngineHelper.Get<Guid>(processRow, Names.Table_RowID);
                    var linkIDs =
                        linkRows?
                            .Where(x => WorkflowEngineHelper.Get<Guid?>(
                                (Dictionary<string, object>)x, ActionOptionActionLinksBase.ActionOption, Names.Table_RowID) == rowID)
                            .Select(x => WorkflowEngineHelper.Get<Guid>(
                                (Dictionary<string, object>)x, ActionOptionActionLinksBase.Link, Names.Table_ID))
                        ?? EmptyHolder<Guid>.Collection;

                    foreach (var linkID in linkIDs)
                    {
                        linksForPerforming.Add(linkID);
                    }
                }

                foreach (var linkID in linksForPerforming)
                {
                    context.Links[linkID] = WorkflowEngineSignal.CreateDefaultSignal(StorageHelper.Clone(context.Signal.Hash));
                }
            }
        }

        #endregion
    }
}
