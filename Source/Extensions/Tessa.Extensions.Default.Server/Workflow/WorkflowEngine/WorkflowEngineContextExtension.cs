﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Tessa.BusinessCalendar;
using Tessa.Cards;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow;
using Tessa.Extensions.Default.Shared;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Extensions.Default.Shared.Workflow.WorkflowEngine;
using Tessa.Platform;
using Tessa.Platform.Data;
using Tessa.Platform.Placeholders;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Tessa.Roles;
using Tessa.Scheme;
using Tessa.Workflow;
using Tessa.Workflow.Helpful;

using static Tessa.Extensions.Default.Shared.Workflow.KrProcess.KrConstants;
using static Tessa.Extensions.Default.Shared.Workflow.WorkflowEngine.WorkflowConstants;

namespace Tessa.Extensions.Default.Server.Workflow.WorkflowEngine
{
    /// <summary>
    /// Предоставляет методы расширения для <see cref="IWorkflowEngineContext"/>.
    /// </summary>
    public static class WorkflowEngineContextExtension
    {
        /// <summary>
        /// Возвращает карточку сателлита основного Kr процесса или значение по умолчанию для типа, если карточка сателлита не найдена. Если карточка отсутствует в кэше карточек контекста, то она загружается из БД.
        /// </summary>
        /// <param name="context">Контекст обработки процесса.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Карточка сателлита основного Kr процесса или значение по умолчанию для типа, если карточка сателлита не найдена.</returns>
        public static async ValueTask<Card> TryGetKrSatelliteCardAsync(
            this IWorkflowEngineContext context,
            CancellationToken cancellationToken = default)
        {
            Check.ArgumentNotNull(context, nameof(context));

            var sCardID = await KrProcessHelper.GetKrSatelliteIDAsync(
                context.ProcessInstance.CardID,
                context.DbScope,
                cancellationToken);

            if (!sCardID.HasValue)
            {
                return default;
            }

            static CardGetRequest GetCardGetRequestFunc(Guid cardID)
            {
                var getRequest = new CardGetRequest()
                {
                    CardID = cardID,
                    RestrictionFlags = CardGetRestrictionValues.Satellite
                };

                getRequest.SetForbidStoringHistory(true);
                return getRequest;
            }

            var sCard = await context.GetCardAsync(
                sCardID.Value,
                GetCardGetRequestFunc,
                context.ValidationResult,
                cancellationToken: cancellationToken);

            return sCard;
        }

        /// <summary>
        /// Асинхронно отправляет задание доработки автором (<see cref="DefaultTaskTypes.KrEditInterjectTypeID"/>). Параметры задания берутся из секции <see cref="KrWeEditInterjectOptionsVirtual.SectionName"/>.
        /// </summary>
        /// <param name="context">Контекст обработки процесса в WorkflowEngine.</param>
        /// <param name="placeholderManager">Объект, управляющий операциями с плейсхолдерами.</param>
        /// <param name="roleRepository">Репозиторий для управления ролевой моделью.</param>
        /// <param name="roleIDDefaultValue">Значение по умолчанию для параметра "Роль".</param>
        /// <param name="periodInDaysDefaultValue">Значение по умолчанию для параметра "Длительность, рабочие дни". Данное значение используется только, если в схеме не указано значение по умолчанию для поля <see cref="KrWeEditInterjectOptionsVirtual.Period"/>.</param>
        /// <returns>Задание доработки автором или значение по умолчанию для типа, если при отправке задания произошла ошибка.</returns>
        public static async Task<CardTask> SendEditInterjectTaskAsync(
            this IWorkflowEngineContext context,
            IPlaceholderManager placeholderManager,
            IRoleRepository roleRepository,
            Guid roleIDDefaultValue,
            double periodInDaysDefaultValue)
        {
            Check.ArgumentNotNull(context, nameof(context));
            Check.ArgumentNotNull(placeholderManager, nameof(placeholderManager));
            Check.ArgumentNotNull(roleRepository, nameof(roleRepository));

            var performerID = await context.GetAsync<Guid?>(
                KrWeEditInterjectOptionsVirtual.SectionName,
                KrWeEditInterjectOptionsVirtual.Role,
                Names.Table_ID) ?? roleIDDefaultValue;
            var performerName = await context.GetAsync<string>(
                KrWeEditInterjectOptionsVirtual.SectionName,
                KrWeEditInterjectOptionsVirtual.Role,
                Table_Field_Name);

            var digest = await context.GetAsync<string>(
                KrWeEditInterjectOptionsVirtual.SectionName,
                KrWeEditInterjectOptionsVirtual.Digest);
            var periodInDays = (double?)(await context.GetAsync<object>(
                KrWeEditInterjectOptionsVirtual.SectionName,
                KrWeEditInterjectOptionsVirtual.Period)
                ?? (await context.CardMetadata.GetSectionsAsync(context.CancellationToken))[KrWeEditInterjectOptionsVirtual.SectionName]
                    .Columns[KrWeEditInterjectOptionsVirtual.Period]
                    .DefaultValue)
                ?? periodInDaysDefaultValue;
            var planned = await context.GetAsync<DateTime?>(
                KrWeEditInterjectOptionsVirtual.SectionName,
                KrWeEditInterjectOptionsVirtual.Planned);
            var authorID = await context.GetAsync<Guid?>(
                KrWeEditInterjectOptionsVirtual.SectionName,
                KrWeEditInterjectOptionsVirtual.Author,
                Names.Table_ID);

            var kindID = await context.GetAsync<Guid?>(
                KrWeEditInterjectOptionsVirtual.SectionName,
                KrWeEditInterjectOptionsVirtual.Kind,
                Names.Table_ID);
            var kindCaption = await context.GetAsync<string>(
                KrWeEditInterjectOptionsVirtual.SectionName,
                KrWeEditInterjectOptionsVirtual.Kind,
                Table_Field_Caption);

            digest = await placeholderManager.ReplaceTextAsync(
                digest,
                context.Session,
                context.Container,
                context.DbScope,
                default,
                cardID: context.ProcessInstance.CardID,
                task: context.Task,
                info: context.CreatePlaceholderInfo(),
                withScripts: true,
                cancellationToken: context.CancellationToken);

            var cardTask =
                await context.SendTaskAsync(
                    DefaultTaskTypes.KrEditInterjectTypeID,
                    digest,
                    planned,
                    (int?)Math.Round(periodInDays * TimeZonesHelper.QuantsInDay),
                    performerID,
                    performerName,
                    cancellationToken: context.CancellationToken);

            if (cardTask is null
                || !context.ValidationResult.IsSuccessful())
            {
                return default;
            }

            if (kindID.HasValue)
            {
                cardTask.Info[CardHelper.TaskKindIDKey] = kindID;
                cardTask.Info[CardHelper.TaskKindCaptionKey] = kindCaption;
            }

            authorID = await context.GetAuthorIDAsync(roleRepository, authorID);
            if (authorID.HasValue)
            {
                cardTask.AuthorID = authorID;
                return cardTask;
            }

            return default;
        }

        /// <summary>
        /// Возвращает идентификатор роли автора задания.
        /// </summary>
        /// <param name="context">Контекст обработки процесса в WorkflowEngine.</param>
        /// <param name="roleRepository">Репозиторий для управления ролевой моделью.</param>
        /// <param name="roleID">Идентификатор роли заданный в параметрах действия.</param>
        /// <returns>Идентификатор роли автора задания или значение по умолчанию для типа, если при обработке возникли ошибки.</returns>
        public static async Task<Guid?> GetAuthorIDAsync(
            this IWorkflowEngineContext context,
            IRoleRepository roleRepository,
            Guid? roleID)
        {
            Check.ArgumentNotNull(context, nameof(context));
            Check.ArgumentNotNull(roleRepository, nameof(roleRepository));

            if (roleID.HasValue)
            {
                (bool isSuccessful, Guid? resultRoleID) =
                    await WorkflowHelper.TryGetPersonalRoleIDAsync(
                        roleID.Value,
                        context.ProcessInstance.CardID,
                        roleRepository,
                        context.ValidationResult,
                        context.CancellationToken);

                if (isSuccessful)
                {
                    if (resultRoleID.HasValue)
                    {
                        return resultRoleID;
                    }

                    context.ValidationResult.AddError("$KrActions_OnlyPersonalOrContextRolesSetAsAuthor");
                }
            }
            else
            {
                var sCard = await context.TryGetKrSatelliteCardAsync(
                    context.CancellationToken);

                if (sCard != null)
                {
                    return sCard.GetApprovalInfoSection().Fields.TryGet<Guid?>(KrApprovalCommonInfo.AuthorID) ?? context.Session.User.ID;
                }
            }

            return default;
        }
    }
}
