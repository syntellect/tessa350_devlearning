﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Tessa.BusinessCalendar;
using Tessa.Cards;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.Handlers;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Platform;
using Tessa.Platform.Collections;
using Tessa.Platform.Data;
using Tessa.Platform.Storage;
using Tessa.Roles;
using Tessa.Workflow;
using Tessa.Workflow.Actions;
using Tessa.Workflow.Actions.Descriptors;
using Tessa.Workflow.Helpful;

namespace Tessa.Extensions.Default.Server.Workflow.WorkflowEngine
{
    /// <summary>
    /// Базовый класс обработчиков действий взаимодействующих с подсистемой маршрутов.
    /// </summary>
    public abstract class KrWorkflowActionBase : WorkflowActionBase
    {
        #region Fields And Consts

        /// <summary>
        /// Имя ключа по которому в параметрах текущего действия содержится значение флага управляющего принудительным увеличением версии основной карточки, даже если других изменений в карточке не было. Если не найден в параметрах действия, то считается равным <see langword="true"/>. Тип значения: <see cref="bool"/>.
        /// </summary>
        protected const string AffectMainCardVersionWhenStateChangedKey = "AffectMainCardVersionWhenStateChanged";

        /// <summary>
        /// Имя ключа по которому в параметрах текущего процесса содержится идентификатор предыдущего состояния карточки. Тип значения: <see cref="int"/>.
        /// </summary>
        protected const string PreviousStateKey = "KrPreviousState";

        protected readonly ICardRepository cardRepository;
        protected readonly IWorkflowEngineCardRequestExtender requestExtender;
        protected readonly IBusinessCalendarService calendarService;

        #endregion

        #region Constructors

        protected KrWorkflowActionBase(
            WorkflowActionDescriptor actionDescriptor,
            ICardRepository cardRepository,
            IWorkflowEngineCardRequestExtender requestExtender,
            IBusinessCalendarService calendarService)
            :base(actionDescriptor)
        {
            this.cardRepository = cardRepository;
            this.requestExtender = requestExtender;
            this.calendarService = calendarService;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Устанавливает состояние карточки.
        /// </summary>
        /// <param name="context">Контекст обработки процесса в WorkflowEngine.</param>
        /// <param name="state">Состояние.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Асинхронная задача.</returns>
        /// <remarks>Предыдущее состояние сохраняется в параметрах процесса (см. <see cref="StorePreviousState(IWorkflowEngineContext, int)"/>).</remarks>
        protected Task SetStateIDAsync(
            IWorkflowEngineContext context,
            KrState state,
            CancellationToken cancellationToken = default)
        {
            return this.SetStateIDAsync(context, state.ID, state.TryGetDefaultName(), cancellationToken: cancellationToken);
        }

        /// <summary>
        /// Устанавливает состояние карточки.
        /// </summary>
        /// <param name="context">Контекст обработки процесса в WorkflowEngine.</param>
        /// <param name="stateID">Идентификатор состояния.</param>
        /// <param name="stateName">Отображаемое имя состояния.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Асинхронная задача.</returns>
        /// <remarks>Предыдущее состояние сохраняется в параметрах процесса (см. <see cref="StorePreviousState(IWorkflowEngineContext, int)"/>).</remarks>
        protected async Task SetStateIDAsync(
            IWorkflowEngineContext context,
            int stateID,
            string stateName,
            CancellationToken cancellationToken = default)
        {
            var sCard = await context.TryGetKrSatelliteCardAsync(
                cancellationToken: cancellationToken);

            if (sCard is null)
            {
                return;
            }

            var approvalCommonInfoFields = sCard.GetApprovalInfoSection().Fields;
            var oldStateID = approvalCommonInfoFields.TryGet<int>(KrConstants.StateID);
            if (oldStateID == stateID)
            {
                // Если состояния равны, то смену состояния не производим.
                return;
            }

            this.StorePreviousState(context, oldStateID);
            var stateIDObj = Int32Boxes.Box(stateID);
            approvalCommonInfoFields[KrConstants.StateID] = stateIDObj;
            approvalCommonInfoFields[KrConstants.StateName] = stateName;
            approvalCommonInfoFields[KrConstants.KrApprovalCommonInfo.StateChangedDateTimeUTC] = DateTime.UtcNow;

            approvalCommonInfoFields = (await context.GetMainCardAsync(context.CancellationToken)).GetApprovalInfoSection().Fields;
            approvalCommonInfoFields[KrConstants.StateID] = stateIDObj;
            approvalCommonInfoFields[KrConstants.StateName] = stateName;

            if (await context.GetAsync<bool?>(AffectMainCardVersionWhenStateChangedKey) ?? true)
            {
                context.ModifyStoreRequest(ModifyRequest);
            }
        }

        protected async Task AddTaskHistoryByTaskAsync(
            IWorkflowEngineContext context,
            Guid taskTypeID,
            Guid optionID,
            string result,
            Action<CardTaskHistoryItem> modifyAction = default)
        {
            if (!(await context.CardMetadata.GetCardTypesAsync(context.CancellationToken)).TryGetValue(taskTypeID, out var taskType))
            {
                return;
            }

            await this.AddTaskHistoryAsync(
                context,
                taskTypeID,
                taskType.Name,
                taskType.Caption,
                optionID,
                result,
                modifyAction);
        }

        protected async Task AddTaskHistoryAsync(
            IWorkflowEngineContext context,
            Guid taskTypeID,
            string taskTypeName,
            string taskTypeCaption,
            Guid optionID,
            string result,
            Action<CardTaskHistoryItem> modifyAction = default)
        {
            var userID = context.Session.User.ID;
            var userName = context.Session.User.Name;

            var groupID = context.ProcessInstance.GetHistoryGroup();
            // Временная зона текущего сотрудника, для записи в историю заданий
            var userZoneInfo = await this.calendarService.GetRoleTimeZoneInfoAsync(userID, context.CancellationToken);
            var option = (await context.CardMetadata.GetEnumerationsAsync(context.CancellationToken)).CompletionOptions[optionID];
            var newItem = new CardTaskHistoryItem
            {
                State = CardTaskHistoryState.Inserted,
                RowID = Guid.NewGuid(),
                TypeID = taskTypeID,
                TypeName = taskTypeName,
                TypeCaption = taskTypeCaption,
                Created = context.StoreDateTime,
                Planned = context.StoreDateTime,
                InProgress = context.StoreDateTime,
                Completed = context.StoreDateTime,
                AuthorID = userID,
                UserID = userID,
                RoleID = userID,
                AuthorName = userName,
                UserName = userName,
                RoleName = userName,
                RoleTypeID = RoleHelper.PersonalRoleTypeID,
                Result = result,
                OptionID = optionID,
                OptionCaption = option.Caption,
                OptionName = option.Name,
                ParentRowID = null,
                CompletedByID = userID,
                CompletedByName = userName,
                GroupRowID = groupID,
                TimeZoneID = userZoneInfo.TimeZoneID,
                TimeZoneUtcOffsetMinutes = (int?)userZoneInfo.TimeZoneUtcOffset.TotalMinutes
            };

            modifyAction?.Invoke(newItem);

            var mainCard = await context.GetMainCardAsync(context.CancellationToken);
            if (mainCard != null)
            {
                mainCard.TaskHistory.Add(newItem);
            }
        }

        /// <summary>
        /// Сохраняет идентификатор предыдущего состояния карточки в параметрах процесса.
        /// </summary>
        /// <param name="context">Контекст обработки процесса в WorkflowEngine.</param>
        /// <param name="previousState">Идентификатор сохраняемого состояния.</param>
        /// <seealso cref="PreviousStateKey"/>
        protected void StorePreviousState(IWorkflowEngineContext context, int previousState)
        {
            context.ProcessInstance.Hash[PreviousStateKey] = Int32Boxes.Box(previousState);
        }

        /// <summary>
        /// Возвращает идентификатор предыдущего состояния карточки из параметров процесса.
        /// </summary>
        /// <param name="context">Контекст обработки процесса в WorkflowEngine.</param>
        /// <returns>Идентификатор предыдущего состояния карточки из параметров процесса. Если не найден в параметрах действия, то считается равным идентификатору состояния <see cref="KrState.Draft"/>.</returns>
        /// <seealso cref="PreviousStateKey"/>
        protected int TryGetPreviousState(IWorkflowEngineContext context)
        {
            return context.ProcessInstance.Hash.TryGet<int?>(PreviousStateKey) ?? KrState.Draft.ID;
        }

        /// <summary>
        /// Добавляет указанный идентификатор задания в список активных заданий.
        /// </summary>
        /// <param name="context">Контекст обработки процесса.</param>
        /// <param name="taskID">Идентификатор задания.</param>
        /// <returns>Асинхронная задача.</returns>
        protected async Task AddActiveTaskAsync(
            IWorkflowEngineContext context,
            Guid taskID)
        {
            var sCard = await context.TryGetKrSatelliteCardAsync(
                cancellationToken: context.CancellationToken);

            if (sCard is null)
            {
                return;
            }

            var activeTasksSection = sCard.GetActiveTasksSection();

            if (activeTasksSection.Rows.Any(p => taskID.Equals(p.Fields[KrConstants.KrActiveTasks.TaskID])))
            {
                throw new InvalidOperationException($"Task with id \"{taskID.ToString()}\" is already active.");
            }

            var row = activeTasksSection.Rows.Add();
            row.State = CardRowState.Inserted;
            row.RowID = Guid.NewGuid();
            row.Fields[KrConstants.KrActiveTasks.TaskID] = taskID;
        }

        /// <summary>
        /// Удаляет указанный идентификатор задания из списка активных заданий.
        /// </summary>
        /// <param name="context">Контекст обработки процесса.</param>
        /// <param name="taskID">Идентификатор задания.</param>
        /// <returns>Значение <see langword="true"/>, если идентификатор задания успешно удалён из списка активных заданий, иначе - <see langword="false"/>.</returns>
        protected async ValueTask<bool> TryRemoveActiveTaskAsync(
            IWorkflowEngineContext context,
            Guid taskID)
        {
            var sCard = await context.TryGetKrSatelliteCardAsync(
                cancellationToken: context.CancellationToken);

            if (sCard is null)
            {
                return false;
            }

            var activeTasksSection = sCard.GetActiveTasksSection();
            var activeTaskRow = activeTasksSection.Rows.FirstOrDefault(p => taskID.Equals(p.Fields[KrConstants.KrActiveTasks.TaskID]));

            switch (activeTaskRow?.State)
            {
                case null:
                case CardRowState.Deleted:
                    return false;
                case CardRowState.Inserted:
                    activeTasksSection.Rows.Remove(activeTaskRow);
                    break;
                case CardRowState.Modified:
                case CardRowState.None:
                    activeTaskRow.State = CardRowState.Deleted;
                    break;
            }

            return true;
        }

        /// <summary>
        /// Возвращает доступную только для чтения коллекцию идентификаторов активных заданий.
        /// </summary>
        /// <param name="context">Контекст обработки процесса.</param>
        /// <returns>Доступная только для чтения коллекция идентификаторов активных заданий.</returns>
        protected async ValueTask<ReadOnlyCollection<Guid>> GetActiveTasksAsync(
            IWorkflowEngineContext context)
        {
            var sCard = await context.TryGetKrSatelliteCardAsync(
                cancellationToken: context.CancellationToken);

            if (sCard is null)
            {
                return EmptyHolder<Guid>.Collection;
            }

            return sCard.GetActiveTasksSection()
                .Rows
                .Select(p => p.Get<Guid>(KrConstants.KrActiveTasks.TaskID))
                .ToList()
                .AsReadOnly();
        }

        /// <summary>
        /// Добавляет в историю процесса запись о задании.
        /// </summary>
        /// <param name="context">Контекст обработки процесса в WorkflowEngine.</param>
        /// <param name="taskRowID">Идентификатор задания.</param>
        /// <param name="cycle">Номер цикла согласования.</param>
        /// <param name="isAdvisory">Значение <see langword="true"/>, если задание является рекомендательным, иначе - <see langword="false"/>.</param>
        /// <returns>Асинхронная задача.</returns>
        protected async Task AddToHistoryAsync(
            IWorkflowEngineContext context,
            Guid taskRowID,
            int cycle,
            bool isAdvisory = default)
        {
            (await context.TryGetKrSatelliteCardAsync(cancellationToken: context.CancellationToken))?.AddToHistory(taskRowID, cycle: cycle, advisory: isAdvisory);
        }

        #endregion

        #region Private Methods

        private void ModifyRequest(CardStoreRequest request)
        {
            request.AffectVersion = true;
        }

        #endregion
    }
}
