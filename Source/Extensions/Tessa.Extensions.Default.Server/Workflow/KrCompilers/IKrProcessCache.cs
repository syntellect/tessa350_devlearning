﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Platform.Collections;

namespace Tessa.Extensions.Default.Server.Workflow.KrCompilers
{
    /// <summary>
    /// Кэш для данных из карточек шаблонов этапов
    /// </summary>
    public interface IKrProcessCache
    {
        /// <summary>
        /// Возвращает информацию обо всех группах этапов.
        /// </summary>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns>Доступный только для чтений словарь, ключом в котором является - ИД группы этапов, значением - информация о группе этапов.</returns>
        ValueTask<IReadOnlyDictionary<Guid, IKrStageGroup>> GetAllStageGroupsAsync(CancellationToken cancellationToken = default);

        /// <summary>
        /// Возвращает список всех групп этапов.
        /// </summary>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns>Доступный только для чтения список всех групп этапов отсортированный по порядковому номеру.</returns>
        ValueTask<IReadOnlyList<IKrStageGroup>> GetOrderedStageGroupsAsync(CancellationToken cancellationToken = default);

        /// <summary>
        /// Возвращает коллекцию с информацией о группах этапов для процесса с указанным ИД.
        /// </summary>
        /// <param name="process">ИД процесса или значение по умолчанию для типа, если необходимо получить список групп этапов не привязанных к процессу (<see cref="IKrStageGroup.SecondaryProcessID"/> = null).</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns>Коллекцию содержащая информацию о группах этапов для процесса с указанным ИД.</returns>
        ValueTask<IReadOnlyList<IKrStageGroup>> GetStageGroupsForSecondaryProcessAsync(
            Guid? process,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Возвращает словарь содержащий информацию обо всех шаблонах этапов.
        /// </summary>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns>Доступны только для чтения словарь содержащий информацию обо всех шаблонах этапов, ключ в котором - ИД шаблона, значение - информация о шаблоне.</returns>
        ValueTask<IReadOnlyDictionary<Guid, IKrStageTemplate>> GetAllStageTemplatesAsync(CancellationToken cancellationToken = default);

        /// <summary>
        /// Возвращает коллекцию с информацией о всех шаблонах этапов входящих в группу с указанным ИД.
        /// </summary>
        /// <param name="groupID">ИД группы этапов.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns>Доступная только для чтения коллекцию с информацией о всех шаблонах этапов входящих в группу с указанным ИД.</returns>
        ValueTask<IReadOnlyList<IKrStageTemplate>> GetStageTemplatesForGroupAsync(
            Guid groupID,
            CancellationToken cancellationToken = default);
        
        /// <summary>
        /// Возвращает словарь содержащий информацию о всех рантайм скриптах.
        /// </summary>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns>Доступный только для чтения словарь содержащий информацию о всех рантайм скриптах, ключ в котором - ИД этапа, значение - информация о рантайм скриптах этапа.</returns>
        ValueTask<IReadOnlyDictionary<Guid, IKrRuntimeStage>> GetAllRuntimeStagesAsync(CancellationToken cancellationToken = default);

        /// <summary>
        /// Возвращает коллекцию содержащую информацию обо всех этапах для указанного шаблона.
        /// </summary>
        /// <param name="templateID">ИД шаблона.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns>Доступная только для чтения коллекцию содержащаю информацию обо всех этапах для указанного шаблона.</returns>
        ValueTask<IReadOnlyList<IKrRuntimeStage>> GetRuntimeStagesForTemplateAsync(
            Guid templateID,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Возвращает коллекцию с информацией обо всех базовых методах.
        /// </summary>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns>Доступная тольок для чтения коллекцию с информацией обо всех базовых методах.</returns>
        ValueTask<IReadOnlyList<IKrCommonMethod>> GetAllCommonMethodsAsync(CancellationToken cancellationToken = default);

        /// <summary>
        /// Возвращает вторичный процесс по его идентификатору.
        /// </summary>
        /// <param name="pid">Идентификатор вторичного процесса.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns>Вторичный процесс.</returns>
        ValueTask<IKrSecondaryProcess> GetSecondaryProcessAsync(
            Guid pid,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Возвращает информацию обо всех вторичных процессах.
        /// </summary>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns>Доступный только для чтения словарь содержащий информацию обо всех вторичных процессах, ключ - ИД вторичного процесса, значение - информация о вторичном процессе.</returns>
        ValueTask<IReadOnlyDictionary<Guid, IKrPureProcess>> GetAllPureProcessesAsync(CancellationToken cancellationToken = default);

        /// <summary>
        /// Возвращает коллекцию содержащую информацию о действиях по указанному типу действия.
        /// </summary>
        /// <param name="actionType">Тип действия.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns>Доступная только для чтения коллекцию содержащаю информацию о действиях по указанному типу действия.</returns>
        ValueTask<IReadOnlyCollection<IKrAction>> GetActionsByTypeAsync(
            string actionType,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Возвращает словарь содержащий информацию обо всех действиях.
        /// </summary>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns>Доступный только для чтения словарь содержащий информацию обо всех действиях, ключ - ИД действия, значение - информация о действии.</returns>
        ValueTask<IReadOnlyDictionary<Guid, IKrAction>> GetAllActionsAsync(CancellationToken cancellationToken = default);

        /// <summary>
        /// Возвращает словарь содержащий информацию обо всех кнопках вторичных процессов.
        /// </summary>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns>Доступный только для чтения словарь содержащий информацию обо всех кнопках вторичных процессов, ключ - ИД кнопки, значение - информация о кнопке.</returns>
        ValueTask<IReadOnlyDictionary<Guid, IKrProcessButton>> GetAllButtonsAsync(CancellationToken cancellationToken = default);

        /// <summary>
        /// Выполняет сброс кэша.
        /// </summary>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns>Асинхронная задача.</returns>
        Task InvalidateAsync(CancellationToken cancellationToken = default);
    }
}
