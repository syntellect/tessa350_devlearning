﻿using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Extensions;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;

namespace Tessa.Extensions.Default.Server.Workflow.KrCompilers.Requests
{
    /// <summary>
    /// Расширение выполняет заполнение виртуальных секций результата
    /// компиляции для карточек KrStageTemplates и KrCommonMethod
    /// </summary>
    public sealed class KrSourceGetExtension: CardGetExtension
    {
        #region fields

        private readonly IKrCompilationResultStorage compilationResultStorage;

        #endregion

        #region constructors

        public KrSourceGetExtension(
            IKrCompilationResultStorage compilationResultStorage)
        {
            this.compilationResultStorage = compilationResultStorage;
        }

        #endregion

        #region private

        private async Task FillCompilerOutputAsync(Card card, CancellationToken cancellationToken = default)
        {
            var output = await this.compilationResultStorage.GetCompilationOutputAsync(card.ID, cancellationToken);
            card.Sections[KrConstants.KrStageBuildOutputVirtual.Name].RawFields[KrConstants.KrStageBuildOutputVirtual.LocalBuildOutput] = output.Local;
            card.Sections[KrConstants.KrStageBuildOutputVirtual.Name].RawFields[KrConstants.KrStageBuildOutputVirtual.GlobalBuildOutput] = output.Global;
        }

        #endregion

        #region base overrides

        public override Task AfterRequest(ICardGetExtensionContext context)
        {
            var card = context.Response.Card;

            return this.FillCompilerOutputAsync(card, context.CancellationToken);
        }

        #endregion
    }
}
