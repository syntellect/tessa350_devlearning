﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.ComponentModel;
using Tessa.Cards.Extensions;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Serialization;
using Tessa.Extensions.Default.Shared;
using Tessa.Platform;
using Tessa.Platform.Data;
using Tessa.Platform.Storage;
using Tessa.Roles;
using Unity;
using static Tessa.Extensions.Default.Shared.Workflow.KrProcess.KrConstants;

namespace Tessa.Extensions.Default.Server.Workflow.KrCompilers.Requests
{
    public sealed class KrSecondaryProcessStoreExtension : KrTemplateStoreExtension
    {
        #region Fields
        
        private readonly ICardTransactionStrategy transactionStrategyWt;

        private readonly ICardGetStrategy getStrategy;

        #endregion


        #region Constructor
        
        public KrSecondaryProcessStoreExtension(
            IKrStageSerializer serializer,
            ICardStoreStrategy cardStoreStrategy,
            ICardMetadata cardMetadata,
            [Dependency(CardTransactionStrategyNames.WithoutTransaction)] ICardTransactionStrategy transactionStrategyWt,
            ICardGetStrategy getStrategy,
            IKrProcessCache krProcessCache)
            : base(serializer, cardStoreStrategy, cardMetadata, krProcessCache)
        {
            this.transactionStrategyWt = transactionStrategyWt;
            this.getStrategy = getStrategy;
        }

        #endregion

        #region Base overrides
        
        /// <inheritdoc />
        protected override async Task<Card> GetInnerCardAsync(
            ICardStoreExtensionContext context)
        {
            var cardID = context.Request.Card.ID;
            var validationResult = context.ValidationResult;
            Card card = null;

            await this.transactionStrategyWt.ExecuteInReaderLockAsync(
                cardID,
                validationResult,
                async p =>
                {
                    var getContext = await this.getStrategy
                        .TryLoadCardInstanceAsync(
                            cardID,
                            p.DbScope.Db,
                            this.CardMetadata,
                            p.ValidationResult,
                            cancellationToken: p.CancellationToken);

                    if (getContext == null)
                    {
                        p.ReportError = true;
                        return;
                    }

                    getContext.SectionsToExclude.AddRange(
                        await this.GetKrSecondaryProcessSectionsToExcludeAsync(p.CancellationToken));

                    await this.getStrategy.LoadSectionsAsync(getContext, p.CancellationToken);

                    if (getContext.Card != null)
                    {
                        card = getContext.Card;
                    }
                    else
                    {
                        p.ReportError = true;
                    }
                },
                context.CancellationToken);

            if (!validationResult.IsSuccessful())
            {
                return null;
            }

            await this.Serializer.DeserializeSectionsAsync(card, card, cancellationToken: context.CancellationToken);
            return card;
        }

        /// <inheritdoc/>
        public override async Task BeforeRequest(ICardStoreExtensionContext context)
        {
            await base.BeforeRequest(context);

            if (!context.ValidationResult.IsSuccessful())
            {
                return;
            }

            if (context.Request.Card.Sections.TryGetValue(KrSecondaryProcessRoles.Name, out var secondaryProcessRolesSection))
            {
                await using (context.DbScope.Create())
                {
                    await SetIsContextForAllRolesAsync(
                        secondaryProcessRolesSection,
                        context.DbScope,
                        context.CancellationToken);
                }
            }
        }

        #endregion

        #region Private methods

        private async ValueTask<Guid[]> GetKrSecondaryProcessSectionsToExcludeAsync(CancellationToken cancellationToken = default)
        {
            var stageTemplateSectionsIDs = new HashSet<Guid>(
                (await this.CardMetadata.GetCardTypesAsync(cancellationToken))[DefaultCardTypes.KrSecondaryProcessTypeID]
                    .SchemeItems
                    .Select(x => x.SectionID));

            return (await this.CardMetadata.GetSectionsAsync(cancellationToken))
                .Select(x => x.ID)
                .Where(id => stageTemplateSectionsIDs.Contains(id)
                    && id != DefaultSchemeHelper.KrStages
                    && id != DefaultSchemeHelper.KrSecondaryProcesses)
                .ToArray();
        }

        /// <summary>
        /// Устанавливает значение <see langword="true"/> в поле IsContext, если роль с идентификатором расположенным в поле RoleID является контекстной (<see cref="RoleHelper.ContextRoleTypeID"/>), иначе - <see langword="false"/>.
        /// </summary>
        /// <param name="section">Табличная секция, в которой находятся ссылочные колонки
        /// ролей, для каждой из которых нужно проставить признак IsContext.</param>
        /// <param name="dbScope">Объект для взаимодействия с базой данных.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Асинхронная задача.</returns>
        private static async Task SetIsContextForAllRolesAsync(
            CardSection section,
            IDbScope dbScope,
            CancellationToken cancellationToken = default)
        {
            ListStorage<CardRow> rows;
            if ((rows = section.TryGetRows()) is null
                || rows.Count == 0)
            {
                return;
            }

            Dictionary<Guid, bool> isContextByRoleID = default;
            foreach (var row in rows)
            {
                if (row.State == CardRowState.Inserted
                    || row.State == CardRowState.Modified)
                {
                    if (isContextByRoleID is null)
                    {
                        isContextByRoleID = new Dictionary<Guid, bool>();
                    }

                    var roleID = row.TryGet<Guid?>("RoleID");
                    if (roleID.HasValue)
                    {
                        if (!isContextByRoleID.TryGetValue(roleID.Value, out var roleIsContext))
                        {
                            var db = dbScope.Db;
                            var builderFactory = dbScope.BuilderFactory;

                            Guid roleTypeID = await db
                                .SetCommand(
                                    builderFactory
                                        .Select().C("TypeID")
                                        .From("Instances").NoLock()
                                        .Where().C("ID").Equals().P("RoleID")
                                        .Build(),
                                    db.Parameter("RoleID", roleID.Value))
                                .LogCommand()
                                .ExecuteAsync<Guid>(cancellationToken);

                            roleIsContext = roleTypeID == RoleHelper.ContextRoleTypeID;
                            isContextByRoleID.Add(roleID.Value, roleIsContext);
                        }

                        row["IsContext"] = BooleanBoxes.Box(roleIsContext);
                    }
                }
            }
        }

        #endregion
    }
}