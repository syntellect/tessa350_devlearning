﻿using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Extensions;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Events;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Scope;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Actions
{
    public sealed class TaskStoreEventEmitter : CardStoreTaskExtension
    {
        private readonly IKrEventManager eventManager;

        private readonly IKrTypesCache typesCache;

        private readonly IKrScope scope;

        private readonly ICardFileManager fileManager;

        public TaskStoreEventEmitter(
            IKrEventManager eventManager,
            IKrTypesCache typesCache,
            IKrScope scope,
            ICardFileManager fileManager)
        {
            this.eventManager = eventManager;
            this.typesCache = typesCache;
            this.scope = scope;
            this.fileManager = fileManager;
        }

        public override async Task StoreTaskBeforeRequest(
            ICardStoreTaskExtensionContext context)
        {
            if (!context.ValidationResult.IsSuccessful()
                || !await KrComponentsHelper.HasBaseAsync(context.Request.Card.TypeID, this.typesCache, context.CancellationToken))
            {
                return;
            }

            var task = context.Task;
            var cardID = context.Request.Card.ID;
            await using var card = new ObviousMainCardAccessStrategy(context.Request.Card, this.fileManager, context.ValidationResult);
            {
                var validationResult = context.ValidationResult;

                if (task.State == CardRowState.Inserted)
                {
                        await this.eventManager.RaiseAsync(
                                DefaultEventTypes.BeforeNewTask,
                                cardID,
                                card,
                                context,
                                validationResult,
                                cancellationToken: context.CancellationToken);
                        return;
                }

                if (task.OptionID.HasValue && task.Action == CardTaskAction.Complete)
                {
                        await this.eventManager.RaiseAsync(
                                DefaultEventTypes.BeforeCompleteTask,
                                cardID,
                                card,
                                context,
                                validationResult,
                                cancellationToken: context.CancellationToken);
                }
            }
        }

        public override async Task StoreTaskBeforeCommitTransaction(
            ICardStoreTaskExtensionContext context)
        {
            if (!context.ValidationResult.IsSuccessful()
                || !await KrComponentsHelper.HasBaseAsync(context.Request.Card.TypeID, this.typesCache, context.CancellationToken))
            {
                return;
            }

            var task = context.Task;
            var cardID = context.Request.Card.ID;
            await using var card = new KrScopeMainCardAccessStrategy(cardID, this.scope);
            var validationResult = context.ValidationResult;

            if (task.State == CardRowState.Inserted)
            {
                await this.eventManager.RaiseAsync(DefaultEventTypes.NewTask, cardID, card, context, validationResult, cancellationToken: context.CancellationToken);
                return;
            }

            if (task.OptionID.HasValue
                && task.Action == CardTaskAction.Complete)
            {
                await this.eventManager.RaiseAsync(DefaultEventTypes.CompleteTask, cardID, card, context, validationResult, cancellationToken: context.CancellationToken);
            }
        }
    }
}