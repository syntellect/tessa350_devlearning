﻿using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Extensions;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Events;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Actions
{
    public sealed class CardNewEventEmitter : CardNewExtension
    {
        private readonly IKrEventManager eventManager;

        private readonly IKrTypesCache typesCache;

        private readonly ICardFileManager fileManager;

        public CardNewEventEmitter(
            IKrEventManager eventManager,
            IKrTypesCache typesCache,
            ICardFileManager fileManager)
        {
            this.eventManager = eventManager;
            this.typesCache = typesCache;
            this.fileManager = fileManager;
        }


        public override async Task AfterRequest(
            ICardNewExtensionContext context)
        {
            if (!context.ValidationResult.IsSuccessful()
                || !await KrComponentsHelper.HasBaseAsync(context.Response.Card.TypeID, this.typesCache, context.CancellationToken))
            {
                return;
            }

            var validationResult = context.ValidationResult;
            var cardID = context.Response.Card.ID;
            await using var strategy = new ObviousMainCardAccessStrategy(context.Response.Card, this.fileManager, context.ValidationResult);
            await this.eventManager.RaiseAsync(
                DefaultEventTypes.NewCard,
                cardID,
                strategy,
                context,
                validationResult,
                cancellationToken: context.CancellationToken);
        }

    }
}