﻿using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Extensions;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Events;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Scope;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Actions
{
    public sealed class CardStoreEventEmitter: CardStoreExtension
    {
        private readonly IKrEventManager eventManager;

        private readonly IKrTypesCache typesCache;

        private readonly IKrScope scope;

        private readonly ICardFileManager fileManager;

        public CardStoreEventEmitter(
            IKrEventManager eventManager,
            IKrTypesCache typesCache,
            IKrScope scope,
            ICardFileManager fileManager)
        {
            this.eventManager = eventManager;
            this.typesCache = typesCache;
            this.scope = scope;
            this.fileManager = fileManager;
        }

        public override async Task BeforeRequest(
            ICardStoreExtensionContext context)
        {
            if (!context.ValidationResult.IsSuccessful()
                || !await KrComponentsHelper.HasBaseAsync(context.Request.Card.TypeID, this.typesCache, context.CancellationToken))
            {
                return;
            }

            var cardID = context.Request.Card.ID;
            await using var card = new ObviousMainCardAccessStrategy(context.Request.Card, this.fileManager, context.ValidationResult);
            var validationResult = context.ValidationResult;

            await this.eventManager.RaiseAsync(
                DefaultEventTypes.BeforeStoreCard,
                cardID,
                card,
                context,
                validationResult,
                cancellationToken: context.CancellationToken);
        }

        public override async Task BeforeCommitTransaction(
            ICardStoreExtensionContext context)
        {
            if (!context.ValidationResult.IsSuccessful()
                || !await KrComponentsHelper.HasBaseAsync(context.Request.Card.TypeID, this.typesCache, context.CancellationToken))
            {
                return;
            }

            var cardID = context.Request.Card.ID;
            await using var card = new KrScopeMainCardAccessStrategy(cardID, this.scope);
            var validationResult = context.ValidationResult;

            await this.eventManager.RaiseAsync(
                DefaultEventTypes.StoreCard,
                cardID,
                card,
                context,
                validationResult,
                cancellationToken: context.CancellationToken);
        }
    }
}