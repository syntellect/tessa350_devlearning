﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using Tessa.Cards;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Serialization;
using Tessa.Extensions.Default.Shared.Workflow.KrCompilers;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Platform;
using Tessa.Platform.Storage;
using Tessa.Properties.Resharper;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess
{
    public readonly struct KrProcessSectionMapper
    {
        /// <summary>
        /// Предоставляет информацию о позиции этапа.
        /// </summary>
        [DebuggerDisplay("{" + nameof(StagePosition.ToDebugString) + "(), nq}")]
        private sealed class StagePosition
        {
            /// <summary>
            /// Порядковый индекс позиции скрытого этапа.
            /// </summary>
            private const int hiddenStagePositionIndex = int.MinValue;

            /// <summary>
            /// Инициализирует новый экземпляр класса <see cref="StagePosition"/>.
            /// </summary>
            /// <param name="posInfo">Информация о позиции этапа полученная с клиента. Формируется при загрузке карточки.</param>
            /// <param name="modifiedRow">Изменённая строка.</param>
            /// <param name="savedRow">Сохранённая ранее строка.</param>
            public StagePosition(
                KrStagePositionInfo posInfo,
                CardRow modifiedRow,
                CardRow savedRow)
            {
                // ao - скрытый
                // no - новый
                // ao so - измененный, но не передвинутый
                // ao so no - измененный и передвинутый
                // co - (no ?? so ?? minValue)

                this.RowID = posInfo?.RowID ?? modifiedRow.RowID;
                this.GroupOrder = posInfo?.GroupOrder ??
                    (modifiedRow.Fields.TryGetValue(KrConstants.StageGroupOrder, out var rowStageGroupOrder)
                    ? (int)rowStageGroupOrder
                    : savedRow.Get<int>(KrConstants.StageGroupOrder));
                this.AbsoluteOrder = posInfo?.AbsoluteOrder;
                this.ShiftedOrder = posInfo?.ShiftedOrder;
                this.NewOrder = modifiedRow?.TryGet<int?>(KrConstants.Order);
                this.CombinedOrder = this.NewOrder ?? this.ShiftedOrder ?? hiddenStagePositionIndex;

                // Этап скрыт на клиенте.
                var isHidden =
                    modifiedRow != null
                    && modifiedRow.State == CardRowState.Deleted
                    && KrStageSerializer.CanBeSkipped(savedRow);

                this.IsHidden = isHidden || this.CombinedOrder == hiddenStagePositionIndex;
                this.IsNew = this.NewOrder.HasValue && !this.AbsoluteOrder.HasValue;
                this.GroupPos = posInfo?.GroupPosition;
                this.Row = modifiedRow;
                this.HiddenRow = posInfo?.CardRow;
            }

            /// <summary>
            /// Идентификатор строки.
            /// </summary>
            public readonly Guid RowID;

            /// <summary>
            /// Порядок сортировки группы этапов.
            /// </summary>
            public readonly int GroupOrder;

            /// <summary>
            /// Абсолютный порядок этапа в маршруте.
            /// </summary>
            public readonly int? AbsoluteOrder;

            /// <summary>
            /// Сдвинутый порядок с учетом скрытых этапов.
            /// </summary>
            public readonly int? ShiftedOrder;

            /// <summary>
            /// Порядок этапа полученный из строки этапа. Значение доступно, если порядок строки был изменён на клиенте.
            /// </summary>
            public readonly int? NewOrder;

            /// <summary>
            /// co
            /// </summary>
            public readonly int CombinedOrder;

            /// <summary>
            /// Этап является скрытым.
            /// </summary>
            public readonly bool IsHidden;

            /// <summary>
            /// Этап добавлен пользователем.
            /// </summary>
            public readonly bool IsNew;

            /// <summary>
            /// Позиция в группе.
            /// </summary>
            public readonly int? GroupPos;

            /// <summary>
            /// Строка из основной карточки. Значение доступно, если строка этапа была изменена на клиенте.
            /// </summary>
            public readonly CardRow Row;

            /// <summary>
            /// Скрытая строка из карточки.
            /// Присутствует, если с клиента приходит скрытая строка на добавление.
            /// </summary>
            public readonly CardRow HiddenRow;

            /// <summary>
            /// Возвращает информацию содержащую отладочную информацию.
            /// </summary>
            /// <returns>
            /// Строковое представление объекта отображаемое в режиме отладки.
            /// </returns>
            [UsedImplicitly]
            private string ToDebugString()
            {
                return $"{DebugHelper.GetTypeName(this)}" +
                    $": Name = {DebugHelper.FormatNullable(this.Row?.TryGet<string>(KrConstants.Name) ?? this.HiddenRow?.TryGet<string>(KrConstants.Name))}, "
                    + nameof(this.GroupOrder) + $" = {this.GroupOrder.ToString()}, "
                    + nameof(this.AbsoluteOrder) + $" = {DebugHelper.FormatNullable(this.AbsoluteOrder)}, "
                    + nameof(this.ShiftedOrder) + $" = {DebugHelper.FormatNullable(this.ShiftedOrder)}, "
                    + nameof(this.NewOrder) + $" = {DebugHelper.FormatNullable(this.NewOrder)}, "
                    + nameof(this.CombinedOrder) + $" = {this.CombinedOrder.ToString()}, "
                    + nameof(this.GroupPos) + $" = {DebugHelper.FormatNullable(this.GroupPos)}, "
                    + nameof(this.IsHidden) + $" = {this.IsHidden.ToString()}";
            }
        }

        private sealed class GroupOrderComparer : IComparer<StagePosition>
        {
            /// <inheritdoc />
            public int Compare(
                StagePosition x,
                StagePosition y) => x.GroupOrder.CompareTo(y.GroupOrder);
        }

        private sealed class CombinedOrderComparer : IComparer<StagePosition>
        {
            /// <inheritdoc />
            public int Compare(
                StagePosition x,
                StagePosition y) =>
                x.CombinedOrder.CompareTo(y.CombinedOrder);
        }


        private static readonly string[] serviceFields =
        {
            KrConstants.KrProcessCommonInfo.CurrentApprovalStageRowID,
            KrConstants.KrApprovalCommonInfo.StateChangedDateTimeUTC,
            KrConstants.StateID,
            KrConstants.StateName,
            KrConstants.KrApprovalCommonInfo.DisapprovedBy,
            KrConstants.KrApprovalCommonInfo.ApprovedBy,
            KrConstants.KrProcessCommonInfo.MainCardID,
        };

        private readonly Card source;
        private readonly Card dest;
        private readonly bool defaultAddDestinationSection;

        private static readonly GroupOrderComparer groupOrderComparer = new GroupOrderComparer();
        private static readonly CombinedOrderComparer combinedOrderComparer = new CombinedOrderComparer();

        public KrProcessSectionMapper(
            Card source,
            Card dest,
            bool addDestinationSection = false)
        {
            this.source = source;
            this.dest = dest;
            this.defaultAddDestinationSection = addDestinationSection;
        }

        public KrProcessSectionMapper Map(
            string sourceSectionAlias,
            string destSectionAlias,
            Action<CardSection, IDictionary<string, object>> modifyAction = null,
            bool? addDestinationSection = null)
        {
            if (!this.source.Sections.TryGetValue(sourceSectionAlias, out var sourceSection))
            {
                return this;
            }

            CardSection destSection = null;
            if (addDestinationSection ?? this.defaultAddDestinationSection)
            {
                destSection = this.dest.Sections.GetOrAdd(destSectionAlias);
            }

            if (destSection != null 
                || this.dest.Sections.TryGetValue(destSectionAlias, out destSection))
            {
                StorageHelper.Merge(sourceSection.GetStorage(), destSection.GetStorage());

                if (destSection.Type == CardSectionType.Entry)
                {
                    modifyAction?.Invoke(destSection, destSection.RawFields);
                }
                else if (destSection.Type == CardSectionType.Table)
                {
                    foreach (var row in destSection.Rows)
                    {
                        modifyAction?.Invoke(destSection, row.GetStorage());
                    }
                }
            }
            return this;
        }

        /// <summary>
        /// Выполнить перенос несерализуемых данных из виртуальной секции KrApprovalCommonInfoVirtual в KrApprovalCommonInfo
        /// </summary>
        public KrProcessSectionMapper MapApprovalCommonInfo()
        {
            if (!this.source.TryGetKrApprovalCommonInfoSection(out var aci))
            {
                return this;
            }

            var satelliteAci = this.dest.GetApprovalInfoSection();
            foreach (var field in aci.RawFields)
            {
                var skip = false;
                for (int i = 0; i < serviceFields.Length; i++)
                {
                    if (field.Key == serviceFields[i])
                    {
                        skip = true;
                        break;
                    }
                }

                if (skip)
                {
                    continue;
                }

                satelliteAci.Fields[field.Key] = field.Value;
            }
            return this;
        }

        /// <summary>
        /// Выполнить перенос несериализуемых данных из виртуальной секции KrStagesVirtual в физическую KrStages.
        /// </summary>
        public KrProcessSectionMapper MapKrStages()
        {
            if (!this.source.TryGetStagesSection(out var sourceSec, true)
                || !this.dest.TryGetStagesSection(out var destSec))
            {
                return this;
            }

            if (HasOrderChanges(sourceSec)
                && this.source.TryGetStagePositions(out var stagePositions)
                && stagePositions.Count > 0)
            {
                MapKrStagesWithHidden(sourceSec, destSec, stagePositions);
            }
            else
            {
                MapKrStagesSimple(sourceSec, destSec);
            }
            return this;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool HasOrderChanges(
            CardSection sec) => sec.Rows.Any(p => p.ContainsKey(KrConstants.Order));

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void MapKrStagesWithHidden(
            CardSection sourceSec,
            CardSection destSec,
            IReadOnlyCollection<KrStagePositionInfo> stagePositionsInfo)
        {
            var srcRows = sourceSec.Rows;
            var destRows = destSec.Rows;

            RepairOrderStages(srcRows, destRows);

            var stagePositions = TransformToPositionsArray(srcRows, stagePositionsInfo, destRows, out var size);
            if (stagePositions.Length == 0)
            {
                return;
            }

            SortStagePositions(stagePositions, size);

            CardRow destRow;
            var order = 0;
            for (var i = 0; i < size; i++)
            {
                var pos = stagePositions[i];
                var srcRow = pos.Row;
                if (srcRow is null)
                {
                    if (pos.HiddenRow != null)
                    {
                        // Новый скрытый этап с клиента
                        var row = destRows.Add();
                        InsertInternal(pos.HiddenRow, row);
                        row.Fields[KrConstants.Order] = order;
                        row.Fields[KrConstants.KrStages.Hidden] = BooleanBoxes.True;
                    }
                    else
                    {
                        // Скрытый этап, в нем нужно только ордер пофиксать
                        destRow = destRows.FirstOrDefault(p => p.RowID == pos.RowID);
                        if (destRow != null)
                        {
                            destRow.Fields[KrConstants.Order] = order;
                        }
                    }
                }
                else
                {
                    switch (srcRow.State)
                    {
                        case CardRowState.Modified:
                            destRow = destRows.FirstOrDefault(p => p.RowID == pos.RowID);
                            if (destRow != null)
                            {
                                ModifyInternal(srcRow, destRow);
                                if (destRow.TryGetValue(KrConstants.Order, out var oldOrder)
                                    && !order.Equals(oldOrder))
                                {
                                    destRow.Fields[KrConstants.Order] = order;
                                }
                            }
                            break;
                        case CardRowState.Inserted:
                            var row = destRows.Add();
                            InsertInternal(srcRow, row);
                            row.Fields[KrConstants.Order] = order;
                            row.Fields[KrConstants.KrStages.Hidden] = BooleanBoxes.False;

                            break;
                        case CardRowState.Deleted:
                            var rowToDelete = destRows.FirstOrDefault(p => p.RowID == srcRow.RowID);
                            var isDeleted = false;
                            if (rowToDelete == null)
                            {
                                isDeleted = true;
                            }
                            else
                            {
                                isDeleted = DeleteInternal(rowToDelete);
                            }

                            if (isDeleted)
                            {
                                // Удаление не меняет порядок
                                order--;
                            }
                            break;
                    }
                }

                order++;
            }
        }

        /// <summary>
        /// Исправляет порядковый номер строки, если до неё было выполнено удаление (пропуск этапа).
        /// </summary>
        /// <param name="sourceRows">Обрабатываемые строки.</param>
        /// <param name="savedRows">Сохранённые ранее строки.</param>
        private static void RepairOrderStages(
            IReadOnlyCollection<CardRow> sourceRows,
            IReadOnlyCollection<CardRow> savedRows)
        {
            int deletedRows = default; // Число удалённых (пропущенных) строк.
            foreach (var sourceRow in sourceRows)
            {
                var savedRow = savedRows.FirstOrDefault(p => p.RowID == sourceRow.RowID);
                if (sourceRow.State == CardRowState.Deleted
                    && (savedRow != null)
                    && KrStageSerializer.CanBeSkipped(savedRow))
                {
                    deletedRows++;
                }
                else if (deletedRows > 0)
                {
                    var oldOrder = savedRow?.TryGet<int?>(KrConstants.Order);
                    var newOrder = sourceRow.Get<int>(KrConstants.Order) + deletedRows;
                    if (oldOrder == newOrder)
                    {
                        sourceRow.Remove(KrConstants.Order);
                    }
                    else
                    {
                        sourceRow[KrConstants.Order] = newOrder;
                    }
                }
            }
        }

        /// <summary>
        /// Сортирует позиции этапов.
        /// </summary>
        /// <param name="stagePositions">Массив, содержащий информацию о позиции этапов.</param>
        /// <param name="size">Реальное число элементов в <paramref name="stagePositions"/>.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void SortStagePositions(
            StagePosition[] stagePositions,
            int size)
        {
            // Сортировка этапов по порядковому номеру группы (GroupOrder).
            Array.Sort(stagePositions, 0, size, groupOrderComparer);

            // Выходит n неупорядоченных подмножеств, которые между собой упорядочены по GroupOrder.
            var currStage = stagePositions[0]; // Текущий этап.
            var currGroupID = currStage.GroupOrder; // Идентификатор текущей группы этапов.
            var groupFirstIdx = 0; // Порядковый номер этапа с которого начинается текущая группа.
            var hiddenCount = currStage.IsHidden ? 1 : 0; // Число скрытых этапов.
            for (var currIdx = 1; currIdx < size; currIdx++)
            {
                currStage = stagePositions[currIdx];
                if (currStage.GroupOrder != currGroupID)
                {
                    SortStagePositionInGroup(stagePositions, groupFirstIdx, currIdx, hiddenCount);
                    hiddenCount = 0;
                    groupFirstIdx = currIdx;
                    currGroupID = currStage.GroupOrder;
                }
                if (currStage.IsHidden)
                {
                    hiddenCount++;
                }
            }
            // Обработка последней группы.
            SortStagePositionInGroup(stagePositions, groupFirstIdx, size, hiddenCount);
        }

        /// <summary>
        /// Выполняет сортировку этапов внутри группы.
        /// </summary>
        /// <param name="stagePositions">Массив, содержащий информацию о позиции этапов.</param>
        /// <param name="groupFirstIdx">Порядковый номер этапа с которого начинается рассматриваемая группа.</param>
        /// <param name="groupLastIdx">Порядковый номер последнего этапа в рассматриваемой группе.</param>
        /// <param name="hiddenStageCount">Число скрытых этапов в рассматрирваемой группе.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void SortStagePositionInGroup(
            StagePosition[] stagePositions,
            int groupFirstIdx,
            int groupLastIdx,
            int hiddenStageCount)
        {
            var groupLength = groupLastIdx - groupFirstIdx;
            if (groupLength <= 1)
            {
                return;
            }

            // Сортировка этапов входящих в groupFirstIdx группу по CombinedOrder. 
            Array.Sort(stagePositions, groupFirstIdx, groupLength, combinedOrderComparer);

            if (hiddenStageCount == 0)
            {
                // Нет скрытых этапов в группе.
                return;
            }

            // Теперь необходимо разобраться, где должны быть во всем этом скрытые этапы с помощью ao
            // Все скрытые этапы, которые содержат только ao, будут в начале т.к. co == int.MinValue
            var firstFixedIndex = groupFirstIdx + hiddenStageCount;
            for (var i = firstFixedIndex - 1; i >= groupFirstIdx; i--, firstFixedIndex--)
            {
                // Частичная сортировка (модификация selection sort) для скрытых элементов
                var hiddenStage = stagePositions[i];
                var newIndex = FindHiddenStageIndex(stagePositions, hiddenStage, i, groupLastIdx - 1);
                if (newIndex != i)
                {
                    MoveStageToTheRight(stagePositions, i, newIndex);
                }
                // newIndex == firstFixedIndex оставляет скрытый этап в начале.
            }
            // CombinedOrder + Offset = new absolute order

        }

        /// <summary>
        /// Выполняет поиск позиции по которой должен распологаться указанный скрытый этап.
        /// </summary>
        /// <param name="stagePositions">Коллекция позиций этапов.</param>
        /// <param name="item">Cкрытый этап, для которого выполняется поиск позиции.</param>
        /// <param name="from">Индекс элемента с которого начинается поиск.</param>
        /// <param name="to">Индекс конечного элемента, до которого выполняется поиск.</param>
        /// <returns>Позиция по которой должен распологаться указанный скрытый этап.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int FindHiddenStageIndex(
            IReadOnlyList<StagePosition> stagePositions,
            StagePosition item,
            int from,
            int to)
        {
            // Поиск выполняется в интервале (<paramref name="from"/>; <paramref name="to"/>].
            var i = from + 1;

            // Находим первый не новый (с ао != null)
            for (; i <= to && stagePositions[i].IsNew; i++)
            {
            }

            // Все видимые этапы новые.
            if (i > to)
            {
                return item.GroupPos == GroupPosition.AtFirst.ID
                    ? from
                    : to;
            }
            var prev = stagePositions[i];

            if (item.AbsoluteOrder < prev.AbsoluteOrder)
            {
                // Место перед prev.
                return item.GroupPos == GroupPosition.AtFirst.ID
                    ? from // в самом начале, т.е. оставляем на месте.
                    : i - 1; // Внизу, где находятся "в конце группы".
                             // Берем i - 1, т.к. на i-той позиции уже с большим ao, т.е. "в конце группы"
            }

            var newCount = 0;
            for (; i <= to; i++)
            {
                var curr = stagePositions[i];
                if (curr.IsNew)
                {
                    newCount++;
                    continue;
                }

                if (prev.AbsoluteOrder < item.AbsoluteOrder
                    && item.AbsoluteOrder < curr.AbsoluteOrder)
                {
                    break;
                }

                prev = curr;
            }
            
            return item.GroupPos == GroupPosition.AtFirst.ID 
                ? i - newCount - 1
                : i - 1;
        }

        /// <summary>
        /// Перемещает элемент массива из позиции с индексом <paramref name="startIndex"/> в <paramref name="destIndex"/>.
        /// </summary>
        /// <param name="array">Массив содержащий перемещаемый элемент.</param>
        /// <param name="startIndex">Индекс по которому расположен перемещаемый элемент.</param>
        /// <param name="destIndex">Индекс по которому должен распологаться перемещаемый элемент.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void MoveStageToTheRight(
            StagePosition[] array,
            int startIndex,
            int destIndex)
        {
            var tmp = array[startIndex];
            // Согласно документации операция безопасна даже при наложении областей.
            Array.Copy(array, startIndex + 1, array, startIndex, destIndex - startIndex);
            array[destIndex] = tmp;
        }

        /// <summary>
        /// Формирует массив элементов содержащих информацию о позиции этапов.
        /// </summary>
        /// <param name="sourceRows">Коллекция изменённых строк - этапов.</param>
        /// <param name="stagePositionsInfo">Коллекция содержащая информацию о позиции этапов сформированная при загрузке карточки.</param>
        /// <param name="savedRows">Коллекция строк этапов содержащая сохранённую ранее информацию.</param>
        /// <param name="size">Реальное число элементов в результирующем массиве.</param>
        /// <returns>Массив, содержащий информацию о позиции этапов.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static StagePosition[] TransformToPositionsArray(
            IReadOnlyCollection<CardRow> sourceRows,
            IReadOnlyCollection<KrStagePositionInfo> stagePositionsInfo,
            IReadOnlyCollection<CardRow> savedRows,
            out int size)
        {
            size = 0;
            // Такой размер позволит точно превысить возможное количество этапов
            var stagePositions = new StagePosition[sourceRows.Count + stagePositionsInfo.Count];

            var modifiedRows = sourceRows
                .OrderBy(p => p.RowID)
                .GetEnumerator();
            var oldRows = stagePositionsInfo
                .OrderBy(p => p.RowID)
                .GetEnumerator();
            
            try
            {
                var hasModifiedRows = modifiedRows.MoveNext();
                var hasOldRows = oldRows.MoveNext();
                while (true)
                {
                    // Отсутствуют изменённые строки?
                    if (!hasModifiedRows)
                    {
                        // Отсутсвуют строки, информация по которым была передана при загрузке карточки?
                        if (!hasOldRows)
                        {
                            // Все этапы обработаны.
                            break;
                        }

                        // Обработка строк, информация по которым была передана при загрузке карточки.
                        do
                        {
                            var tailOldRow = oldRows.Current;
                            stagePositions[size++] = new StagePosition(tailOldRow, null, default);
                        } while (oldRows.MoveNext());
                        break;
                    }

                    // Отсутсвуют строки, информация по которым была передана при загрузке карточки?
                    if (!hasOldRows)
                    {
                        // Обработка изменённых строк.
                        do
                        {
                            var tailModifiedRow = modifiedRows.Current;
                            stagePositions[size++] = new StagePosition(null, tailModifiedRow, savedRows.FirstOrDefault(i => i.RowID == tailModifiedRow.RowID));
                        } while (modifiedRows.MoveNext());
                        break;
                    }

                    var modifiedRow = modifiedRows.Current;
                    var oldRow = oldRows.Current;
                    var comparisonResult = modifiedRow.RowID.CompareTo(oldRow.RowID);
                    switch (comparisonResult)
                    {
                        case 1:
                            // modifiedRow > oldRow
                            // Для старой строки нет модифицированной
                            stagePositions[size++] = new StagePosition(oldRow, null, default);
                            hasOldRows = oldRows.MoveNext();
                            break;
                        case 0:
                            // Строка на вставку есть и в списке старых этапов,
                            // И присутствует в измененных
                            // Возможна ситуация, когда modifiedRow.State == CardRowState.Inserted
                            // Это специфичный кейс, когда новый этап вставляется, 
                            // но о нем есть информация как о старом.
                            // Это создание копии или по шаблону.
                            stagePositions[size++] = new StagePosition(oldRow, modifiedRow, savedRows.FirstOrDefault(i => i.RowID == modifiedRow.RowID));
                            hasModifiedRows = modifiedRows.MoveNext();
                            hasOldRows = oldRows.MoveNext();
                            break;
                        case -1:
                            // modifiedRow < oldRow
                            // Для модифицированной строки нет старой - это явная вставка новой
                            stagePositions[size++] = new StagePosition(null, modifiedRow, savedRows.FirstOrDefault(i => i.RowID == modifiedRow.RowID));
                            hasModifiedRows = modifiedRows.MoveNext();
                            break;
                    }
                }
            }
            finally
            {
                modifiedRows.Dispose();
                oldRows.Dispose();
            }

            return stagePositions;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void MapKrStagesSimple(CardSection sourceSec, CardSection destSec)
        {
            var rows = destSec.Rows;
            foreach (var stSecRow in sourceSec.Rows)
            {
                switch (stSecRow.State)
                {
                    case CardRowState.Modified:
                        {
                            var row = rows.FirstOrDefault(p => p.RowID == stSecRow.RowID);

                            if (row != null)
                            {
                                ModifyInternal(stSecRow, row);
                                if (stSecRow.TryGetValue(KrConstants.Order, out var order))
                                {
                                    row.Fields[KrConstants.Order] = order;
                                }
                            }

                            break;
                        }
                    case CardRowState.Inserted:
                        {
                            var row = rows.Add();
                            InsertInternal(stSecRow, row);
                            if (stSecRow.TryGetValue(KrConstants.Order, out var order))
                            {
                                row.Fields[KrConstants.Order] = order;
                            }
                            break;
                        }
                    case CardRowState.Deleted:
                        var rowToDelete = rows.FirstOrDefault(p => p.RowID == stSecRow.RowID);
                        if (rowToDelete != null)
                        {
                            DeleteInternal(rowToDelete);
                        }
                        break;
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void InsertInternal(
            CardRow sourceRow,
            CardRow destRow)
        {
            destRow.RowID = sourceRow.RowID;
            destRow.State = CardRowState.Inserted;
            foreach (var pair in sourceRow
                .Where(k => !k.Key.StartsWith(CardHelper.SystemKeyPrefix)
                    && !k.Key.StartsWith(CardHelper.UserKeyPrefix)
                    && !StageTypeSettingsNaming.IsPlainName(k.Key)
                    && !StageTypeSettingsNaming.IsSectionName(k.Key)
                    && k.Key != KrConstants.Order))
            {
                destRow.Fields[pair.Key] = pair.Value;
            }

            destRow.Fields[KrConstants.KrStages.ExtraSources] = null;
            destRow.Fields[KrConstants.KrStages.NestedProcessID] = null;
            destRow.Fields[KrConstants.KrStages.ParentStageRowID] = null;
            destRow.Fields[KrConstants.KrStages.NestedOrder] = null;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void ModifyInternal(
            CardRow sourceRow,
            CardRow destRow)
        {
            destRow.State = CardRowState.Modified;
            foreach (var pair in sourceRow
                .Where(k => !k.Key.StartsWith(CardHelper.SystemKeyPrefix)
                    && !k.Key.StartsWith(CardHelper.UserKeyPrefix)
                    && destRow.ContainsKey(k.Key)
                    && !StageTypeSettingsNaming.IsPlainName(k.Key)
                    && !StageTypeSettingsNaming.IsSectionName(k.Key)
                    && k.Key != KrConstants.Order))
            {
                destRow.Fields[pair.Key] = pair.Value;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool DeleteInternal(
            CardRow destRow)
        {
            if (KrStageSerializer.CanBeSkipped(destRow))
            {
                destRow.State = CardRowState.Modified;

                destRow.Fields[KrConstants.KrStages.Skip] = BooleanBoxes.True;
                destRow.Fields[KrConstants.KrStages.Hidden] = BooleanBoxes.True;
                return false;
            }

            destRow.State = CardRowState.Deleted;
            return true;
        }
    }
}