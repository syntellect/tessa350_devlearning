﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tessa.Cards.Extensions;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Platform.Validation;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Scope
{
    public sealed class KrScopeStoreExtension : CardStoreExtension
    {
        #region fields

        private readonly IKrTypesCache krTypesCache;

        private List<KrProcessClientCommand> clientCommands;

        private IValidationResultBuilder scopeValidationResult;

        #endregion

        #region constructor

        public KrScopeStoreExtension(
            IKrTypesCache krTypesCache)
        {
            this.krTypesCache = krTypesCache;
        }

        #endregion

        #region base overrides

        public override async Task BeforeCommitTransaction(
            ICardStoreExtensionContext context)
        {
            if (!context.ValidationResult.IsSuccessful()
                || !await KrComponentsHelper.HasBaseAsync(context.Request.Card.TypeID, this.krTypesCache, context.CancellationToken))
            {
                return;
            }

            var scopeContext = KrScopeContext.Current;
            if (scopeContext != null)
            {
                await scopeContext.LevelStack.Peek().ApplyChangesAsync(
                    context.Request.Card.ID,
                    overridenValidationResult: context.ValidationResult,
                    cancellationToken: context.CancellationToken);
                this.clientCommands = scopeContext.GetKrProcessClientCommands();
                this.scopeValidationResult = scopeContext.ValidationResult;
            }
        }

        public override Task AfterRequest(
            ICardStoreExtensionContext context)
        {
            if (KrScopeContext.HasCurrent)
            {
                return Task.CompletedTask;
            }

            if (this.clientCommands != null)
            {
                context.Response.AddKrProcessClientCommands(this.clientCommands);
            }

            if (this.scopeValidationResult != null)
            {
                context.ValidationResult.Add(this.scopeValidationResult);
            }

            return Task.CompletedTask;
        }

        #endregion
    }
}