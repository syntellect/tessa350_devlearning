﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.ComponentModel;
using Tessa.Cards.Numbers;
using Tessa.Cards.Workflow;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Serialization;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Platform.Data;
using Tessa.Platform.Scopes;
using Tessa.Platform.Validation;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Scope
{
    public sealed class KrScopeLevel : IAsyncDisposable
    {
        #region fields

        private readonly ICardRepository cardRepository;
        private readonly IKrTokenProvider tokenProvider;
        private readonly IKrTypesCache krTypesCache;
        private readonly IKrStageSerializer serializer;
        private readonly ICardGetStrategy getStrategy;
        private readonly ICardStreamServerRepository streamServerRepository;

        private readonly IInheritableScopeInstance<KrScopeContext> scope;
        private readonly IDbScope dbScope;
        private readonly ICardMetadata cardMetadata;
        private readonly IValidationResultBuilder validationResult;


        #endregion

        #region constructor

        public KrScopeLevel(
            ICardRepository cardRepository,
            IKrTokenProvider tokenProvider,
            IKrTypesCache krTypesCache,
            IKrStageSerializer serializer,
            ICardGetStrategy getStrategy,
            ICardTransactionStrategy cardCardTransactionStrategy,
            IDbScope dbScope,
            ICardMetadata cardMetadata,
            ICardStreamServerRepository streamServerRepository,
            IValidationResultBuilder validationResult,
            bool withReaderLocks)
        {
            this.cardRepository = cardRepository;
            this.tokenProvider = tokenProvider;
            this.krTypesCache = krTypesCache;
            this.serializer = serializer;
            this.getStrategy = getStrategy;
            this.CardTransactionStrategy = cardCardTransactionStrategy;
            this.dbScope = dbScope;
            this.cardMetadata = cardMetadata;
            this.streamServerRepository = streamServerRepository;
            this.validationResult = validationResult;
            this.WithReaderLocks = withReaderLocks;

            this.scope = KrScopeContext.Create();
            this.scope.Value.LevelStack.Push(this);
        }

        #endregion

        #region properties

        public Guid LevelID { get; } = Guid.NewGuid();

        public bool Exited { get; private set; } // = false;

        public ICardTransactionStrategy CardTransactionStrategy { get; }

        public bool WithReaderLocks { get; }

        #endregion

        #region public

        public async Task ApplyChangesAsync(
            Guid mainCardID,
            IValidationResultBuilder overridenValidationResult = null,
            CancellationToken cancellationToken = default)
        {
            var scopeContext = KrScopeContext.Current;
            if (scopeContext == null)
            {
                return;
            }

            var locks = scopeContext.Locks;

            if (scopeContext.MainKrSatellites.TryGetItem(mainCardID, out var satellite)
                && !locks.Contains(satellite.ID))
            {
                ProcessInfoCacheHelper.Update(this.serializer, satellite);
                if (satellite.StoreMode == CardStoreMode.Insert || satellite.HasChanges())
                {
                    await this.StoreCardAsync(satellite, null, this.GetSuitableValidationResult(overridenValidationResult), cancellationToken: cancellationToken);
                }
            }

            var forceAffectVersion = scopeContext.ForceIncrementCardVersion.Remove(mainCardID);
            if (scopeContext.Cards.TryGetValue(mainCardID, out var mainCard)
                && !locks.Contains(mainCard.ID))
            {
                if (mainCard.HasChanges()
                    || mainCard.TryGetWorkflowQueue()?.Items?.Count > 0
                    || mainCard.HasNumberQueueToProcess()
                    || forceAffectVersion)
                {
                    scopeContext.CardFileContainers.TryGetValue(mainCardID, out var container);
                    await this.StoreCardAsync(
                        mainCard,
                        container,
                        this.GetSuitableValidationResult(overridenValidationResult),
                        forceAffectVersion: forceAffectVersion,
                        cancellationToken: cancellationToken);
                    // Освобождение ресурсов файлового контейнера будет выполнено в методе KrScopeLevel.Exit() после завершения сохранения карточки или при вызове KrScopeLevel.Dispose().
                }
            }
            else if (forceAffectVersion)
            {
                // Карточка не загружена, но нужно обязательно увеличить ее версию
                await this.ForceIncrementVersionAsync(mainCardID, this.GetSuitableValidationResult(overridenValidationResult), cancellationToken);
            }

            var deletedSecondarySatellites = new List<Guid>(scopeContext.SecondaryKrSatellites.Count);
            foreach (var secondarySatellitePair in scopeContext.SecondaryKrSatellites)
            {
                var secondarySatellite = secondarySatellitePair.Value;
                var secondaryProcessMainCardID = secondarySatellite
                    .GetApprovalInfoSection()
                    .Fields[KrConstants.KrProcessCommonInfo.MainCardID];

                if (locks.Contains(secondarySatellite.ID)
                    || !secondaryProcessMainCardID.Equals(mainCardID))
                {
                    continue;
                }

                var currentRowID = secondarySatellite
                    .GetApprovalInfoSection()
                    .Fields[KrConstants.KrProcessCommonInfo.CurrentApprovalStageRowID];

                // Процесс по сателлиту закончился, сателлит больше не нужен.
                if (currentRowID is null)
                {
                    // Если карточка уже создана, ее нужно удалить
                    if (secondarySatellite.StoreMode == CardStoreMode.Update)
                    {
                        await this.DeleteCardAsync(secondarySatellite, this.GetSuitableValidationResult(overridenValidationResult), cancellationToken);
                    }
                    // Если карточка только создана, но не сохранялась, можно просто забыть про нее.

                    deletedSecondarySatellites.Add(secondarySatellitePair.Key);
                }
                else
                {
                    ProcessInfoCacheHelper.Update(this.serializer, secondarySatellite);
                    if (secondarySatellite.StoreMode == CardStoreMode.Insert
                        || secondarySatellite.HasChanges())
                    {
                        await this.StoreCardAsync(secondarySatellite, null, this.GetSuitableValidationResult(overridenValidationResult), cancellationToken: cancellationToken);
                    }
                }
            }

            foreach (var id in deletedSecondarySatellites)
            {
                scopeContext.SecondaryKrSatellites.Remove(id);
            }
        }

        public async ValueTask ExitAsync(bool throwIfExited = true, bool throwErrors = true)
        {
            if (this.Exited)
            {
                if (throwIfExited)
                {
                    throw new InvalidOperationException("Current scope has already been disposed.");
                }
                return;
            }

            this.Exited = true;
            if (this.scope == null)
            {
                return;
            }
            var stackTop = this.scope.Value.LevelStack.Pop();
            if (stackTop != this)
            {
                const string text = "Trying to exit from non-top level.";
                if (throwErrors)
                {
                    throw new InvalidOperationException(text);
                }

                this.validationResult?.AddError(nameof(KrScopeLevel), text);
            }

            var ctx = KrScopeContext.Current;
            var locks = ctx.Locks;
            var cardFileContainers = ctx.CardFileContainers;
            this.scope.Dispose();
            if (!ctx.IsDisposed)
            {
                return;
            }

            if (cardFileContainers?.Count > 0)
            {
                foreach ((_, ICardFileContainer container) in cardFileContainers)
                {
                    if (container != null)
                    {
                        await container.DisposeAsync();
                    }
                }
            }

            foreach (var disposableObject in ctx.DisposableObjects)
            {
                disposableObject?.Dispose();
            }

            foreach (var disposableObject in ctx.AsyncDisposableObjects)
            {
                if (disposableObject != null)
                {
                    await disposableObject.DisposeAsync();
                }
            }

            if (locks.Count != 0)
            {
                var text = $"Disposed KrScope contains locks ({string.Join(", ", locks)}). " +
                    "All cards inside scope must be saved at the end (no card should be locked).";
                if (throwErrors)
                {
                    throw new InvalidOperationException(text);
                }

                this.validationResult?.AddError(nameof(KrScopeLevel), text);
            }
        }

        /// <inheritdoc />
        public async ValueTask DisposeAsync()
        {
            if (!this.Exited)
            {
                await this.ExitAsync(false, false);
            }
        }

        #endregion

        #region private

        private IValidationResultBuilder GetSuitableValidationResult(
            IValidationResultBuilder overriden) =>
            overriden ?? this.validationResult;

        private async Task StoreCardAsync(
            Card card,
            ICardFileContainer fileContainer,
            IValidationResultBuilder result,
            bool forceAffectVersion = false,
            CancellationToken cancellationToken = default)
        {
            var copy = card.Clone();
            card.RemoveChanges(CardRemoveChangesDeletedHandling.Remove);
            card.RemoveWorkflowQueue();
            card.RemoveNumberQueue();

            var storeMode = copy.StoreMode;
            if (storeMode == CardStoreMode.Update)
            {
                copy.UpdateStates();
            }

            copy.RemoveAllButChanged(storeMode);
            var request = new CardStoreRequest { Card = copy, AffectVersion = forceAffectVersion };

            if (await KrComponentsHelper.HasBaseAsync(card.TypeID, this.krTypesCache, cancellationToken))
            {
                this.tokenProvider.CreateToken(copy).Set(copy.Info);
            }

            var digest = await this.cardRepository.GetDigestAsync(card, CardDigestEventNames.ActionHistoryStoreRouteProcess, cancellationToken);
            if (digest != null)
            {
                request.SetDigest(digest);
            }

            var response = await CardHelper.StoreAsync(request, fileContainer?.FileContainer, this.cardRepository, this.streamServerRepository, cancellationToken);
            result.Add(response.ValidationResult);
            card.Version = response.CardVersion;
        }

        private async Task DeleteCardAsync(
            Card card,
            IValidationResultBuilder result,
            CancellationToken cancellationToken = default)
        {
            var req = new CardDeleteRequest
            {
                CardID = card.ID,
                CardTypeID = card.TypeID,
                DeletionMode = CardDeletionMode.WithoutBackup,
            };
            var resp = await this.cardRepository.DeleteAsync(req, cancellationToken);
            result.Add(resp.ValidationResult);
        }

        private Task ForceIncrementVersionAsync(
            Guid mainCardID,
            IValidationResultBuilder result,
            CancellationToken cancellationToken = default)
        {
            return this.CardTransactionStrategy.ExecuteInReaderLockAsync(
                mainCardID,
                result,
                p => this.ForceIncrementVersionInternalAsync(mainCardID, result, p.CancellationToken),
                cancellationToken
            );
        }

        private async Task ForceIncrementVersionInternalAsync(
            Guid mainCardID,
            IValidationResultBuilder result,
            CancellationToken cancellationToken = default)
        {
            var getContext = await this.getStrategy.TryLoadCardInstanceAsync(
                mainCardID,
                this.dbScope.Db,
                this.cardMetadata,
                result,
                cancellationToken: cancellationToken);

            var card = getContext.Card;
            var token = this.tokenProvider.CreateToken(card);
            token.Set(card.Info);

            string digest;
            CardStoreRequest workflowRequest = WorkflowScopeContext.Current.StoreContext?.Request;
            if (workflowRequest != null)
            {
                digest = workflowRequest.TryGetDigest();
            }
            else if (await this.getStrategy.LoadSectionsAsync(getContext, cancellationToken))
            {
                digest = await this.cardRepository.GetDigestAsync(card, CardDigestEventNames.ActionHistoryStoreRouteProcess, cancellationToken);
                card.RemoveAllButChanged();
            }
            else
            {
                digest = null;
            }

            var storeRequest = new CardStoreRequest { Card = card, AffectVersion = true, };
            storeRequest.SetDigest(digest);

            var storeResponse = await this.cardRepository.StoreAsync(storeRequest, cancellationToken);
            result.Add(storeResponse.ValidationResult);
        }

        #endregion
    }
}