﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.ComponentModel;
using Tessa.Cards.Extensions;
using Tessa.Cards.Workflow;
using Tessa.Extensions.Default.Server.Workflow.KrCompilers;
using Tessa.Extensions.Default.Server.Workflow.KrObjectModel;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Events;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Scope;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Platform;
using Tessa.Platform.Collections;
using Tessa.Platform.Data;
using Tessa.Platform.Runtime;
using Tessa.Platform.Validation;
using Tessa.Scheme;
using Unity;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow
{
    /// <summary>
    /// Предоставляет методы для запуска процесса на сервере.
    /// </summary>
    public sealed class KrProcessServerLauncher: IKrProcessLauncher
    {
        #region nested types

        /// <summary>
        /// Предоставляет параметры запуска процесса с сервера.
        /// </summary>
        public sealed class SpecificParameters : IKrProcessLauncherSpecificParameters
        {
            /// <summary>
            /// Возвращает или задаёт стратегию доступа к основной карточке.
            /// </summary>
            public IMainCardAccessStrategy MainCardAccessStrategy { get; set; }

            /// <inheritdoc/>
            public bool RaiseErrorWhenExecutionIsForbidden { get; set; } = true;

            /// <summary>
            /// Возвращает или задаёт значение, показывающее, что процесс запустить требуется в текущем выполнении запроса.
            /// </summary>
            /// <remarks>С учетом данной настройки процесс может быть запланирован только в BeforeRequest. Если не указывать параметр, то процесс будет запущен с помощью вложенного сохранения. В общем случае запускать необходимо именно во вложенном сохранении.</remarks>
            public bool UseSameRequest { get; set; } = false;
        }

        private sealed class CardInfo
        {
            public Guid CardTypeID;
            public string CardTypeName;
            public string CardTypeCaption;
            public Guid? DocTypeID;
            public int StateID;
        }

        #endregion

        #region fields

        private readonly IKrProcessRunnerProvider runnerProvider;

        private readonly Func<IKrExecutor> executorFunc;

        private readonly IKrScope krScope;

        private readonly ICardRepository cardRepository;

        private readonly ICardRepository cardRepositoryEwt;

        private readonly IKrTokenProvider tokenProvider;

        private readonly IKrTypesCache typesCache;

        private readonly IDbScope dbScope;

        private readonly ICardTransactionStrategy transactionStrategy;

        private readonly ICardGetStrategy getStrategy;

        private readonly ICardMetadata cardMetadata;

        private readonly ICardTaskHistoryManager taskHistoryManager;

        private readonly IKrProcessCache processCache;

        private readonly IKrSecondaryProcessExecutionEvaluator secondaryProcessExecutionEvaluator;

        private readonly ISession session;

        private readonly IKrEventManager eventManager;

        private readonly IObjectModelMapper objectModelMapper;

        private readonly ISignatureProvider signatureProvider;

        #endregion

        #region constructor

        public KrProcessServerLauncher(
            IKrProcessRunnerProvider runnerProvider,
            [Dependency(KrExecutorNames.CacheExecutor)] Func<IKrExecutor> executorFunc,
            IKrScope krScope,
            [Dependency(CardRepositoryNames.Extended)] ICardRepository cardRepository,
            [Dependency(CardRepositoryNames.ExtendedWithoutTransaction)]ICardRepository cardRepositoryEwt,
            IKrTokenProvider tokenProvider,
            IKrTypesCache typesCache,
            IDbScope dbScope,
            [Dependency(CardTransactionStrategyNames.Default)] ICardTransactionStrategy transactionStrategy,
            ICardGetStrategy getStrategy,
            ICardMetadata cardMetadata,
            ICardTaskHistoryManager taskHistoryManager,
            IKrProcessCache processCache,
            IKrSecondaryProcessExecutionEvaluator secondaryProcessExecutionEvaluator,
            ISession session,
            IKrEventManager eventManager,
            IObjectModelMapper objectModelMapper,
            ISignatureProvider signatureProvider)
        {
            this.runnerProvider = runnerProvider;
            this.executorFunc = executorFunc;
            this.krScope = krScope;
            this.cardRepository = cardRepository;
            this.cardRepositoryEwt = cardRepositoryEwt;
            this.tokenProvider = tokenProvider;
            this.typesCache = typesCache;
            this.dbScope = dbScope;
            this.transactionStrategy = transactionStrategy;
            this.getStrategy = getStrategy;
            this.cardMetadata = cardMetadata;
            this.taskHistoryManager = taskHistoryManager;
            this.processCache = processCache;
            this.secondaryProcessExecutionEvaluator = secondaryProcessExecutionEvaluator;
            this.session = session;
            this.eventManager = eventManager;
            this.objectModelMapper = objectModelMapper;
            this.signatureProvider = signatureProvider;
        }

        #endregion

        #region implementation

        /// <inheritdoc />
        public async Task<IKrProcessLaunchResult> LaunchAsync(
            KrProcessInstance krProcess,
            ICardExtensionContext cardContext = null,
            IKrProcessLauncherSpecificParameters specificParameters = null,
            CancellationToken cancellationToken = default)
        {
            bool raiseErrorIfForbbiden = false;
            if (specificParameters is SpecificParameters sp)
            {
                raiseErrorIfForbbiden = sp.RaiseErrorWhenExecutionIsForbidden;
            }

            var pid = krProcess.ProcessID;
            var process = await this.processCache.GetSecondaryProcessAsync(pid, cancellationToken);
            var validationResult = new ValidationResultBuilder();
            if (process is null)
            {
                return this.ErrorResult(validationResult, "$KrSecondaryProcess_Unknown", krProcess.ProcessID);
            }
            if (process is IKrPureProcess pure
                && !pure.AllowClientSideLaunch
                && IsClientSideLaunch(cardContext))
            {
                return this.ErrorResult(validationResult, "$KrSecondaryProcess_ClientSideIsForbidden", process.ID, process.Name);
            }

            IKrProcessLaunchResult result;
            await using (this.dbScope.Create())
            {
                await using var level = this.krScope.EnterNewLevel(validationResult, this.WithReaderLocks());
                if (krProcess.CardID.HasValue)
                {
                    if (process.IsGlobal)
                    {
                        return this.ErrorResult(validationResult, "$KrSecondaryProcess_IsNotLocal", process.ID, process.Name);
                    }
                    var cardID = krProcess.CardID.Value;
                    await using var cardLoadingStrategy = (specificParameters as SpecificParameters)?.MainCardAccessStrategy
                        ?? new KrScopeMainCardAccessStrategy(cardID, this.krScope, validationResult);
                    var cardInfo = await this.SelectCardInfoAsync(cardID, cardContext, cancellationToken);
                    var components = await KrComponentsHelper.GetKrComponentsAsync(cardInfo.CardTypeID, cardInfo.DocTypeID, this.typesCache, cancellationToken);

                    if (krProcess.SerializedProcess != null
                        && krProcess.SerializedProcessSignature != null)
                    {
                        if (!KrProcessHelper.VerifyWorkflowProcess(krProcess, this.signatureProvider))
                        {
                            return this.ErrorResult(validationResult, "$KrSecondaryProcess_SignatureVerifyingFailed", process.ID, process.Name);
                        }

                        result = await this.StartSyncProcessAsync(
                            krProcess,
                            NullMainCardAccessStrategy.Instance,
                            process,
                            cardContext,
                            cardInfo,
                            (pci, holder, npid) =>
                            {
                                var wp = KrProcessHelper.DeserializeWorkflowProcess(krProcess.SerializedProcess);
                                holder.MainWorkflowProcess = wp;
                                return wp;
                            },
                            true,
                            cancellationToken);
                    }
                    else
                    {
                        var (evaluationResult, errorResult) = await this.EvaluateLocalAsync(
                            process,
                            validationResult,
                            cardLoadingStrategy,
                            cardID,
                            cardInfo,
                            components,
                            cardContext,
                            raiseErrorIfForbbiden,
                            cancellationToken);

                        if (!evaluationResult)
                        {
                            return errorResult;
                        }

                        result = await (process.Async
                            ? this.StartAsyncProcessAsync(cardID, krProcess, cardContext, specificParameters, cancellationToken: cancellationToken)
                            : this.StartSyncProcessAsync(krProcess, cardLoadingStrategy, process, cardContext, cardInfo, this.CreateWorkflowProcess, cancellationToken: cancellationToken));
                    }

                    await level.ApplyChangesAsync(cardID, cancellationToken: cancellationToken);
                }
                else
                {
                    if (!process.IsGlobal)
                    {
                        return this.ErrorResult(validationResult, "$KrSecondaryProcess_IsNotGlobal", process.ID, process.Name);
                    }
                    if (process.Async)
                    {
                        return this.ErrorResult(validationResult, "$KrSecondaryProcess_AsyncWithoutCard", process.ID, process.Name);
                    }

                    if (krProcess.SerializedProcess != null
                        && krProcess.SerializedProcessSignature != null)
                    {
                        if (!KrProcessHelper.VerifyWorkflowProcess(krProcess, this.signatureProvider))
                        {
                            return this.ErrorResult(validationResult, "$KrSecondaryProcess_SignatureVerifyingFailed", process.ID, process.Name);
                        }

                        result = await this.StartSyncProcessAsync(
                            krProcess,
                            NullMainCardAccessStrategy.Instance,
                            process,
                            cardContext,
                            null,
                            (pci, holder, npid) =>
                            {
                                var wp = KrProcessHelper.DeserializeWorkflowProcess(krProcess.SerializedProcess);
                                holder.MainWorkflowProcess = wp;
                                return wp;
                            },
                            true,
                            cancellationToken: cancellationToken);
                    }
                    else
                    {
                        var (evaluationResult, errorResult) = await this.EvaluateGlobalAsync(
                            process,
                            validationResult,
                            cardContext,
                            raiseErrorIfForbbiden,
                            cancellationToken);

                        if (!evaluationResult)
                        {
                            return errorResult;
                        }

                        result = await this.StartSyncProcessAsync(
                            krProcess,
                            NullMainCardAccessStrategy.Instance,
                            process,
                            cardContext,
                            null,
                            this.CreateWorkflowProcess,
                            cancellationToken: cancellationToken);
                    }
                }

                // Если сейчас верхний уровень krScope, то перед его полным закрытием надо записать клиентские команды.
                if (this.krScope.Depth == 1)
                {
                    var commands = this.krScope.GetKrProcessClientCommands();
                    if (commands != null
                        && cardContext != null)
                    {
                        // Установка команд поддеживается только в два типа реквестов
                        switch (cardContext)
                        {
                            case CardRequestExtensionContext cardRequestExtensionContext:
                                cardRequestExtensionContext.Response?.AddKrProcessClientCommands(commands);
                                break;
                            case CardStoreExtensionContext cardStoreExtensionContext:
                                cardStoreExtensionContext.Response?.AddKrProcessClientCommands(commands);
                                break;
                        }
                    }

                    // Вносим накопившиеся в scope сообщения в результат.
                    validationResult.Add(this.krScope.ValidationResult);
                }
            }

            result.ValidationResult.Add(validationResult);
            return result;
        }

        #endregion

        #region private

        private static bool IsClientSideLaunch(
            ICardExtensionContext cardContext)
        {
            // Если контекст отсутствует, считаем, что запускаем код с сервера.
            if (cardContext is null)
            {
                return false;
            }

            bool ClientServiceType(CardServiceType type) => type != CardServiceType.Default;

            switch (cardContext)
            {
                case ICardDeleteExtensionContext cardDeleteExtensionContext:
                    return ClientServiceType(cardDeleteExtensionContext.Request.ServiceType);
                case ICardGetFileContentExtensionContext cardGetFileContentExtensionContext:
                    return ClientServiceType(cardGetFileContentExtensionContext.Request.ServiceType);
                case ICardGetExtensionContext cardGetExtensionContext:
                    return ClientServiceType(cardGetExtensionContext.Request.ServiceType);
                case ICardGetFileVersionsExtensionContext cardGetFileVersionsExtensionContext:
                    return ClientServiceType(cardGetFileVersionsExtensionContext.Request.ServiceType);
                case ICardNewExtensionContext cardNewExtensionContext:
                    return ClientServiceType(cardNewExtensionContext.Request.ServiceType);
                case ICardRequestExtensionContext cardRequestExtensionContext:
                    return ClientServiceType(cardRequestExtensionContext.Request.ServiceType);
                case ICardStoreExtensionContext cardStoreExtensionContext:
                    return ClientServiceType(cardStoreExtensionContext.Request.ServiceType);
                case ICardStoreTaskExtensionContext cardStoreTaskExtensionContext:
                    return ClientServiceType(cardStoreTaskExtensionContext.Request.ServiceType);
                default:
                    throw new ArgumentOutOfRangeException($"Can't recognize client-side launch for {cardContext.GetType().FullName} context");
            }
        }

        private Task<(bool evaluationResult, IKrProcessLaunchResult errorResult)> EvaluateGlobalAsync(
            IKrSecondaryProcess process,
            IValidationResultBuilder validationResult,
            ICardExtensionContext cardContext,
            bool raiseErrorIfForbbiden,
            CancellationToken cancellationToken)
        {
            return this.EvaluateLocalAsync(
                process,
                validationResult,
                NullMainCardAccessStrategy.Instance,
                null,
                null,
                null,
                cardContext,
                raiseErrorIfForbbiden,
                cancellationToken
            );
        }

        private async Task<(bool evaluationResult, IKrProcessLaunchResult errorResult)> EvaluateLocalAsync(
            IKrSecondaryProcess process,
            IValidationResultBuilder validationResult,
            IMainCardAccessStrategy mainCardAccessStrategy,
            Guid? cardID,
            CardInfo cardInfo,
            KrComponents? components,
            ICardExtensionContext cardContext,
            bool raiseErrorIfForbbiden,
            CancellationToken cancellationToken)
        {
            IKrProcessLaunchResult errorResult = default;
            var evaluatorContext = new KrSecondaryProcessEvaluatorContext(
                process,
                validationResult,
                mainCardAccessStrategy,
                cardID,
                cardInfo?.CardTypeID,
                cardInfo?.CardTypeName,
                cardInfo?.CardTypeCaption,
                cardInfo?.DocTypeID,
                components,
                (KrState)(cardInfo?.StateID ?? KrState.Draft.ID),
                null,
                cardContext,
                cancellationToken);
            var evaluationResult = await this.secondaryProcessExecutionEvaluator.EvaluateAsync(evaluatorContext);
            if (!evaluationResult)
            {
                if (raiseErrorIfForbbiden)
                {
                    var msg = string.IsNullOrWhiteSpace(process.ExecutionAccessDeniedMessage)
                        ? "$KrSecondaryProcess_SecondaryProcessLaunchIsForbiddenViaRestrictions"
                        : process.ExecutionAccessDeniedMessage;
                    errorResult = this.ErrorResult(validationResult, msg);
                }
                else
                {
                    errorResult = new KrProcessLaunchResult(
                        KrProcessLaunchStatus.Forbidden,
                        null,
                        validationResult.Build(),
                        null,
                        null,
                        null);
                }
            }

            return (evaluationResult, errorResult);
        }

        private async Task<IKrProcessLaunchResult> StartAsyncProcessAsync(
            Guid cardID,
            KrProcessInstance krProcess,
            ICardExtensionContext cardContext,
            IKrProcessLauncherSpecificParameters specificParameters,
            CancellationToken cancellationToken = default)
        {
            var validationResultBuilder = new ValidationResultBuilder();
            bool useSameRequest = false;
            if (specificParameters is SpecificParameters sp)
            {
                useSameRequest = sp.UseSameRequest;
            }

            var processID = Guid.NewGuid();
            var nested = krProcess.ParentStageRowID.HasValue
                && krProcess.ProcessHolderID.HasValue
                && krProcess.NestedOrder.HasValue;
            var startingProcessName = nested
                ? KrConstants.KrNestedProcessName
                : KrConstants.KrSecondaryProcessName;
            var secondaryProcessInfo = new StartingSecondaryProcessInfo(
                krProcess.ProcessID,
                krProcess.ProcessInfo,
                krProcess.ParentStageRowID,
                krProcess.ParentProcessTypeName,
                krProcess.ParentProcessID,
                krProcess.ProcessHolderID,
                krProcess.NestedOrder);

            if (useSameRequest)
            {
                if (!(cardContext is ICardStoreExtensionContext storeCardContext))
                {
                    throw new InvalidOperationException($"Can't apply {nameof(SpecificParameters.UseSameRequest)} " +
                        $"to any CardContext except {typeof(ICardStoreExtensionContext).FullName}.");
                }

                storeCardContext.Request.SetStartingProcessID(processID);
                storeCardContext.Request.SetStartingProcessName(startingProcessName);
                storeCardContext.Request.SetStartingSecondaryProcess(secondaryProcessInfo);
                return new KrProcessLaunchResult(
                    KrProcessLaunchStatus.Undefined, processID, validationResultBuilder.Build(), null, null, null);
            }

            var suitableCardRepo = KrProcessHelper.IsTransactionOpened(cardContext?.DbScope)
                ? this.cardRepositoryEwt
                : this.cardRepository;
            (Card mainCard, ValidationResult result) = await this.GetCardInstanceAsync(cardID, cancellationToken);
            if (!result.IsSuccessful)
            {
                validationResultBuilder.Add(result);
                return new KrProcessLaunchResult(
                    KrProcessLaunchStatus.Error, null, validationResultBuilder.Build(), null, null, null);
            }
            var storeRequest = new CardStoreRequest { Card = mainCard };
            storeRequest.SetStartingProcessID(processID);
            storeRequest.SetStartingProcessName(startingProcessName);
            storeRequest.SetStartingSecondaryProcess(secondaryProcessInfo);

            // Разрешим делать с карточкой все что угодно,
            // т.к. необходимо было выполнить проверку при запуске процесса
            var nextKrToken = this.tokenProvider.CreateToken(mainCard);
            nextKrToken.Set(mainCard.Info);
            var storeResponse = await suitableCardRepo.StoreAsync(storeRequest, cancellationToken);
            validationResultBuilder.Add(storeResponse.ValidationResult);
            var status = storeResponse.Info.GetAsyncProcessCompletedSimultaniosly()
                ? KrProcessLaunchStatus.Complete
                : KrProcessLaunchStatus.InProgress;

            return new KrProcessLaunchResult(
                status, processID, validationResultBuilder.Build(), storeResponse.Info.GetProcessInfoAtEnd(), storeResponse, null);
        }

        private async Task<IKrProcessLaunchResult> StartSyncProcessAsync(
            KrProcessInstance krProcess,
            IMainCardAccessStrategy mainCardAccessStrategy,
            IKrSecondaryProcess secondaryProcess,
            ICardExtensionContext cardContext,
            CardInfo cardInfo,
            Func<ProcessCommonInfo, ProcessHolder, Guid?, WorkflowProcess> createWorkflowProcessFunc,
            bool resurrection = false,
            CancellationToken cancellationToken = default)
        {
            var nestedProcessID = GetNestedProcessID(krProcess);
            var contextualSatellite = await this.GetContextualSatelliteAsync(krProcess, cancellationToken);
            (ProcessHolder processHolder, bool processHolderCreated, ProcessCommonInfo pci) = await this.GetProcessHolderAsync(
                krProcess,
                contextualSatellite,
                secondaryProcess.ID,
                nestedProcessID,
                cancellationToken);
            var workflowProcess = createWorkflowProcessFunc(pci, processHolder, nestedProcessID);

            var validationResultBuilder = new ValidationResultBuilder();
            IKrTaskHistoryResolver taskHistoryResolver;
            KrComponents? components;
            if (krProcess.CardID.HasValue)
            {
                taskHistoryResolver =
                    new KrTaskHistoryResolver(mainCardAccessStrategy, cardContext, validationResultBuilder, this.taskHistoryManager);
                components = await KrComponentsHelper.GetKrComponentsAsync(cardInfo.CardTypeID, cardInfo.DocTypeID, this.typesCache, cancellationToken);
            }
            else
            {
                taskHistoryResolver = null;
                components = null;
            }

            if (!resurrection)
            {
                var executor = this.executorFunc();
                var ctx = new KrExecutionContext(
                    cardContext,
                    mainCardAccessStrategy: mainCardAccessStrategy,
                    cardID: krProcess.CardID,
                    cardTypeID: cardInfo?.CardTypeID,
                    cardTypeName: cardInfo?.CardTypeName,
                    cardTypeCaption: cardInfo?.CardTypeCaption,
                    docTypeID: cardInfo?.DocTypeID,
                    krComponents: components,
                    workflowProcess: workflowProcess,
                    compilationResult: null,
                    secondaryProcess: secondaryProcess,
                    cancellationToken: cancellationToken
                );

                var executorResult = await executor.ExecuteAsync(ctx);
                validationResultBuilder.Add(executorResult.Result);

                if (!validationResultBuilder.IsSuccessful())
                {
                    return new KrProcessLaunchResult(
                        KrProcessLaunchStatus.Error, null, validationResultBuilder.Build(), null, null, null);
                }
            }

            var runnerContext = new KrProcessRunnerContext(
                workflowAPI: null,
                taskHistoryResolver: taskHistoryResolver,
                mainCardAccessStrategy: mainCardAccessStrategy,
                cardID: krProcess.CardID,
                cardTypeID: cardInfo?.CardTypeID,
                cardTypeName: cardInfo?.CardTypeName,
                cardTypeCaption: cardInfo?.CardTypeCaption,
                docTypeID: cardInfo?.DocTypeID,
                krComponents: components,
                contextualSatellite: contextualSatellite,
                processHolderSatellite: null,
                processHolder: processHolder,
                workflowProcess: workflowProcess,
                processInfo: null,
                validationResult: validationResultBuilder,
                cardContext: cardContext,
                isProcessHolderCreated: processHolderCreated,
                updateCardFuncAsync: this.UpdateCardAsync,
                secondaryProcess: secondaryProcess,
                parentProcessID: krProcess.ParentProcessID,
                parentProcessTypeName: krProcess.ParentProcessTypeName,
                defaultPreparingGroupStrategyFunc: this.DefaultPreparingStrategy,
                resurrection: resurrection,
                cancellationToken: cancellationToken);

            await this.runnerProvider.GetRunner(KrProcessRunnerNames.Sync).RunAsync(runnerContext);

            await this.eventManager.RaiseAsync(
                DefaultEventTypes.SyncProcessCompleted,
                currentStage: null,
                runnerMode: KrProcessRunnerMode.Sync,
                runnerContext: runnerContext,
                info: null,
                cancellationToken: cancellationToken);

            await runnerContext.UpdateCardAsync();

            return new KrProcessLaunchResult(
                KrProcessLaunchStatus.Complete, null, validationResultBuilder.Build(), workflowProcess.InfoStorage, null, null);
        }

        private IPreparingGroupRecalcStrategy DefaultPreparingStrategy() =>
            new ForwardPreparingGroupRecalcStrategy(this.dbScope, this.session);

        private async Task<CardInfo> SelectCardInfoAsync(
            Guid cardID,
            ICardExtensionContext cardContext,
            CancellationToken cancellationToken = default)
        {
            var cardInfo = new CardInfo();
            var selectCardTypeInfo = true;

            if (cardContext != null)
            {
                var cardType = cardContext.CardType;

                if (cardType != null)
                {
                    selectCardTypeInfo = false;

                    cardInfo.CardTypeID = cardType.ID;
                    cardInfo.CardTypeName = cardType.Name;
                    cardInfo.CardTypeCaption = cardType.Caption;
                }

                var card = cardContext switch
                {
                    ICardNewExtensionContext extensionContext => extensionContext.Response.TryGetCard(),
                    ICardStoreExtensionContext extensionContext => extensionContext.Request.TryGetCard(),
                    ICardStoreTaskExtensionContext extensionContext => extensionContext.Request.TryGetCard(),
                    _ => default,
                };

                if (card != null)
                {
                    cardInfo.DocTypeID = await KrProcessSharedHelper.GetDocTypeIDAsync(card, cancellationToken: cancellationToken);

                    if (card.StoreMode == CardStoreMode.Insert)
                    {
                        return cardInfo;
                    }
                }
            }

            if (cardInfo.DocTypeID is null)
            {
                cardInfo.DocTypeID = await KrProcessSharedHelper.GetDocTypeIDAsync(cardID, this.dbScope, cancellationToken);
            }

            cardInfo.StateID = (await KrProcessSharedHelper.GetKrStateAsync(cardID, this.dbScope, cancellationToken) ?? KrState.Draft).ID;

            if (selectCardTypeInfo)
            {
                (cardInfo.CardTypeID, cardInfo.CardTypeName, cardInfo.CardTypeCaption) = await this.GetCardTypeInfoAsync(cardID, cancellationToken);
            }

            return cardInfo;
        }

        /// <summary>
        /// Возвращает кортеж содержащий информацию о типе указанной карточки.
        /// </summary>
        /// <param name="cardID">Идентификатор карточки.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Кортеж &lt;Идентификатор типа карточки; Имя типа карточки; Отображаемое имя типа карточки&gt;.</returns>
        private async Task<(Guid, string, string)> GetCardTypeInfoAsync(
            Guid cardID,
            CancellationToken cancellationToken)
        {
            await using (this.dbScope.Create())
            {
                var db = this.dbScope.Db;
                var query = this.dbScope.BuilderFactory
                    .Select()
                        .C("t", "ID")
                        .C("t", "Name")
                        .C("t", "Caption")
                    .From(Names.Instances, "i").NoLock()
                    .InnerJoin(Names.Types, "t").NoLock()
                        .On().C("i", Names.Instances_TypeID).Equals().C("t", Names.Types_ID)
                    .Where()
                        .C("i", Names.Instances_ID).Equals().P("CardID")
                    .Build();

                db
                    .SetCommand(
                        query,
                        db.Parameter("CardID", cardID))
                    .LogCommand();

                await using var reader = await db.ExecuteReaderAsync(cancellationToken);
                if (await reader.ReadAsync(cancellationToken))
                {
                    return (reader.GetGuid(0), reader.GetString(1), reader.GetString(2));
                }
            }

            return default;
        }

        private async Task<(Card card, ValidationResult result)> GetCardInstanceAsync(Guid cardID, CancellationToken cancellationToken = default)
        {
            var validationResultBuilder = new ValidationResultBuilder();
            Card card = null;

            await this.GetSuitableTransactionStrategy().ExecuteInReaderLockAsync(
                cardID,
                validationResultBuilder,
                async p =>
                {
                    var getContext = await this.getStrategy
                        .TryLoadCardInstanceAsync(
                            cardID,
                            p.DbScope.Db,
                            this.cardMetadata,
                            p.ValidationResult,
                            cancellationToken: p.CancellationToken);
                    card = getContext.Card;
                },
                cancellationToken);
            
            return (card, validationResultBuilder.Build());
        }

        private ICardTransactionStrategy GetSuitableTransactionStrategy() =>
            this.krScope.CurrentLevel?.CardTransactionStrategy ?? this.transactionStrategy;

        private IKrProcessLaunchResult ErrorResult(
            IValidationResultBuilder validationResult,
            string errorText,
            params object[] args)
        {
            validationResult.AddError(this, errorText, args);
            return new KrProcessLaunchResult(
                KrProcessLaunchStatus.Error, null, validationResult.Build(), null, null, null);
        }

        private async ValueTask<Card> GetContextualSatelliteAsync(KrProcessInstance krProcess, CancellationToken cancellationToken = default)
        {
            if (krProcess.CardID.HasValue
                && krProcess.CardID != default(Guid))
            {
                return await this.krScope.GetKrSatelliteAsync(krProcess.CardID.Value, cancellationToken: cancellationToken);
            }

            return null;
        }

        private ProcessHolder CreateProcessHolder(
            Card contextualSatellite,
            KrProcessInstance krProcessInstance) =>
            new ProcessHolder
            {
                Persistent = false,
                MainProcessType = KrConstants.KrSecondaryProcessName,
                ProcessHolderID = Guid.NewGuid(),
                PrimaryProcessCommonInfo = contextualSatellite != null
                    ? this.objectModelMapper.GetMainProcessCommonInfo(contextualSatellite)
                    : null,
                MainProcessCommonInfo = new MainProcessCommonInfo(
                    null,
                    krProcessInstance.ProcessInfo ?? new Dictionary<string, object>(),
                    krProcessInstance.ProcessID,
                    null,
                    null,
                    null,
                    (int)KrState.Draft,
                    default,
                    default)
            };

        private static Guid? GetNestedProcessID(KrProcessInstance krProcess) =>
            krProcess.ParentStageRowID.HasValue && krProcess.NestedOrder.HasValue
                ? (Guid?)Guid.NewGuid()
                : null;

        private async ValueTask<(ProcessHolder processHolder, bool processHolderCreated, ProcessCommonInfo pci)> GetProcessHolderAsync(
            KrProcessInstance krProcess,
            Card contextualSatellite,
            Guid secondaryProcessID,
            Guid? nestedProcessID,
            CancellationToken cancellationToken = default)
        {
            var processHolderID = krProcess.ProcessHolderID;
            var processHolder = processHolderID.HasValue
                ? this.krScope.GetProcessHolder(processHolderID.Value)
                : null;
            var processHolderCreated = false;
            ProcessCommonInfo pci;
            // Если pci нет, то запускаем главный процесс
            if (processHolder is null)
            {
                processHolder = this.CreateProcessHolder(contextualSatellite, krProcess);
                pci = processHolder.MainProcessCommonInfo;
                processHolderCreated = true;
            }
            // Иначе располагаемся в нестеде
            else if (nestedProcessID.HasValue
                && krProcess.ParentStageRowID.HasValue
                && krProcess.NestedOrder.HasValue)
            {
                if (processHolder.NestedProcessCommonInfos is null)
                {
                    if (processHolder.Persistent)
                    {
                        Card processHolderSatellite;
                        switch (processHolder.MainProcessType)
                        {
                            case KrConstants.KrProcessName:
                                processHolderSatellite = contextualSatellite;
                                break;
                            case KrConstants.KrSecondaryProcessName:
                                processHolderSatellite = await this.krScope.GetSecondaryKrSatelliteAsync(processHolder.ProcessHolderID, cancellationToken);
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }

                        processHolder.NestedProcessCommonInfosList =
                            this.objectModelMapper.GetNestedProcessCommonInfos(processHolderSatellite);
                    }
                    else
                    {
                        processHolder.NestedProcessCommonInfosList = new List<NestedProcessCommonInfo>();
                    }

                }
                var npci = new NestedProcessCommonInfo(
                    null,
                    krProcess.ProcessInfo,
                    secondaryProcessID,
                    nestedProcessID.Value,
                    krProcess.ParentStageRowID.Value,
                    krProcess.NestedOrder.Value
                    );
                processHolder.NestedProcessCommonInfos.Add(npci);
                pci = npci;
            }
            else
            {
                throw new InvalidOperationException("Inconsistent starting sync process parameters.");
            }

            return (processHolder, processHolderCreated, pci);
        }

        private WorkflowProcess CreateWorkflowProcess(
            ProcessCommonInfo pci,
            ProcessHolder processHolder,
            Guid? nestedProcessID)
        {
            var workflowProcess = new WorkflowProcess(
                pci.Info,
                processHolder.MainProcessCommonInfo.Info,
                new SealableObjectList<Stage>(),
                saveInitialStages: true,
                nestedProcessID: nestedProcessID);

            if (nestedProcessID.HasValue)
            {
                processHolder.NestedWorkflowProcesses[nestedProcessID.Value] = workflowProcess;
            }
            else
            {
                processHolder.MainWorkflowProcess = workflowProcess;
            }

            this.objectModelMapper.FillWorkflowProcessFromPci(
                workflowProcess,
                pci,
                processHolder.PrimaryProcessCommonInfo);

            return workflowProcess;
        }

        private bool WithReaderLocks() => this.krScope.CurrentLevel?.WithReaderLocks ?? true;

        private async ValueTask UpdateCardAsync(
            IKrProcessRunnerContext context)
        {
            // Если холдер был создан тут, значит синхронный процесс - главный,
            // достаточно сохранить только основной процесс.
            // Также нужно удостоверится, что CardID осмысленный, т.е.
            // выполнение идет в уже созданной в базе карточке
            // (если null, то карточки нет, если Guid.Empty, то еще не сохранена)
            if (context.IsProcessHolderCreated
                && context.CardID.HasValue
                && context.CardID != Guid.Empty)
            {
                this.objectModelMapper.ObjectModelToPci(
                    context.ProcessHolder.MainWorkflowProcess,
                    context.ProcessHolder.MainProcessCommonInfo,
                    context.ProcessHolder.MainProcessCommonInfo,
                    context.ProcessHolder.PrimaryProcessCommonInfo);

                await this.objectModelMapper.SetMainProcessCommonInfoAsync(
                    context.CardID.Value,
                    context.ContextualSatellite,
                    context.ProcessHolder.PrimaryProcessCommonInfo,
                    context.CancellationToken);
            }
        }

        #endregion

    }
}