﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Workflow;
using Tessa.Extensions.Default.Server.Workflow.KrCompilers;
using Tessa.Extensions.Default.Server.Workflow.KrObjectModel;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Events;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using static Tessa.Extensions.Default.Shared.Workflow.KrProcess.KrConstants;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow
{
    public sealed class KrProcessWorkflowWorker: WorkflowWorker<KrProcessWorkflowManager>
    {
        #region constructor

        public KrProcessWorkflowWorker(KrProcessWorkflowManager manager)
            : base(manager)
        {
        }

        #endregion

        #region private

        /// <summary>
        /// WorkflowContext
        /// </summary>
        private KrProcessWorkflowContext WCtx => this.Manager.WorkflowContext;

        private async Task StartRunnerAsync(IWorkflowProcessInfo info, CancellationToken cancellationToken = default)
        {
            // ValidationResult здесь и во всех вложенных выводах используется из context.ValidationResult
            // расширения на сохранение карточки. Таким образом все, что могло возникнуть здесь и глубже, будет корректно
            // выведено.

            var scope = this.WCtx.KrScope;
            if (!scope.MultirunEnabled(info.ProcessID)
                && !scope.FirstLaunchPerRequest(info.ProcessID))
            {
                return;
            }
            scope.AddToLaunchedLevels(info.ProcessID);

            var startingSecondaryProcess = this.WCtx.CardStoreContext.Request.GetStartingSecondaryProcess();
            StoreParentProcess(info, startingSecondaryProcess);

            var mainCardID = this.Manager.Request.Card.ID;
            var contextualSatellite = await this.WCtx.KrScope.GetKrSatelliteAsync(mainCardID, cancellationToken: cancellationToken);
            
            // Получаем холдер процесса в зависимости от типа процесса и его вложенности
            (Card processHolderSatellite, Guid processHolderID, ProcessHolder processHolder) = await this.GetProcessHolderAsync(
                info,
                startingSecondaryProcess,
                contextualSatellite,
                cancellationToken);

            bool processHolderCreated = default;

            var isNested = string.Equals(info.ProcessTypeName, KrNestedProcessName, StringComparison.Ordinal);

            if (processHolder is null)
            {
                // Холдер отсутствует, т.е. для текущего сателлита-холдер запускается самый верхний процесс.
                processHolder = new ProcessHolder();
                processHolderCreated = true;
                // Если запускается нестед, значит по нестеду совершено какое-то действие (уже был запущен ранее)
                if (isNested)
                {
                    processHolder.MainProcessType = GetMainProcessType(info, null);
                    processHolder.Persistent = true;
                    processHolder.ProcessHolderID = processHolderID;
                }
                // Иначе запускается основной процесс
                else
                {
                    processHolder.MainProcessType = info.ProcessTypeName;
                    processHolder.Persistent = true;
                    processHolder.ProcessHolderID = processHolderID;
                }
                this.WCtx.KrScope.AddProcessHolder(processHolder);
            }

            // Строим модель процесса на основе доступных сателлитов и холдеров
            (WorkflowProcess workflowProcess, ProcessCommonInfo pci) = await this.GetWorkflowProcessAsync(
                info,
                startingSecondaryProcess,
                contextualSatellite,
                processHolderSatellite,
                isNested,
                processHolder,
                cancellationToken);

            if (!processHolder.Persistent)
            {
                this.Manager.ValidationResult.AddError("$KrProcess_Error_AsyncProcessWithoutPersistentHolder");
                return;
            }

            var mainCardKey = scope.LockCard(mainCardID);
            var contextualKey = scope.LockCard(contextualSatellite.ID);
            var processHolderKey = scope.LockCard(processHolderSatellite.ID);

            if (startingSecondaryProcess != null)
            {
                StorageHelper.Merge(startingSecondaryProcess.ProcessInfo, workflowProcess.InfoStorage);
            }

            var secondaryProcess = await this.GetSecondaryProcessAsync(
                info,
                startingSecondaryProcess?.SecondaryProcessID,
                pci,
                cancellationToken);

            await using var cardLoadingStrategy = new KrScopeMainCardAccessStrategy(this.WCtx.CardID, this.WCtx.KrScope);
            var taskHistoryResolver = new KrTaskHistoryResolver(
                cardLoadingStrategy,
                this.WCtx,
                this.WCtx.ValidationResult,
                this.WCtx.TaskHistoryManager);

            var runnerContext = new KrProcessRunnerContext(
                workflowAPI: new WorkflowAPIBridge(this.Manager, info),
                taskHistoryResolver: taskHistoryResolver,
                mainCardAccessStrategy: cardLoadingStrategy,
                cardID: this.WCtx.CardID,
                cardTypeID: this.WCtx.CardTypeID,
                cardTypeName: this.WCtx.CardTypeName,
                cardTypeCaption: this.WCtx.CardTypeCaption,
                docTypeID: this.WCtx.DocTypeID,
                krComponents: this.WCtx.KrComponents,
                contextualSatellite: contextualSatellite,
                processHolderSatellite: processHolderSatellite,
                workflowProcess: workflowProcess,
                processHolder: processHolder,
                processInfo: info,
                validationResult: this.Manager.ValidationResult,
                cardContext: this.WCtx.CardStoreContext,
                isProcessHolderCreated: processHolderCreated,
                updateCardFuncAsync: this.UpdateCardAsync,
                secondaryProcess: secondaryProcess,
                parentProcessID: info.ProcessParameters.TryGet<Guid?>(Keys.ParentProcessID),
                parentProcessTypeName: info.ProcessParameters.TryGet<string>(Keys.ParentProcessType),
                defaultPreparingGroupStrategyFunc: this.DefaultPreparingStrategy,
                cancellationToken: cancellationToken);

            await this.WCtx.AsyncProcessRunner.RunAsync(runnerContext);

            if (runnerContext.WorkflowProcess.CurrentApprovalStageRowID is null)
            {
                await this.WCtx.EventManager.RaiseAsync(
                    DefaultEventTypes.AsyncProcessCompleted,
                    currentStage: null,
                    runnerMode: KrProcessRunnerMode.Async,
                    runnerContext: runnerContext,
                    cancellationToken: cancellationToken);

                this.Manager.ProcessesAwaitingRemoval.Add(info);

                this.WCtx.CardStoreContext.Info.SetProcessInfoAtEnd(workflowProcess.InfoStorage);

                if (runnerContext.InitiationCause == KrProcessRunnerInitiationCause.StartProcess)
                {
                    this.WCtx.CardStoreContext.Info.SetAsyncProcessCompletedSimultaniosly();
                }
            }

            await runnerContext.UpdateCardAsync();

            if (runnerContext.IsProcessHolderCreated)
            {
                this.WCtx.KrScope.RemoveProcessHolder(processHolder.ProcessHolderID);
            }

            scope.ReleaseCard(mainCardID, mainCardKey);
            scope.ReleaseCard(contextualSatellite.ID, contextualKey);
            scope.ReleaseCard(processHolderSatellite.ID, processHolderKey);
        }

        private async ValueTask<(WorkflowProcess workflowProcess, ProcessCommonInfo pci)> GetWorkflowProcessAsync(
            IWorkflowProcessInfo info,
            StartingSecondaryProcessInfo startingSecondaryProcess,
            Card contextualSatellite,
            Card processHolderSatellite,
            bool isNested,
            ProcessHolder processHolder,
            CancellationToken cancellationToken = default)
        {
            if (processHolder.MainWorkflowProcess is null)
            {
                processHolder.MainProcessCommonInfo =
                    this.WCtx.ObjectModelMapper.GetMainProcessCommonInfo(processHolderSatellite);
            }
            
            processHolder.PrimaryProcessCommonInfo ??= processHolder.MainProcessType == KrProcessName
                ? processHolder.MainProcessCommonInfo
                : this.WCtx.ObjectModelMapper.GetMainProcessCommonInfo(contextualSatellite, false);

            if (isNested
                && processHolder.NestedProcessCommonInfos is null)
            {
                processHolder.NestedProcessCommonInfosList =
                    this.WCtx.ObjectModelMapper.GetNestedProcessCommonInfos(processHolderSatellite);
            }

            ProcessCommonInfo pci = null;
            WorkflowProcess workflowProcess = null;
            var nestedProcessID = GetNestedProcessID(info, isNested);

            if (isNested
                && nestedProcessID.HasValue)
            {
                if (!processHolder.NestedProcessCommonInfos.TryGetItem(nestedProcessID.Value, out var npci))
                {
                    npci = new NestedProcessCommonInfo(
                        null,
                        new Dictionary<string, object>(),
                        startingSecondaryProcess.SecondaryProcessID ?? Guid.Empty,
                        nestedProcessID.Value,
                        startingSecondaryProcess.ParentStageRowID ?? Guid.Empty,
                        startingSecondaryProcess.NestedOrder ?? 0
                        );
                    processHolder.NestedProcessCommonInfos.Add(npci);
                }
                pci = npci;

                if (!processHolder.NestedWorkflowProcesses.TryGetValue(nestedProcessID.Value, out workflowProcess))
                {
                    var (templates, stages) = await this.WCtx.ProcessCache.GetRelatedTemplatesAsync(processHolderSatellite, nestedProcessID, cancellationToken);
                    workflowProcess = await this.WCtx.ObjectModelMapper.CardRowsToObjectModelAsync(
                        processHolderSatellite,
                        npci,
                        processHolder.MainProcessCommonInfo,
                        templates,
                        stages,
                        nestedProcessID: npci.NestedProcessID,
                        cancellationToken: cancellationToken);
                    this.WCtx.ObjectModelMapper.FillWorkflowProcessFromPci(
                        workflowProcess,
                        pci,
                        processHolder.PrimaryProcessCommonInfo);
                    processHolder.NestedWorkflowProcesses[nestedProcessID.Value] = workflowProcess;
                }
            }
            else if (!isNested)
            {
                workflowProcess = processHolder.MainWorkflowProcess;
                if (workflowProcess is null)
                {
                    var (templates, stages) = await this.WCtx.ProcessCache.GetRelatedTemplatesAsync(processHolderSatellite, nestedProcessID, cancellationToken);
                    workflowProcess = await this.WCtx.ObjectModelMapper.CardRowsToObjectModelAsync(
                        processHolderSatellite,
                        processHolder.MainProcessCommonInfo,
                        processHolder.MainProcessCommonInfo,
                        templates,
                        stages,
                        cancellationToken: cancellationToken);
                    this.WCtx.ObjectModelMapper.FillWorkflowProcessFromPci(
                        workflowProcess,
                        processHolder.MainProcessCommonInfo,
                        processHolder.PrimaryProcessCommonInfo);
                    processHolder.MainWorkflowProcess = workflowProcess;
                }

                pci = processHolder.MainProcessCommonInfo;
            }

            return (workflowProcess, pci);
        }

        private static Guid? GetNestedProcessID(IWorkflowProcessInfo info, bool nested)
        {
            var nestedProcessID = info.ProcessParameters.TryGet<Guid?>(Keys.NestedProcessID);
            if (nestedProcessID == null && nested)
            {
                nestedProcessID = Guid.NewGuid();
                info.ProcessParameters[Keys.NestedProcessID] = nestedProcessID;
                info.PendingProcessParametersUpdate = true;
                return nestedProcessID;
            }

            return nestedProcessID;
        }

        private async ValueTask<(Card processHolderSatellite, Guid processHolderID, ProcessHolder processHolder)> GetProcessHolderAsync(
            IWorkflowProcessInfo info,
            StartingSecondaryProcessInfo startingSecondaryProcess,
            Card contextualSatellite,
            CancellationToken cancellationToken = default)
        {
            var mainCardID = this.Manager.Request.Card.ID;
            Card processHolderSatellite;
            Guid processHolderID;
            ProcessHolder processHolder;
            switch (info.ProcessTypeName)
            {
                case KrProcessName:
                    processHolderSatellite = contextualSatellite;
                    processHolderID = mainCardID;
                    processHolder = this.TryGetProcessHolder(processHolderID);
                    break;
                case KrSecondaryProcessName:
                    processHolderSatellite = await this.WCtx.KrScope.GetSecondaryKrSatelliteAsync(info.ProcessID, cancellationToken);
                    processHolderID = info.ProcessID;
                    processHolder = this.TryGetProcessHolder(processHolderID);
                    break;
                case KrNestedProcessName:
                    var processHolderIDNullable = GetProcessHolderID(info, startingSecondaryProcess);
                    if (processHolderIDNullable is null)
                    {
                        throw new InvalidOperationException(
                            $"Process holder ID is null for process type {KrNestedProcessName}");
                    }
                    processHolder = this.TryGetProcessHolder(processHolderIDNullable.Value);
                    var mainProcessType = GetMainProcessType(info, processHolder);

                    switch (mainProcessType)
                    {
                        case KrProcessName:
                            processHolderID = mainCardID;
                            processHolderSatellite = contextualSatellite;
                            break;
                        case KrSecondaryProcessName:
                            processHolderID = processHolderIDNullable.Value;
                            processHolderSatellite = await this.WCtx.KrScope.GetSecondaryKrSatelliteAsync(processHolderID, cancellationToken);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(mainProcessType), mainProcessType, "Unsupported process type.");
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(info) + "." + nameof(info.ProcessTypeName), info.ProcessTypeName, "Unsupported process type.");
            }

            return (processHolderSatellite, processHolderID, processHolder);
        }

        private static void StoreParentProcess(
            IWorkflowProcessInfo info,
            StartingSecondaryProcessInfo startingSecondaryProcess)
        {
            if (startingSecondaryProcess != null)
            {
                info.ProcessParameters[Keys.ParentProcessType] = startingSecondaryProcess.ParentProcessTypeName;
                info.ProcessParameters[Keys.ParentProcessID] = startingSecondaryProcess.ParentProcessID;
                info.PendingProcessParametersUpdate = true;
            }
        }

        private static Guid? GetProcessHolderID(
            IWorkflowProcessInfo info,
            StartingSecondaryProcessInfo startingSecondaryProcess)
        {
            var processHolderID = startingSecondaryProcess?.ProcessHolderID;
            if (processHolderID != null)
            {
                info.ProcessParameters[Keys.ProcessHolderID] = processHolderID;
                info.PendingProcessParametersUpdate = true;
                return processHolderID;
            }

            return info.ProcessParameters.TryGet<Guid?>(Keys.ProcessHolderID);
        }

        private static string GetMainProcessType(
            IWorkflowProcessInfo info,
            ProcessHolder holder)
        {
            if (holder != null)
            {
                info.ProcessParameters[Keys.MainProcessType] = holder.MainProcessType;
                info.PendingProcessParametersUpdate = true;
                return holder.MainProcessType;
            }
            return info.ProcessParameters.TryGet<string>(Keys.MainProcessType);
        }

        private async Task ObjectModelToCardRowsAsync(
            ProcessHolder processHolder,
            Card processHolderSatellite,
            Card contextualSatellite,
            CancellationToken cancellationToken)
        {
            foreach (var nested in processHolder.NestedWorkflowProcesses)
            {
                await this.WCtx.ObjectModelMapper.ObjectModelToCardRowsAsync(
                    nested.Value,
                    processHolderSatellite,
                    processHolder.NestedProcessCommonInfos[nested.Key],
                    cancellationToken);

                this.WCtx.ObjectModelMapper.ObjectModelToPci(
                    nested.Value,
                    processHolder.NestedProcessCommonInfos[nested.Key],
                    processHolder.MainProcessCommonInfo,
                    processHolder.PrimaryProcessCommonInfo);
            }

            if (processHolder.MainWorkflowProcess != null)
            {
                await this.WCtx.ObjectModelMapper.ObjectModelToCardRowsAsync(
                    processHolder.MainWorkflowProcess,
                    processHolderSatellite,
                    processHolder.MainProcessCommonInfo,
                    cancellationToken);

                this.WCtx.ObjectModelMapper.ObjectModelToPci(
                    processHolder.MainWorkflowProcess,
                    processHolder.MainProcessCommonInfo,
                    processHolder.MainProcessCommonInfo,
                    processHolder.PrimaryProcessCommonInfo);
            }

            var mainCardID = this.Manager.Request.Card.ID;

            // Переносим основную инфу по главному процессу.
            // Это состояние карточки, инициатор и т.д
            await this.WCtx.ObjectModelMapper.SetMainProcessCommonInfoAsync(
                mainCardID,
                contextualSatellite,
                processHolder.PrimaryProcessCommonInfo,
                cancellationToken);

            // Если это не основной процесс, то переносим инфу по главному процессу в его карточку-холдер
            if (!ReferenceEquals(contextualSatellite, processHolderSatellite))
            {
                await this.WCtx.ObjectModelMapper.SetMainProcessCommonInfoAsync(
                    mainCardID,
                    processHolderSatellite,
                    processHolder.MainProcessCommonInfo,
                    cancellationToken);
            }

            // Переносим оставшиеся процессы-нестеды
            this.WCtx.ObjectModelMapper.SetNestedProcessCommonInfos(
                processHolderSatellite,
                processHolder.NestedProcessCommonInfos);
        }

        private IPreparingGroupRecalcStrategy DefaultPreparingStrategy() =>
            new ForwardPreparingGroupRecalcStrategy(this.WCtx.DbScope, this.WCtx.Session);

        private async ValueTask<IKrSecondaryProcess> GetSecondaryProcessAsync(
            IWorkflowProcessInfo info,
            Guid? secondaryProcessID,
            ProcessCommonInfo pci,
            CancellationToken cancellationToken = default)
        {
            if (info.ProcessTypeName != KrSecondaryProcessName
                && info.ProcessTypeName != KrNestedProcessName)
            {
                return null;
            }

            var process = secondaryProcessID.HasValue
                ? await this.WCtx.ProcessCache.GetSecondaryProcessAsync(secondaryProcessID.Value, cancellationToken)
                : null;

            if (process != null)
            {
                pci.SecondaryProcessID = process.ID;
                return process;
            }
            if (pci.SecondaryProcessID.HasValue)
            {
                return await this.WCtx.ProcessCache.GetSecondaryProcessAsync(pci.SecondaryProcessID.Value, cancellationToken);
            }

            return null;
        }

        private ProcessHolder TryGetProcessHolder(Guid? processHolderID)
        {
            return processHolderID is null
                ? null
                : this.WCtx.KrScope.GetProcessHolder(processHolderID.Value);
        }

        private async ValueTask UpdateCardAsync(
            IKrProcessRunnerContext context)
        {
            // Только создающий процесс холдер может переводить его обратно.
            if (context.IsProcessHolderCreated)
            {
                await this.ObjectModelToCardRowsAsync(
                    context.ProcessHolder,
                    context.ProcessHolderSatellite,
                    context.ContextualSatellite,
                    context.CancellationToken);
            }
        }

        #endregion

        #region base overrides

        protected override Task StartProcessCoreAsync(IWorkflowProcessInfo processInfo, CancellationToken cancellationToken = default)
        {
            return this.StartRunnerAsync(processInfo, cancellationToken);
        }

        protected override async Task CompleteTaskCoreAsync(IWorkflowTaskInfo taskInfo, CancellationToken cancellationToken = default)
        {
            if (taskInfo.ProcessTypeName == KrSecondaryProcessName)
            {
                var card = await this.WCtx.KrScope.GetSecondaryKrSatelliteAsync(taskInfo.ProcessID, cancellationToken);
                this.Manager.SpecifySatelliteID(card.ID, false);
            }

            await this.StartRunnerAsync(taskInfo, cancellationToken);
        }

        protected override async Task ReinstateTaskCoreAsync(IWorkflowTaskInfo taskInfo, CancellationToken cancellationToken = default)
        {
            if (taskInfo.ProcessTypeName == KrSecondaryProcessName)
            {
                var card = await this.WCtx.KrScope.GetSecondaryKrSatelliteAsync(taskInfo.ProcessID, cancellationToken);
                this.Manager.SpecifySatelliteID(card.ID);
            }

            await this.StartRunnerAsync(taskInfo, cancellationToken);
        }

        protected override async Task<bool> ProcessSignalCoreAsync(IWorkflowSignalInfo signalInfo, CancellationToken cancellationToken = default)
        {
            if (signalInfo.ProcessTypeName == KrSecondaryProcessName)
            {
                var card = await this.WCtx.KrScope.GetSecondaryKrSatelliteAsync(signalInfo.ProcessID, cancellationToken);
                this.Manager.SpecifySatelliteID(card.ID, false);
            }

            await this.StartRunnerAsync(signalInfo, cancellationToken);
            return true;
        }

        #endregion

    }
}
