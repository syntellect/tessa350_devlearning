﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Caching;
using Tessa.Extensions.Default.Server.Workflow.KrCompilers;
using Tessa.Extensions.Default.Server.Workflow.KrCompilers.SqlProcessing;
using Tessa.Extensions.Default.Server.Workflow.KrObjectModel;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Scope;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Serialization;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.Handlers;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.StateMachine;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Platform.Data;
using Tessa.Platform.Runtime;
using Tessa.Platform.Validation;
using Unity;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow
{
    public sealed class KrAsyncProcessRunner : KrProcessRunnerBase
    {
        public KrAsyncProcessRunner(
            IKrProcessContainer container,
            IKrCompilationCache compilationCache,
            [Dependency(KrExecutorNames.CacheExecutor)] Func<IKrExecutor> executorFunc,
            IKrScope scope,
            IDbScope dbScope,
            IKrProcessCache processCache,
            IUnityContainer unityContainer,
            ISession session,
            IKrProcessRunnerProvider runnerProvider,
            IKrTypesCache typesCache,
            ICardMetadata cardMetadata,
            IKrProcessStateMachine stateMachine,
            IKrStageInterrupter interrupter,
            IKrSqlExecutor sqlExecutor,
            ICardCache cardCache,
            IKrStageSerializer serializer,
            IObjectModelMapper mapper,
            IKrTokenProvider tokenProvider,
            [Dependency(CardRepositoryNames.Extended)] ICardRepository cardRepositoryExt,
            [Dependency(CardRepositoryNames.ExtendedWithoutTransaction)] ICardRepository cardRepositoryEwt,
            [Dependency(CardRepositoryNames.Extended)] ICardStreamServerRepository streamServerRepositoryExt,
            [Dependency(CardRepositoryNames.ExtendedWithoutTransaction)] ICardStreamServerRepository streamServerRepositoryEwt)
            : base(
                container, 
                compilationCache,
                executorFunc,
                scope, 
                dbScope,
                processCache, 
                unityContainer,
                session,
                runnerProvider,
                typesCache,
                cardMetadata,
                stateMachine,
                interrupter,
                sqlExecutor,
                cardCache, 
                serializer,
                mapper,
                tokenProvider,
                cardRepositoryExt,
                cardRepositoryEwt,
                streamServerRepositoryExt,
                streamServerRepositoryEwt)
        {
        }

        /// <inheritdoc />
        protected override KrProcessRunnerMode RunnerMode { get; } = KrProcessRunnerMode.Async;

        protected override async Task<bool> Prepare(IKrProcessRunnerContext context)
        {
            if (this.Scope.HasLaunchedRunner(context.ProcessInfo.ProcessID))
            {
                context.ValidationResult.AddError(this, "$KrProcess_ErrorMessage_NestedProcessRunner");
                return false;
            }
            this.Scope.AddLaunchedRunner(context.ProcessInfo.ProcessID);
            
            if (context.InitiationCause != KrProcessRunnerInitiationCause.StartProcess)
            {
                return true;
            }

            if (context.WorkflowProcess.CurrentApprovalStageRowID.HasValue)
            {
                context.ValidationResult.AddError(this, "$KrStages_ProcessAlreadyStarted");
                return false;
            }

            await this.InitialRecalcAsync(context);
            if (!context.ValidationResult.IsSuccessful())
            {
                return false;
            }

            if (context.WorkflowProcess.Stages.Count == 0)
            {
                context.ValidationResult.AddError(this, KrErrorHelper.FormatEmptyRoute(context.SecondaryProcess));
                return false;
            }

            foreach (var stage in context.WorkflowProcess.Stages)
            {
                stage.State = KrStageState.Inactive;
            }

            if (context.ProcessInfo?.ProcessTypeName == KrConstants.KrProcessName
                && context.WorkflowProcess.Author is null)
            {
                await this.SetAuthorAsync(context);
            }

            return true;
        }

        protected override void Finalize(
            IKrProcessRunnerContext context,
            Exception exc = null)
        {
            if (context.WorkflowProcess.Stages.Count != 0
                && context.InitiationCause == KrProcessRunnerInitiationCause.StartProcess
                && context.WorkflowProcess.Stages.All(p => p.State == KrStageState.Skipped || p.Hidden))
            {
                context.ValidationResult.AddError(this, KrErrorHelper.FormatEmptyRoute(context.SecondaryProcess));
            }
            this.Scope.RemoveLaunchedRunner(context.ProcessInfo.ProcessID);
        }

        protected override async Task<NextAction> ProcessStageHandlerResultAsync(
            Stage stage,
            StageHandlerResult result,
            IKrProcessRunnerContext context)
        {
            // InProgress и None не делают ничего.
            if (result.Action != StageHandlerAction.InProgress
                && result.Action != StageHandlerAction.None)
            {
                return await base.ProcessStageHandlerResultAsync(stage, result, context);
            }

            return new NextAction();
        }

        private async Task InitialRecalcAsync(
            IKrProcessRunnerContext context)
        {
            if (!context.CardID.HasValue)
            {
                return;
            }

            var executionUnits = context.SecondaryProcess != null
                ? (await this.ProcessCache.GetStageGroupsForSecondaryProcessAsync(context.SecondaryProcess.ID, context.CancellationToken)).Select(p => p.ID)
                : null;
            await using var cardLoadingStrategy = new KrScopeMainCardAccessStrategy(context.CardID.Value, this.Scope);
            var ctx = new KrExecutionContext(
                context.CardContext,
                cardLoadingStrategy,
                context.CardID,
                context.CardTypeID,
                context.CardTypeName,
                context.CardTypeCaption,
                context.DocTypeID,
                context.KrComponents,
                context.WorkflowProcess,
                compilationResult: null,
                executionUnits: executionUnits, // или null, тогда выполнится все что возможно
                secondaryProcess: context.SecondaryProcess, // или null
                cancellationToken: context.CancellationToken
            );

            var executor = this.ExecutorFunc();
            var result = await executor.ExecuteAsync(ctx);
            context.ValidationResult.Add(result.Result);
        }

        private async Task SetAuthorAsync(
            IKrProcessRunnerContext context)
        {
            var process = context.WorkflowProcess;
            var contextualSatellite = context.ContextualSatellite;
            var user = this.Session.User;

            // Для дальнейшей доступности автора в объектной модели
            process.Author = new Author(user.ID, user.Name);
            
            // Для дальнейшей доступности в холдере.
            context.ProcessHolder.PrimaryProcessCommonInfo.AuthorID = user.ID;
            context.ProcessHolder.PrimaryProcessCommonInfo.AuthorName = user.Name;
            
            // Для дальнейшей доступности автора в сателлите
            var aciFields = contextualSatellite.GetApprovalInfoSection().RawFields;
            aciFields[KrConstants.KrApprovalCommonInfo.AuthorID] = user.ID;
            aciFields[KrConstants.KrApprovalCommonInfo.AuthorName] = user.Name;

            // Внесем изменения в базу для доступа из контекстных ролей
            var db = this.DbScope.Db;
            await db.SetCommand(
                    this.DbScope.BuilderFactory
                        .Update(KrConstants.KrApprovalCommonInfo.Name)
                        .C(KrConstants.KrApprovalCommonInfo.AuthorID).Assign().P("aid")
                        .C(KrConstants.KrApprovalCommonInfo.AuthorName).Assign().P("an")
                        .Where().C("ID").Equals().P("ID")
                        .Build(),
                    db.Parameter("aid", user.ID),
                    db.Parameter("an", user.Name),
                    db.Parameter("ID", contextualSatellite.ID))
                .LogCommand()
                .ExecuteNonQueryAsync(context.CancellationToken);
        }
    }
}
