﻿using System.Threading.Tasks;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow
{
    public interface IKrProcessRunner
    {
        Task RunAsync(IKrProcessRunnerContext context);
    }
}