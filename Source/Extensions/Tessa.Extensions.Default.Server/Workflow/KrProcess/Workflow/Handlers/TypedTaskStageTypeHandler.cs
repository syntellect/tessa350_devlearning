﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tessa.BusinessCalendar;
using Tessa.Cards;
using Tessa.Cards.Caching;
using Tessa.Extensions.Default.Server.Workflow.KrCompilers;
using Tessa.Extensions.Default.Server.Workflow.KrObjectModel;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Scope;
using Tessa.Extensions.Default.Shared;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Localization;
using Tessa.Notices;
using Tessa.Platform;
using Tessa.Platform.Data;
using Tessa.Platform.Runtime;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Tessa.Properties.Resharper;
using Tessa.Roles;
using Unity;
using NotificationHelper = Tessa.Extensions.Default.Shared.Notices.NotificationHelper;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.Handlers
{
    public class TypedTaskStageTypeHandler : StageTypeHandlerBase
    {
        #region nested types

        public class ScriptContext
        {
            private readonly Func<Guid, Performer, DateTime?, double?, string, CancellationToken, Task<CardTask>> sendTaskAction;

            private readonly IStageTypeHandlerContext context;

            private readonly IStageTasksRevoker tasksRevoker;

            public ScriptContext(
                CardTask task,
                IStageTypeHandlerContext context,
                IStageTasksRevoker tasksRevoker,
                Func<Guid, Performer, DateTime?, double?, string, CancellationToken, Task<CardTask>> sendTaskAction)
            {
                this.Task = task;
                this.sendTaskAction = sendTaskAction;
                this.context = context;
                this.tasksRevoker = tasksRevoker;
            }

            public CardTask Task { [UsedImplicitly] get; }

            [UsedImplicitly]
            public bool CompleteStage { get; set; } = false;

            [UsedImplicitly]
            public async Task<CardTask> SendTaskAsync(
                Performer performer,
                Guid? taskType = null,
                DateTime? planned = null,
                double? timeLimit = null,
                string digest = null,
                CancellationToken cancellationToken = default)
            {
                var actualTaskType = taskType
                    ?? this.context.Stage.SettingsStorage.TryGet<Guid>(KrConstants.KrTypedTaskSettingsVirtual.TaskTypeID);
                var actualDigest = digest
                    ?? this.context.Stage.SettingsStorage.TryGet<string>(KrConstants.KrTypedTaskSettingsVirtual.TaskDigest);

                return await this.sendTaskAction(actualTaskType, performer, planned, timeLimit, actualDigest, cancellationToken);
            }

            [UsedImplicitly]
            public async Task<int> RevokeTaskAsync(
                Guid taskID,
                CancellationToken cancellationToken = default)
            {
                var cardID = this.context.MainCardID;
                if (cardID is null)
                {
                    throw new NullReferenceException("MainCardID");
                }

                return await this.tasksRevoker.RevokeTaskAsync(new StageTaskRevokerContext(this.context, cancellationToken)
                {
                    CardID = cardID.Value,
                    TaskID = taskID,
                })
                    ? 1
                    : 0;
            }

            [UsedImplicitly]
            public async Task<int> RevokeTaskAsync(
                IEnumerable<Guid> taskIDs,
                CancellationToken cancellationToken = default)
            {
                var cardID = this.context.MainCardID;
                if (cardID is null)
                {
                    throw new NullReferenceException("MainCardID");
                }

                return await this.tasksRevoker.RevokeTasksAsync(new StageTaskRevokerContext(this.context, cancellationToken)
                {
                    CardID = cardID.Value,
                    TaskIDs = taskIDs.ToList(),
                });
            }
        }

        #endregion

        #region fields

        public static readonly string ScriptContextParameterType =
            $"global::{typeof(TypedTaskStageTypeHandler).FullName}.{nameof(ScriptContext)}";

        public const string AfterTaskMethodName = "AfterTask";

        public const string MethodParameterName = "TypedTaskContext";

        protected const string ActiveTasksCount = nameof(ActiveTasksCount);

        protected const string CompleteStageCountdown = nameof(CompleteStageCountdown);

        protected readonly IKrScope KrScope;

        protected readonly ISession Session;

        protected readonly IRoleRepository RoleRepository;

        protected readonly IKrCompilationCache CompilationCache;

        protected readonly IUnityContainer UnityContainer;

        protected readonly IStageTasksRevoker TasksRevoker;

        protected readonly IDbScope DbScope;

        protected readonly INotificationManager NotificationManager;

        protected readonly ICardCache CardCache;

        #endregion

        #region constructor

        public TypedTaskStageTypeHandler(
            IKrScope krScope,
            ISession session,
            IRoleRepository roleRepository,
            IKrCompilationCache compilationCache,
            IUnityContainer unityContainer,
            IStageTasksRevoker tasksRevoker,
            IDbScope dbScope,
            [Dependency(NotificationManagerNames.DeferredWithoutTransaction)] INotificationManager notificationManager,
            ICardCache cardCache)
        {
            this.KrScope = krScope;
            this.Session = session;
            this.RoleRepository = roleRepository;
            this.CompilationCache = compilationCache;
            this.UnityContainer = unityContainer;
            this.TasksRevoker = tasksRevoker;
            this.DbScope = dbScope;
            this.NotificationManager = notificationManager;
            this.CardCache = cardCache;
        }

        #endregion

        #region Base overrides

        /// <inheritdoc />
        public override Task BeforeInitializationAsync(IStageTypeHandlerContext context)
        {
            HandlerHelper.ClearCompletedTasks(context.Stage);

            return Task.CompletedTask;
        }

        /// <inheritdoc />
        public override async Task<StageHandlerResult> HandleStageStartAsync(
            IStageTypeHandlerContext context)
        {
            var stage = context.Stage;
            var performers = stage.Performers;
            if (performers.Count == 0)
            {
                return StageHandlerResult.CompleteResult;
            }

            var settingsStorage = context.Stage.SettingsStorage;
            var taskType = settingsStorage.TryGet<Guid?>(KrConstants.KrTypedTaskSettingsVirtual.TaskTypeID);

            if (!taskType.HasValue)
            {
                context.ValidationResult.AddError(
                    this,
                    LocalizationManager.Format(
                        "$KrProcess_ErrorMessage_ErrorFormat2",
                        KrErrorHelper.GetTraceTextFromStage(stage),
                        "$KrStages_TypedTask_TaskType"));
                return StageHandlerResult.EmptyResult;
            }

            var krSettings = await this.CardCache.Cards.GetAsync(DefaultCardTypes.KrSettingsTypeName);
            var rows = default(ListStorage<CardRow>);
            if (!krSettings.Sections.TryGetValue(KrConstants.KrSettingsRouteExtraTaskTypes.Name, out var krSettingsRouteExtraTaskTypes)
                || (rows = krSettingsRouteExtraTaskTypes.TryGetRows()) is null
                || !rows.Any(i => i.Fields.Get<Guid>(KrConstants.KrSettingsRouteExtraTaskTypes.TaskTypeID) == taskType.Value))
            {
                context.ValidationResult.AddError(
                    this,
                    LocalizationManager.Format(
                        "$KrProcess_ErrorMessage_ErrorFormat2",
                        KrErrorHelper.GetTraceTextFromStage(stage),
                        LocalizationManager.Format(
                            "$KrStages_TypedTask_TaskTypeUndefined",
                            settingsStorage.TryGet<string>(KrConstants.KrTypedTaskSettingsVirtual.TaskTypeCaption),
                            settingsStorage.TryGet<string>(KrConstants.KrTypedTaskSettingsVirtual.TaskTypeName))));
                return StageHandlerResult.EmptyResult;
            }

            var digest = settingsStorage.TryGet<string>(KrConstants.KrTypedTaskSettingsVirtual.TaskDigest);

            foreach (var performer in performers)
            {
                await this.SendTaskAsync(context, taskType.Value, performer, null, null, digest);
            }

            stage.InfoStorage.Remove(CompleteStageCountdown);
            stage.InfoStorage[ActiveTasksCount] = Int32Boxes.Box(performers.Count);

            return StageHandlerResult.InProgressResult;
        }

        /// <inheritdoc />
        public override async Task<StageHandlerResult> HandleTaskCompletionAsync(
            IStageTypeHandlerContext context)
        {
            var task = context.TaskInfo.Task;
            var stage = context.Stage;

            var completeStageCountdown = stage.InfoStorage.TryGet<int>(CompleteStageCountdown);
            if (completeStageCountdown >= 1)
            {
                stage.InfoStorage[CompleteStageCountdown] = Int32Boxes.Box(--completeStageCountdown);
                return completeStageCountdown == 0
                    ? StageHandlerResult.CompleteResult
                    : StageHandlerResult.InProgressResult;
            }

            if (task.State == CardRowState.Deleted)
            {
                stage.InfoStorage[ActiveTasksCount] = Int32Boxes.Box(stage.InfoStorage.TryGet<int>(ActiveTasksCount) - 1);

                var taskStorage = StorageHelper.Clone(task.GetStorage());
                var taskCopy = new CardTask(taskStorage);
                taskCopy.RemoveChanges();

                taskStorage.Remove(nameof(CardTask.SectionRows));
                taskStorage.Remove(nameof(CardTask.Card));
                taskStorage.Remove(CardInfoStorageObject.InfoKey);

                HandlerHelper.AppendToCompletedTasks(stage, taskCopy);
            }

            var inst = await HandlerHelper.CreateScriptInstanceAsync(
                this.CompilationCache,
                context.Stage.ID,
                context.ValidationResult,
                context.CancellationToken);
            await HandlerHelper.InitScriptContextAsync(this.UnityContainer, inst, context);
            var ctx = new ScriptContext (
                task,
                context,
                this.TasksRevoker,
                async (ttid, prf, plnd, tmlmt, dg, ct) =>
                {
                    stage.InfoStorage[ActiveTasksCount] = Int32Boxes.Box(stage.InfoStorage.TryGet<int>(ActiveTasksCount) + 1);
                    return await this.SendTaskAsync(context, ttid, prf, plnd, tmlmt, dg);
                });
            await inst.InvokeExtraAsync(AfterTaskMethodName, ctx);

            if (ctx.CompleteStage)
            {
                return await this.CompleteStageAsync(context);
            }

            return stage.InfoStorage.TryGet<int>(ActiveTasksCount) == 0
                ? StageHandlerResult.CompleteResult
                : StageHandlerResult.InProgressResult;
        }

        public override async Task<bool> HandleStageInterruptAsync(IStageTypeHandlerContext context) =>
            await this.TasksRevoker.RevokeAllStageTasksAsync(new StageTaskRevokerContext(context, context.CancellationToken));

        #endregion

        #region protected

        protected async Task<CardTask> SendTaskAsync(
            IStageTypeHandlerContext context,
            Guid taskType,
            Performer performer,
            DateTime? planned,
            double? timeLimit,
            string digest)
        {
            var groupID = await HandlerHelper.GetTaskHistoryGroupAsync(context, this.KrScope);
            var author = await HandlerHelper.GetStageAuthorAsync(context, this.RoleRepository, this.Session);
            if (author == null)
            {
                return null;
            }
            var (kindID, kindCaption) = HandlerHelper.GetTaskKind(context);

            var authorID = author.AuthorID;
            var task = (await context.WorkflowAPI.SendTaskAsync(
                taskType,
                digest,
                performer.PerformerID,
                performer.PerformerName,
                modifyTaskAction: (t, ct) =>
                {
                    t.AuthorID = authorID;
                    t.AuthorName = null; // AuthorName и AuthorPosition определяются системой, когда явно указано null
                    t.GroupRowID = groupID;
                    t.Planned = planned ?? context.Stage.Planned;
                    t.PlannedQuants =
                        timeLimit.HasValue
                            ? (int)Math.Round(timeLimit.Value * TimeZonesHelper.QuantsInDay)
                            : context.Stage.PlannedQuants;
                    HandlerHelper.SetTaskKind(t, kindID, kindCaption, context);

                    return new ValueTask();
                },
                cancellationToken: context.CancellationToken)).Task;
            task.Flags |= CardTaskFlags.CreateHistoryItem;
            context.ContextualSatellite.AddToHistory(task.RowID,
                context.WorkflowProcess.InfoStorage.TryGet(KrConstants.Keys.Cycle, 1));

            await context.WorkflowAPI.AddActiveTaskAsync(task.RowID, context.CancellationToken);
            context.ValidationResult.Add(
                await this.NotificationManager.SendAsync(
                    DefaultNotifications.TaskNotification,
                    new[] { task.RoleID },
                    new NotificationSendContext()
                    {
                        MainCardID = context.MainCardID ?? Guid.Empty,
                        Info = NotificationHelper.GetInfoWithTask(task),
                        ModifyEmailActionAsync = async (email, ct) =>
                        {
                            NotificationHelper.ModifyEmailForMobileApprovers(
                                email,
                                task,
                                await NotificationHelper.GetMobileApprovalEmailAsync(this.CardCache, ct));

                            NotificationHelper.ModifyTaskCaption(
                                email,
                                task);
                        },
                        GetCardFuncAsync = (ct) => context.MainCardAccessStrategy.GetCardAsync(cancellationToken: ct),
                    },
                    context.CancellationToken));

            return task;
        }

        protected async Task<StageHandlerResult> CompleteStageAsync(IStageTypeHandlerContext context)
        {
            var stage = context.Stage;
            await using (this.DbScope.Create())
            {
                var db = this.DbScope.Db;
                // Получение списка заданий из таблицы WorkflowTasks
                var currentTasks = await db.SetCommand(
                        this.DbScope.BuilderFactory
                            .Select()
                            .C("RowID")
                            .From("WorkflowTasks").NoLock()
                            .Where().C("ProcessRowID").Equals().P("pid")
                            .Build(),
                        db.Parameter("pid", context.ProcessInfo.ProcessID))
                    .LogCommand()
                    .ExecuteListAsync<Guid>(context.CancellationToken);
                if (currentTasks.Count == 0)
                {
                    return StageHandlerResult.CompleteResult;
                }
                stage.InfoStorage[CompleteStageCountdown] = Int32Boxes.Box(currentTasks.Count);
                await this.TasksRevoker.RevokeAllStageTasksAsync(new StageTaskRevokerContext(context, context.CancellationToken));
                return StageHandlerResult.InProgressResult;
            }
        }

        #endregion
    }
}