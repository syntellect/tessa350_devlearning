﻿using System.Threading.Tasks;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.Handlers
{
    public class ScriptStageTypeHandler: StageTypeHandlerBase
    {
        #region Base Overrides

        public override Task<StageHandlerResult> HandleStageStartAsync(IStageTypeHandlerContext ctx) => 
            Task.FromResult(StageHandlerResult.CompleteResult);

        #endregion
    }
}