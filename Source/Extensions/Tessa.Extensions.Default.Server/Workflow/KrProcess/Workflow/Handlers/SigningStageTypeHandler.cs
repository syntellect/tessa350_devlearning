﻿using System;
using System.Threading.Tasks;
using Tessa.BusinessCalendar;
using Tessa.Cards;
using Tessa.Cards.Caching;
using Tessa.Cards.ComponentModel;
using Tessa.Cards.Extensions;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Scope;
using Tessa.Extensions.Default.Shared;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Notices;
using Tessa.Platform;
using Tessa.Platform.Runtime;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Tessa.Roles;
using Unity;
using static Tessa.Extensions.Default.Shared.Workflow.KrProcess.KrConstants;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.Handlers
{
    public class SigningStageTypeHandler : SubtaskStageTypeHandler
    {
        private const string TotalPerformerCount = nameof(TotalPerformerCount);
        private const string CurrentPerformerCount = nameof(CurrentPerformerCount);
        private const string Disapproved = Keys.Disapproved;

        public SigningStageTypeHandler(
            IRoleRepository roleRepository,
            ICardMetadata cardMetadata,
            ICardGetStrategy cardGetStrategy,
            ICardCache cardCache,
            IKrScope krScope,
            IBusinessCalendarService calendarService,
            ISession session,
            IStageTasksRevoker tasksRevoker,
            [Dependency(NotificationManagerNames.DeferredWithoutTransaction)] INotificationManager notificationManager)
            : base(roleRepository, calendarService, cardMetadata, cardGetStrategy, krScope, tasksRevoker, notificationManager, cardCache, session)
        {
            this.CardCache = cardCache ?? throw new ArgumentNullException(nameof(cardCache));
        }

        #region Protected Properties

        protected ICardCache CardCache { get; set; }

        /// <inheritdoc />
        protected override string CommentNameField { get; } = KrSigningStageSettingsVirtual.Comment;

        #endregion

        #region Protected Methods

        /// <summary>
        /// Обрабатывает завершение задания подписания.
        /// </summary>
        /// <param name="context">Контекст обработчика этапа.</param>
        /// <param name="declined">Значение <see langword="true"/>, если задание подпсиания было завершено вариантом завершения <see cref="DefaultCompletionOptions.Decline"/>, иначе (<see cref="DefaultCompletionOptions.Sign"/>) - <see cref="false"/>.</param>
        /// <returns>Результат выполнения.</returns>
        protected virtual async Task<StageHandlerResult> CompleteSigningTaskAsync(IStageTypeHandlerContext context, bool declined)
        {
            var task = context.TaskInfo.Task;
            HandlerHelper.AppendToCompletedTasksWithPreparing(context.Stage, task);
            
            await this.RevokeSubTasksAsync(context, task,
                new[] { DefaultTaskTypes.KrRequestCommentTypeID },
                t => t.Result = "$ApprovalHistory_ParentTaskIsCompleted");

            var field = declined ? KrApprovalCommonInfo.DisapprovedBy : KrApprovalCommonInfo.ApprovedBy;
            var fields = context.ContextualSatellite.Sections[KrApprovalCommonInfo.Name].Fields;
            var result = StringBuilderHelper.Acquire();

            var value = fields.TryGet<string>(field);
            if (!string.IsNullOrEmpty(value))
            {
                result.Append(value).Append("; ");
            }

            var storeContext = (ICardStoreExtensionContext)context.CardExtensionContext;
            var user = storeContext.Session.User;
            result.Append(user.Name);

            if (task.RoleID != user.ID)
            {
                result.Append(" (");

                var role = KrAdditionalApprovalMarker.Unmark(task.RoleName);
                result.Append(role);

                result.Append(')');
            }

            fields[field] = result.ToStringAndRelease();

            var stage = context.Stage;
            var total = stage.InfoStorage.Get<int>(TotalPerformerCount);
            var current = stage.InfoStorage.Get<int>(CurrentPerformerCount);
            stage.InfoStorage[CurrentPerformerCount] = Int32Boxes.Box(++current);

            var changeStateOnEnd = stage.SettingsStorage.Get<bool>(KrSigningStageSettingsVirtual.ChangeStateOnEnd);

            if (declined)
            {
                stage.InfoStorage[Disapproved] = BooleanBoxes.True;
            }
            if (declined
                || !(await this.CardCache.Cards
                    .GetAsync(KrSettings.Name, context.CancellationToken))
                    .Sections[KrSettings.Name].RawFields.Get<bool>(KrSettings.HideCommentForApprove))
            {
                await HandlerHelper.SetTaskResultAsync(context, task, task.Card.Sections[nameof(KrTask)].Fields.TryGet<string>(nameof(KrTask.Comment)));
            }

            var notReturnEdit = (context.ProcessInfo.ProcessParameters.TryGet<bool?>(Keys.NotReturnEdit, true) ?? true)
                && stage.SettingsStorage.Get<bool>(KrSigningStageSettingsVirtual.NotReturnEdit);
            var returnWhenDeclined = stage.SettingsStorage.Get<bool>(KrSigningStageSettingsVirtual.ReturnWhenDeclined);
            if (current == total)
            {
                return stage.InfoStorage.TryGet<bool>(Disapproved)
                    ? await DeclineAndCompleteAsync()
                    : SignAndComplete();
            }

            if (!stage.SettingsStorage.Get<bool>(KrSigningStageSettingsVirtual.IsParallel))
            {
                if (declined && returnWhenDeclined)
                {
                    return await DeclineAndCompleteAsync();
                }
                
                var author = await HandlerHelper.GetStageAuthorAsync(context, this.RoleRepository, this.Session);
                if (author == null)
                {
                    return StageHandlerResult.EmptyResult;
                }
                var authorID = author.AuthorID;
                
                var groupID = await HandlerHelper.GetTaskHistoryGroupAsync(context, this.KrScope);
                var (kindID, kindCaption) = HandlerHelper.GetTaskKind(context);
                await this.SendTaskAsync(
                    context,
                    DefaultTaskTypes.KrSigningTypeID,
                    this.GetTaskDigest(context),
                    stage.Performers[current],
                    (t, ct) =>
                    {
                        t.AuthorID = authorID;
                        t.AuthorName = null;    // AuthorName и AuthorPosition определяются системой, когда явно указано null
                        t.GroupRowID = groupID;
                        t.Planned = stage.Planned;
                        t.PlannedQuants = stage.PlannedQuants;
                        HandlerHelper.SetTaskKind(t, kindID, kindCaption, context);
                        return new ValueTask();
                    });
            }

            return StageHandlerResult.InProgressResult;

            StageHandlerResult SignAndComplete()
            {
                StagePositionInGroup(out var hasPreviouslyDisapproved, out var hasNext);
                var returnToAuthor = stage.SettingsStorage.Get<bool>(KrSigningStageSettingsVirtual.ReturnToAuthor);
                
                if (changeStateOnEnd)
                {
                    if (hasPreviouslyDisapproved)
                    {
                        context.WorkflowProcess.State = KrState.Declined;
                        this.KrScope.Info[Keys.IgnoreChangeState] = BooleanBoxes.True;
                    }
                    else if (!returnToAuthor || notReturnEdit)
                    {
                        context.WorkflowProcess.State = KrState.Signed;
                    }
                }

                if (!notReturnEdit)
                {
                    if (hasPreviouslyDisapproved
                        && !hasNext)
                    {
                        // Последний этап завершен. Этот подписан, но предыдущие могли быть и не подписаны
                        // Если были неподписанные, то возвращаемся в начало на доработку
                        return StageHandlerResult.CurrentGroupTransition();
                    }

                    if (returnToAuthor)
                    {
                        // Выполнить переход к этапу "Доработка" расположенному в текущей группе этапов.
                        Guid? editID = null;
                        var cycle = GetCycle(context);
                        context.WorkflowProcess.Stages.ForEachStageInGroup(
                            stage.StageGroupID,
                            currentStage =>
                            {
                                if (currentStage.ID == stage.ID)
                                {
                                    return false;
                                }

                                if (currentStage.StageTypeID == StageTypeDescriptors.EditDescriptor.ID)
                                {
                                    stage.InfoStorage[Interjected] = Int32Boxes.Box(cycle);
                                    currentStage.InfoStorage[EditStageTypeHandler.ReturnToStage] = stage.ID;
                                    editID = currentStage.ID;
                                    return false;
                                }
                                return true;
                            });

                        // Решарпер считает, что т.к. editID замкнуто в лямбде, то оно может быть изменено в другом потоке
                        // Дадим ему уверенность, переприсвоив в локальную переменную
                        var localEditID = editID;
                        if (localEditID.HasValue)
                        {
                            var transRes = StageHandlerResult.Transition(localEditID.Value, keepStageStates: true);
                            return this.StageCompleted(context, transRes);
                        }


                        context.ValidationResult.AddError(this, "$KrMessages_NoEditStage");
                    }
                }

                return this.StageCompleted(context, StageHandlerResult.CompleteResult);
            }

            async Task<StageHandlerResult> DeclineAndCompleteAsync()
            {
                StagePositionInGroup(out _, out var hasNext);

                if (!returnWhenDeclined && hasNext)
                {
                    context.WorkflowProcess.InfoStorage[Disapproved] = BooleanBoxes.True;
                    return this.StageCompleted(context, StageHandlerResult.CompleteResult);
                }

                if (changeStateOnEnd)
                {
                    context.WorkflowProcess.State = KrState.Declined;
                    this.KrScope.Info[Keys.IgnoreChangeState] = BooleanBoxes.True;
                }

                if (notReturnEdit)
                {
                    return this.StageCompleted(context, StageHandlerResult.CompleteResult);
                }

                var utcNow = DateTime.UtcNow;
                var option = (await this.CardMetadata.GetEnumerationsAsync(context.CancellationToken)).CompletionOptions[DefaultCompletionOptions.RebuildDocument];
                var groupID = await HandlerHelper.GetTaskHistoryGroupAsync(context, this.KrScope);
                // Временная зона текущего сотрудника, для записи в историю заданий
                var userZoneInfo = await this.CalendarService.GetRoleTimeZoneInfoAsync(user.ID, context.CancellationToken);
                var item = new CardTaskHistoryItem
                {
                    State = CardTaskHistoryState.Inserted,
                    RowID = Guid.NewGuid(),
                    TypeID = DefaultTaskTypes.KrRebuildTypeID,
                    TypeName = DefaultTaskTypes.KrRebuildTypeName,
                    TypeCaption = "$CardTypes_TypesNames_KrRebuild",
                    Created = utcNow,
                    Planned = utcNow,
                    InProgress = utcNow,
                    Completed = utcNow,
                    CompletedByID = user.ID,
                    CompletedByName = user.Name,
                    AuthorID = user.ID,
                    AuthorName = user.Name,
                    UserID = user.ID,
                    UserName = user.Name,
                    RoleID = user.ID,
                    RoleName = user.Name,
                    RoleTypeID = RoleHelper.PersonalRoleTypeID,
                    OptionID = option.ID,
                    OptionName = option.Name,
                    OptionCaption = option.Caption,
                    GroupRowID = groupID,
                    TimeZoneID = userZoneInfo.TimeZoneID,
                    TimeZoneUtcOffsetMinutes = (int?)userZoneInfo.TimeZoneUtcOffset.TotalMinutes
                };

                (await this.KrScope.GetMainCardAsync(storeContext.Request.Card.ID, cancellationToken: context.CancellationToken)).TaskHistory.Add(item);
                context.ContextualSatellite.AddToHistory(item.RowID, context.WorkflowProcess.InfoStorage.TryGet(Keys.Cycle, 1));

                return this.StageCompleted(context, StageHandlerResult.GroupTransition(stage.StageGroupID));
            }

            void StagePositionInGroup(out bool hasPreviouslyDisapproved, out bool hasNext)
            {
                var hasPreviouslyDisapprovedClosure = false;
                var hasNextClosure = false;

                var equator = false;
                context.WorkflowProcess.Stages.ForEachStageInGroup(
                    context.Stage.StageGroupID,
                    currStage =>
                    {
                        if (currStage.ID == context.Stage.ID)
                        {
                            equator = true;
                            return;
                        }

                        if (!equator
                            && currStage.State == KrStageState.Completed
                            && currStage.StageTypeID == StageTypeDescriptors.SigningDescriptor.ID
                            && currStage.InfoStorage.TryGet<bool?>(Disapproved) == true)
                        {
                            hasPreviouslyDisapprovedClosure = true;
                        }

                        if (!hasNextClosure
                            && equator
                            && currStage.StageTypeID == StageTypeDescriptors.SigningDescriptor.ID)
                        {
                            hasNextClosure = true;
                        }
                    });
                hasPreviouslyDisapproved = hasPreviouslyDisapprovedClosure;
                hasNext = hasNextClosure;
            }
        }

        #endregion

        #region Base Overrides

        /// <inheritdoc />
        public override async Task<StageHandlerResult> HandleStageStartAsync(IStageTypeHandlerContext context)
        {
            var stage = context.Stage;
            if (IsInterjected(context))
            {
                if (stage.SettingsStorage.Get<bool>(KrSigningStageSettingsVirtual.ChangeStateOnEnd))
                {
                    context.WorkflowProcess.State = KrState.Signed;
                }

                stage.InfoStorage.Remove(Interjected);
                return StageHandlerResult.CompleteResult;
            }

            var baseResult = await base.HandleStageStartAsync(context);
            if (baseResult != StageHandlerResult.EmptyResult)
            {
                return baseResult;
            }

            var performers = stage.Performers;
            if (performers.Count == 0)
            {
                if (stage.SettingsStorage.Get<bool>(KrSigningStageSettingsVirtual.ChangeStateOnEnd))
                {
                    context.WorkflowProcess.State = KrState.Signed;
                }

                return StageHandlerResult.CompleteResult;
            }

            if (stage.SettingsStorage.Get<bool>(KrSigningStageSettingsVirtual.ChangeStateOnStart))
            {
                context.WorkflowProcess.State = KrState.Signing;
            }

            stage.InfoStorage[TotalPerformerCount] = Int32Boxes.Box(performers.Count);
            stage.InfoStorage[CurrentPerformerCount] = Int32Boxes.Zero;
            stage.InfoStorage[Disapproved] = BooleanBoxes.False;

            var groupID = await HandlerHelper.GetTaskHistoryGroupAsync(context, this.KrScope);
            var author = await HandlerHelper.GetStageAuthorAsync(context, this.RoleRepository, this.Session);
            if (author == null)
            {
                return StageHandlerResult.EmptyResult;
            }
            var authorID = author.AuthorID;
            var (kindID, kindCaption) = HandlerHelper.GetTaskKind(context);
            if (stage.SettingsStorage.Get<bool>(KrSigningStageSettingsVirtual.IsParallel))
            {
                foreach (var performer in performers)
                {
                    await this.SendTaskAsync(
                        context,
                        DefaultTaskTypes.KrSigningTypeID,
                        this.GetTaskDigest(context),
                        performer,
                        (t, ct) =>
                        {
                            t.AuthorID = authorID;
                            t.AuthorName = null;    // AuthorName и AuthorPosition определяются системой, когда явно указано null
                            t.GroupRowID = groupID;
                            t.Planned = stage.Planned;
                            t.PlannedQuants = stage.PlannedQuants;
                            HandlerHelper.SetTaskKind(t, kindID, kindCaption, context);
                            return new ValueTask();
                        });
                }
            }
            else
            {
                await this.SendTaskAsync(
                    context,
                    DefaultTaskTypes.KrSigningTypeID,
                    this.GetTaskDigest(context),
                    performers[0],
                    (t, ct) =>
                    {
                        t.AuthorID = authorID;
                        t.AuthorName = null;    // AuthorName и AuthorPosition определяются системой, когда явно указано null
                        t.GroupRowID = groupID;
                        t.Planned = stage.Planned;
                        t.PlannedQuants = stage.PlannedQuants;
                        HandlerHelper.SetTaskKind(t, kindID, kindCaption, context);

                        return new ValueTask();
                    });
            }

            return StageHandlerResult.InProgressResult;
        }

        /// <inheritdoc />
        public override async Task<StageHandlerResult> HandleTaskCompletionAsync(IStageTypeHandlerContext context)
        {
            var baseResult = await base.HandleTaskCompletionAsync(context);
            if (baseResult != StageHandlerResult.EmptyResult)
            {
                return baseResult;
            }

            var task = context.TaskInfo.Task;
            await context.WorkflowAPI.RemoveActiveTaskAsync(task.RowID, context.CancellationToken);

            var optionID = task.OptionID;
            if (optionID == DefaultCompletionOptions.Sign)
            {
                return await this.CompleteSigningTaskAsync(context, false);
            }

            if (optionID == DefaultCompletionOptions.Decline)
            {
                return await this.CompleteSigningTaskAsync(context, true);
            }

            return StageHandlerResult.EmptyResult;
        }

        /// <inheritdoc />
        public override Task<bool> HandleStageInterruptAsync(IStageTypeHandlerContext context)
            => this.HandleStageInterruptAsync(context,
                new[]
                {
                    DefaultTaskTypes.KrSigningTypeID,
                    DefaultTaskTypes.KrRequestCommentTypeID
                },
                t => t.Result = "$ApprovalHistory_TaskCancelledBacauseCancelling");

        #endregion
    }
}
