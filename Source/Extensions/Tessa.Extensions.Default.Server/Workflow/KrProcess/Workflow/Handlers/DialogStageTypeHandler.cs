using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Extensions;
using Tessa.Extensions.Default.Server.Workflow.KrCompilers;
using Tessa.Extensions.Default.Server.Workflow.KrCompilers.UserAPI;
using Tessa.Extensions.Default.Server.Workflow.KrObjectModel;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Scope;
using Tessa.Extensions.Default.Shared;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess.ClientCommandInterpreter;
using Tessa.Localization;
using Tessa.Platform;
using Tessa.Platform.Data;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Tessa.Properties.Resharper;
using Unity;
using static Tessa.Extensions.Default.Shared.Workflow.KrProcess.KrConstants;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.Handlers
{
    public class DialogStageTypeHandler : StageTypeHandlerBase
    {
        #region Nested Types

        /// <summary>
        /// Базовый класс, представляющий контекст скрипта этапа "Диалог".
        /// </summary>
        public abstract class ScriptContextBase
        {
            /// <summary>
            /// Стратегия доступа к карточке диалога.
            /// </summary>
            protected readonly IMainCardAccessStrategy dialogCardAccessStrategy;

            /// <summary>
            /// Инициализирует новый экземпляр класса <see cref="ScriptContextBase"/>.
            /// </summary>
            /// <param name="dialogCardAccessStrategy">Стратегия доступ к карточке диалога.</param>
            /// <param name="buttonName">Алиас нажатой кнопки.</param>
            /// <param name="storeMode">Режим сохранения карточки диалога.</param>
            protected ScriptContextBase(
                IMainCardAccessStrategy dialogCardAccessStrategy,
                string buttonName,
                CardTaskDialogStoreMode storeMode)
            {
                this.dialogCardAccessStrategy = dialogCardAccessStrategy;
                this.ButtonName = buttonName;
                this.StoreMode = storeMode;
            }

            /// <summary>
            /// Возвращает карточку диалога.
            /// </summary>
            /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
            /// <remarks>Карточка диалога.</remarks>
            [UsedImplicitly]
            public ValueTask<Card> GetDialogCardAsync(CancellationToken cancellationToken = default) => this.dialogCardAccessStrategy.GetCardAsync(cancellationToken: cancellationToken);

            /// <summary>
            /// Возвращает алиас нажатой кнопки.
            /// </summary>
            [UsedImplicitly]
            public string ButtonName { get; }

            /// <summary>
            /// Возвращает способ сохранения карточки диалога.
            /// </summary>
            /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
            /// <remarks>Карточка диалога.</remarks>
            [UsedImplicitly]
            public CardTaskDialogStoreMode StoreMode { get; }

            /// <summary>
            /// Асинхронно возвращает контент файла карточки диалога с временем жизни: запрос или задание.
            /// </summary>
            /// <param name="file">Информация о файле, контент которого требуется получить.</param>
            /// <returns>Массив байт, являющийся контентом указанного файла.</returns>
            public Task<byte[]> GetFileContentAsync(
                CardFile file)
            {
                if (this.StoreMode == CardTaskDialogStoreMode.Info
                    || this.StoreMode == CardTaskDialogStoreMode.Settings)
                {
                    return CardTaskDialogHelper.GetFileContentFromBase64Async(CardTaskDialogHelper.GetFileContentFromInfo(file));
                }

                throw new InvalidOperationException(
                    $"Method \"{nameof(GetFileContentAsync)}\" not allowed for dialogs with \"{this.StoreMode.ToString()}\" store mode.{Environment.NewLine}" +
                    $"Use \"{nameof(GetFileContainerAsync)}\" instead.");
            }

            /// <summary>
            /// Задаёт контент файла карточки диалога с временем жизни запрос.
            /// </summary>
            /// <param name="file">Информация о файле, контент которого требуется задать.</param>
            /// <param name="content">Задаваемый контент файла.</param>
            public void SetFileContent(CardFile file, byte[] content)
            {
                if (this.StoreMode == CardTaskDialogStoreMode.Info)
                {
                    var fileVersion = CardTaskDialogHelper.GetFileVersionFromInfo(file);
                    fileVersion.Size = content.Length;

                    var base64 = Convert.ToBase64String(content);
                    CardTaskDialogHelper.SetFileContentToInfo(file, base64, fileVersion);
                }
                else
                {
                    throw new InvalidOperationException(
                        $"Method \"{nameof(SetFileContent)}\" not allowed for dialogs with \"{this.StoreMode.ToString()}\" store mode.{Environment.NewLine}" +
                        $"Use \"{nameof(GetFileContainerAsync)}\" instead.");
                }
            }

            /// <summary>
            /// Асинхронно возвращает файловый контейнер для карточки диалога с временем жизни карточка и задание.
            /// </summary>
            /// <param name="validationResult">Результат валидации.</param>
            /// <returns>Файловый контейнер или значение null, если при его создании произошла ошибка.</returns>
            public ValueTask<ICardFileContainer> GetFileContainerAsync(
                IValidationResultBuilder validationResult = null,
                CancellationToken cancellationToken = default)
            {
                if (this.StoreMode != CardTaskDialogStoreMode.Card
                    && this.StoreMode != CardTaskDialogStoreMode.Settings)
                {
                    throw new InvalidOperationException(
                        $"Method \"{nameof(GetFileContainerAsync)}\" not allowed for dialogs with \"{this.StoreMode.ToString()}\" store mode.{Environment.NewLine}" +
                        $"Use \"{nameof(GetFileContentAsync)}\" or \"{nameof(SetFileContent)}\" instead.");
                }

                return this.dialogCardAccessStrategy.GetFileContainerAsync(validationResult, cancellationToken);
            }
        }

        /// <summary>
        /// Представляет контекст скрипта валидации.
        /// </summary>
        public sealed class ScriptContext : ScriptContextBase
        {
            /// <summary>
            /// Инициализирует новый экземпляр класса <see cref="ScriptContext"/>.
            /// </summary>
            /// <param name="dialogCardAccessStrategy">Стратегия доступ к карточке диалога.</param>
            /// <param name="buttonName">Алиас нажатой кнопки.</param>
            /// <param name="storeMode">Режим сохранения карточки диалога.</param>
            public ScriptContext(
                IMainCardAccessStrategy dialogCardAccessStrategy,
                string buttonName,
                CardTaskDialogStoreMode storeMode)
                : base(dialogCardAccessStrategy, buttonName, storeMode)
            {
            }

            /// <summary>
            /// Возвращает или задаёт значение флага прерывания обработки диалога.
            /// </summary>
            [UsedImplicitly]
            public bool Cancel { get; set; }

            /// <summary>
            /// Возвращает или задаёт значение флага завершения этапа диалога.
            /// </summary>
            [UsedImplicitly]
            public bool CompleteDialog { get; set; }
        }

        /// <summary>
        /// Представляет контекст скрипта сохранения.
        /// </summary>
        public sealed class SavingScriptContext : ScriptContextBase
        {
            /// <summary>
            /// Инициализирует новый экземпляр класса <see cref="SavingScriptContext"/>.
            /// </summary>
            /// <param name="dialogCard">Стратегия доступ к карточке диалога.</param>
            /// <param name="buttonName">Алиас нажатой кнопки.</param>
            /// <param name="storeMode">Режим сохранения карточки диалога.</param>
            public SavingScriptContext(
                IMainCardAccessStrategy dialogCard,
                string buttonName,
                CardTaskDialogStoreMode storeMode)
                : base(dialogCard, buttonName, storeMode)
            {
            }
        }

        #endregion

        #region Fields

        public const string ChangedCardKey = CardHelper.SystemKeyPrefix + "ChangedCard";

        public const string ChangedCardFileContainerKey = CardHelper.SystemKeyPrefix + "ChangedCardFileContainer";

        public const string DialogsProcessInfoKey = CardHelper.SystemKeyPrefix + "Dialogs";

        public static readonly string ScriptContextParameterType =
            $"global::{typeof(DialogStageTypeHandler).FullName}.{nameof(ScriptContext)}";

        public const string MethodName = "DialogActionScript";

        public const string MethodParameterName = "Dialog";

        public static readonly string SavingScriptContextParameterType =
            $"global::{typeof(DialogStageTypeHandler).FullName}.{nameof(SavingScriptContext)}";

        public const string SavingMethodName = "SavingDialogScript";

        public const string SavingMethodParameterName = "Dialog";

        protected readonly ICardRepository CardRepositoryDwt;

        protected readonly IDbScope DbScope;

        protected readonly IKrProcessCache ProcessCache;

        protected readonly ISignatureProvider SignatureProvider;

        protected readonly IStageTasksRevoker TasksRevoker;

        protected readonly IKrTypesCache typesCache;

        protected readonly ICardFileManager cardFileManager;

        protected readonly ICardRepository cardRepository;

        protected readonly Func<ICardTaskCompletionOptionSettingsBuilder> ctcBuilderFactory;

        #endregion

        #region Constructor

        public DialogStageTypeHandler(
            IKrScope krScope,
            IKrCompilationCache compilationCache,
            IUnityContainer unityContainer,
            [Dependency(CardRepositoryNames.DefaultWithoutTransaction)] ICardRepository cardRepositoryDwt,
            IDbScope dbScope,
            IKrProcessCache processCache,
            ISignatureProvider signatureProvider,
            IStageTasksRevoker tasksRevoker,
            IKrTypesCache typesCache,
            ICardFileManager cardFileManager,
            ICardRepository cardRepository,
            Func<ICardTaskCompletionOptionSettingsBuilder> ctcBuilderFactory)
        {
            this.KrScope = krScope;
            this.CompilationCache = compilationCache;
            this.UnityContainer = unityContainer;
            this.CardRepositoryDwt = cardRepositoryDwt;
            this.DbScope = dbScope;
            this.ProcessCache = processCache;
            this.SignatureProvider = signatureProvider;
            this.TasksRevoker = tasksRevoker;
            this.typesCache = typesCache;
            this.cardFileManager = cardFileManager;
            this.cardRepository = cardRepository;
            this.ctcBuilderFactory = ctcBuilderFactory;
        }

        #endregion

        #region Properties

        protected IKrScope KrScope { get; }

        protected IKrCompilationCache CompilationCache { get; }

        protected IUnityContainer UnityContainer { get; }

        #endregion

        #region Base overrides

        /// <inheritdoc />
        public override async Task BeforeInitializationAsync(
            IStageTypeHandlerContext context)
        {
            if (!(await this.ProcessCache.GetAllRuntimeStagesAsync(context.CancellationToken)).TryGetValue(context.Stage.ID, out var runtimeStage)
                || string.IsNullOrWhiteSpace(runtimeStage.RuntimeSourceBefore))
            {
                // Если нет скрипта, то загружать карточку нет смысла.
                return;
            }

            // Тут возможны варианты:
            // Диалог неперсистентный и квазиперсистентный: создаем новую, без вариантов.
            // Диалог персистентный: либо берем готовую карточку по алиасу, либо создаем новую.

            var stage = context.Stage;
            var settingsStorage = stage.SettingsStorage;
            var storeMode = (CardTaskDialogStoreMode) settingsStorage.TryGet<int>(KrDialogStageTypeSettingsVirtual.CardStoreModeID);
            var alias = settingsStorage.TryGet<string>(KrDialogStageTypeSettingsVirtual.DialogAlias);

            IMainCardAccessStrategy newCardAccessStrategy;
            Guid persistentCardID;
            if (storeMode == CardTaskDialogStoreMode.Card
                && (persistentCardID = GetAliasedDialogID(context, alias)) != Guid.Empty
                && await KrProcessHelper.CardExistsAsync(persistentCardID, this.DbScope, context.CancellationToken))
            {
                newCardAccessStrategy = new KrScopeMainCardAccessStrategy(persistentCardID, this.KrScope, context.ValidationResult);
            }
            else
            {
                newCardAccessStrategy = new ObviousMainCardAccessStrategy(
                    async (validationResult, ct) =>
                    {
                        var cardNewRequest = new CardNewRequest();

                        var typeID = settingsStorage.TryGet<Guid?>(KrDialogStageTypeSettingsVirtual.DialogTypeID);
                        if (typeID.HasValue)
                        {
                            var docType = (await this.typesCache.GetDocTypesAsync(ct)).FirstOrDefault(x => x.ID == typeID);
                            if (docType != default)
                            {
                                cardNewRequest.Info[Keys.DocTypeID] = typeID;
                                cardNewRequest.Info[Keys.DocTypeTitle] = docType.Caption;

                                typeID = docType.CardTypeID;
                            }
                        }
                        else
                        {
                            var templateID = settingsStorage.TryGet<Guid?>(KrDialogStageTypeSettingsVirtual.TemplateID);
                            if (templateID.HasValue)
                            {
                                typeID = await KrProcessHelper.GetTemplateCardTypeAsync(templateID.Value, this.DbScope, ct);
                                if (!typeID.HasValue)
                                {
                                    context.ValidationResult.AddError(
                                        this,
                                        LocalizationManager.Format(
                                            "$KrProcess_ErrorMessage_ErrorFormat2",
                                            KrErrorHelper.GetTraceTextFromStage(stage),
                                            "$KrStages_CreateCard_TemplateNotFound"));
                                    return default;
                                }
                            }
                            else
                            {
                                context.ValidationResult.AddError(
                                    this,
                                    LocalizationManager.Format(
                                        "$KrProcess_ErrorMessage_ErrorFormat2",
                                        KrErrorHelper.GetTraceTextFromStage(stage),
                                        "$KrStages_Dialog_TemplateAndTypeNotSpecified"));
                                return default;
                            }
                        }

                        cardNewRequest.CardTypeID = typeID;

                        var newResponse = await this.CardRepositoryDwt.NewAsync(cardNewRequest, ct);

                        if (validationResult != null)
                        {
                            var validationResultResponse = newResponse.TryGetValidationResult();
                            if (validationResultResponse != null)
                            {
                                validationResult.Add(validationResultResponse);
                            }
                        }

                        return newResponse.TryGetCard();
                    },
                    this.cardFileManager,
                    context.ValidationResult);
            }

            this.KrScope.AddDisposableObject(newCardAccessStrategy);
            context.Stage.InfoStorage[Keys.NewCard] = newCardAccessStrategy;
        }

        /// <inheritdoc />
        public override async Task<StageHandlerResult> HandleStageStartAsync(
            IStageTypeHandlerContext context)
        {
            StageHandlerResult result;
            switch (context.RunnerMode)
            {
                case KrProcessRunnerMode.Sync:
                    result = await this.StartSyncDialogAsync(context);
                    break;
                case KrProcessRunnerMode.Async:
                    result = await this.StartAsyncDialogAsync(context);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(context) + "." + nameof(context.RunnerMode), context.RunnerMode, default);
            }

            return result;
        }

        /// <inheritdoc />
        public override async Task<StageHandlerResult> HandleResurrectionAsync(
            IStageTypeHandlerContext context)
        {
            CardTaskDialogActionResult actionInfo;
            switch (context.CardExtensionContext)
            {
                case ICardStoreExtensionContext storeContext:
                    var card = storeContext.Request.Card;
                    actionInfo = CardTaskDialogHelper.GetCardTaskDialogAcionResult(card.Info);
                    break;
                case ICardRequestExtensionContext requestContext:
                    actionInfo = CardTaskDialogHelper.GetCardTaskDialogAcionResult(requestContext.Request);
                    break;
                default:
                    return StageHandlerResult.CompleteResult;
            }

            var dialogCardAccessStrategy = new ObviousMainCardAccessStrategy(actionInfo.DialogCard, this.cardFileManager, context.ValidationResult);
            this.KrScope.AddDisposableObject(dialogCardAccessStrategy);

            var scriptContext = new ScriptContext(
                dialogCardAccessStrategy,
                actionInfo.PressedButtonName,
                actionInfo.StoreMode)
            {
                Cancel = false,
                CompleteDialog = true,
            };
            var inst = await HandlerHelper.CreateScriptInstanceAsync(
                this.CompilationCache,
                context.Stage.ID,
                context.ValidationResult,
                context.CancellationToken);
            await HandlerHelper.InitScriptContextAsync(this.UnityContainer, inst, context);
            await inst.InvokeExtraAsync(MethodName, scriptContext);

            if (!context.ValidationResult.IsSuccessful())
            {
                return StageHandlerResult.EmptyResult;
            }

            if (scriptContext.Cancel)
            {
                ValidationSequence
                    .Begin(context.ValidationResult)
                    .Error(DefaultValidationKeys.CancelDialog)
                    .End();
                return StageHandlerResult.CancelProcessResult;
            }

            await UserAPIHelper.PrepareFileInDialogCardForStoreAsync(
                this.DbScope,
                this.cardRepository,
                actionInfo,
                context.MainCardAccessStrategy,
                dialogCardAccessStrategy,
                context.ValidationResult,
                context.CancellationToken);

            return StageHandlerResult.CompleteResult;

        }

        /// <inheritdoc />
        public override async Task<StageHandlerResult> HandleTaskCompletionAsync(
            IStageTypeHandlerContext context)
        {
            var task = context.TaskInfo.Task;
            if (task.OptionID == DefaultCompletionOptions.Complete)
            {
                return StageHandlerResult.CompleteResult;
            }

            if (task.OptionID != DefaultCompletionOptions.ShowDialog)
            {
                throw new InvalidOperationException();
            }

            var actionInfo = CardTaskDialogHelper.GetCardTaskDialogAcionResult(task);
            var coInfo = CardTaskDialogHelper.GetCompletionOptionSettings(task, DefaultCompletionOptions.ShowDialog);

            if (!string.IsNullOrEmpty(coInfo.DialogAlias)
                && coInfo.StoreMode == CardTaskDialogStoreMode.Card
                && coInfo.PersistentDialogCardID != default)
            {
                AddAliasedDialog(context, coInfo.DialogAlias, coInfo.PersistentDialogCardID);
            }

            var dialogCardAccessStrategy = this.GetCard(coInfo, actionInfo, context);
            this.KrScope.AddDisposableObject(dialogCardAccessStrategy);

            Card updatedSettingsCard = null;
            if (actionInfo.StoreMode == CardTaskDialogStoreMode.Settings)
            {
                var savingScriptContext = new SavingScriptContext(
                    dialogCardAccessStrategy,
                    actionInfo.PressedButtonName,
                    actionInfo.StoreMode);
                var saving = await HandlerHelper.CreateScriptInstanceAsync(
                    this.CompilationCache,
                    context.Stage.ID,
                    context.ValidationResult,
                    context.CancellationToken);
                await HandlerHelper.InitScriptContextAsync(this.UnityContainer, saving, context);
                await saving.InvokeExtraAsync(SavingMethodName, savingScriptContext);
                if (!context.ValidationResult.IsSuccessful())
                {
                    return StageHandlerResult.EmptyResult;
                }

                if (dialogCardAccessStrategy.WasUsed)
                {
                    updatedSettingsCard = await dialogCardAccessStrategy.GetCardAsync(cancellationToken: context.CancellationToken);
                }
            }

            var scriptContext = new ScriptContext(
                dialogCardAccessStrategy,
                actionInfo.PressedButtonName,
                actionInfo.StoreMode)
            {
                Cancel = false,
                CompleteDialog = actionInfo.CompleteDialog,
            };
            var inst = await HandlerHelper.CreateScriptInstanceAsync(
                this.CompilationCache,
                context.Stage.ID,
                context.ValidationResult,
                context.CancellationToken);
            await HandlerHelper.InitScriptContextAsync(this.UnityContainer, inst, context);
            await inst.InvokeExtraAsync(MethodName, scriptContext);

            if (!context.ValidationResult.IsSuccessful())
            {
                return StageHandlerResult.EmptyResult;
            }

            if (scriptContext.Cancel)
            {
                ValidationSequence
                    .Begin(context.ValidationResult)
                    .Error(DefaultValidationKeys.CancelDialog)
                    .End();
                return StageHandlerResult.InProgressResult;
            }

            await UserAPIHelper.PrepareFileInDialogCardForStoreAsync(
                this.DbScope,
                this.cardRepository,
                actionInfo,
                context.MainCardAccessStrategy,
                dialogCardAccessStrategy,
                context.ValidationResult,
                context.CancellationToken);

            if (!context.ValidationResult.IsSuccessful())
            {
                return StageHandlerResult.EmptyResult;
            }

            if (this.KrScope.Exists
                && context.MainCardID != null)
            {
                if (scriptContext.CompleteDialog)
                {
                    var taskCopy = new CardTask(StorageHelper.Clone(task.GetStorage()));
                    taskCopy.RemoveChanges();
                    taskCopy.Action = CardTaskAction.Complete;
                    taskCopy.State = CardRowState.Deleted;
                    taskCopy.OptionID = DefaultCompletionOptions.Complete;
                    
                    var co = CardTaskDialogHelper.GetCompletionOptionSettings(taskCopy, DefaultCompletionOptions.ShowDialog);
                    CardTaskDialogButtonInfo pressedButton;
                    if (!string.IsNullOrEmpty(actionInfo.PressedButtonName)
                        && (pressedButton = co.Buttons.FirstOrDefault(
                            i => string.Equals(i.Name, actionInfo.PressedButtonName, StringComparison.Ordinal))) != null
                        && !string.IsNullOrEmpty(pressedButton.Caption))
                    {
                        taskCopy.Result = pressedButton.Caption;
                    }
                    
                    if (updatedSettingsCard != null)
                    {
                        updatedSettingsCard.RemoveChanges();
                        co.DialogCard = updatedSettingsCard;
                    }

                    (await this.KrScope.GetMainCardAsync(context.MainCardID.Value, cancellationToken: context.CancellationToken)).Tasks.Add(taskCopy);
                }
                else if (updatedSettingsCard != null)
                {
                    var taskCopy = new CardTask(StorageHelper.Clone(task.GetStorage()));
                    taskCopy.RemoveChanges();
                    taskCopy.Action = CardTaskAction.None;
                    taskCopy.OptionID = null;
                    taskCopy.State = CardRowState.Modified;
                    taskCopy.Flags |= CardTaskFlags.HistoryItemCreated;
                    var co = CardTaskDialogHelper.GetCompletionOptionSettings(taskCopy, DefaultCompletionOptions.ShowDialog);
                    updatedSettingsCard.RemoveChanges();
                    co.DialogCard = updatedSettingsCard;
                    (await this.KrScope.GetMainCardAsync(context.MainCardID.Value, cancellationToken: context.CancellationToken)).Tasks.Add(taskCopy);
                }
            }

            return StageHandlerResult.InProgressResult;
        }

        /// <inheritdoc />
        public override async Task<StageHandlerResult> HandleSignalAsync(
            IStageTypeHandlerContext context)
        {
            var signal = context.SignalInfo;
            var actionInfo = CardTaskDialogHelper.GetCardTaskDialogAcionResult(signal.Signal.Parameters);

            var dialogCardAccessStrategy = new ObviousMainCardAccessStrategy(actionInfo.DialogCard, this.cardFileManager, context.ValidationResult);
            this.KrScope.AddDisposableObject(dialogCardAccessStrategy);

            var scriptContext = new SavingScriptContext(
                dialogCardAccessStrategy,
                actionInfo.PressedButtonName,
                actionInfo.StoreMode);
            var inst = await HandlerHelper.CreateScriptInstanceAsync(
                this.CompilationCache,
                context.Stage.ID,
                context.ValidationResult,
                context.CancellationToken);
            await HandlerHelper.InitScriptContextAsync(this.UnityContainer, inst, context);
            await inst.InvokeExtraAsync(SavingMethodName, scriptContext);

            if (!context.ValidationResult.IsSuccessful())
            {
                return StageHandlerResult.EmptyResult;
            }

            if (dialogCardAccessStrategy.WasUsed)
            {
                context.CardExtensionContext.Info[ChangedCardKey] = (await dialogCardAccessStrategy.GetCardAsync(cancellationToken: context.CancellationToken)).GetStorage();

                if (dialogCardAccessStrategy.WasFileContainerUsed
                    && actionInfo.StoreMode == CardTaskDialogStoreMode.Card)
                {
                    context.CardExtensionContext.Info[ChangedCardFileContainerKey] =
                        (await dialogCardAccessStrategy.GetFileContainerAsync(cancellationToken: context.CancellationToken)).FileContainer;
                }
            }

            await UserAPIHelper.PrepareFileInDialogCardForStoreAsync(
                this.DbScope,
                this.cardRepository,
                actionInfo,
                context.MainCardAccessStrategy,
                dialogCardAccessStrategy,
                context.ValidationResult,
                context.CancellationToken);
            
            return StageHandlerResult.InProgressResult;
        }

        /// <inheritdoc />
        public override Task<bool> HandleStageInterruptAsync(IStageTypeHandlerContext context) =>
            this.TasksRevoker.RevokeAllStageTasksAsync(new StageTaskRevokerContext(context, context.CancellationToken));

        /// <inheritdoc />
        public override async Task AfterPostprocessingAsync(IStageTypeHandlerContext context)
        {
            await base.AfterPostprocessingAsync(context);

            if (context.Stage.InfoStorage.TryGetValue(Keys.NewCard, out var newCardObj))
            {
                context.Stage.InfoStorage.Remove(Keys.NewCard);
                if (newCardObj is IMainCardAccessStrategy cardAccessStrategy)
                {
                    await cardAccessStrategy.DisposeAsync();
                }
            }
        }

        #endregion

        #region protected

        /// <summary>
        /// Асинхронно выполняет запуск диалога в синхронном режиме.
        /// </summary>
        /// <param name="context">Контекст обработчика этапа.</param>
        /// <returns>Результат выполнения этапа.</returns>
        protected async Task<StageHandlerResult> StartSyncDialogAsync(
            IStageTypeHandlerContext context)
        {
            var stage = context.Stage;
            var storeMode = (CardTaskDialogStoreMode)stage
                .SettingsStorage
                .TryGet<int>(KrDialogStageTypeSettingsVirtual.CardStoreModeID);
            if (storeMode != CardTaskDialogStoreMode.Info)
            {
                context.ValidationResult.AddError(
                    this,
                    LocalizationManager.Format(
                        "$KrProcess_ErrorMessage_ErrorFormat2",
                        KrErrorHelper.GetTraceTextFromStage(stage),
                        "$KrStages_Dialog_StartingSyncDialogWithNotInfoStoreMode"));
                return StageHandlerResult.EmptyResult;
            }

            var coSettings = await this.CreateCompletionOptionSettingsAsync(context);
            if (coSettings is null)
            {
                return StageHandlerResult.EmptyResult;
            }

            var cardID = context.MainCardID ?? Guid.Empty;
            var processID = context.SecondaryProcess.ID;

            var serializedProcess = KrProcessHelper.SerializeWorkflowProcess(context.WorkflowProcess);
            var signature = KrProcessHelper.SignWorkflowProcess(serializedProcess, cardID, processID, this.SignatureProvider);

            var processInstance = new KrProcessInstance(
                context.SecondaryProcess.ID,
                context.MainCardID,
                serializedProcess,
                signature);

            await this.PrepareNewCardInSettinsFromStageInfoAsync(stage, coSettings, context.CancellationToken);

            this.KrScope.TryAddClientCommand(
                new KrProcessClientCommand(
                    DefaultCommandTypes.ShowAdvancedDialog,
                    new Dictionary<string, object>
                    {
                        [Keys.ProcessInstance] = processInstance.GetStorage(),
                        [Keys.CompletionOptionSettings] = coSettings.GetStorage(),
                    }));

            return StageHandlerResult.CancelProcessResult;
        }

        /// <summary>
        /// Асинхронно выполняет запуск диалога в асинхронном режиме.
        /// </summary>
        /// <param name="context">Контекст обработчика этапа.</param>
        /// <returns>Результат выполнения этапа.</returns>
        protected async Task<StageHandlerResult> StartAsyncDialogAsync(
            IStageTypeHandlerContext context)
        {
            var performer = context.Stage.Performer;
            var api = context.WorkflowAPI;

            var coSettings = await this.CreateCompletionOptionSettingsAsync(context);
            if (coSettings is null)
            {
                return StageHandlerResult.EmptyResult;
            }

            await this.PrepareNewCardInSettinsFromStageInfoAsync(context.Stage, coSettings, context.CancellationToken);

            var taskGroupRowID = await HandlerHelper.GetTaskHistoryGroupAsync(context, this.KrScope);

            var (kindID, kindCaption) = HandlerHelper.GetTaskKind(context);
            await api.SendTaskAsync(
                DefaultTaskTypes.KrShowDialogTypeID,
                context.Stage.SettingsStorage.TryGet<string>(KrDialogStageTypeSettingsVirtual.TaskDigest),
                performer.PerformerID,
                performer.PerformerName,
                modifyTaskAction: (t, ct) =>
                {
                    t.GroupRowID = taskGroupRowID;
                    t.Planned = context.Stage.Planned;
                    t.PlannedQuants = context.Stage.PlannedQuants;
                    t.Flags |= CardTaskFlags.CreateHistoryItem;
                    HandlerHelper.SetTaskKind(t, kindID, kindCaption, context);
                    CardTaskDialogHelper.SetCompletionOptionSettings(t, coSettings);

                    return new ValueTask();
                },
                cancellationToken: context.CancellationToken);

            return StageHandlerResult.InProgressResult;
        }

        protected IMainCardAccessStrategy GetCard(
            CardTaskCompletionOptionSettings coInfo,
            CardTaskDialogActionResult actionInfo,
            IStageTypeHandlerContext context)
        {
            switch (coInfo.StoreMode)
            {
                case CardTaskDialogStoreMode.Info:
                    return  new ObviousMainCardAccessStrategy(actionInfo.DialogCard, this.cardFileManager, context.ValidationResult);
                case CardTaskDialogStoreMode.Settings:
                    return  new ObviousMainCardAccessStrategy(coInfo.DialogCard, this.cardFileManager, context.ValidationResult);
                case CardTaskDialogStoreMode.Card:
                    return new KrScopeMainCardAccessStrategy(coInfo.PersistentDialogCardID, this.KrScope, context.ValidationResult);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Асинхронно создаёт объект <see cref="CardTaskCompletionOptionSettings"/> представляющий параметры диалога.
        /// </summary>
        /// <param name="context">Контекст обработчика этапа.</param>
        /// <returns>Параметры диалога.</returns>
        protected async ValueTask<CardTaskCompletionOptionSettings> CreateCompletionOptionSettingsAsync(IStageTypeHandlerContext context)
        {
            var stage = context.Stage;
            var settingsStorage = stage.SettingsStorage;

            var storeModeInt = settingsStorage.TryGet<int?>(KrDialogStageTypeSettingsVirtual.CardStoreModeID);
            if (!storeModeInt.HasValue)
            {
                context.ValidationResult.AddError(
                    this,
                    LocalizationManager.Format(
                        "$KrProcess_ErrorMessage_ErrorFormat2", 
                        KrErrorHelper.GetTraceTextFromStage(stage),
                        "$KrStages_Dialog_CardStoreModeNotSpecified"));
            }

            var openModeInt = settingsStorage.TryGet<int?>(KrDialogStageTypeSettingsVirtual.OpenModeID);
            if (!openModeInt.HasValue)
            {
                context.ValidationResult.AddError(
                    this,
                    LocalizationManager.Format(
                        "$KrProcess_ErrorMessage_ErrorFormat2",
                        KrErrorHelper.GetTraceTextFromStage(stage),
                        "$KrStages_Dialog_CardOpenModeNotSpecified"));
            }

            var dialogTypeID = settingsStorage.TryGet<Guid?>(KrDialogStageTypeSettingsVirtual.DialogTypeID);
            Guid? templateID = default;
            CardTaskDialogNewMethod сardNewMethod = CardTaskDialogNewMethod.Default;
            if (!dialogTypeID.HasValue)
            {
                templateID = settingsStorage.TryGet<Guid?>(KrDialogStageTypeSettingsVirtual.TemplateID);
                if (templateID.HasValue)
                {
                    dialogTypeID = templateID;
                    сardNewMethod = CardTaskDialogNewMethod.Template;
                }
                else
                {
                    context.ValidationResult.AddError(
                        this,
                        LocalizationManager.Format(
                            "$KrProcess_ErrorMessage_ErrorFormat2",
                            KrErrorHelper.GetTraceTextFromStage(stage),
                            "$KrStages_Dialog_TemplateAndTypeNotSpecified"));
                }
            }

            if (!context.ValidationResult.IsSuccessful())
            {
                return default;
            }

            var ctcBuilder = this.ctcBuilderFactory();

            var buttonsSettings = settingsStorage.TryGet<IList<object>>(KrDialogButtonSettingsVirtual.Synthetic);
            if (buttonsSettings != null)
            {
                foreach (var buttonStorage in buttonsSettings.Cast<Dictionary<string, object>>())
                {
                    var button = new CardTaskDialogButtonInfo
                    {
                        Name = buttonStorage.TryGet<string>(KrDialogButtonSettingsVirtual.Name),
                        CardButtonType = (CardButtonType)buttonStorage.TryGet<int>(KrDialogButtonSettingsVirtual.TypeID),
                        Caption = buttonStorage.TryGet<string>(KrDialogButtonSettingsVirtual.Caption),
                        Icon = buttonStorage.TryGet<string>(KrDialogButtonSettingsVirtual.Icon),
                        Cancel = buttonStorage.TryGet<bool>(KrDialogButtonSettingsVirtual.Cancel),
                        Order = buttonStorage.TryGet<int>(Order),
                    };
                    ctcBuilder.AddButton(button);
                }
            }

            var coSettings = await ctcBuilder
                .SetCompletionOption(DefaultCompletionOptions.ShowDialog)
                .SetDialogType(dialogTypeID.Value)
                .SetTaskButtonCaption(settingsStorage.TryGet<string>(KrDialogStageTypeSettingsVirtual.ButtonName))
                .SetDialogName(settingsStorage.TryGet<string>(KrDialogStageTypeSettingsVirtual.DialogName))
                .SetDialogAlias(settingsStorage.TryGet<string>(KrDialogStageTypeSettingsVirtual.DialogAlias))
                .SetDialogCaption(settingsStorage.TryGet<string>(KrDialogStageTypeSettingsVirtual.DisplayValue))
                .SetStoreMode((CardTaskDialogStoreMode)storeModeInt)
                .SetOpenMode((CardTaskDialogOpenMode)openModeInt)
                .SetKeepFiles(settingsStorage.TryGet<bool>(KrDialogStageTypeSettingsVirtual.KeepFiles))
                .SetCardNewMethod(сardNewMethod)
                .BuildAsync(context.ValidationResult, context.CancellationToken);

            if (!string.IsNullOrEmpty(coSettings.DialogAlias)
                && coSettings.StoreMode == CardTaskDialogStoreMode.Card)
            {
                var persistentCardID = GetAliasedDialogID(context, coSettings.DialogAlias);
                coSettings.PersistentDialogCardID = persistentCardID;
            }

            return coSettings;
        }

        protected static void AddAliasedDialog(
            IStageTypeHandlerContext context,
            string alias,
            Guid dialogCardID)
        {
            var processInfo = context.WorkflowProcess.InfoStorage;

            if (!processInfo.TryGetValue(DialogsProcessInfoKey, out var dialogsStorageObj)
                || !(dialogsStorageObj is IDictionary<string, object> dialogsStorage))
            {
                dialogsStorage = new Dictionary<string, object>();
                processInfo[DialogsProcessInfoKey] = dialogsStorage;
            }

            dialogsStorage[alias] = dialogCardID.ToString("N");
        }

        protected static Guid GetAliasedDialogID(
            IStageTypeHandlerContext context,
            string alias)
        {
            var processInfo = context.WorkflowProcess.InfoStorage;

            if (processInfo.TryGetValue(DialogsProcessInfoKey, out var dialogsStorageObj)
                && dialogsStorageObj is IDictionary<string, object> dialogsStorage
                && dialogsStorage.TryGetValue(alias, out var cardIDObj)
                && cardIDObj is string cardIDStr
                && Guid.TryParse(cardIDStr, out var cardID))
            {
                return cardID;
            }

            return default;
        }

        #endregion

        #region Private

        /// <summary>
        /// Инициализирует параметры диалога информацией о подготовленной карточке хранящейся в <see cref="Stage.InfoStorage"/> этапа по ключу <see cref="Keys.NewCard"/>.
        /// </summary>
        /// <param name="stage">Этап из которого загружается информация по подготовленной карточке.</param>
        /// <param name="coSettings">Параметры диалога.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns>Асинхронная задача.</returns>
        private async Task PrepareNewCardInSettinsFromStageInfoAsync(
            Stage stage,
            CardTaskCompletionOptionSettings coSettings,
            CancellationToken cancellationToken = default)
        {
            if (stage.InfoStorage.TryGetValue(Keys.NewCard, out var newCardObj))
            {
                stage.InfoStorage.Remove(Keys.NewCard);
                if (newCardObj is IMainCardAccessStrategy cardAccessStrategy
                    && cardAccessStrategy.WasUsed)
                {
                    await using (cardAccessStrategy)
                    {
                        var card = await cardAccessStrategy.GetCardAsync(cancellationToken: cancellationToken);
                        var temporaryFiles = card.TryGetFiles()?.Clone();
                        card.RemoveAllButChanged();
                        card.Files = temporaryFiles;
                        var cardBytes = card.ToSerializable().Serialize();
                        var cardSignature = this.SignatureProvider.Sign(cardBytes);
                        coSettings.PreparedNewCard = cardBytes;
                        coSettings.PreparedNewCardSignature = cardSignature;
                    }
                }
            }
        }

        #endregion

    }
}