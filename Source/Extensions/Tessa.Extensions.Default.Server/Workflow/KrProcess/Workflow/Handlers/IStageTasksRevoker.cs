﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tessa.Cards;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.Handlers
{
    public interface IStageTasksRevoker
    {
        /// <summary>
        /// Завершить все задания этапа.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        Task<bool> RevokeAllStageTasksAsync(IStageTasksRevokerContext context);

        /// <summary>
        /// Завершить задание <see cref="IStageTasksRevokerContext.TaskID"/>
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        Task<bool> RevokeTaskAsync(IStageTasksRevokerContext context);

        /// <summary>
        /// Завершить все задания из <see cref="IStageTasksRevokerContext.TaskIDs"/>
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        Task<int> RevokeTasksAsync(IStageTasksRevokerContext context);
    }
}