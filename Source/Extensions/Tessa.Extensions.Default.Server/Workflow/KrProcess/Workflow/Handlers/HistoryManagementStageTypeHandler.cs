﻿using System;
using System.Threading.Tasks;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Scope;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.Handlers
{
    public class HistoryManagementStageTypeHandler : StageTypeHandlerBase
    {
        #region Constructors

        public HistoryManagementStageTypeHandler(IKrScope krScope)
        {
            this.KrScope = krScope;
        }

        #endregion

        #region Protected Properties

        protected IKrScope KrScope { get; set; }

        #endregion

        #region Base Overrides

        public override async Task<StageHandlerResult> HandleStageStartAsync(IStageTypeHandlerContext context)
        {
            if (!context.MainCardID.HasValue)
            {
                context.ValidationResult.AddError(this, "$KrStages_HistoryManagement_GlobalContextError");
                return StageHandlerResult.SkipResult;
            }

            var resolver = context.TaskHistoryResolver;
            var taskHistoryGroupID =
                context.Stage.SettingsStorage.TryGet<Guid?>(
                    KrConstants.KrHistoryManagementStageSettingsVirtual.TaskHistoryGroupTypeID);
            var parentTaskHistoryGroupID =
                context.Stage.SettingsStorage.TryGet<Guid?>(
                    KrConstants.KrHistoryManagementStageSettingsVirtual.ParentTaskHistoryGroupTypeID);
            var newIteration =
                context.Stage.SettingsStorage.TryGet<bool?>(
                    KrConstants.KrHistoryManagementStageSettingsVirtual.NewIteration);
            if (taskHistoryGroupID.HasValue)
            {
                var group = await resolver.ResolveTaskHistoryGroupAsync(taskHistoryGroupID.Value, parentTaskHistoryGroupID, newIteration == true, cancellationToken: context.CancellationToken);
                await this.KrScope.SetCurrentHistoryGroupAsync(context.MainCardID.Value, group.RowID, cancellationToken: context.CancellationToken);
            }
            else
            {
                await this.KrScope.SetCurrentHistoryGroupAsync(context.MainCardID.Value, null, cancellationToken: context.CancellationToken);
            }

            return StageHandlerResult.CompleteResult;
        }

        #endregion
    }
}