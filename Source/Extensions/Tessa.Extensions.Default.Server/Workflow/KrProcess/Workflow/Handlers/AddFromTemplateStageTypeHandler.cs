﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Scope;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Files;
using Tessa.Platform.Data;
using Tessa.Platform.IO;
using Tessa.Platform.Placeholders;
using Tessa.Platform.Runtime;
using Tessa.Platform.Storage;
using Unity;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.Handlers
{
    public class AddFromTemplateStageTypeHandler : StageTypeHandlerBase
    {
        protected readonly ICardStreamServerRepository CardStreamRepository;

        protected readonly IPlaceholderManager PlaceholderManager;

        protected readonly IDbScope DbScope;

        protected readonly IUnityContainer UnityContainer;

        protected readonly ISession Session;

        protected readonly IKrScope KrScope;

        public AddFromTemplateStageTypeHandler(
            ICardStreamServerRepository cardStreamRepository,
            IPlaceholderManager placeholderManager,
            IDbScope dbScope,
            ISession session,
            IUnityContainer unityContainer,
            IKrScope krScope)
        {
            this.CardStreamRepository = cardStreamRepository;
            this.PlaceholderManager = placeholderManager;
            this.DbScope = dbScope;
            this.Session = session;
            this.UnityContainer = unityContainer;
            this.KrScope = krScope;
        }

        /// <inheritdoc />
        public override async Task<StageHandlerResult> HandleStageStartAsync(
            IStageTypeHandlerContext context)
        {
            var templateID = context.Stage.SettingsStorage.TryGet<Guid?>(KrConstants.KrAddFromTemplateSettingsVirtual.FileTemplateID);
            var fileName = context.Stage.SettingsStorage.TryGet<string>(KrConstants.KrAddFromTemplateSettingsVirtual.Name);

            if (templateID.HasValue)
            {
                var result = await this.CardStreamRepository.GenerateFileFromTemplateAsync(
                    templateID.Value,
                    context.MainCardID,
                    info: new Dictionary<string, object>(StringComparer.Ordinal)
                    {
                        [PlaceholderHelper.CardFuncAsyncKey] =
                            new Func<IPlaceholderContext, ValueTask<Card>>(ctx => context.MainCardAccessStrategy.GetCardAsync(withoutTransaction: true, cancellationToken: context.CancellationToken)),
                    },
                    cancellationToken: context.CancellationToken);

                context.ValidationResult.Add(result.Response.ValidationResult);

                if (result.HasContent)
                {
                    var fileContainer = await context.MainCardAccessStrategy.GetFileContainerAsync(cancellationToken: context.CancellationToken);
                    await using var s = await result.GetContentOrThrowAsync(context.CancellationToken);
                    var data = await s.ReadAllBytesAsync(context.CancellationToken);

                    await fileContainer
                        .FileContainer
                        .BuildFile(await this.GetFileNameAsync(context, result.Response.TryGetSuggestedFileName(), fileName))
                        .SetContent(data)
                        .AddWithNotificationAsync(cancellationToken: context.CancellationToken);
                }
            }

            return StageHandlerResult.CompleteResult;
        }

        #region protected

        protected async Task<string> GetFileNameAsync(
            IStageTypeHandlerContext context,
            string suggestedName,
            string fileNameTemplate)
        {
            if (string.IsNullOrWhiteSpace(fileNameTemplate))
            {
                return suggestedName;
            }

            var extension = Path.GetExtension(suggestedName);

            return await this.ExtendFileNameAsync(context, fileNameTemplate) + extension;
        }

        protected async Task<string> ExtendFileNameAsync(IStageTypeHandlerContext context, string fileNameTemplate) =>
            await this.PlaceholderManager.ReplaceTextAsync(
                fileNameTemplate,
                this.Session,
                this.UnityContainer,
                this.DbScope,
                null,
                await context.MainCardAccessStrategy.GetCardAsync(cancellationToken: context.CancellationToken),
                info: CreatePlaceholderInfo(context),
                cancellationToken: context.CancellationToken);

        protected static Dictionary<string, object> CreatePlaceholderInfo(IStageTypeHandlerContext context) =>
            new Dictionary<string, object>(StringComparer.Ordinal)
            {
                [PlaceholderHelper.TaskKey] = context.TaskInfo?.Task,
                ["WorkflowProcess"] = context.WorkflowProcess,
                ["Stage"] = context.Stage,
            };

        #endregion

    }
}