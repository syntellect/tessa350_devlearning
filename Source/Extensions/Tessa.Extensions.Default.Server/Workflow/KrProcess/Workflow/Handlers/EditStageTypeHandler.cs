﻿using System;
using System.Threading.Tasks;
using Tessa.BusinessCalendar;
using Tessa.Cards;
using Tessa.Cards.Caching;
using Tessa.Cards.Extensions;
using Tessa.Extensions.Default.Server.Workflow.KrObjectModel;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Scope;
using Tessa.Extensions.Default.Shared;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Notices;
using Tessa.Platform;
using Tessa.Platform.Runtime;
using Tessa.Platform.Storage;
using Tessa.Roles;
using Unity;
using static Tessa.Extensions.Default.Shared.Workflow.KrProcess.KrConstants;
using NotificationHelper = Tessa.Extensions.Default.Shared.Notices.NotificationHelper;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.Handlers
{
    public class EditStageTypeHandler : StageTypeHandlerBase
    {
        #region Constants

        /// <summary>
        /// Имя ключа по которому в <see cref="Stage.InfoStorage"/> содержится идентификатор этапа на который необходимо выполнить переход после завершения этапа "Доработка". Используется при возврате на этап согласование или подписание после доработки автором. Тип значения: <see cref="Guid"/>.
        /// </summary>
        public const string ReturnToStage = nameof(ReturnToStage);

        #endregion

        #region Constructors

        public EditStageTypeHandler(
            IKrScope krScope,
            IBusinessCalendarService calendarService,
            ISession session,
            IRoleRepository roleRepository,
            IStageTasksRevoker tasksRevoker,
            [Dependency(NotificationManagerNames.DeferredWithoutTransaction)] INotificationManager notificationManager,
            ICardCache cardCache)
        {
            this.KrScope = krScope ?? throw new ArgumentNullException(nameof(krScope));
            this.CalendarService = calendarService;
            this.Session = session;
            this.RoleRepository = roleRepository;
            this.TasksRevoker = tasksRevoker;
            this.NotificationManager = notificationManager;
            this.CardCache = cardCache;
        }

        #endregion

        #region Protected Properties

        protected IKrScope KrScope { get; }
        protected IBusinessCalendarService CalendarService { get; }
        protected ISession Session { get; }
        protected IRoleRepository RoleRepository { get; }
        protected IStageTasksRevoker TasksRevoker { get; }
        protected INotificationManager NotificationManager { get; }
        protected ICardCache CardCache { get; }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Начинает новый цикл согласования.
        /// </summary>
        /// <param name="context">Контекст обработчика этапа.</param>
        /// <param name="result">Результат выполнения или значение <see langword="null"/>, если необходимо завершить этап, если не выполняется обработка доработки автором, то в этом случае игнорируется. Если не задан, то этап завершается с результатом выполнения <see cref="StageHandlerResult.CompleteResult"/>.</param>
        /// <returns>Результат выполнения этапа.</returns>
        protected virtual StageHandlerResult StartApproval(IStageTypeHandlerContext context, StageHandlerResult? result = default)
        {
            var returnToStage = context.Stage.InfoStorage.TryGet<Guid?>(ReturnToStage);
            if (returnToStage.HasValue)
            {
                context.Stage.InfoStorage.Remove(ReturnToStage);
                return StageHandlerResult.Transition(returnToStage.Value, keepStageStates: true);
            }

            var fields = context.ContextualSatellite.Sections[KrApprovalCommonInfo.Name].Fields;
            fields[KrApprovalCommonInfo.ApprovedBy] = string.Empty;
            fields[KrApprovalCommonInfo.DisapprovedBy] = string.Empty;

            return result ?? StageHandlerResult.CompleteResult;
        }

        /// <summary>
        /// Увеличивает номер цикла согласования, если это разрешено настроками этапа.
        /// </summary>
        /// <param name="context">Контекст обработчика этапа.</param>
        protected virtual void TryIncrementCycle(IStageTypeHandlerContext context)
        {
            if (context.Stage.SettingsStorage.TryGet<bool?>(KrEditSettingsVirtual.IncrementCycle) == true)
            {
                var info = context.WorkflowProcess.InfoStorage;
                info[Keys.Cycle] = Int32Boxes.Box(info.TryGet<int>(Keys.Cycle) + 1);
            }
        }

        /// <summary>
        /// Возвращает значение, показывающее, выполнялся ли предыдущий этап из другой группы этапов или карточки.
        /// </summary>
        /// <param name="stage">Текущий этап.</param>
        /// <param name="cardID">Идентификатор карточки в рамках которой выполняется этап <paramref name="cardID"/>.</param>
        /// <returns>Значение <see langword="true"/>, если отсутствует информация о ходе выполнения процесса или, если есть этап выполнявшийся в другой группе или карточке или такой не найден, иначе - <see langword="false"/>, если найден не скрытый предыдущий этап выполнявшийся в группе, что и текущий этап и текущей карточке.</returns>
        protected virtual bool TransitFromDifferentGroup(Stage stage, Guid? cardID)
        {
            var trace = this.KrScope.GetKrProcessRunnerTrace();

            if (trace is null
                || trace.Count == 0)
            {
                return true;
            }

            for (var i = trace.Count - 1; 0 <= i; i--)
            {
                var traceItem = trace[i];
                var prevStage = traceItem.Stage;
                if (prevStage.StageGroupID != stage.StageGroupID
                    || traceItem.CardID != cardID)
                {
                    return true;
                }
                if (!prevStage.Hidden)
                {
                    return false;
                }
            }

            return true;
        }

        #endregion

        #region Base Overrides
        
        /// <inheritdoc />
        public override Task BeforeInitializationAsync(IStageTypeHandlerContext context)
        {
            HandlerHelper.ClearCompletedTasks(context.Stage);
            return Task.CompletedTask;
        }

        /// <inheritdoc/>
        public override async Task<StageHandlerResult> HandleStageStartAsync(IStageTypeHandlerContext context)
        {
            var stage = context.Stage;
            var settings = stage.SettingsStorage;
            var returnToStage = stage.InfoStorage.ContainsKey(ReturnToStage);
            if (!returnToStage)
            {
                this.TryIncrementCycle(context);
            }

            if (settings.TryGet<bool?>(KrEditSettingsVirtual.DoNotSkipStage) != true
                && this.TransitFromDifferentGroup(stage, context.MainCardID))
            {
                SetVisibility(false);
                return this.StartApproval(context, StageHandlerResult.SkipResult);
            }

            SetVisibility(true);

            var author = await HandlerHelper.GetStageAuthorAsync(context, this.RoleRepository, this.Session);
            if (author is null)
            {
                return StageHandlerResult.EmptyResult;
            }
            var authorID = author.AuthorID;
            
            var digest = settings.TryGet<string>(KrEditSettingsVirtual.Comment);
            var groupID = await HandlerHelper.GetTaskHistoryGroupAsync(context, this.KrScope);
            var incrementCycle = settings.TryGet<bool?>(KrEditSettingsVirtual.IncrementCycle) == true;

            var sentTask = (await context.WorkflowAPI.SendTaskAsync(
                returnToStage || !incrementCycle
                    ? DefaultTaskTypes.KrEditInterjectTypeID
                    : DefaultTaskTypes.KrEditTypeID,
                string.Empty,
                settings.Get<Guid>(KrSinglePerformerVirtual.PerformerID),
                settings.Get<string>(KrSinglePerformerVirtual.PerformerName),
                modifyTaskAction: (p, ct) =>
                {
                    p.AuthorID = authorID;
                    p.AuthorName = null;    // AuthorName и AuthorPosition определяются системой, когда явно указано null
                    p.Planned = stage.Planned;
                    p.PlannedQuants = stage.PlannedQuants;
                    p.GroupRowID = groupID;
                    if (!string.IsNullOrWhiteSpace(digest))
                    {
                        p.Digest = digest;
                    }
                    var (kindID, kindCaption) = HandlerHelper.GetTaskKind(context);
                    HandlerHelper.SetTaskKind(p, kindID, kindCaption, context);

                    return new ValueTask();
                },
                cancellationToken: context.CancellationToken)).Task;
            sentTask.Flags |= CardTaskFlags.CreateHistoryItem;
            context.ContextualSatellite.AddToHistory(sentTask.RowID, context.WorkflowProcess.InfoStorage.TryGet(Keys.Cycle, 1));
            await context.WorkflowAPI.AddActiveTaskAsync(sentTask.RowID, context.CancellationToken);

            if (context.CardExtensionContext is ICardStoreExtensionContext)
            {
                context.ValidationResult.Add(
                    await this.NotificationManager.SendAsync(
                        DefaultNotifications.TaskNotification,
                        new[] { sentTask.RoleID },
                        new NotificationSendContext()
                        {
                            MainCardID = context.MainCardID ?? Guid.Empty,
                            Info = NotificationHelper.GetInfoWithTask(sentTask),
                            ModifyEmailActionAsync = async (email, ct) =>
                            {
                                NotificationHelper.ModifyEmailForMobileApprovers(
                                    email,
                                    sentTask,
                                    await NotificationHelper.GetMobileApprovalEmailAsync(this.CardCache, ct));

                                NotificationHelper.ModifyTaskCaption(
                                    email,
                                    sentTask);
                            },
                            GetCardFuncAsync = (ct) => context.MainCardAccessStrategy.GetCardAsync(cancellationToken: ct),
                        },
                        context.CancellationToken));
            }

            if (settings.TryGet<bool>(KrEditSettingsVirtual.ChangeState)
                && !returnToStage
                && !this.KrScope.Info.TryGet<bool>(Keys.IgnoreChangeState))
            {
                context.WorkflowProcess.State = KrState.Editing;
            }

            return StageHandlerResult.InProgressResult;

            void SetVisibility(
                bool visible)
            {
                if (settings.TryGet<bool?>(KrEditSettingsVirtual.ManageStageVisibility) == true)
                {
                    context.Stage.Hidden = !visible;
                }
            }
        }

        /// <inheritdoc/>
        public override async Task<StageHandlerResult> HandleTaskCompletionAsync(IStageTypeHandlerContext context)
        {
            var task = context.TaskInfo.Task;
            var taskType = task.TypeID;
            if (taskType != DefaultTaskTypes.KrEditTypeID
                 && taskType != DefaultTaskTypes.KrEditInterjectTypeID)
            {
                return StageHandlerResult.EmptyResult;
            }

            if (task.Card.Sections.TryGetValue(KrTaskCommentVirtual.Name, out var commSec)
                && commSec.Fields.TryGetValue(KrTaskCommentVirtual.Comment, out var commentObj)
                && commentObj is string comment)
            {
                context.WorkflowProcess.AuthorComment = comment;

                if (!string.IsNullOrEmpty(comment))
                {
                    await HandlerHelper.SetTaskResultAsync(context, task, comment);
                }
            }

            await context.WorkflowAPI.RemoveActiveTaskAsync(context.TaskInfo.Task.RowID, context.CancellationToken);
            
            HandlerHelper.AppendToCompletedTasksWithPreparing(context.Stage, task);
            
            return this.StartApproval(context);
        }

        /// <inheritdoc/>
        public override async Task<bool> HandleStageInterruptAsync(
            IStageTypeHandlerContext context)
        {
            context.Stage.InfoStorage.Remove(ReturnToStage);
            return await this.TasksRevoker.RevokeAllStageTasksAsync(new StageTaskRevokerContext(context, context.CancellationToken));
        }

        #endregion
    }
}