﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tessa.BusinessCalendar;
using Tessa.Cards;
using Tessa.Cards.Caching;
using Tessa.Cards.ComponentModel;
using Tessa.Cards.Extensions;
using Tessa.Extensions.Default.Server.Workflow.KrObjectModel;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Scope;
using Tessa.Extensions.Default.Shared;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Localization;
using Tessa.Notices;
using Tessa.Platform;
using Tessa.Platform.Data;
using Tessa.Platform.Runtime;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Tessa.Roles;
using Unity;
using static Tessa.Extensions.Default.Shared.Workflow.KrProcess.KrConstants;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.Handlers
{
    public class ApprovalStageTypeHandler : SubtaskStageTypeHandler
    {
        #region Constructors

        public ApprovalStageTypeHandler(
            IRoleRepository roleRepository,
            ICardMetadata cardMetadata,
            ICardGetStrategy cardGetStrategy,
            ICardCache cardCache,
            IKrScope krScope,
            IBusinessCalendarService calendarService,
            ISession session,
            IStageTasksRevoker tasksRevoker,
            [Dependency(NotificationManagerNames.DeferredWithoutTransaction)]
            INotificationManager notificationManager)
            : base(roleRepository, calendarService, cardMetadata, cardGetStrategy, krScope, tasksRevoker, notificationManager, cardCache, session)
        {
            this.CardCache = cardCache ?? throw new ArgumentNullException(nameof(cardCache));
        }

        #endregion

        #region Protected Properties and Constants

        protected const string TotalPerformerCount = nameof(TotalPerformerCount);
        protected const string CurrentPerformerCount = nameof(CurrentPerformerCount);
        protected const string Disapproved = Keys.Disapproved;

        protected ICardCache CardCache { get; set; }

        /// <inheritdoc />
        protected override string CommentNameField { get; } = KrApprovalSettingsVirtual.Comment;

        #endregion

        #region Protected Methods

        /// <summary>
        /// Обрабатывает завершение задания согласования.
        /// </summary>
        /// <param name="context">Контекст обработчика этапа.</param>
        /// <param name="disapproved">Значение <see langword="true"/>, если задание согласования было завершено вариантом завершения <see cref="DefaultCompletionOptions.Disapprove"/>, иначе (<see cref="DefaultCompletionOptions.Approve"/>) - <see cref="false"/>.</param>
        /// <returns>Результат выполнения.</returns>
        protected virtual async Task<StageHandlerResult> CompleteApprovalTaskAsync(IStageTypeHandlerContext context, bool disapproved)
        {
            var task = context.TaskInfo.Task;

            await context.WorkflowAPI.RemoveActiveTaskAsync(task.RowID, context.CancellationToken);
            await this.RevokeSubTasksAsync(context, task,
                new[]
                {
                    DefaultTaskTypes.KrAdditionalApprovalTypeID,
                    DefaultTaskTypes.KrRequestCommentTypeID
                },
                t =>
                {
                    t.OptionID = t.TypeID == DefaultTaskTypes.KrAdditionalApprovalTypeID
                        ? DefaultCompletionOptions.Revoke
                        : DefaultCompletionOptions.Cancel;
                    t.Result = "$ApprovalHistory_ParentTaskIsCompleted";
                });

            var field = disapproved ? KrApprovalCommonInfo.DisapprovedBy : KrApprovalCommonInfo.ApprovedBy;
            var fields = context.ContextualSatellite.Sections[KrApprovalCommonInfo.Name].Fields;
            var result = StringBuilderHelper.Acquire();

            var value = fields.TryGet<string>(field);
            if (!string.IsNullOrEmpty(value))
            {
                result.Append(value).Append("; ");
            }

            var storeContext = (ICardStoreExtensionContext) context.CardExtensionContext;
            var user = storeContext.Session.User;
            result.Append(user.Name);

            if (task.RoleID != user.ID)
            {
                result.Append(" (").Append(task.RoleName).Append(')');
            }

            fields[field] = result.ToStringAndRelease();

            var stage = context.Stage;
            var total = stage.InfoStorage.Get<int>(TotalPerformerCount);
            var current = stage.InfoStorage.Get<int>(CurrentPerformerCount);
            stage.InfoStorage[CurrentPerformerCount] = Int32Boxes.Box(++current);

            // Значение true, если выставлен флаг "Рекомендательное согласование", иначе - false.
            var advisory = stage.SettingsStorage.TryGet<bool?>(KrApprovalSettingsVirtual.Advisory) ?? false;

            // Значение true, если выставлен флаг "Изменять состояние при завершении", иначе - false.
            var changeStateOnEnd = stage.SettingsStorage.Get<bool>(KrApprovalSettingsVirtual.ChangeStateOnEnd);

            if (disapproved)
            {
                stage.InfoStorage[Disapproved] = BooleanBoxes.True;
            }

            var isSetCommentTaskResult =
                disapproved
                || !(await this.CardCache.Cards
                    .GetAsync(KrSettings.Name))
                    .Sections[KrSettings.Name].RawFields.Get<bool>(KrSettings.HideCommentForApprove);

            var comment =
                isSetCommentTaskResult
                    ? task.Card.Sections[KrTask.Name].Fields.TryGet<string>(KrTask.Comment)
                    : default;

            if (advisory)
            {
                comment = LocalizationManager.Format("$KrProcess_AdvisoryApprovalCommentFormat", comment).TrimEnd();
            }

            if (advisory || isSetCommentTaskResult)
            {
                await HandlerHelper.SetTaskResultAsync(context, task, comment);
            }

            // Значение true, если выставлен флаг "Не возвращать на доработку", иначе - false.
            var notReturnEdit = (context.ProcessInfo.ProcessParameters.TryGet<bool?>(Keys.NotReturnEdit, true) ?? true)
                && stage.SettingsStorage.Get<bool>(KrApprovalSettingsVirtual.NotReturnEdit);

            // Значение true, если выставлен флаг "Вернуть при несогласовании", иначе - false.
            var returnWhenDisapproved = stage.SettingsStorage.Get<bool>(KrApprovalSettingsVirtual.ReturnWhenDisapproved);
            
            if (current == total)
            {
                return !advisory && stage.InfoStorage.TryGet<bool>(Disapproved)
                    ? await DisapproveAndCompleteAsync()
                    : ApproveAndComplete();
            }

            if (!stage.SettingsStorage.Get<bool>(KrApprovalSettingsVirtual.IsParallel))
            {
                if (!advisory && disapproved && returnWhenDisapproved)
                {
                    return await DisapproveAndCompleteAsync();
                }

                await this.SendApprovalTaskAsync(context, stage.Performers[current]);
            }

            return StageHandlerResult.InProgressResult;

            StageHandlerResult ApproveAndComplete()
            {
                StagePositionInGroup(context.WorkflowProcess.Stages, stage, out var hasPreviouslyDisapproved, out var hasNext);
                var returnToAuthor = stage.SettingsStorage.Get<bool>(KrApprovalSettingsVirtual.ReturnToAuthor);

                if (changeStateOnEnd)
                {
                    if (hasPreviouslyDisapproved)
                    {
                        this.KrScope.Info[Keys.IgnoreChangeState] = BooleanBoxes.True;
                        context.WorkflowProcess.State = KrState.Disapproved;
                    }
                    else if (!returnToAuthor || notReturnEdit)
                    {
                        context.WorkflowProcess.State = KrState.Approved;
                    }
                }

                if (!notReturnEdit)
                {
                    if (hasPreviouslyDisapproved
                        && !hasNext)
                    {
                        // Последний этап завершен. Этот согласован, но предыдущие могли быть и не согласованы.
                        // Если были несогласованные, то возвращаемся в начало на доработку.
                        return StageHandlerResult.CurrentGroupTransition();
                    }

                    if (returnToAuthor)
                    {
                        // Выполнить переход к этапу "Доработка" расположенному в текущей группе этапов.
                        Guid? editID = null;
                        var cycle = GetCycle(context);
                        context.WorkflowProcess.Stages.ForEachStageInGroup(
                            stage.StageGroupID,
                            currentStage =>
                            {
                                if (currentStage.ID == stage.ID)
                                {
                                    return false;
                                }

                                if (currentStage.StageTypeID == StageTypeDescriptors.EditDescriptor.ID)
                                {
                                    stage.InfoStorage[Interjected] = Int32Boxes.Box(cycle);
                                    currentStage.InfoStorage[EditStageTypeHandler.ReturnToStage] = stage.ID;
                                    editID = currentStage.ID;
                                    return false;
                                }

                                return true;
                            });

                        // Решарпер считает, что т.к. editID замкнуто в лямбде, то оно может быть изменено в другом потоке
                        // Дадим ему уверенность, переприсвоив в локальную переменную
                        var localEditID = editID;
                        if (localEditID.HasValue)
                        {
                            var transRes = StageHandlerResult.Transition(localEditID.Value, keepStageStates: true);
                            return this.StageCompleted(context, transRes);
                        }

                        context.ValidationResult.AddError(this, "$KrMessages_NoEditStage");
                    }
                }

                return this.StageCompleted(context, StageHandlerResult.CompleteResult);
            }

            async Task<StageHandlerResult> DisapproveAndCompleteAsync()
            {
                StagePositionInGroup(context.WorkflowProcess.Stages, stage, out _, out var hasNext);

                if (!returnWhenDisapproved && hasNext)
                {
                    context.WorkflowProcess.InfoStorage[Disapproved] = BooleanBoxes.True;
                    return this.StageCompleted(context, StageHandlerResult.CompleteResult);
                }

                if (changeStateOnEnd)
                {
                    context.WorkflowProcess.State = KrState.Disapproved;
                    this.KrScope.Info[Keys.IgnoreChangeState] = BooleanBoxes.True;
                }

                if (notReturnEdit)
                {
                    return this.StageCompleted(context, StageHandlerResult.CompleteResult);
                }

                var utcNow = DateTime.UtcNow;
                var option = (await this.CardMetadata.GetEnumerationsAsync(context.CancellationToken)).CompletionOptions[DefaultCompletionOptions.RebuildDocument];
                var groupID = await HandlerHelper.GetTaskHistoryGroupAsync(context, this.KrScope);
                var userZoneInfo = await this.CalendarService.GetRoleTimeZoneInfoAsync(user.ID, context.CancellationToken);
                var item = new CardTaskHistoryItem
                {
                    State = CardTaskHistoryState.Inserted,
                    RowID = Guid.NewGuid(),
                    TypeID = DefaultTaskTypes.KrRebuildTypeID,
                    TypeName = DefaultTaskTypes.KrRebuildTypeName,
                    TypeCaption = "$CardTypes_TypesNames_KrRebuild",
                    Created = utcNow,
                    Planned = utcNow,
                    InProgress = utcNow,
                    Completed = utcNow,
                    CompletedByID = user.ID,
                    CompletedByName = user.Name,
                    AuthorID = user.ID,
                    AuthorName = user.Name,
                    UserID = user.ID,
                    UserName = user.Name,
                    RoleID = user.ID,
                    RoleName = user.Name,
                    RoleTypeID = RoleHelper.PersonalRoleTypeID,
                    OptionID = option.ID,
                    OptionName = option.Name,
                    OptionCaption = option.Caption,
                    GroupRowID = groupID,
                    TimeZoneID = userZoneInfo.TimeZoneID,
                    TimeZoneUtcOffsetMinutes = (int?) userZoneInfo.TimeZoneUtcOffset.TotalMinutes
                };

                (await this.KrScope.GetMainCardAsync(storeContext.Request.Card.ID, cancellationToken: context.CancellationToken)).TaskHistory.Add(item);
                context.ContextualSatellite.AddToHistory(
                    item.RowID,
                    context.WorkflowProcess.InfoStorage.TryGet(Keys.Cycle, 1));

                return this.StageCompleted(context, StageHandlerResult.GroupTransition(stage.StageGroupID));
            }
        }

        /// <summary>
        /// Обрабатывает создание заданий дополнительного согласования.
        /// </summary>
        /// <param name="context">Контекст обработчика этапа.</param>
        /// <returns>Результат выполнения.</returns>
        protected virtual async Task<StageHandlerResult> StartAdditionalApprovalTaskAsync(IStageTypeHandlerContext context)
        {
            var task = context.TaskInfo.Task;

            // TODO: Cтрашный костыль.
            var firstIsResponsible = task.Card.Info
                .Get<bool>(nameof(KrAdditionalApproval.FirstIsResponsible));
            if (firstIsResponsible)
            {
                var isResponsibleSet = task.Card.Sections[nameof(KrAdditionalApprovalInfo)].Rows
                    .Any(u => u.State != CardRowState.Deleted && u.Get<bool>(nameof(KrAdditionalApprovalInfo.IsResponsible)));
                if (isResponsibleSet)
                {
                    context.ValidationResult.AddError(this, "$KrMessages_MoreThenOneResponsible");
                    return StageHandlerResult.EmptyResult;
                }
            }

            var utcNow = DateTime.UtcNow;
            
            // TODO: Cтрашный костыль.
            var comment = task.Card.Info.Get<string>(Comment);
            var plannedDays = task.Card.Info.Get<double>(nameof(KrAdditionalApproval.TimeLimitation));

            var isResponsible = firstIsResponsible;
            var roles = task.Card.Sections[KrAdditionalApprovalUsers.Name].Rows
                .OrderBy(u => u.Get<int>(Order))
                .Select(u => new RoleUser(
                    u.Get<Guid>(KrAdditionalApprovalUsers.RoleID),
                    u.Get<string>(KrAdditionalApprovalUsers.RoleName)));

            var storeContext = (ICardStoreExtensionContext) context.CardExtensionContext;
            var user = storeContext.Session.User;

            foreach (var role in roles)
            {
                await this.SendAdditionalApprovalTaskAsync(
                    context, task, role.ID, role.Name, isResponsible, utcNow,
                    user.ID, user.Name,
                    comment,
                    plannedDays);

                if (isResponsible)
                {
                    isResponsible = false;
                }
            }

            return StageHandlerResult.InProgressResult;
        }

        /// <summary>
        /// Обрабатывает завершение задания дополнительного согласования.
        /// </summary>
        /// <param name="context">Контекст обработчика этапа.</param>
        /// <param name="disapproved">Значение <see langword="true"/>, если задание было завершено вариантом <see cref="DefaultCompletionOptions.Disapprove"/>, иначе (<see cref="DefaultCompletionOptions.Approve"/>) - <see langword="false"/>.</param>
        /// <returns>Результат выполнения.</returns>
        protected virtual async Task<StageHandlerResult> CompleteAdditionalApprovalTaskAsync(IStageTypeHandlerContext context, bool disapproved)
        {
            var task = context.TaskInfo.Task;

            await context.WorkflowAPI.RemoveActiveTaskAsync(task.RowID, context.CancellationToken);
            this.SubTaskCompleted(context);
            await this.RevokeSubTasksAsync(context, task,
                new[]
                {
                    DefaultTaskTypes.KrRequestCommentTypeID,
                    DefaultTaskTypes.KrAdditionalApprovalTypeID
                },
                t =>
                {
                    t.OptionID = DefaultCompletionOptions.Revoke;
                    t.Result = "$ApprovalHistory_ParentTaskIsCompleted";
                });

            var parentTaskID = task.ParentRowID;
            if (parentTaskID == null)
            {
                return StageHandlerResult.EmptyResult;
            }

            var taskComment = context
                .TaskInfo
                .Task
                .Card
                .Sections[KrAdditionalApprovalTaskInfo.Name]
                .RawFields
                .Get<string>(Comment);
            await HandlerHelper.SetTaskResultAsync(context, task, taskComment);

            Guid? approverID;
            Guid? responsibleID;
            int notCompleted;

            var storeContext = (ICardStoreExtensionContext) context.CardExtensionContext;
            var scope = storeContext.DbScope;
            await using (scope.Create())
            {
                approverID = await scope.Db
                    .SetCommand(
                        scope.BuilderFactory
                            .Select().C("UserID")
                            .From("Tasks").NoLock()
                            .Where().C("RowID").Equals().P("RowID")
                            .Build(),
                        scope.Db.Parameter("RowID", parentTaskID.Value))
                    .LogCommand()
                    .ExecuteAsync<Guid?>(context.CancellationToken);

                var isResponsible = task.Card.Sections[nameof(KrAdditionalApprovalTaskInfo)].Fields
                    .Get<bool>(nameof(KrAdditionalApprovalTaskInfo.IsResponsible));

                responsibleID = isResponsible
                    ? await scope.Db
                        .SetCommand(
                            scope.BuilderFactory
                                .Select().C("t", "UserID")
                                .From("Tasks", "t").NoLock()
                                .InnerJoin("KrAdditionalApprovalInfo", "i").NoLock()
                                .On().C("i", "RowID").Equals().C("t", "RowID")
                                .Where().C("i", "ID").Equals().P("RowID")
                                .And().C("i", "IsResponsible").Equals().V(true)
                                .Build(),
                            scope.Db.Parameter("RowID", parentTaskID.Value))
                        .LogCommand()
                        .ExecuteAsync<Guid?>(context.CancellationToken)
                    : null;

                notCompleted = await scope.Db
                    .SetCommand(
                        scope.BuilderFactory
                            .Select().Count().Substract(1)
                            .From("KrAdditionalApprovalInfo").NoLock()
                            .Where().C("ID").Equals().P("RowID")
                            .And().C("Completed").IsNull()
                            .Build(),
                        scope.Db.Parameter("RowID", parentTaskID.Value))
                    .LogCommand()
                    .ExecuteAsync<int>(context.CancellationToken);
            }

            var roleList = new List<Guid>();
            if (approverID.HasValue)
            {
                roleList.Add(approverID.Value);
            }

            if (responsibleID.HasValue)
            {
                roleList.Add(responsibleID.Value);
            }

            if (roleList.Count > 0)
            {
                var isCompleted = notCompleted == 0;
                var cardID = context.MainCardID ?? Guid.Empty;
                context.ValidationResult.Add(
                    await NotificationManager
                        .SendAsync(
                            isCompleted
                                ? DefaultNotifications.AdditionalApprovalNotificationCompleted
                                : DefaultNotifications.AdditionalApprovalNotification,
                            roleList,
                            new NotificationSendContext()
                            {
                                MainCardID = cardID,
                                Info = Shared.Notices.NotificationHelper.GetInfoWithTask(task),
                                GetCardFuncAsync = (ct) => context.MainCardAccessStrategy.GetCardAsync(cancellationToken: ct),
                                ModifyEmailActionAsync = async (email, ct) =>
                                {
                                    if (!isCompleted)
                                    {
                                        email.PlaceholderAliases.SetReplacement(
                                            "subjectLabel",
                                            disapproved
                                                ? "$DisapprovedAdditionalApprovalNotificationTemplate_SubjectLabel"
                                                : "$ApprovedAdditionalApprovalNotificationTemplate_SubjectLabel");
                                        email.PlaceholderAliases.SetReplacement(
                                            "taskCount",
                                            $"text:{notCompleted}");
                                    }

                                    email.PlaceholderAliases.SetReplacement(
                                        "resultLabel",
                                        disapproved
                                            ? "$DisapprovedNotificationTemplate_BodyLabel"
                                            : "$ApprovedNotificationTemplate_BodyLabel");
                                }
                            },
                            context.CancellationToken));
            }

            return StageHandlerResult.InProgressResult;
        }

        /// <summary>
        /// Отправляет задание согласования.
        /// </summary>
        /// <param name="context">Контекст обработчика этапа.</param>
        /// <param name="performer">Исполнитель.</param>
        /// <returns>Асинхронная задача.</returns>
        protected virtual async Task SendApprovalTaskAsync(
            IStageTypeHandlerContext context,
            Performer performer)
        {
            var roleName = KrAdditionalApprovalMarker.Unmark(performer.PerformerName);
            var groupID = await HandlerHelper.GetTaskHistoryGroupAsync(context, this.KrScope);
            var author = await HandlerHelper.GetStageAuthorAsync(context, this.RoleRepository, this.Session);
            if (author == null)
            {
                return;
            }

            var authorID = author.AuthorID;
            var (kindID, kindCaption) = HandlerHelper.GetTaskKind(context);

            var approvalTask = await this.SendTaskAsync(
                context,
                DefaultTaskTypes.KrApproveTypeID,
                this.GetTaskDigest(context),
                performer.PerformerID,
                roleName,
                (t, ct) =>
                {
                    t.AuthorID = authorID;
                    t.AuthorName = null; // AuthorName и AuthorPosition определяются системой, когда явно указано null
                    t.GroupRowID = groupID;
                    t.Planned = context.Stage.Planned;
                    t.PlannedQuants = context.Stage.PlannedQuants;
                    HandlerHelper.SetTaskKind(t, kindID, kindCaption, context);

                    return new ValueTask();
                });

            var created = DateTime.UtcNow;
            var performerRowID = performer.RowID;
            var aa = context.Stage.SettingsStorage.TryGet<List<object>>(KrAdditionalApprovalUsersCardVirtual.Synthetic);
            if (aa != null)
            {
                foreach (var row in aa)
                {
                    if (row is Dictionary<string, object> additional &&
                        additional.Get<Guid>(nameof(KrAdditionalApprovalUsersCardVirtual.MainApproverRowID)) == performerRowID)
                    {
                        await this.SendAdditionalApprovalTaskAsync(
                            context,
                            approvalTask,
                            additional.Get<Guid>(nameof(KrAdditionalApprovalUsersCardVirtual.RoleID)),
                            additional.Get<string>(nameof(KrAdditionalApprovalUsersCardVirtual.RoleName)),
                            additional.Get<bool>(nameof(KrAdditionalApprovalUsersCardVirtual.IsResponsible)),
                            created,
                            approvalTask.AuthorID,
                            approvalTask.AuthorName,
                            null);
                    }
                }
            }
        }

        /// <summary>
        /// Отправляет задание дополнительного согласования.
        /// </summary>
        /// <param name="context">Контекст обработчика этапа.</param>
        /// <param name="approvalTask">Родительское задание согласования.</param>
        /// <param name="roleID">Идентификатор роли на которую отправляется задание.</param>
        /// <param name="roleName">Имя роли на которую отправляется задание.</param>
        /// <param name="isResponsible">Значение <see langword="true"/>, если <paramref name="roleID"/> является ответственным исполнителем, иначе - <see langword="false"/>.</param>
        /// <param name="created">Дата создания задания.</param>
        /// <param name="authorID">Идентификатор роли автора задания.</param>
        /// <param name="authorName">Имя роли автора задания.</param>
        /// <param name="comment">Комментария к заданию.</param>
        /// <param name="plannedDays"></param>
        /// <returns>Асинхронная задача.</returns>
        protected virtual async Task SendAdditionalApprovalTaskAsync(
            IStageTypeHandlerContext context,
            CardTask approvalTask,
            Guid roleID,
            string roleName,
            bool isResponsible,
            DateTime created,
            Guid? authorID,
            string authorName,
            string comment,
            double? plannedDays = null)
        {
            var storeContext = (ICardStoreExtensionContext) context.CardExtensionContext;
            var user = storeContext.Session.User;

            // Временная зона текущего сотрудника, для записи в историю заданий.
            var userZoneInfo = await this.CalendarService.GetRoleTimeZoneInfoAsync(user.ID, context.CancellationToken);
            var groupID = await HandlerHelper.GetTaskHistoryGroupAsync(context, this.KrScope);
            var option = (await this.CardMetadata.GetEnumerationsAsync(context.CancellationToken)).CompletionOptions[DefaultCompletionOptions.AdditionalApproval];
            var infoItem = new CardTaskHistoryItem
            {
                State = CardTaskHistoryState.Inserted,
                ParentRowID = approvalTask.RowID,
                RowID = Guid.NewGuid(),
                TypeID = DefaultTaskTypes.KrInfoAdditionalApprovalTypeID,
                TypeName = DefaultTaskTypes.KrInfoAdditionalApprovalTypeName,
                TypeCaption = "$CardTypes_TypesNames_KrAdditionalApproval",
                Created = created,
                Planned = created,
                InProgress = created,
                Completed = created,
                CompletedByID = user.ID,
                CompletedByName = user.Name,
                AuthorID = user.ID,
                AuthorName = user.Name,
                UserID = user.ID,
                UserName = user.Name,
                RoleID = user.ID,
                RoleName = user.Name,
                RoleTypeID = RoleHelper.PersonalRoleTypeID,
                OptionID = option.ID,
                OptionName = option.Name,
                OptionCaption = option.Caption,
                GroupRowID = groupID,
                Result = comment ?? (isResponsible
                    ? "{$KrMessages_ResponsibleAdditionalApprovalComment}"
                    : "{$KrMessages_DefaultAdditionalApprovalComment}"),
                TimeZoneID = userZoneInfo.TimeZoneID,
                TimeZoneUtcOffsetMinutes = (int?) userZoneInfo.TimeZoneUtcOffset.TotalMinutes
            };

            var cycle = context.WorkflowProcess.InfoStorage.TryGet(Keys.Cycle, 1);
            var history = (await this.KrScope.GetMainCardAsync(storeContext.Request.Card.ID, cancellationToken: context.CancellationToken)).TaskHistory;
            var satellite = context.ContextualSatellite;

            history.Add(infoItem);
            satellite.AddToHistory(infoItem.RowID, cycle);

            var authorComment = context.WorkflowProcess.AuthorComment;
            var digest = string.IsNullOrWhiteSpace(authorComment)
                ? comment
                : comment + Environment.NewLine + authorComment;

            if (isResponsible)
            {
                digest = string.IsNullOrWhiteSpace(digest)
                    ? "{$KrMessages_ResponsibleAdditionalDigest}"
                    : "{$KrMessages_ResponsibleAdditionalDigest}." + Environment.NewLine + digest;
            }

            var additionalTask =
                await this.SendSubTaskAsync(
                    context,
                    approvalTask,
                    DefaultTaskTypes.KrAdditionalApprovalTypeID,
                    digest,
                    roleID,
                    roleName,
                    modifyTask: (t, ct) =>
                    {
                        t.AuthorID = authorID;
                        t.AuthorName = authorName;
                        t.ParentRowID = approvalTask.RowID;
                        t.Planned = null;
                        t.HistoryItemParentRowID = infoItem.RowID;
                        t.GroupRowID = groupID;

                        if (plannedDays.HasValue)
                        {
                            t.Planned = null;
                            t.PlannedQuants = (int) (plannedDays * 32.0); // округление до кванта в меньшую сторону
                        }
                        else
                        {
                            t.Planned = context.Stage.Planned;
                            t.PlannedQuants = context.Stage.PlannedQuants;
                        }

                        return new ValueTask();
                    },
                    createHistory: true);

            var additionalCard = additionalTask.Card;
            additionalCard.Sections[TaskCommonInfo.Name].Fields[Info] = comment;
            additionalCard.Sections[KrAdditionalApprovalTaskInfo.Name].Fields[KrAdditionalApprovalTaskInfo.AuthorRoleID] = approvalTask.RoleID;
            additionalCard.Sections[KrAdditionalApprovalTaskInfo.Name].Fields[KrAdditionalApprovalTaskInfo.AuthorRoleName] = approvalTask.RoleName;
            additionalCard.Sections[KrAdditionalApprovalTaskInfo.Name].Fields[KrAdditionalApprovalTaskInfo.IsResponsible] = isResponsible;
        }

        #endregion

        #region Base Overrides

        /// <inheritdoc/>
        public override async Task<StageHandlerResult> HandleStageStartAsync(IStageTypeHandlerContext context)
        {
            var stage = context.Stage;
            if (IsInterjected(context))
            {
                if (stage.SettingsStorage.Get<bool>(KrApprovalSettingsVirtual.ChangeStateOnEnd))
                {
                    context.WorkflowProcess.State = KrState.Approved;
                }

                stage.InfoStorage.Remove(Interjected);
                return StageHandlerResult.CompleteResult;
            }

            var baseResult = await base.HandleStageStartAsync(context);
            if (baseResult != StageHandlerResult.EmptyResult)
            {
                return baseResult;
            }

            var performers = stage.Performers;
            if (performers.Count == 0)
            {
                if (stage.SettingsStorage.Get<bool>(KrApprovalSettingsVirtual.ChangeStateOnEnd))
                {
                    context.WorkflowProcess.State = KrState.Approved;
                }

                return StageHandlerResult.CompleteResult;
            }

            if (stage.SettingsStorage.Get<bool>(KrApprovalSettingsVirtual.ChangeStateOnStart))
            {
                context.WorkflowProcess.State = KrState.Active;
            }

            stage.InfoStorage[TotalPerformerCount] = Int32Boxes.Box(performers.Count);
            stage.InfoStorage[CurrentPerformerCount] = Int32Boxes.Zero;
            stage.InfoStorage[Disapproved] = BooleanBoxes.False;

            if (stage.SettingsStorage.Get<bool>(KrApprovalSettingsVirtual.IsParallel))
            {
                foreach (var performer in performers)
                {
                    await this.SendApprovalTaskAsync(context, performer);
                }
            }
            else
            {
                await this.SendApprovalTaskAsync(context, performers[0]);
            }

            return StageHandlerResult.InProgressResult;
        }

        /// <inheritdoc/>
        public override async Task<StageHandlerResult> HandleTaskCompletionAsync(IStageTypeHandlerContext context)
        {
            var baseResult = await base.HandleTaskCompletionAsync(context);
            if (baseResult != StageHandlerResult.EmptyResult)
            {
                return baseResult;
            }

            var task = context.TaskInfo.Task;
            if (task.TypeID == DefaultTaskTypes.KrAdditionalApprovalTypeID)
            {
                var optionID = task.OptionID;
                HandlerHelper.AppendToCompletedTasksWithPreparing(context.Stage, task);
                if (optionID == DefaultCompletionOptions.Approve)
                {
                    return await this.CompleteAdditionalApprovalTaskAsync(context, false);
                }

                if (optionID == DefaultCompletionOptions.Disapprove)
                {
                    return await this.CompleteAdditionalApprovalTaskAsync(context, true);
                }

                if (optionID == DefaultCompletionOptions.Revoke)
                {
                    await this.RevokeSubTasksAsync(context, task,
                        new[]
                        {
                            DefaultTaskTypes.KrAdditionalApprovalTypeID,
                            DefaultTaskTypes.KrRequestCommentTypeID
                        },
                        t =>
                        {
                            t.OptionID = t.TypeID == DefaultTaskTypes.KrAdditionalApprovalTypeID
                                ? DefaultCompletionOptions.Revoke
                                : DefaultCompletionOptions.Cancel;
                            t.Result = "$ApprovalHistory_ParentTaskIsCompleted";
                        });

                    return this.SubTaskCompleted(context);
                }

                if (optionID == DefaultCompletionOptions.AdditionalApproval)
                {
                    return await this.StartAdditionalApprovalTaskAsync(context);
                }
            }
            else
            {
                var optionID = task.OptionID;
                HandlerHelper.AppendToCompletedTasksWithPreparing(context.Stage, task);
                if (optionID == DefaultCompletionOptions.Approve)
                {
                    return await this.CompleteApprovalTaskAsync(context, false);
                }

                if (optionID == DefaultCompletionOptions.Disapprove)
                {
                    return await this.CompleteApprovalTaskAsync(context, true);
                }

                if (optionID == DefaultCompletionOptions.AdditionalApproval)
                {
                    return await this.StartAdditionalApprovalTaskAsync(context);
                }
            }

            return StageHandlerResult.EmptyResult;
        }

        /// <inheritdoc/>
        public override Task<bool> HandleStageInterruptAsync(IStageTypeHandlerContext context)
            => this.HandleStageInterruptAsync(context,
                new[]
                {
                    DefaultTaskTypes.KrApproveTypeID,
                    DefaultTaskTypes.KrAdditionalApprovalTypeID,
                    DefaultTaskTypes.KrRequestCommentTypeID
                },
                t => t.Result = "$ApprovalHistory_TaskCancelled");

        /// <inheritdoc/>
        protected override Task HandleTaskDelegateAsync(IStageTypeHandlerContext context, CardTask delegatedTask)
        {
            var task = context.TaskInfo.Task;
            if (task.Card.Sections.TryGetValue(KrAdditionalApprovalInfo.Name, out var oldSection))
            {
                var newSection = new CardSection(KrAdditionalApprovalInfo.Name, oldSection.GetStorage())
                {
                    Type = CardSectionType.Table
                };

                foreach (var row in newSection.Rows)
                {
                    row.Fields["ID"] = delegatedTask.RowID;
                    row.State = CardRowState.Inserted;
                }

                delegatedTask.Card.Sections[KrAdditionalApprovalInfo.Name].Set(newSection);
            }

            var storeContext = (ICardStoreExtensionContext) context.CardExtensionContext;
            storeContext.Request.Info.Add(KrReassignAdditionalApprovalStoreExtension.ReassignTo, delegatedTask.RowID);

            return Task.CompletedTask;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Определяет в текущей группе этапов:
        /// <list type="number">
        ///     <item>
        ///         <description>Были ли до текущего этапа не согласованнные этапы "Согласования".</description>
        ///     </item>
        ///     <item>
        ///         <description>Есть ли этапы "Согласования" после этого этапа.</description>    
        ///     </item>
        /// </list>
        /// </summary>
        /// <param name="processStages"></param>
        /// <param name="currentStage"></param>
        /// <param name="hasPreviouslyDisapproved">Значение <see langword="true"/>, если до текущего этапа были не согласованные этапы "Согласования", иначе - <see langword="false"/>.</param>
        /// <param name="hasNext">Значение <see langword="true"/>, если есть этапы "Согласования" после текущего этапа, иначе - <see langword="false"/>.</param>
        private static void StagePositionInGroup(IList<Stage> processStages, Stage currentStage, out bool hasPreviouslyDisapproved, out bool hasNext)
        {
            var hasPreviouslyDisapprovedClosure = false;
            var hasNextClosure = false;

            // Признак нахождения текущего этапа среди этапов "Согласования" в текущей группе этапов.
            var equator = false;
            processStages.ForEachStageInGroup(
                currentStage.StageGroupID,
                currStage =>
                {
                    if (currStage.ID == currentStage.ID)
                    {
                        equator = true;
                        return;
                    }

                    if (!equator
                        && currStage.State == KrStageState.Completed
                        && currStage.StageTypeID == StageTypeDescriptors.ApprovalDescriptor.ID
                        && !(currStage.SettingsStorage.TryGet<bool?>(KrApprovalSettingsVirtual.Advisory) ?? false)
                        && currStage.InfoStorage.TryGet<bool?>(Disapproved) == true)
                    {
                        hasPreviouslyDisapprovedClosure = true;
                    }

                    if (!hasNextClosure
                        && equator
                        && currStage.StageTypeID == StageTypeDescriptors.ApprovalDescriptor.ID)
                    {
                        hasNextClosure = true;
                    }
                });
            hasPreviouslyDisapproved = hasPreviouslyDisapprovedClosure;
            hasNext = hasNextClosure;
        }

        #endregion
    }
}