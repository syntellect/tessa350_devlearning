﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Tessa.Platform;

namespace Tessa.Extensions.Default.Server.Workflow.KrObjectModel
{
    /// <summary>
    /// Предоставляет информацию о вложенном процессе.
    /// </summary>
    [Serializable]
    public sealed class NestedProcessCommonInfo: ProcessCommonInfo
    {
        #region Constructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="NestedProcessCommonInfo"/>.
        /// </summary>
        /// <param name="currentStageRowID">Идентификатор текущего этапа.</param>
        /// <param name="info">Дополнительная информация по процессу.</param>
        /// <param name="secondaryProcessID">Идентификатор вторичного процесса.</param>
        /// <param name="nestedProcessID">Идентификатор дочернего процесса.</param>
        /// <param name="parentStageRowID">Идентификатор родительского этапа.</param>
        /// <param name="nestedOrder">Порядковый номер дочернего процесса.</param>
        public NestedProcessCommonInfo(
            Guid? currentStageRowID,
            IDictionary<string, object> info,
            Guid? secondaryProcessID,
            Guid nestedProcessID,
            Guid parentStageRowID,
            int nestedOrder)
            : base(currentStageRowID, info, secondaryProcessID)
        {
            this.Init(nameof(this.NestedProcessID), GuidBoxes.Box(nestedProcessID));
            this.Init(nameof(this.ParentStageRowID), GuidBoxes.Box(parentStageRowID));
            this.Init(nameof(this.NestedOrder), Int32Boxes.Box(nestedOrder));
        }
        
        /// <inheritdoc />
        public NestedProcessCommonInfo(
            Dictionary<string, object> storage)
            : base(storage)
        {
        }

        /// <inheritdoc />
        private NestedProcessCommonInfo(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Возвращает или задаёт идентификатор дочернего процесса.
        /// </summary>
        public Guid NestedProcessID
        {
            get => this.Get<Guid>(nameof(this.NestedProcessID));
            set => this.Set(nameof(this.NestedProcessID), value);
        }

        /// <summary>
        /// Возвращает или задаёт идентификатор родительского этапа.
        /// </summary>
        public Guid ParentStageRowID
        {
            get => this.Get<Guid>(nameof(this.ParentStageRowID));
            set => this.Set(nameof(this.ParentStageRowID), value);
        }

        /// <summary>
        /// Возвращает или задаёт порядковый номер дочернего процесса.
        /// </summary>
        public int NestedOrder
        {
            get => this.Get<int>(nameof(this.NestedOrder));
            set => this.Set(nameof(this.NestedOrder), value);
        }

        #endregion
    }
}