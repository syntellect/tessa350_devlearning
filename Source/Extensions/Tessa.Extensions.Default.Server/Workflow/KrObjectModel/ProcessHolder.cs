﻿using System;
using System.Collections.Generic;
using Tessa.Platform.Collections;

namespace Tessa.Extensions.Default.Server.Workflow.KrObjectModel
{
    public sealed class ProcessHolder
    {
        /// <summary>
        /// Возвращает или задаёт имя типа процесса.
        /// </summary>
        public string MainProcessType { get; set; }
        
        public bool Persistent { get; set; }
        
        public Guid ProcessHolderID { get; set; }
        
        public WorkflowProcess MainWorkflowProcess { get; set; }

        public Dictionary<Guid, WorkflowProcess> NestedWorkflowProcesses { get; } =
            new Dictionary<Guid, WorkflowProcess>();

        /// <summary>
        /// Возвращает или задаёт информацию об основном процессе полученную из сателлита-холдера.
        /// </summary>
        public MainProcessCommonInfo PrimaryProcessCommonInfo { get; set; }

        /// <summary>
        /// Возвращает или задаёт основную информацию об основном процессе.
        /// </summary>
        /// <remarks>Дополняет <see cref="PrimaryProcessCommonInfo"/>. Содержит информацию из контекстуального сателлита. Если процесс является основным (<see cref="MainProcessType"/> равно <see cref="Shared.Workflow.KrProcess.KrConstants.KrProcessName"/>)</remarks>
        public MainProcessCommonInfo MainProcessCommonInfo { get; set; }
        
        /// <summary>
        /// Возвращает или задаёт набор объектов типа <see cref="NestedProcessCommonInfo"/> содержащих информацию о вложенных объектах.
        /// </summary>
        public HashSet<Guid, NestedProcessCommonInfo> NestedProcessCommonInfos { get; set; }

        /// <summary>
        /// Инициализирует значение <see cref="NestedProcessCommonInfos"/> указанным списком объектов типа <see cref="NestedProcessCommonInfo"/> содержащих информацию по вложенным процессам.
        /// </summary>
        public List<NestedProcessCommonInfo> NestedProcessCommonInfosList
        {
            set
            {
                if (value is null)
                {
                    this.NestedProcessCommonInfos = null;
                }
                else
                {
                    this.NestedProcessCommonInfos = 
                        new HashSet<Guid, NestedProcessCommonInfo>(x => x.NestedProcessID, value);
                }
            }
        }

    }
}