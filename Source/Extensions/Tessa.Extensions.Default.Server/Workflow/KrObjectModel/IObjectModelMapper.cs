﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Extensions.Default.Server.Workflow.KrCompilers;
using Tessa.Extensions.Default.Shared.Workflow.KrCompilers;

namespace Tessa.Extensions.Default.Server.Workflow.KrObjectModel
{
    /// <summary>
    /// Описывает объект обеспечивающий работу с хранилищами Kr процесса и объектной моделью процесса.
    /// </summary>
    public interface IObjectModelMapper
    {
        /// <summary>
        /// Загружает из сателлита-холдера информацию по основному процессу.
        /// </summary>
        /// <param name="processHolderSatellite">Сателлите-холдере процесса из которого выполянется загрузка информации по основному процессу.</param>
        /// <param name="withInfo">Значение <see langword="true"/>, если необходимо загрузить дополнительную ифнормацию по процессу, иначе - <see langword="false"/>.</param>
        /// <returns>Информация по основному процессу.</returns>
        MainProcessCommonInfo GetMainProcessCommonInfo(
            Card processHolderSatellite, 
            bool withInfo = true);

        /// <summary>
        /// Асинхронно устанавливает в сателлите-холдере процесса информацию по основному процессу.
        /// </summary>
        /// <param name="mainCardID">Идентификатор карточки документа.</param>
        /// <param name="processHolderSatellite">Сателлите-холдере процесса в который необходимо установить информацию по процессу.</param>
        /// <param name="processCommonInfo">Информация по основному процессу.</param>
        /// <param name="cancellationToken">
        /// Объект, посредством которого можно отменить асинхронную задачу.
        /// </param>
        /// <returns>Асинхронная задача.</returns>
        ValueTask SetMainProcessCommonInfoAsync(
            Guid mainCardID,
            Card processHolderSatellite,
            MainProcessCommonInfo processCommonInfo,
            CancellationToken cancellationToken = default);
        
        /// <summary>
        /// Загрузить из сателлита-холдера основную информацию по вложенным процессам.
        /// </summary>
        /// <param name="processHolderSatellite">Сателлит-холдер содержащий загружаемую информацию.</param>
        /// <returns>Список объектов <see cref="NestedProcessCommonInfo"/> содержащих информацию по вложенным процессам.</returns>
        List<NestedProcessCommonInfo> GetNestedProcessCommonInfos(
            Card processHolderSatellite);

        /// <summary>
        /// Установить в сателлит-холдер основную информацию по вложенным процессам.
        /// </summary>
        /// <param name="processHolderSatellite">Сателлит-холдер в который должна быть сохранена информация.</param>
        /// <param name="nestedProcessCommonInfos">Список объектов <see cref="NestedProcessCommonInfo"/> содержащих информацию по вложенным процессам.</param>
        void SetNestedProcessCommonInfos(
            Card processHolderSatellite,
            IReadOnlyCollection<NestedProcessCommonInfo> nestedProcessCommonInfos);

        /// <summary>
        /// Заполняет информацию в объектной модели указанной информацией по процессу и основному процессу.
        /// </summary>
        /// <param name="workflowProcess">Объектная модель в которую выполняется запись информации.</param>
        /// <param name="commonInfo">Информация о процессе.</param>
        /// <param name="primaryProcessCommonInfo">Информация об основном процессе.</param>
        void FillWorkflowProcessFromPci(
            WorkflowProcess workflowProcess,
            ProcessCommonInfo commonInfo,
            MainProcessCommonInfo primaryProcessCommonInfo);

        /// <summary>
        /// Преобразовать секционную модель процесса маршрутов в объектную модель.
        /// Данный метод удобно использовать для преобразования карточек шаблона этапов.
        /// </summary>
        /// <param name="primaryPci"></param>
        /// <param name="stageTemplate"></param>
        /// <param name="runtimeStages"></param>
        /// <param name="initialStage"></param>
        /// <param name="saveInitialStages"></param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns></returns>
        ValueTask<WorkflowProcess> CardRowsToObjectModelAsync(
            IKrStageTemplate stageTemplate,
            IReadOnlyCollection<IKrRuntimeStage> runtimeStages,
            MainProcessCommonInfo primaryPci,
            bool initialStage = true,
            bool saveInitialStages = false,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Переносит информацию о процессе из объектной модели в <paramref name="pci"/>, <paramref name="mainPci"/>, <paramref name="primaryPci"/>.
        /// </summary>
        /// <param name="process">Объектная модель процесса.</param>
        /// <param name="pci">Основная информация о текущем процессе</param>
        /// <param name="mainPci"></param>
        /// <param name="primaryPci">Информация об основном процессе полученная из сателлита-холдера.</param>
        void ObjectModelToPci(
            WorkflowProcess process,
            ProcessCommonInfo pci,
            MainProcessCommonInfo mainPci,
            MainProcessCommonInfo primaryPci);

        /// <summary>
        /// Преобразовать секционную модель процесса маршрутов в объектную модель.
        /// Данный метод удобно использовать для преобразования карточек документов.
        /// </summary>
        /// <param name="processHolder"></param>
        /// <param name="pci"></param>
        /// <param name="mainPci"></param>
        /// <param name="templates"></param>
        /// <param name="runtimeStages"></param>
        /// <param name="initialStage"></param>
        /// <param name="nestedProcessID"></param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить выполнения асинхронной задачи.</param>
        /// <returns></returns>
        ValueTask<WorkflowProcess> CardRowsToObjectModelAsync(
            Card processHolder,
            ProcessCommonInfo pci,
            MainProcessCommonInfo mainPci,
            IReadOnlyDictionary<Guid, IKrStageTemplate> templates,
            IReadOnlyDictionary<Guid, IReadOnlyCollection<IKrRuntimeStage>> runtimeStages,
            bool initialStage = true,
            Guid? nestedProcessID = null,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Преобразовать объектную модель процесса маршрутов в секционную модель с отслеживанием изменений.
        /// </summary>
        /// <param name="process">
        /// Переносимый процесс.
        /// </param>
        /// <param name="baseCard">
        /// Карточка, в которую необходимо перенести процесс.
        /// </param>
        /// <param name="pci">
        /// Основная информация о текущем процессе
        /// </param>
        /// <param name="cancellationToken">
        /// Объект, посредством которого можно отменить асинхронную задачу.
        /// </param>
        /// <returns></returns>
        ValueTask<List<RouteDiff>> ObjectModelToCardRowsAsync(
            WorkflowProcess process,
            Card baseCard,
            ProcessCommonInfo pci,
            CancellationToken cancellationToken = default);
    }
}