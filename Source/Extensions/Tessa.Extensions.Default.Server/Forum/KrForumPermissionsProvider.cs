﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.ComponentModel;
using Tessa.Cards.Extensions;
using Tessa.Extensions.Default.Server.Workflow.KrPermissions;
using Tessa.Extensions.Default.Server.Workflow.KrProcess;
using Tessa.Extensions.Default.Shared.Workflow.KrPermissions;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Forums;
using Tessa.Localization;
using Tessa.Platform.Validation;

namespace Tessa.Extensions.Default.Server.Forum
{
    public class KrForumPermissionsProvider : ForumPermissionsProvider
    {
        private readonly IKrPermissionsManager permissionManager;
        
        private readonly IKrTokenProvider krTokenProvider;

        public KrForumPermissionsProvider(
            IKrPermissionsManager permissionManager,
            IKrTokenProvider krTokenProvider)
        {
            this.permissionManager = permissionManager;
            this.krTokenProvider = krTokenProvider;
        }

        public override async Task<(bool result, ValidationResult validationResult)> CheckHasPermissionAddTopicAsync(
            ICardExtensionContext context, 
            Guid cardID, 
            CancellationToken cancellationToken = default)
        {
            return  await this.CheckHasPermissions(
                KrPermissionFlagDescriptors.AddTopics, 
                context, 
                cardID,
                cancellationToken: cancellationToken);
        }


        public override async Task<(bool result, ValidationResult validationResult)> CheckHasPermissionIsSuperModeratorAsync(
            ICardExtensionContext context, 
            Guid cardID, 
            CancellationToken cancellationToken = default)
        {
            return await this.CheckHasPermissions(
                KrPermissionFlagDescriptors.SuperModeratorMode,
                context,
                cardID,
                cancellationToken: cancellationToken);
        }
        
        public override async Task<(bool result, ValidationResult validationResult)> CheckHasPermissionIsEditMyMessagesAsync(
            ICardExtensionContext context,
            Guid cardID,
            Guid topicID,
            CancellationToken cancellationToken = default)
        {
            var info = new Dictionary<string, object>();
            info[nameof(topicID)] = topicID;
            return await this.CheckHasPermissions(
                KrPermissionFlagDescriptors.EditMyMessages,
                context,
                cardID,
                info,
                cancellationToken);
        }

        public override async Task<(bool result, ValidationResult validationResult)> CheckHasPermissionIsEditAllMessagesAsync(
            ICardExtensionContext context,
            Guid cardID,
            Guid topicID,
            CancellationToken cancellationToken = default)
        {
            var info = new Dictionary<string, object>();
            info[nameof(topicID)] = topicID;
            return await this.CheckHasPermissions(
                KrPermissionFlagDescriptors.EditAllMessages,
                context,
                cardID,
                info,
                cancellationToken);
        }

        public override async Task<Dictionary<string, object>> GetTokenWithEditMessagesPermissionsAsync(
            ICardExtensionContext context,
            Guid cardID,
            int cardVersion,
            Guid topicID,
            CancellationToken cancellationToken = default)
        {
            var requriedPermissions = new List<KrPermissionFlagDescriptor>()
            {
                KrPermissionFlagDescriptors.EditMyMessages, 
                KrPermissionFlagDescriptors.EditAllMessages
            };
            
            var permissionContext = await permissionManager.TryCreateContextAsync(
                new KrPermissionsCreateContextParams
                {
                    CardID = cardID,
                    ExtensionContext = context,
                    AdditionalInfo = context.Info,
                    ValidationResult = new ValidationResultBuilder(),
                },
                cancellationToken);

            if (permissionContext == null)
            {
                // карточка не входит в типовое решение, возвращаем полные права в токене
                KrToken krToken = this.krTokenProvider.CreateToken(
                    cardID, 
                    cardVersion,
                    permissionsVersion: CardComponentHelper.DoNotCheckVersion,
                    permissions:requriedPermissions,
                    null,
                    (t)=> t.Info[nameof(topicID)] = topicID);
                
                return krToken.GetStorage();
            }
            
            
            var info = new Dictionary<string, object>();
            info[nameof(topicID)] = topicID;
            permissionContext.Info[nameof(KrForumPermissionsProvider)] = info;

             IKrPermissionsManagerResult result = await this.permissionManager.GetEffectivePermissionsAsync(
                permissionContext,
                requriedPermissions.ToArray());
            
            // Создаем токен
            KrToken token = this.krTokenProvider.CreateToken(
                cardID,
                cardVersion,
                result.Version, 
                result.Permissions,
                null,
                (t)=> t.Info[nameof(topicID)] = topicID);
            
            return token.GetStorage();
        }
        
        public override async Task<ValidationResult> ValidateTokenAsync(
            Guid cardID,
            int cardVersion,
            Dictionary<string, object> info,
            CancellationToken cancellationToken = default)
        {
            var tokenResult = new ValidationResultBuilder();
            
            if (info.Any()) 
            {
                var token = new KrToken(info);
                await this.krTokenProvider.ValidateTokenAsync(
                    new Card(){ID = cardID, Version = cardVersion},
                    token,
                    tokenResult,
                    cancellationToken);
            }
            else
            {
                tokenResult.Add(
                    ForumValidationKey.GetTopic, 
                    ValidationResultType.Error, 
                    $"KrToken don't exist for cardID: {cardID}");
            }
            
            return tokenResult.Build();
        }
        
        public override bool IsEnableEditMyMessages(Dictionary<string, object> token)
        {
            var krToken = new KrToken(token);
            return krToken.HasPermission(KrPermissionFlagDescriptors.EditMyMessages);
        }
        
        public override bool IsEnableEditAllMessages(Dictionary<string, object> token)
        {
            var krToken = new KrToken(token);
            return krToken.HasPermission(KrPermissionFlagDescriptors.EditAllMessages);
        }

        private async Task<(bool result, ValidationResult validationResult)> CheckHasPermissions(
            KrPermissionFlagDescriptor required, 
            ICardExtensionContext context, 
            Guid cardID,
            Dictionary<string, object> info = null,
            CancellationToken cancellationToken = default)
        {
            var permissionContext = await permissionManager.TryCreateContextAsync(
                new KrPermissionsCreateContextParams
                {
                    CardID = cardID,
                    ExtensionContext = context,
                    AdditionalInfo = context.Info,
                    ValidationResult = new ValidationResultBuilder(),
                },
                cancellationToken);

            if (permissionContext == null)
            {
                // карточка не входит в типовое решение, возвращаем тру и варнинг,
                // так как пользоваться контролом можно без прав доступа, но все же так не задумано) 
                var vr = new ValidationResultBuilder();
                vr.Add(ForumValidationKey.PermissionWarning, 
                       ValidationResultType.Warning, 
                       string.Format(LocalizationManager.Localize("$Forum_ValidationKey_PermissionWarning_CardIsNotIncludedInStandardSolution"),cardID));
                
                return (result: true, validationResult: vr.Build());
            }

            if (info != null)
            {
                permissionContext.Info[nameof(KrForumPermissionsProvider)] = info;
            }
            
            var result = await permissionManager.CheckRequiredPermissionsAsync(
                permissionContext,
                required);
            
            return (result.Result, validationResult: result ? new ValidationResultBuilder().Build() : permissionContext.ValidationResult.Build());
        }
    }
}
