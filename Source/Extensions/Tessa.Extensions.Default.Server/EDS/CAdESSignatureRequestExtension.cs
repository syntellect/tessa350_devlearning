﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tessa.Cards.Extensions;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Tessa.Platform.EDS;
using Tessa.Extensions.Default.Shared.EDS;
using Unity;

namespace Tessa.Extensions.Default.Server.EDS
{
    public sealed class CAdESSignatureRequestExtension : CardRequestExtension
    {
        #region fields

        private readonly IEDSProvider edsProvider;

        #endregion

        #region Constructors

        public CAdESSignatureRequestExtension([OptionalDependency]IEDSProvider edsProvider)
        {
            this.edsProvider = edsProvider;
        }

        #endregion

        #region Base Overrides

        public override async Task AfterRequest(ICardRequestExtensionContext context)
        {
            if (!context.RequestIsSuccessful)
            {
                return;
            }

            try
            {
                var signature = context.Request.Info.Get<string>(CAdESSignatureHelper.SignatureKey);
                var edsAction = context.Request.Info.Get<EDSAction>(CAdESSignatureHelper.EDSActionKey);

                if (edsAction == EDSAction.Sign)
                {
                    context.Request.Info.TryGetValue(CAdESSignatureHelper.CertificateKey, out var certStr);
                    var (result, signatureType, signatureProfile) = this.edsProvider != null
                        ? await this.edsProvider.ExtendDocumentAsync(signature, certStr as string, context.CancellationToken)
                        : (string.Empty, SignatureType.None, SignatureProfile.None);

                    context.Response.Info[CAdESSignatureHelper.SignatureKey] = result;
                    context.Response.Info[CAdESSignatureHelper.SignatureTypeKey] = signatureType;
                    context.Response.Info[CAdESSignatureHelper.SignatureProfileKey] = signatureProfile;
                }
                else if (edsAction == EDSAction.Verify)
                {
                    var targetSignatureType = context.Request.Info.Get<SignatureType>(".targetSignatureType");
                    var targetSignatureProfile = context.Request.Info.Get<SignatureProfile>(".targetSignatureProfile");

                    var signaturesValidations = this.edsProvider?.ValidateDocument(signature, (int)targetSignatureType, (int)targetSignatureProfile) ?? new List<Dictionary<string, object>>();

                    context.Response.Info[".signatureValidation"] = signaturesValidations;
                }
                else if (edsAction == EDSAction.GetBESFromExtended)
                {
                    context.Response.Info[CAdESSignatureHelper.SignatureKey] = this.edsProvider?.GetBesSignature(signature) ?? string.Empty;
                }
                else if (edsAction == EDSAction.GetToBeSigned)
                {
                    var certificate = context.Request.Info.Get<string>(CAdESSignatureHelper.CertificateKey);
                    var signingTime = context.Request.Info.Get<DateTime>(CAdESSignatureHelper.SigningTimeKey);
                    var digestAlgorithm = context.Request.Info.TryGet<string>(CAdESSignatureHelper.DigestAlgorithmKey);
                    var encriptionAlgorithm = context.Request.Info.TryGet<string>(CAdESSignatureHelper.EncriptionAlgorithmKey);

                    context.Response.Info[CAdESSignatureHelper.SignatureKey] = await this.edsProvider?.ToBeSignedAsync(signature, certificate, signingTime, digestAlgorithm, encriptionAlgorithm) ?? string.Empty;
                }
                else if (edsAction == EDSAction.GetBesSignature)
                {
                    var file = context.Request.Info.Get<string>(CAdESSignatureHelper.FileKey);
                    var certificate = context.Request.Info.Get<string>(CAdESSignatureHelper.CertificateKey);
                    var signingTime = context.Request.Info.Get<DateTime>(CAdESSignatureHelper.SigningTimeKey);
                    var digestAlgorithm = context.Request.Info.TryGet<string>(CAdESSignatureHelper.DigestAlgorithmKey);
                    var encriptionAlgorithm = context.Request.Info.TryGet<string>(CAdESSignatureHelper.EncriptionAlgorithmKey);

                    context.Response.Info[CAdESSignatureHelper.SignatureKey] = await this.edsProvider?.GetSignedDocumentAsync(signature, file, certificate, signingTime, digestAlgorithm, encriptionAlgorithm) ?? string.Empty;
                }
                else if (edsAction == EDSAction.GetSignatureAttributesFromSignature)
                {
                    var signatureAttributes = this.edsProvider?.GetSignatureAttributesFromSignature(signature);

                    context.Response.Info[CAdESSignatureHelper.HashKey] = signatureAttributes.Hash != null ? Convert.ToBase64String(signatureAttributes.Hash) : string.Empty;
                    context.Response.Info[CAdESSignatureHelper.HashOidKey] = signatureAttributes?.HashOid ?? string.Empty;
                    context.Response.Info[CAdESSignatureHelper.CertificateKey] = signatureAttributes.Certificate != null ? Convert.ToBase64String(signatureAttributes.Certificate) : string.Empty;
                    context.Response.Info[CAdESSignatureHelper.SignatureKey] = signatureAttributes.Signature != null ? Convert.ToBase64String(signatureAttributes.Signature) : string.Empty;
                    context.Response.Info[CAdESSignatureHelper.SignedAttributesKey] = signatureAttributes.SignedAttributes != null ? Convert.ToBase64String(signatureAttributes.SignedAttributes) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                context.ValidationResult.AddException(this, ex);
            }
        }

        #endregion
    }
}
