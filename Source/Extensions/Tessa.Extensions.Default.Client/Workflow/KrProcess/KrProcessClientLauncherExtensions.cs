﻿using System.Threading.Tasks;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.UI;
using Tessa.UI.Cards;

namespace Tessa.Extensions.Default.Client.Workflow.KrProcess
{
    public static class KrProcessClientLauncherExtensions
    {
        public static async Task<IKrProcessLaunchResult> LaunchWithCardEditorAsync(
            this IKrProcessLauncher launcher,
            KrProcessInstance krProcess,
            ICardEditorModel cardEditor,
            bool raiseErrorWhenExecutionIsForbidden)
        {
            krProcess = KrProcessBuilder
                .ModifyProcess(krProcess)
                .Build();

            await using (UIContext.Create(new UIContext(cardEditor)))
            {
                var specificParam = new KrProcessClientLauncher.SpecificParameters
                {
                    UseCurrentCardEditor = true,
                    RaiseErrorWhenExecutionIsForbidden = raiseErrorWhenExecutionIsForbidden
                };
                return await launcher.LaunchAsync(krProcess, null, specificParam);
            }
        }
    }
}