﻿using System;
using System.Threading.Tasks;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess.ClientCommandInterpreter;
using Tessa.UI;

namespace Tessa.Extensions.Default.Client.Workflow.KrProcess.CommandInterpreter
{
    /// <summary>
    /// Обработчик клиентской команды <see cref="DefaultCommandTypes.OpenCard"/>.
    /// </summary>
    public sealed class OpenCardClientCommandHandler : ClientCommandHandlerBase
    {
        #region Fields

        private readonly IUIHost uiHost;

        #endregion

        #region Constructor

        public OpenCardClientCommandHandler(
            IUIHost uiHost)
        {
            this.uiHost = uiHost;
        }

        #endregion

        #region Base Overrides

        /// <inheritdoc/>
        public override async Task Handle(
            IClientCommandHandlerContext context)
        {
            var command = context.Command;
            if (command.Parameters.TryGetValue("cardID", out var cardIDObj)
                && cardIDObj is Guid cardID)
            {
                await DispatcherHelper.InvokeInUIAsync(async () =>
                {
                    await this.uiHost.OpenCardAsync(cardID, cancellationToken: context.CancellationToken);
                });
            }
        }

        #endregion
    }
}