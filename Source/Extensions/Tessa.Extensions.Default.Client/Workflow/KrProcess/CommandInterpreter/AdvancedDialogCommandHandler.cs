﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Tessa.Cards;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess.ClientCommandInterpreter;
using Tessa.Extensions.Platform.Client.UI;
using Tessa.Files;
using Tessa.Platform;
using Tessa.Platform.Runtime;
using Tessa.Platform.Validation;
using Tessa.UI;
using Tessa.UI.Cards;

namespace Tessa.Extensions.Default.Client.Workflow.KrProcess.CommandInterpreter
{
    /// <summary>
    /// Базовый класс обработчика клиентской команды отображения диалога.
    /// </summary>
    public abstract class AdvancedDialogCommandHandler : ClientCommandHandlerBase
    {
        #region Fields

        private readonly Func<IAdvancedCardDialogManager> createAdvancedCardDialogManager;

        private readonly ISession session;

        private readonly Func<IUIHost> createUIHost;

        private readonly Func<ICardEditorModel> createCardEditor;

        #endregion

        #region Constructor

        protected AdvancedDialogCommandHandler(
            Func<IAdvancedCardDialogManager> createAdvancedCardDialogManager,
            ISession session,
            Func<IUIHost> createUIHost,
            Func<ICardEditorModel> createCardEditor)
        {
            this.createAdvancedCardDialogManager = createAdvancedCardDialogManager;
            this.session = session;
            this.createUIHost = createUIHost;
            this.createCardEditor = createCardEditor;
        }

        #endregion

        #region Base Overrides

        /// <inheritdoc />
        public override Task Handle(
            IClientCommandHandlerContext context)
        {
            var coSettings = PrepareDialogCommand(context);

            if (coSettings is null)
            {
                return Task.CompletedTask;
            }

            var editor = UIContext.Current.CardEditor;
            if (editor != null)
            {
                editor.Info[CardTaskDialogUIExtension.DialogNonTaskCompletionOptionSettingsKey] = coSettings;
                editor.Info[CardTaskDialogUIExtension.OnDialogButtonPressedKey] =
                    (Func<ICardEditorModel, CardTaskCompletionOptionSettings, string, bool, Task>)(
                        async (
                            dialogCardEditor,
                            cos,
                            buttonName,
                            completeTask) => await this.CompleteDialogAsync(
                                dialogCardEditor,
                                editor,
                                cos,
                                buttonName,
                                completeTask,
                                context));
            }
            else
            {
                var _ = this.ShowGlobalDialogAsync(
                    coSettings,
                    context);
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Protected Abstract Methods

        /// <summary>
        /// Метод для подготовки диалога для выполнения.
        /// </summary>
        /// <param name="context">Контекст обработки клиентской команды.</param>
        /// <returns>Возвращает информацию для формирования диалога, или null, если не невозможно сформировать диалог.</returns>
        protected abstract CardTaskCompletionOptionSettings PrepareDialogCommand(IClientCommandHandlerContext context);

        /// <summary>
        /// Метод выполнения диалога.
        /// </summary>
        /// <param name="actionResult">Результат выполнения диалога.</param>
        /// <param name="context">Контекст команды диалога.</param>
        /// <param name="cardEditor">Редактор карточки диалога.</param>
        /// <param name="parentCardEditor">Редактор карточки, для которой открывается диалог, если диалог открывается в рамках карточки.</param>
        /// <returns>Возвращает true, если необходимо закрыть диалог, иначе false.</returns>
        protected abstract ValueTask<bool> CompleteDialogCoreAsync(
            CardTaskDialogActionResult actionResult,
            IClientCommandHandlerContext context,
            ICardEditorModel cardEditor,
            ICardEditorModel parentCardEditor = null);

        #endregion

        #region Private

        /// <summary>
        /// Отображает карточку в окне диалога.
        /// </summary>
        /// <param name="coSettings">Параметры диалога.</param>
        /// <param name="context">Контекст обработки клиентской комманды.</param>
        /// <returns>Асинхронная задача.</returns>
        private async Task ShowGlobalDialogAsync(
            CardTaskCompletionOptionSettings coSettings,
            IClientCommandHandlerContext context)
        {
            var info = coSettings.Info;
            if (coSettings.PreparedNewCard != null
                && coSettings.PreparedNewCardSignature != null)
            {
                info[CardHelper.NewCardBilletKey] = coSettings.PreparedNewCard;
                info[CardHelper.NewCardBilletSignatureKey] = coSettings.PreparedNewCardSignature;
            }
            
            info[CardTaskDialogHelper.StoreMode] = Int32Boxes.Box((int)coSettings.StoreMode);

            await using (UIContext.Create(new UIContext(this.createCardEditor(), this.createAdvancedCardDialogManager().CreateUIContextActionOverridings())))
            {
                await CreateNewCardAsync(
                    this.createUIHost(),
                    coSettings,
                    info,
                    modifyEditorActionAsync: async (ICardEditorModel cardEditor, CancellationToken ct) =>
                    {
                        cardEditor.Context.SetDialogClosingAction((uiCtx, eventArgs) =>
                        {
                            if (uiCtx.CardEditor.OperationInProgress)
                            {
                                eventArgs.Cancel = true;
                            }
                            else if (!uiCtx.CardEditor.IsClosed)
                            {
                                bool dialogResult = Keyboard.Modifiers.HasNot(ModifierKeys.Alt)
                                    && TessaDialog.Confirm("$KrProcess_Dialog_ConfirmClose");

                                if (!dialogResult)
                                {
                                    eventArgs.Cancel = true;
                                }
                            }

                            return TaskBoxes.False;
                        });
                        this.PrepareDialog(cardEditor, coSettings, context);
                    },
                    cancellationToken: context.CancellationToken).ConfigureAwait(false);
            }
        }
        
         private void PrepareDialog(
            ICardEditorModel editor,
            CardTaskCompletionOptionSettings coSettings,
            IClientCommandHandlerContext context)
         {
            editor.StatusBarIsVisible = false;
            if (coSettings.DialogName != null)
            {
                editor.DialogName = coSettings.DialogName;
            }

            var toolbarActions = editor.Toolbar.Actions;
            var bottomActions = editor.BottomToolbar.Actions;
            var bottomButtons = editor.BottomDialogButtons;

            toolbarActions.Clear();
            bottomActions.Clear();
            bottomButtons.Clear();

            foreach (var actionInfo in coSettings.Buttons)
            {
                var name = actionInfo.Name;
                var cancel = actionInfo.Cancel;
                var completeDialog = actionInfo.CompleteDialog;

                object icon = null;
                if (!string.IsNullOrEmpty(actionInfo.Icon))
                {
                    icon = editor.Toolbar.CreateIcon(actionInfo.Icon);
                }

                Func<object, bool> canExecuteButtonAction = (obj) => !editor.BlockingOperationInProgress;

                switch (actionInfo.CardButtonType)
                {
                    case CardButtonType.ToolbarButton:
                        Func<UIButton, ValueTask> topActionAsync = GetButtonActionAsync(name, cancel, completeDialog);
                        toolbarActions.Add(new CardToolbarAction(
                            name,
                            actionInfo.Caption,
                            icon,
                            // ReSharper disable once AccessToModifiedClosure
                            new DelegateCommand(async _ => await topActionAsync(null), canExecuteButtonAction),
                            order: actionInfo.Order
                        ));
                        break;
                    case CardButtonType.BottomToolbarButton:
                        Func<UIButton, ValueTask> bottomActionAsync = GetButtonActionAsync(name, cancel, completeDialog);
                        bottomActions.Add(new CardToolbarAction(
                            name,
                            actionInfo.Caption,
                            icon,
                            // ReSharper disable once AccessToModifiedClosure
                            new DelegateCommand(async p => await bottomActionAsync(null), canExecuteButtonAction),
                            order: actionInfo.Order
                        ));
                        break;
                    case CardButtonType.BottomDialogButton:
                        bottomButtons.Add(new UIButton(
                            actionInfo.Caption,
                            GetButtonActionAsync(name, cancel, completeDialog),
                            () => canExecuteButtonAction(default)
                        ));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            Func<UIButton, ValueTask> GetButtonActionAsync(string name, bool cancel, bool completeDialog)
            {
                return !cancel
                    ? (Func<UIButton, ValueTask>) (async _ => await this.CompleteDialogAsync(editor, null, coSettings, name, completeDialog, context))
                    : async _ => await CancelCommand(editor);
            }
        }

        private static async Task CancelCommand(ICardEditorModel editor)
        {
            await editor.CloseAsync();
        }

        private async Task CompleteDialogAsync(
            ICardEditorModel dialogCardEditor,
            ICardEditorModel parentCardEditor,
            CardTaskCompletionOptionSettings coSettings,
            string buttonName,
            bool completeDialog,
            IClientCommandHandlerContext context)
        {
            bool closeDialog = false;
            using (dialogCardEditor.SetOperationInProgress(true))
            {
                var dialogCard = dialogCardEditor.CardModel.Card.Clone();
                var files = dialogCardEditor.CardModel.FileContainer.Files;

                var validationResult = await this.PrepareFilesForStoreAsync(
                    dialogCard,
                    files);

                await TessaDialog.ShowNotEmptyAsync(validationResult);
                if (validationResult.HasErrors)
                {
                    return;
                }

                var actionResult = new CardTaskDialogActionResult
                {
                    MainCardID = default,
                    PressedButtonName = buttonName,
                    StoreMode = coSettings.StoreMode,
                    KeepFiles = coSettings.KeepFiles,
                    CompleteDialog = completeDialog,
                };
                actionResult.SetDialogCard(dialogCard);

                closeDialog = await this.CompleteDialogCoreAsync(actionResult, context, dialogCardEditor, parentCardEditor);
            }
            if (closeDialog)
            {
                await dialogCardEditor.CloseAsync();
            }
        }

        /// <summary>
        /// Асинхронно задаёт контент указанных файлов в соответствующие <see cref="CardFile.Info"/> карточки файлов.
        /// </summary>
        /// <param name="dialogCard">Карточка диалога.</param>
        /// <param name="files">Коллекция файлов.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Результат выполнения операции, агрегированный для всех файлов.</returns>
        private async Task<ValidationResult> PrepareFilesForStoreAsync(
            Card dialogCard,
            ICollection<IFile> files,
            CancellationToken cancellationToken = default)
        {
            var validationResults = new ValidationResultBuilder();
            validationResults.Add(await files.EnsureAllContentModifiedAsync(cancellationToken));
            
            if (validationResults.IsSuccessful())
            {
                validationResults.Add(await CardTaskDialogHelper.SetFileContentToInfoAsync(dialogCard, files, this.session, cancellationToken));
            }

            return validationResults.Build();
        }

        /// <summary>
        /// Асинхронно создаёт и открывает карточку в диалоге. Карточка создаётся в режиме по умолчанию или по шаблону.
        /// </summary>
        /// <param name="uiHost">Объект, предоставляющий упрощённый доступ к основным функциям платформы, которые связаны с отображением информации пользователю.</param>
        /// <param name="completionOptionSettings">Параметры этапа.</param>
        /// <param name="info">Дополнительная информация для расширений или <c>null</c>, если дополнительная информация не требуется.</param>
        /// <param name="modifyCardActionAsync">Метод, изменяющий созданную карточку, или <c>null</c>, если изменять карточку не требуется.</param>
        /// <param name="modifyEditorActionAsync">Метод, изменяющий редактор созданной карточки, или <c>null</c>, если изменять редактор не требуется.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Асинхронная задача.</returns>
        private static async Task CreateNewCardAsync(
            IUIHost uiHost,
            CardTaskCompletionOptionSettings completionOptionSettings,
            Dictionary<string, object> info = default,
            Func<Card, CancellationToken, ValueTask> modifyCardActionAsync = default,
            Func<ICardEditorModel, CancellationToken, ValueTask> modifyEditorActionAsync = default,
            CancellationToken cancellationToken = default)
        {
            switch (completionOptionSettings.CardNewMethod)
            {
                case CardTaskDialogNewMethod.Default:
                    var createCardOptions = new CreateCardOptions
                    {
                        Info = info,
                        DisplayValue = completionOptionSettings.DisplayValue,
                        UIContext = UIContext.Current
                    };

                    if (modifyCardActionAsync != null)
                    {
                        createCardOptions.CardModelModifierActionAsync = (ctx) => modifyCardActionAsync(ctx.Card, ctx.CancellationToken);
                    }

                    if (modifyEditorActionAsync != null)
                    {
                        createCardOptions.CardEditorModifierActionAsync = (ctx) => modifyEditorActionAsync(ctx.Editor, ctx.CancellationToken);
                    }

                    await uiHost.CreateCardAsync(
                        completionOptionSettings.DialogTypeID,
                        options: createCardOptions,
                        cancellationToken: cancellationToken).ConfigureAwait(false);
                    break;
                case CardTaskDialogNewMethod.Template:
                    await uiHost.CreateFromTemplateAsync(
                        completionOptionSettings.DialogTypeID,
                        info,
                        modifyCardActionAsync: modifyCardActionAsync,
                        modifyEditorActionAsync: modifyEditorActionAsync,
                        options: new OpenCardOptions
                        {
                            DisplayValue = completionOptionSettings.DisplayValue,
                            UIContext = UIContext.Current
                        },
                        cancellationToken: cancellationToken).ConfigureAwait(false);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(
                        nameof(CardTaskCompletionOptionSettings.CardNewMethod),
                        completionOptionSettings.CardNewMethod,
                        default);
            }
        }

        #endregion

    }
}