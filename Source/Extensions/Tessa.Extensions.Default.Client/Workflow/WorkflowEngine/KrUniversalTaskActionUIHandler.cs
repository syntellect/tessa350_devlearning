﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading;
using System.Threading.Tasks;

using Tessa.Cards;
using Tessa.Extensions.Default.Client.UI;
using Tessa.Extensions.Default.Shared.Workflow.WorkflowEngine;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Tessa.Scheme;
using Tessa.UI;
using Tessa.UI.Cards;
using Tessa.UI.Cards.Controls;
using Tessa.UI.WorkflowViewer.Actions;
using Tessa.Workflow;
using Tessa.Workflow.Actions;
using Tessa.Workflow.Storage;

using static Tessa.Extensions.Default.Shared.Workflow.WorkflowEngine.WorkflowConstants;

namespace Tessa.Extensions.Default.Client.Workflow.WorkflowEngine
{
    /// <summary>
    /// Описывает логику пользовательского инфтерфейса для действия <see cref="KrDescriptors.KrUniversalTaskDescriptor"/>.
    /// </summary>
    public sealed class KrUniversalTaskActionUIHandler : WorkflowActionUIHandlerBase
    {
        #region Fields

        private static readonly ImmutableHashSet<string> exceptSectionFieldNames =
            ExceptSectionFieldNames.Union(
                new string[]
                {
                    KrUniversalTaskActionButtonsVirtual.Caption,
                    KrUniversalTaskActionButtonsVirtual.IsShowComment,
                    KrUniversalTaskActionButtonsVirtual.IsAdditionalOption,
                    KrUniversalTaskActionButtonsVirtual.Digest,
                });

        private readonly ICardMetadata cardMetadata;
        private readonly TableRowContentIndicator[] tableIndicators = new TableRowContentIndicator[2];

        #endregion

        #region Constuctors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="KrUniversalTaskActionUIHandler"/>.
        /// </summary>
        /// <param name="cardMetadata">Репозиторий с метаинформацией.</param>
        public KrUniversalTaskActionUIHandler(
            ICardMetadata cardMetadata)
            : base(KrDescriptors.KrUniversalTaskDescriptor)
        {
            this.cardMetadata = cardMetadata;
        }

        #endregion

        #region Base overrides

        /// <inheritdoc />
        protected override async Task AttachToCardCoreAsync(WorkflowEngineBindingContext bindingContext)
        {
            if (bindingContext.ActionTemplate != null)
            {
                bindingContext.Section = bindingContext.Card.Sections.GetOrAdd(KrUniversalTaskActionVirtual.SectionName);
                bindingContext.SectionMetadata = (await bindingContext.CardMetadata.GetSectionsAsync(bindingContext.CancellationToken).ConfigureAwait(false))[KrUniversalTaskActionVirtual.SectionName];

                await this.AttachFieldToTemplateAsync(
                    bindingContext,
                    KrUniversalTaskActionVirtual.InitTaskScript,
                    typeof(string),
                    KrUniversalTaskActionVirtual.SectionName,
                    KrUniversalTaskActionVirtual.InitTaskScript);
            }

            await this.AttachEntrySectionAsync(
                bindingContext,
                KrUniversalTaskActionVirtual.SectionName,
                KrUniversalTaskActionVirtual.SectionName);

            await this.AttachTableSectionAsync(
                bindingContext,
                KrUniversalTaskActionButtonsVirtual.SectionName,
                KrUniversalTaskActionButtonsVirtual.SectionName);

            await this.AttachTableSectionToTemplateAsync(
                bindingContext,
                KrUniversalTaskActionButtonLinksVirtual.SectionName,
                KrUniversalTaskActionButtonLinksVirtual.SectionName);

            await this.AttachTableSectionAsync(
                bindingContext,
                KrUniversalTaskActionNotificationRolesVitrual.SectionName,
                KrUniversalTaskActionNotificationRolesVitrual.SectionName);

            await this.AttachTableSectionToTemplateAsync(
                bindingContext,
                WorkflowTaskActionBase.EventsSectionName,
                WorkflowTaskActionBase.EventsSectionName);

            this.AddLinkBinding(
                bindingContext,
                new[] { KrUniversalTaskActionButtonLinksVirtual.Link, Names.Table_ID },
                KrUniversalTaskActionButtonLinksVirtual.SectionName);
        }

        /// <inheritdoc />
        protected override async Task UpdateFormCoreAsync(
            WorkflowStorageBase action,
            WorkflowStorageBase node,
            WorkflowStorageBase process,
            ICardModel cardModel,
            WorkflowActionStorage actionTemplate = null,
            CancellationToken cancellationToken = default)
        {
            var card = cardModel.Card;
            var mainSection = card.Sections[KrUniversalTaskActionVirtual.SectionName];

            mainSection.FieldChanged += this.MainSection_FieldChanged;

            var completionOptionsTable = (GridViewModel)cardModel.Controls[WorkflowConstants.UI.CompletionOptionsTable];
            completionOptionsTable.RowAdding += this.CompletionOptionsTable_RowAdding;
            completionOptionsTable.RowEditorClosing += this.CompletionOptionsTable_RowEditorClosing;

            this.tableIndicators[0] = await TableRowContentIndicator.InitializeAndStartTrackingAsync(
                cardModel,
                this.cardMetadata,
                WorkflowConstants.UI.CompletionOptionsTable,
                exceptSectionNames: ExceptSectionNames,
                exceptSectionFieldNames: new Dictionary<string, ICollection<string>>(StringComparer.Ordinal)
                {
                    {
                        KrUniversalTaskActionButtonsVirtual.SectionName,
                        exceptSectionFieldNames
                    }
                },
                cancellationToken: cancellationToken).ConfigureAwait(false);

            this.tableIndicators[1] = await TableRowContentIndicator.InitializeAndStartTrackingAsync(
                cardModel,
                this.cardMetadata,
                WorkflowConstants.UI.ActionEventsTable,
                exceptSectionFieldNames: new Dictionary<string, ICollection<string>>(StringComparer.Ordinal)
                {
                    {
                        WorkflowTaskActionBase.EventsSectionName,
                        ExceptSectionFieldNames
                    }
                },
                cancellationToken: cancellationToken).ConfigureAwait(false);
        }

        /// <inheritdoc/>
        protected override async Task InvalidateFormCore(ICardModel cardModel, CancellationToken cancellationToken = default)
        {
            await base.InvalidateFormCore(cardModel, cancellationToken);

            if (cardModel is null)
            {
                return;
            }

            var card = cardModel.Card;
            var mainSection = card.Sections[KrUniversalTaskActionVirtual.SectionName];
            mainSection.FieldChanged -= this.MainSection_FieldChanged;

            var completionOptionsTable = (GridViewModel)cardModel.Controls[WorkflowConstants.UI.CompletionOptionsTable];
            completionOptionsTable.RowAdding -= this.CompletionOptionsTable_RowAdding;
            completionOptionsTable.RowEditorClosing -= this.CompletionOptionsTable_RowEditorClosing;

            foreach (var tableIndicator in this.tableIndicators)
            {
                tableIndicator.StopTracking();
            }
        }

        #endregion

        #region Private methods

        private void MainSection_FieldChanged(object sender, CardFieldChangedEventArgs e)
        {
            var mainSectionInner = (CardSection)sender;
            switch (e.FieldName)
            {
                case KrUniversalTaskActionVirtual.Period when e.FieldValue != null:
                    mainSectionInner.Fields[KrUniversalTaskActionVirtual.Planned] = default;
                    break;
                case KrUniversalTaskActionVirtual.Planned when e.FieldValue != null:
                    mainSectionInner.Fields[KrUniversalTaskActionVirtual.Period] = default;
                    break;
            }
        }

        /// <summary>
        /// Обработчик события <see cref="GridViewModel.RowEditorClosing"/>. Проверяет корректность заполнения парамтеров вариантов завершения действия.
        /// </summary>
        /// <param name="sender">Источник события.</param>
        /// <param name="e">Информация о событии.</param>
        private void CompletionOptionsTable_RowEditorClosing(object sender, GridRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.TryGet<string>(KrUniversalTaskActionButtonsVirtual.Caption)))
            {
                TessaDialog.ShowNotEmpty(
                    ValidationResult.FromText(
                        this,
                        Localization.LocalizationManager.Format("$KrActions_UniversalTask_CompletionOptionCaptionEmpty"),
                        ValidationResultType.Error));
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Обработчик события <see cref="GridViewModel.RowAdding"/>. Выполняет инициализацию строки содержащую параметры настраиваемого варианта завершения.
        /// </summary>
        /// <param name="sender">Источник события.</param>
        /// <param name="e">Информация о событии.</param>
        private void CompletionOptionsTable_RowAdding(object sender, GridRowAddingEventArgs e)
        {
            var rowFields = e.Row.Fields;
            rowFields[KrUniversalTaskActionButtonsVirtual.OptionID] = rowFields[CardRow.RowIDKey];
        }

        #endregion
    }
}
