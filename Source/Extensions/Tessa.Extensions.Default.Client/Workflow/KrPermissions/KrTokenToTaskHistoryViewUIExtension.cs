﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Tessa.Cards;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Localization;
using Tessa.Platform;
using Tessa.Platform.Collections;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Tessa.UI;
using Tessa.UI.Cards;
using Tessa.UI.Cards.Controls;
using Tessa.UI.Cards.Tasks;
using Tessa.UI.Controls;
using Tessa.UI.Menu;
using Tessa.UI.Views.Content;
using Tessa.Views;
using Tessa.Views.Metadata;
using Tessa.Views.Metadata.Criteria;

namespace Tessa.Extensions.Default.Client.Workflow.KrPermissions
{
    public sealed class KrTokenToTaskHistoryViewUIExtension :
        CardUIExtension
    {
        #region Contructor

        public KrTokenToTaskHistoryViewUIExtension() { }

        #endregion

        #region base overrides

        public override async Task Initializing(ICardUIExtensionContext context)
        {
            ValidationResult result = await CardHelper
                .ExecuteTypeExtensionsAsync(
                    CardTypeExtensionTypes.MakeViewTaskHistory,
                    context.Card,
                    context.Model.CardMetadata,
                    ExecuteInitializingAsync,
                    context,
                    cancellationToken: context.CancellationToken);

            context.ValidationResult.Add(result);
        }

        #endregion

        #region Type extension methods

        private static Task ExecuteInitializingAsync(ITypeExtensionContext typeContext)
        {
            var context = (ICardUIExtensionContext)typeContext.ExternalContext;
            var settings = typeContext.Settings;
            var viewControlAlias = settings.TryGet<string>(CardTypeExtensionSettings.ViewControlAlias);

            context.Model.ControlInitializers.Add((control, m, r, ct) =>
            {
                if (control is CardViewControlViewModel viewControl)
                {
                    if (viewControl.Name == viewControlAlias)
                    {
                        KrToken token = KrToken.TryGet(context.Card.Info);
                        if (token != null)
                        {
                            string tokenString = token.GetStorage().ToSerializable().ToBase64String();

                            IViewParameterMetadata tokenMetadata = viewControl.ViewMetadata.Parameters.FindByName("Token");
                            if (tokenMetadata != null)
                            {
                                RequestParameter tokenParameter = new RequestParameterBuilder()
                                    .WithMetadata(tokenMetadata)
                                    .AddCriteria(new EqualsCriteriaOperator(), tokenString, tokenString)
                                    .AsRequestParameter();
                                viewControl.Parameters.Add(tokenParameter);
                            }
                        }
                    }

                }

                return new ValueTask();
            });

            return Task.CompletedTask;
        }

        #endregion
    }
}
