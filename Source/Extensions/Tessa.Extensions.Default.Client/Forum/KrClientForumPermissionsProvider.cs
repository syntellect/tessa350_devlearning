﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Extensions;
using Tessa.Extensions.Default.Shared.Workflow.KrPermissions;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Forums;
using Tessa.Platform.Validation;
using static Tessa.Forums.ForumPermissionsRequestExtensions;

namespace Tessa.Extensions.Default.Client.Forum
{
    public class KrClientForumPermissionsProvider : ForumPermissionsProvider
    {
        #region Private Fields

        private readonly ICardRepository cardRepository;

        #endregion

        #region Constructors

        public KrClientForumPermissionsProvider(ICardRepository cardRepository)
        {
            this.cardRepository = cardRepository;
        }

        #endregion
        
        public override async Task<(bool result, ValidationResult validationResult)> CheckHasPermissionAddTopicAsync(ICardExtensionContext context, Guid cardID, CancellationToken cancellationToken = default)
        {
            (var responseObject, ValidationResult validationResult) = await this.cardRepository.ForumAddTopicPermissionRequestAsync(
                new ForumPermissionsRequestObject()
                {
                    CardID = cardID,
                });
            
            return (responseObject.IsExistRequiredPermission, validationResult);
        }

        public override async Task<(bool result, ValidationResult validationResult)> CheckHasPermissionIsSuperModeratorAsync(ICardExtensionContext context, Guid cardID, CancellationToken cancellationToken = default)
        {
            (var responseObject, ValidationResult validationResult) = await this.cardRepository.ForumSuperModeratorPermissionRequestAsync(
                new ForumPermissionsRequestObject()
                {
                    CardID = cardID,
                });
            
            
            return (responseObject.IsExistRequiredPermission, validationResult);
        }
        
        public override async Task<(bool result, ValidationResult validationResult)> CheckHasPermissionIsEditMyMessagesAsync(
            ICardExtensionContext context,
            Guid cardID,
            Guid topicID,
            CancellationToken cancellationToken = default)
        {
            (var responseObject, ValidationResult validationResult) = await this.cardRepository.ForumEditMyMessagesPermissionRequestAsync(
                new ForumPermissionsRequestObject()
                {
                    CardID = cardID,
                });
            
            
            return (responseObject.IsExistRequiredPermission, validationResult);
        }

        public override async Task<(bool result, ValidationResult validationResult)> CheckHasPermissionIsEditAllMessagesAsync(
            ICardExtensionContext context,
            Guid cardID,
            Guid topicID,
            CancellationToken cancellationToken = default)
        {
            (var responseObject, ValidationResult validationResult) = await this.cardRepository.ForumEditAllMessagesPermissionRequestAsync(
                new ForumPermissionsRequestObject()
                {
                    CardID = cardID,
                });
            
            
            return (responseObject.IsExistRequiredPermission, validationResult);
        }

        public override bool IsEnableAddTopic(Card card)
        {
            // если карточка не содержет токен, возвражаем true, так как карточка не входит в типовое решение.
            // если токен есть, смотрим если ли там нужные права
            return !this.TryGetKrToken(card, out var krToken) 
                || krToken.HasPermission(KrPermissionFlagDescriptors.AddTopics);
        }

        public override bool IsEnableSuperModeratorMode(Card card)
        {
            // если карточка не содержет токен, возвражаем true, так как карточка не входит в типовое решение.
            // если токен есть, смотрим если ли там нужные права
            return !this.TryGetKrToken(card, out var krToken) 
                || krToken.HasPermission(KrPermissionFlagDescriptors.SuperModeratorMode);
        }

        public override bool IsExistToken(Card card)
        {
            var krToken = KrToken.TryGet(card.Info);
            return krToken != null;
        }
        
        public override bool IsEnableEditMyMessages(Dictionary<string, object> token)
        {
            var krToken = new KrToken(token);
            return krToken.HasPermission(KrPermissionFlagDescriptors.EditMyMessages);
        }
        
        public override bool IsEnableEditAllMessages(Dictionary<string, object> token)
        {
            var krToken = new KrToken(token);
            return krToken.HasPermission(KrPermissionFlagDescriptors.EditAllMessages);
        }

        private bool TryGetKrToken(Card card, out KrToken krToken)
        {
            krToken = KrToken.TryGet(card.Info);
            return krToken != null;
        }
    }
}
