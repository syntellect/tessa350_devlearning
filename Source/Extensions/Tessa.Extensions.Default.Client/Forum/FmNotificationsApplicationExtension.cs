﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Forums;
using Tessa.Forums.ForumNotification;
using Tessa.Platform.Licensing;
using Tessa.Platform.Runtime;
using Tessa.UI;
using Tessa.UI.Controls.Forums;

namespace Tessa.Extensions.Default.Client.Forum
{
    internal class FmNotificationsApplicationExtension : ApplicationExtension
    {
        #region Private Fields

        private Timer timer;

        private readonly INotificationButtonUIManager manager;

        private readonly IUserSettings userSettings;

        private readonly IForumServerSettings forumServerSettings;

        private readonly ILicenseManager licenseManager;

        private readonly IFmNotificationProvider notificationProvider;
        
        private readonly FmSessionExecutor fmSessionExecutor;

        private int forumRefreshInterval;
        
        /// <summary>
        /// Флаг, по которумы мы понимаем выполняется ли сейчас запрос (обработка этого запроса) к серверу
        /// </summary>
        private int isProcessingRequest;
        
        #endregion

        #region Constructors

        public FmNotificationsApplicationExtension(
            INotificationButtonUIManager manager,
            IUserSettings userSettings,
            IForumServerSettings forumServerSettings,
            ILicenseManager licenseManager,
            IFmNotificationProvider notificationProvider,
            FmSessionExecutor fmSessionExecutor)
        {
            this.manager = manager;
            this.userSettings = userSettings;
            this.forumServerSettings = forumServerSettings;
            this.licenseManager = licenseManager;
            this.fmSessionExecutor = fmSessionExecutor;
            this.notificationProvider = notificationProvider;

        }

        #endregion

        #region Private Methods

        private async void CheckNotificationsInSeparateSessionAsync(CancellationToken cancellationToken = default)
        {
            // если isProcessingRequest == (0) false то записываем в isProcessingRequest = 1 
            if (Interlocked.CompareExchange(ref this.isProcessingRequest, 1, 0) == 0)
            {
                try
                {
                    // получаем данные представления
                    await this.fmSessionExecutor.TryExecuteInSeparateSessionAsync(
                        this.CheckNotificationsAsync,
                        ApplicationIdentifiers.TessaClientNotifications,
                        cancellationToken).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    await this.manager.ShowErrorAsync(ex);
                }
                finally
                {
                    Interlocked.Exchange(ref this.isProcessingRequest, 0);
                }
            }
        }

        private async Task<bool> CheckNotificationsAsync(
            IFmNotificationProvider notificationProviderInSeparateSession,
            CancellationToken cancellationToken = default)
        {
            // Обрабатываем случай, у пользователя были включены уведомления и он их отключил.
            // Таймер крутится, но не обновляет ленту уведомлений.
            if (!this.userSettings.EnableMessageIndicator)
            {
                return false;
            }
            
            // Для обновления индикатора в фоновом режиме, обновляем его в в новой сессии, для этого подменяем notificationProvider
            this.manager.SetNotificationProvider(notificationProviderInSeparateSession);
            
            if (await this.manager.IsExistNotificationsAsync(cancellationToken))
            {
                await this.manager.ShowNotificationButtonAsync(false, true, cancellationToken);
            }
            this.manager.SetNotificationProvider(this.notificationProvider);
            return true;
        }

        #endregion

        #region BaseOverride

        public override async Task Initialize(IApplicationExtensionContext context)
        {
            // проверяем this.userSettings.EnableMessageIndicator, если отключен, то выходим.
            // Но при этом надо учесть момент, что для того чтобы включить индикатор, необходим перезапуск приложения.

            if (LicensingHelper.CheckForumLicense(await licenseManager.GetLicenseAsync(context.CancellationToken), out _))
            {
                if (!this.userSettings.DoNotShowMessageIndicatorOnStartup &&
                    this.userSettings.EnableMessageIndicator &&
                    await this.manager.IsExistNotificationsAsync(context.CancellationToken))
                {
                    await this.manager.ShowNotificationButtonAsync(true, false, context.CancellationToken);
                }

                if (this.timer != null)
                {
                    await this.timer.DisposeAsync();
                }

                this.forumRefreshInterval = await this.forumServerSettings.GetForumRefreshIntervalAsync(context.CancellationToken);

                // таймер запускаем с задержкой в period, т.к. выше уже выполнялась проверка IsExistNotificationsAsync
                long period = forumRefreshInterval * 1000L;

                this.timer = new Timer(
                    p => this.CheckNotificationsInSeparateSessionAsync(),
                    null,
                    period,
                    period);
            }
        }

        public override async Task Shutdown(IApplicationExtensionContext context)
        {
            if (this.timer != null)
            {
                await this.timer.DisposeAsync();
            }

            this.timer = null;
        }

        #endregion
    }
}