﻿using System;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Caching;

// ReSharper disable InconsistentNaming

namespace Tessa.Extensions.Default.Client.EDS
{
    public class CryptoProEDSManager : CAdESManager
    {
        #region algo constants
        
        ///<summary>
        ///  	Алгоритм ГОСТ Р 34.11-94.
        ///</summary>
        private const int CADESCOM_HASH_ALGORITHM_CP_GOST_3411 = 100;

        ///<summary>
        ///  	Алгоритм ГОСТ Р 34.11-2012.
        ///</summary>
        private const int CADESCOM_HASH_ALGORITHM_CP_GOST_3411_2012_256 = 101;

        ///<summary>
        ///  	Алгоритм ГОСТ Р 34.11-2012 HMAC.
        ///</summary>
        private const int CADESCOM_HASH_ALGORITHM_CP_GOST_3411_2012_256_HMAC = 111;

        ///<summary>
        ///  	Алгоритм ГОСТ Р 34.11-2012.
        ///</summary>
        private const int CADESCOM_HASH_ALGORITHM_CP_GOST_3411_2012_512 = 102;

        ///<summary>
        ///  	Алгоритм ГОСТ Р 34.11-2012 HMAC.
        ///</summary>
        private const int CADESCOM_HASH_ALGORITHM_CP_GOST_3411_2012_512_HMAC = 112;

        ///<summary>
        ///  	Алгоритм ГОСТ Р 34.11-94 HMAC.
        ///</summary>
        private const int CADESCOM_HASH_ALGORITHM_CP_GOST_3411_HMAC = 110;

        ///<summary>
        ///  	Алгоритм MD2.
        ///</summary>
        private const int CADESCOM_HASH_ALGORITHM_MD2 = 1;

        ///<summary>
        ///  	Алгоритм MD4.
        ///</summary>
        private const int CADESCOM_HASH_ALGORITHM_MD4 = 2;

        ///<summary>
        ///  	Алгоритм MD5.
        ///</summary>
        private const int CADESCOM_HASH_ALGORITHM_MD5 = 3;

        ///<summary>
        ///  	Алгоритм SHA1 с длиной ключа 256 бит.
        ///</summary>
        private const int CADESCOM_HASH_ALGORITHM_SHA_256 = 4;

        ///<summary>
        ///  	Алгоритм SHA1 с длиной ключа 384 бита.
        ///</summary>
        private const int CADESCOM_HASH_ALGORITHM_SHA_384 = 5;

        ///<summary>
        ///  	Алгоритм SHA1 с длиной ключа 512 бит.
        ///</summary>
        private const int CADESCOM_HASH_ALGORITHM_SHA_512 = 6;

        ///<summary>
        ///  	Алгоритм SHA1.
        ///</summary>
        private const int CADESCOM_HASH_ALGORITHM_SHA1 = 0;
        
        #endregion

        private const int CAPICOM_CURRENT_USER_STORE = 2;
        
        private const int CADESCOM_BASE64_TO_BINARY = 1;

        public CryptoProEDSManager(ICardRepository cardRepository, ICardCache cardCache) : base(cardRepository, cardCache) { }

        public override byte[] GenerateSignature(byte[] certificate, byte[] file, string hashAlgoOid)
        {
            var certBC = new X509Certificate2(certificate);

            var hashAlgo = GetHashAlgoByOid(hashAlgoOid);

            var oHashedData = GetHashObject(file, hashAlgo);

            var oCertificate = GetSignerCertificate(certBC.SerialNumber);

            var oRawSignature = ComHelper.GetComObj("CAdESCOM.RawSignature");

            var sRawSignature = (string)oRawSignature.Invoke("SignHash", oHashedData, oCertificate);
            var bytes = GetBytesFromHex(sRawSignature);
            Array.Reverse(bytes);

            return bytes;
        }

        public override async Task<(bool success, string errorText)> VerifySignatureAsync(byte[] encodedSignature, byte[] file)
        {
            var errorText = string.Empty;

            var signatureAttributes = await GetAttributesFromSignatureAsync(encodedSignature).ConfigureAwait(false);
            var hashAlgo = GetHashAlgoByOid(signatureAttributes.HashOid);
            var sigbytes = signatureAttributes.Signature.ToArray();
            Array.Reverse(sigbytes);
            var sRawSignature = BitConverter.ToString(sigbytes).Replace("-", string.Empty);
            var hashBytes = signatureAttributes.Hash.ToArray();
            var sRawFileHash = BitConverter.ToString(hashBytes).Replace("-", string.Empty);

            try
            {
                var hashHashData = GetHashObject(file, hashAlgo);
                if ((string)hashHashData.Get("Value") != sRawFileHash)
                {
                    throw new ArgumentException("$UI_Signature_MessageDigestInvalid");
                }

                var oCertificate = ComHelper.GetComObj("CAdESCOM.Certificate");
                oCertificate.Invoke("Import", Convert.ToBase64String(signatureAttributes.Certificate));

                var oHashedData = GetHashObject(signatureAttributes.SignedAttributes, hashAlgo);

                var oRawSignature = ComHelper.GetComObj("CAdESCOM.RawSignature");

                oRawSignature.Invoke("VerifyHash", oHashedData, oCertificate, sRawSignature);

            }
            catch (TargetInvocationException ex)
            {
                errorText = ex.Message;
                var innerMessage = ex.InnerException?.Message;
                if (!string.IsNullOrEmpty(innerMessage))
                {
                    errorText = $"{ex.Message}: {innerMessage}";
                }
                return (false, errorText);
            }

            return (true, errorText);
        }

        private static object GetSignerCertificate(string serialNumber)
        {
            object oStore = ComHelper.GetComObj("CAdESCOM.Store");

            oStore.Invoke("Open", CAPICOM_CURRENT_USER_STORE);

            var certificates = oStore.Get("Certificates");
            var count = (int)(certificates.Get("Count"));
            for (int i = 1; i <= count; i++)
            {
                var cert = certificates.Get("Item", i);
                var certSerial = (string)cert.Get("SerialNumber");
                if (certSerial.Equals(serialNumber, StringComparison.InvariantCultureIgnoreCase))
                {
                    return cert;
                }
            }
            return null;
        }

        private static int GetHashAlgoByOid(string hashOid) =>
            hashOid switch
            {
                "1.2.643.2.2.9" => CADESCOM_HASH_ALGORITHM_CP_GOST_3411,
                "1.2.643.7.1.1.2.2" => CADESCOM_HASH_ALGORITHM_CP_GOST_3411_2012_256,
                "1.2.643.7.1.1.4.1" => CADESCOM_HASH_ALGORITHM_CP_GOST_3411_2012_256_HMAC,
                "1.2.643.7.1.1.2.3" => CADESCOM_HASH_ALGORITHM_CP_GOST_3411_2012_512,
                "1.2.643.7.1.1.4.2" => CADESCOM_HASH_ALGORITHM_CP_GOST_3411_2012_512_HMAC,
                "1.2.643.2.2.10" => CADESCOM_HASH_ALGORITHM_CP_GOST_3411_HMAC,
                "1.2.840.113549.2.2" => CADESCOM_HASH_ALGORITHM_MD2,
                "1.2.840.113549.2.4" => CADESCOM_HASH_ALGORITHM_MD4,
                "1.2.840.113549.2.5" => CADESCOM_HASH_ALGORITHM_MD5,
                "2.16.840.1.101.3.4.2.1" => CADESCOM_HASH_ALGORITHM_SHA_256,
                "2.16.840.1.101.3.4.2.2" => CADESCOM_HASH_ALGORITHM_SHA_384,
                "2.16.840.1.101.3.4.2.3" => CADESCOM_HASH_ALGORITHM_SHA_512,
                "1.3.14.3.2.26" => CADESCOM_HASH_ALGORITHM_SHA1,
                _ => throw new ArgumentException("Unsupported digest algorithm")
            };

        // private int GetHashAlgoByEncAlgo(string keyAlgo) =>
        //     keyAlgo switch
        //     {
        //         RosstandartObjectIdentifiers.id_tc26_gost_3410_12_256_Id => CADESCOM_HASH_ALGORITHM_CP_GOST_3411_2012_256,
        //         RosstandartObjectIdentifiers.id_tc26_gost_3410_12_512_Id => CADESCOM_HASH_ALGORITHM_CP_GOST_3411_2012_512,
        //         CryptoProObjectIdentifiers.GostR3410x94_Id => CADESCOM_HASH_ALGORITHM_CP_GOST_3411,
        //         CryptoProObjectIdentifiers.GostR3410x2001_Id => CADESCOM_HASH_ALGORITHM_CP_GOST_3411,
        //         _ => CADESCOM_HASH_ALGORITHM_SHA1
        //     };

        private static object GetHashObject(byte[] file, int hashAlgo)
        {
            var dataInBase64 = Convert.ToBase64String(file);

            var oHashedData = ComHelper.GetComObj("CAdESCOM.HashedData");
            oHashedData.Set("Algorithm", hashAlgo);
            oHashedData.Set("DataEncoding", CADESCOM_BASE64_TO_BINARY);
            oHashedData.Invoke("Hash", dataInBase64);

            // Получаем хэш-значение
            //var sHashValue = (string)oHashedData.Get("Value");

            return oHashedData;
        }

        private static byte[] GetBytesFromHex(string hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        // private static class RosstandartObjectIdentifiers
        // {
        //     public const string id_tc26_gost_3410_12_256_Id = "1.2.643.7.1.1.1.1";
        //     public const string id_tc26_gost_3410_12_512_Id = "1.2.643.7.1.1.1.2";
        // }
        //
        // private static class CryptoProObjectIdentifiers
        // {
        //     public const string GostR3410x94_Id = "1.2.643.2.2.20";
        //     public const string GostR3410x2001_Id = "1.2.643.2.2.19";
        // }
    }
}
