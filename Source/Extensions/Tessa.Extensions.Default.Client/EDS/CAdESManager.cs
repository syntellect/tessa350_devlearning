﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Caching;
using Tessa.Extensions.Default.Shared;
using Tessa.Extensions.Default.Shared.EDS;
using Tessa.Platform.EDS;
using Tessa.Platform.Validation;

namespace Tessa.Extensions.Default.Client.EDS
{
    public abstract class CAdESManager : ICAdESManager
    {
        #region Constructors

        protected CAdESManager(ICardRepository cardRepository, ICardCache cardCache)
        {
            this.CardRepository = cardRepository;
            this.CardCache = cardCache;
        }

        #endregion

        #region Protected Properties

        protected ICardRepository CardRepository { get; }
        
        protected ICardCache CardCache { get; }

        #endregion

        public virtual async Task<IReadOnlyCollection<SignatureValidationInfo>> CheckExtendedSignatureAsync(SignedData signedData, CancellationToken cancellationToken = default)
        {
            var request = new CardRequest
            {
                RequestType = DefaultRequestTypes.CAdESSignature,
            };

            CAdESSignatureHelper.SetValidationInfo(
                request,
                signedData
            );

            var response = await this.CardRepository.RequestAsync(request, cancellationToken).ConfigureAwait(false);
            var result = response.ValidationResult.Build();
            if (!result.IsSuccessful)
            {
                throw new ValidationException(result);
            }

            return CAdESSignatureHelper.GetValidationInfo(response);
        }

        public virtual (IEDSCertificate certificate, string errorText) DecodeCertificateFromSignature(byte[] encodedSignature)
        {
            try
            {
                var cms = new SignedCms();
                try
                {
                    // Декодируем подпись
                    cms.Decode(encodedSignature);
                }
                catch (CryptographicException e)
                {
                    var errorText = e.Message.Trim();
                    return (null, errorText);
                }
                var certEnum = cms.Certificates.GetEnumerator();
                certEnum.MoveNext();
                var certificate = certEnum.Current;
                return (new EDSCertificate(certificate), null);
            }
            catch (Exception e)
            {
                return (null, e.Message.Trim());
            }
        }

        public virtual async Task<SignedData> ExtendSignatureAsync(byte[] certificate, byte[] signature, CancellationToken cancellationToken = default)
        {
            var request = new CardRequest
            {
                RequestType = DefaultRequestTypes.CAdESSignature,
            };

            CAdESSignatureHelper.SetSigningInfo(
                request,
                signature,
                certificate
            );

            var response = await this.CardRepository.RequestAsync(request, cancellationToken).ConfigureAwait(false);
            var result = response.ValidationResult.Build();
            if (!result.IsSuccessful)
            {
                throw new ValidationException(result);
            }

            return CAdESSignatureHelper.GetSignedData(response);
        }

        public virtual async Task<byte[]> SignDocumentAsync(byte[] certificate, byte[] file)
        {
            var signingTime = DateTime.Now;
            var settingsCard = await this.CardCache.Cards.GetAsync(SignatureHelpers.SignatureSettingsType).ConfigureAwait(false);
            var (digestOid, encryptOid) = SignatureHelpers.GetDigestAndEcryptOid(settingsCard, certificate);
            var toBeSigned = await GetToBeSignedDocumentAsync(certificate, file, signingTime, digestOid, encryptOid).ConfigureAwait(false);
            byte[] signatureValue = GenerateSignature(certificate, toBeSigned, digestOid);

            return await MakeBesSignaturePkcs7Async(certificate, file, signingTime, signatureValue, digestOid, encryptOid).ConfigureAwait(false);
        }

        public async Task<byte[]> GetBesSignatureFromExtendedAsync(byte[] signature, CancellationToken cancellationToken = default)
        {
            var request = new CardRequest
            {
                RequestType = DefaultRequestTypes.CAdESSignature,
            };

            CAdESSignatureHelper.SetGetBESInfo(
                request,
                signature
            );

            var response = await this.CardRepository.RequestAsync(request, cancellationToken).ConfigureAwait(false);
            var result = response.ValidationResult.Build();
            if (!result.IsSuccessful)
            {
                throw new ValidationException(result);
            }

            return CAdESSignatureHelper.GetBESInfo(response);
        }

        public async Task<SignatureAttributes> GetAttributesFromSignatureAsync(byte[] signature, CancellationToken cancellationToken = default)
        {
            var request = new CardRequest
            {
                RequestType = DefaultRequestTypes.CAdESSignature,
            };

            CAdESSignatureHelper.SetGetSignedAttributesInfo(
                request,
                signature
            );

            var response = await this.CardRepository.RequestAsync(request, cancellationToken).ConfigureAwait(false);
            var result = response.ValidationResult.Build();
            if (!result.IsSuccessful)
            {
                throw new ValidationException(result);
            }

            return CAdESSignatureHelper.GetSignatureAttributes(response);
        }

        public async Task<byte[]> GetToBeSignedDocumentAsync(
            byte[] certificate,
            byte[] file,
            DateTime signingTime,
            string digestAlgorithmOid = null,
            string encriptionAlgorithmOid = null,
            CancellationToken cancellationToken = default)
        {
            var request = new CardRequest
            {
                RequestType = DefaultRequestTypes.CAdESSignature,
            };

            CAdESSignatureHelper.SetToBeSignedInfo(
                request,
                certificate,
                file,
                signingTime,
                digestAlgorithmOid,
                encriptionAlgorithmOid
            );

            var response = await this.CardRepository.RequestAsync(request, cancellationToken).ConfigureAwait(false);
            var result = response.ValidationResult.Build();
            if (!result.IsSuccessful)
            {
                throw new ValidationException(result);
            }

            return CAdESSignatureHelper.GetToBeSignedInfo(response);
        }

        public async Task<byte[]> MakeBesSignaturePkcs7Async(
            byte[] certificate,
            byte[] file,
            DateTime signingTime,
            byte[] signature,
            string digestAlgorithmOid = null,
            string encriptionAlgorithmOid = null,
            CancellationToken cancellationToken = default)
        {
            var request = new CardRequest
            {
                RequestType = DefaultRequestTypes.CAdESSignature,
            };

            CAdESSignatureHelper.SetBesSignaturePkcs7Info(
                request,
                certificate,
                file,
                signingTime,
                signature,
                digestAlgorithmOid,
                encriptionAlgorithmOid
            );

            var response = await this.CardRepository.RequestAsync(request, cancellationToken).ConfigureAwait(false);
            var result = response.ValidationResult.Build();
            if (!result.IsSuccessful)
            {
                throw new ValidationException(result);
            }

            return CAdESSignatureHelper.GetBesSignaturePkcs7Info(response);
        }

        public virtual byte[] GenerateSignature(byte[] certificate, byte[] file, string digestOid)
        {
            throw new NotSupportedException();
        }

        public virtual Task<(bool success, string errorText)> VerifySignatureAsync(byte[] encodedSignature, byte[] file)
        {
            throw new NotSupportedException();
        }
    }
}
