﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NLog;
using Tessa.Cards;
using Tessa.Cards.Caching;
using Tessa.Platform.EDS;

namespace Tessa.Extensions.Default.Client.EDS
{
    public class AggregateEDSManager : CAdESManager
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        
        private readonly ICAdESManager cryptoProEdsManager;
        
        private readonly ICAdESManager defaultEdsManager;

        public AggregateEDSManager(
            ICardRepository cardRepository,
            ICardCache cardCache,
            ICAdESManager defaultEdsManager,
            ICAdESManager cryptoProEdsManager)
            : base(cardRepository, cardCache)
        {
            this.cryptoProEdsManager = cryptoProEdsManager;
            this.defaultEdsManager = defaultEdsManager;
        }

        public override byte[] GenerateSignature(byte[] certificate, byte[] file, string digestOid)
        {
            return SignatureHelpers.GostHashes.Contains(digestOid)
                ? this.cryptoProEdsManager.GenerateSignature(certificate, file, digestOid)
                : this.defaultEdsManager.GenerateSignature(certificate, file, digestOid);
        }

        public override async Task<(bool success, string errorText)> VerifySignatureAsync(byte[] encodedSignature, byte[] file)
        {
            try
            {
                var result = await this.cryptoProEdsManager.VerifySignatureAsync(encodedSignature, file);
                if (result.success)
                {
                    return result;
                }
            }
            catch (ArgumentException ex)
            {
                return (false, ex.Message);
            }
            catch (Exception ex)
            {
                logger.Info(ex);
            }

            return await this.defaultEdsManager.VerifySignatureAsync(encodedSignature, file);
        }
    }
}
