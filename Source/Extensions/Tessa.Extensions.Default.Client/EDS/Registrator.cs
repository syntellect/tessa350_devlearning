﻿using Tessa.Cards;
using Tessa.Cards.Caching;
using Tessa.Cards.Extensions;
using Tessa.Platform.EDS;
using Tessa.UI.Cards;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace Tessa.Extensions.Default.Client.EDS
{
    [Registrator]
    public sealed class Registrator : RegistratorBase
    {
        private const string DefaultName = "default";
        
        private const string CryptoProName = "cryptopro";
        
        public override void RegisterUnity()
        {
            this.UnityContainer
                .RegisterType<ICAdESManager, DefaultEDSManager>(DefaultName, new ContainerControlledLifetimeManager())
                .RegisterType<ICAdESManager, CryptoProEDSManager>(CryptoProName, new ContainerControlledLifetimeManager())
                .RegisterType<ICAdESManager, AggregateEDSManager>(
                    new ContainerControlledLifetimeManager(),
                    new InjectionConstructor(
                        typeof(ICardRepository),
                        typeof(ICardCache),
                        new ResolvedParameter<ICAdESManager>(DefaultName),
                        new ResolvedParameter<ICAdESManager>(CryptoProName)))
                ;
        }

        public override void RegisterExtensions(IExtensionContainer extensionContainer)
        {
            extensionContainer
                .RegisterExtension<ICardUIExtension, SignatureSettingsUIExtension>(x => x
                    .WithOrder(ExtensionStage.AfterPlatform, 1)
                    .WithSingleton()
                    .WhenCardTypes(SignatureHelpers.SignatureSettingsType))

                .RegisterExtension<ICardStoreExtension, SignatureSettingsStoreExtension>(x => x
                    .WithOrder(ExtensionStage.AfterPlatform, 1)
                    .WithSingleton()
                    .WhenCardTypes(SignatureHelpers.SignatureSettingsType))
                    ;
        }
    }
}
