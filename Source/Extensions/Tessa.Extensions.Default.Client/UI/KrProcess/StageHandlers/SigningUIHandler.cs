﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Tessa.Cards;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Platform.Storage;
using Tessa.UI.Cards;
using Tessa.UI.Cards.Controls;

namespace Tessa.Extensions.Default.Client.UI.KrProcess.StageHandlers
{
    /// <summary>
    /// Представляет обработчик действий выполняемых с настроками этапа <see cref="StageTypeDescriptors.SigningDescriptor"/>.
    /// </summary>
    public sealed class SigningUIHandler : StageTypeUIHandlerBase
    {
        private CardRow settings;

        private IControlViewModel returnIfNotSignedFlagControl;
        private IControlViewModel returnAfterSigningFlagControl;

        /// <inheritdoc/>
        public override Task Initialize(IKrStageTypeUIHandlerContext context)
        {
            IControlViewModel flagsTabs;
            IFormViewModel tabViewModel;
            IBlockViewModel innerFlagsBlock;

            if (context.RowModel.Blocks.TryGet("SigningStageFlags", out var flagsBlock)
                && (flagsTabs = flagsBlock.Controls.FirstOrDefault(p => p.Name == "FlagsTabs")) != null
                && flagsTabs is TabControlViewModel flagsTabsViewModel)
            {
                if ((tabViewModel = flagsTabsViewModel.Tabs.FirstOrDefault(p => p.Name == "CommonSettings")) != null
                    && (innerFlagsBlock = tabViewModel.Blocks.FirstOrDefault(p => p.Name == "StageFlags")) != null)
                {
                    this.returnIfNotSignedFlagControl = innerFlagsBlock.Controls.FirstOrDefault(p => p.Name == KrConstants.Ui.ReturnIfNotSigned);
                }

                if ((tabViewModel = flagsTabsViewModel.Tabs.FirstOrDefault(p => p.Name == "AdditionalSettings")) != null
                    && (innerFlagsBlock = tabViewModel.Blocks.FirstOrDefault(p => p.Name == "StageFlags")) != null)
                {
                    this.returnAfterSigningFlagControl = innerFlagsBlock.Controls.FirstOrDefault(p => p.Name == KrConstants.Ui.ReturnAfterSigning);
                }
            }

            this.settings = context.Row;
            this.settings.FieldChanged += this.OnSettingsFieldChanged;

            NotReturnEditConfigureFields(this.settings.TryGet<bool>(KrConstants.KrSigningStageSettingsVirtual.NotReturnEdit));

            return Task.CompletedTask;
        }

        /// <inheritdoc/>
        public override Task Finalize(IKrStageTypeUIHandlerContext context)
        {
            this.settings.FieldChanged -= this.OnSettingsFieldChanged;
            this.settings = default;

            return Task.CompletedTask;
        }

        /// <summary>
        /// Обработчик события <see cref="CardRow.FieldChanged"/> строки настроек этапа.
        /// </summary>
        /// <param name="sender">Источник события.</param>
        /// <param name="e">Информация о событии.</param>
        private void OnSettingsFieldChanged(object sender, CardFieldChangedEventArgs e)
        {
            if (e.FieldName == KrConstants.KrSigningStageSettingsVirtual.NotReturnEdit
                && e.FieldValue is bool notReturnEdit)
            {
                this.NotReturnEditConfigureFields(notReturnEdit);
            }
        }

        /// <summary>
        /// Настраивает поля взависимости от значения флага <see cref=" KrConstants.KrSigningStageSettingsVirtual.NotReturnEdit"/>.
        /// </summary>
        /// <param name="isNotReturnEdit">Значение флага <see cref=" KrConstants.KrSigningStageSettingsVirtual.NotReturnEdit"/>.</param>
        private void NotReturnEditConfigureFields(bool isNotReturnEdit)
        {
            if (isNotReturnEdit)
            {
                this.returnIfNotSignedFlagControl.ControlVisibility = Visibility.Collapsed;
                this.returnAfterSigningFlagControl.ControlVisibility = Visibility.Collapsed;
            }
            else
            {
                this.returnIfNotSignedFlagControl.ControlVisibility = Visibility.Visible;
                this.returnAfterSigningFlagControl.ControlVisibility = Visibility.Visible;
            }
        }
    }
}