﻿using System.Threading.Tasks;
using Tessa.Properties.Resharper;
using Tessa.UI.Cards.Controls;
using Tessa.UI.Menu;
using Tessa.Views;

namespace Tessa.Extensions.Default.Client.UI.CardFiles
{
    /// <summary>
    /// Стратегия инициализации модели представления
    /// </summary>
    public class FilesViewCardControlInitializationStrategy : ViewCardControlInitializationStrategy
    {

        #region private fields

        private readonly FilesViewInitializationOptions options;

        #endregion

        #region Public properties

        public FilesViewInitializationOptions Options { get => this.options; }

        #endregion

        #region Constructor

        public FilesViewCardControlInitializationStrategy([NotNull] IViewService viewService,
            [NotNull] CreateMenuContextFunc createMenuContextFunc,
            [NotNull] IViewCardControlContentItemsFactory contentItemsFactory)
            : base(viewService, createMenuContextFunc, contentItemsFactory)
        {
            this.options = new FilesViewInitializationOptions();
        }

        #endregion

        #region Base overrides
        
        public override ValueTask InitializeMatadataAsync(CardViewControlInitializationContext context)
        {
            context.ControlViewModel.ViewMetadata = FilesViewMetadata.Create(this.options);

            return new ValueTask();
        }
        public override ValueTask InitializeDataProviderAsync(CardViewControlInitializationContext context)
        {
            context.ControlViewModel.DataProvider =
                new CardFilesDataProvider(
                    context.ControlViewModel.ViewMetadata,
                    FilesViewGeneratorBaseUIExtension.TryGetFileControl(context.Model.Info, context.ControlViewModel.Name).Items);

            return new ValueTask();
        }

        #endregion
    }
}
