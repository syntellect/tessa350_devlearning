﻿using System;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Extensions.Default.Shared.Cards;
using Tessa.Extensions.Default.Shared.Views;
using Tessa.Platform.Runtime;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Tessa.UI.Cards;
using Tessa.UI.Files;
using Tessa.UI.Views.Content;
using Tessa.Views;

namespace Tessa.Extensions.Default.Client.UI.CardFiles
{
    /// <summary>
    /// Реализация расширения типа карточки для преобразования представления в файловый контрол
    /// </summary>
    public sealed class InitializeFilesViewUIExtension : FilesViewGeneratorBaseUIExtension
    {
        #region Private fields

        private readonly ICardMetadata cardMetadata;

        private readonly FilesViewCardControlInitializationStrategy initializationStrategy;

        #endregion

        #region Constructor

        public InitializeFilesViewUIExtension(
            ISession session,
            IExtensionContainer extensionContainer,
            IViewService viewService,
            ICardMetadata cardMetadata,
            FilesViewCardControlInitializationStrategy initializationStrategy)
            : base(session, extensionContainer, viewService)
        {
            this.initializationStrategy = initializationStrategy ?? throw new ArgumentNullException(nameof(initializationStrategy));
            this.cardMetadata = cardMetadata;
        }

        #endregion

        #region Private methods

        private async Task ExecuteInitializingActionAsync(ITypeExtensionContext context)
        {
            var settings = context.Settings;
            var filesViewAlias = settings.TryGet<string>(DefaultCardTypeExtensionSettings.FilesViewAlias);

            var options = new FileControlCreationParams
            {
                CategoriesViewAlias = settings.TryGet<string>(DefaultCardTypeExtensionSettings.CategoriesViewAlias),
                PreviewControlName = settings.TryGet<string>(DefaultCardTypeExtensionSettings.PreviewControlName),
                IsCategoriesEnabled = settings.TryGet<bool>(DefaultCardTypeExtensionSettings.IsCategoriesEnabled),
                IsIgnoreExistingCategories = settings.TryGet<bool>(DefaultCardTypeExtensionSettings.IsIgnoreExistingCategories),
                IsManualCategoriesCreationDisabled = settings.TryGet<bool>(DefaultCardTypeExtensionSettings.IsManualCategoriesCreationDisabled),
                IsNullCategoryCreationDisabled = settings.TryGet<bool>(DefaultCardTypeExtensionSettings.IsNullCategoryCreationDisabled)
            };

            await InitializeFileControlAsync(context.ExternalContext as ICardUIExtensionContext, filesViewAlias, options);
        }

        private async Task ExecuteInitializedActionAsync(ITypeExtensionContext context)
        {
            var settings = context.Settings;
            var filesViewAlias = settings.TryGet<string>(DefaultCardTypeExtensionSettings.FilesViewAlias);
            this.initializationStrategy.Options.IsCategoryFilterEnabled = settings.TryGet<bool>(DefaultCardTypeExtensionSettings.IsCategoryFilterEnabled);

            var fileControl = await AttachViewToFileControlAsync(
                ((ICardUIExtensionContext) context.ExternalContext).Model,
                filesViewAlias,
                CreateRowFunc,
                this.initializationStrategy,
                cancellationToken: context.CancellationToken);

            var defaultGroup = settings.TryGet<string>(DefaultCardTypeExtensionSettings.DefaultGroup);
            if (!string.IsNullOrEmpty(defaultGroup))
            {
                await fileControl.SelectGroupingAsync(fileControl.Groupings.TryGet(defaultGroup), context.CancellationToken);
            }
        }

        private static ViewControlRowViewModel CreateRowFunc(TableRowCreationOptions options)
        {
            var fileViewModel = (IFileViewModel) options.Data[ColumnsConst.FileViewModel];
            options.Data.Remove(ColumnsConst.FileViewModel);
            
            return new TableFileRowViewModel(fileViewModel, options);
        }

        #endregion

        #region Base overrides

        public override async Task Initializing(ICardUIExtensionContext context)
        {
            ValidationResult result = await CardHelper
                .ExecuteTypeExtensionsAsync(
                    DefaultCardTypeExtensionTypes.InitializeFilesView,
                    context.Card,
                    this.cardMetadata,
                    ExecuteInitializingActionAsync,
                    context,
                    cancellationToken: context.CancellationToken);

            context.ValidationResult.Add(result);
        }

        public override async Task Initialized(ICardUIExtensionContext context)
        {
            ValidationResult result = await CardHelper
                .ExecuteTypeExtensionsAsync(
                    DefaultCardTypeExtensionTypes.InitializeFilesView,
                    context.Card,
                    this.cardMetadata,
                    ExecuteInitializedActionAsync,
                    context);

            context.ValidationResult.Add(result);
        }

        #endregion
    }
}