﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Tessa.Cards;
using Tessa.Extensions.Default.Shared.Views;
using Tessa.Files;
using Tessa.Localization;
using Tessa.Platform.Collections;
using Tessa.Platform.Runtime;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Tessa.UI;
using Tessa.UI.Cards;
using Tessa.UI.Cards.Controls;
using Tessa.UI.Controls;
using Tessa.UI.Files;
using Tessa.UI.Files.Controls;
using Tessa.UI.Menu;
using Tessa.UI.Views.Content;
using Tessa.Views;

namespace Tessa.Extensions.Default.Client.UI.CardFiles
{
    public abstract class FilesViewGeneratorBaseUIExtension : CardUIExtension
    {
        #region private fields & constructor

        /// <inheritdoc />
        protected FilesViewGeneratorBaseUIExtension(
            ISession session,
            IExtensionContainer extensionContainer,
            IViewService viewService)
        {
            this.Session = session ?? throw new ArgumentNullException(nameof(session));
            this.ExtensionContainer = extensionContainer ?? throw new ArgumentNullException(nameof(extensionContainer));
            this.ViewService = viewService ?? throw new ArgumentNullException(nameof(viewService));
        }

        #endregion

        #region Static helpers

        public static ViewFileControl TryGetFileControl(ISerializableObject info, string viewName)
        {
            return info.TryGetValue(viewName, out var o)
                ? o as ViewFileControl
                : null;
        }

        #endregion

        #region Protected Properties

        protected IExtensionContainer ExtensionContainer { get; }

        protected ISession Session { get; }

        protected IViewService ViewService { get; }

        #endregion

        #region protected API

        /// <summary>
        /// Создает скрытый FileControl, через который представление
        /// будет взаимодействовать с файловым API. Для каждого алиаса представления
        /// должен быть создан свой файлконтрол. Создание происходит в CardUIExtension.Initializing.
        /// </summary>
        /// <param name="context">Контекст создания карточки</param>
        /// <param name="viewControlName">Алиас контрола представления, которое будет адаптировано под отображение файлов</param>
        /// <param name="creationParams">Параметры файлового контрола</param>
        protected async Task InitializeFileControlAsync(
            ICardUIExtensionContext context,
            string viewControlName,
            FileControlCreationParams creationParams)
        {
            if (context.CancellationToken.IsCancellationRequested)
            {
                return;
            }

            var categoriesView = await this.ViewService.GetByNameAsync(creationParams.CategoriesViewAlias, context.CancellationToken);
            if (categoriesView is null)
            {
                context.ValidationResult.AddError(
                    $"Categories View:'{creationParams.CategoriesViewAlias}' isn't found'");
                return;
            }

            var model = context.Model;
            var cardMetadata = model.GeneralMetadata;
            var fileContainer = model.FileContainer;
            var fileTypes =
                CardHelper.GetCardFileTypes(
                    await CardHelper.GetFileCardTypesAsync(
                        cardMetadata,
                        this.Session.User.IsAdministrator(),
                        context.CancellationToken));
            model.ControlInitializers.Add(async (control, m, r, ct) =>
            {
                if (control is CardViewControlViewModel viewControl)
                {
                    if (viewControl.Name != viewControlName)
                    {
                        return;
                    }

                    if (FormCreationContext.Current.FileControls.Any(x => x.Name == viewControl.Name))
                    {
                        context.ValidationResult.AddError($"Multiple FileViewControlViewModel with Name='{viewControl.Name}' was found on the form.");
                        return;
                    }

                    var fileControl = new ViewFileControl(
                        viewControl,
                        fileContainer,
                        this.ExtensionContainer,
                        model.MenuContext,
                        fileTypes,
                        creationParams.IsCategoriesEnabled,
                        creationParams.IsManualCategoriesCreationDisabled,
                        creationParams.IsNullCategoryCreationDisabled,
                        false,
                        creationParams.IsIgnoreExistingCategories,
                        this.Session,
                        previewControlName: creationParams.PreviewControlName,
                        name: viewControl.Name)
                    {
                        CategoryFilterAsync = async (categories, ct2) =>
                        {
                            ITessaViewResult result = null;
                            var request = new TessaViewRequest(categoriesView.Metadata);

                            IList<object> categoriesViewMapping = new List<object>();
                            var parameters =
                                ViewMappingHelper.AddRequestParameters(
                                    categoriesViewMapping,
                                    model,
                                    this.Session,
                                    categoriesView);
                            if (parameters != null)
                            {
                                request.Values.AddRange(parameters);
                            }

                            await model.ExecuteInContextAsync(
                                async (c, ct3) => { result = await categoriesView.GetDataAsync(request, ct3).ConfigureAwait(false); },
                                ct2).ConfigureAwait(false);

                            var rows = (result != null ? result.Rows : null)
                                ?? EmptyHolder<object>.Array;

                            // категории из представления в порядке, в котором их вернуло представление (кроме строчек null)
                            var viewCategories = rows
                                .Cast<IList<object>>()
                                .Where(x => x.Count > 0)
                                .Select(x => (IFileCategory) new FileCategory((Guid) x[0], (string) x[1]))
                                .ToArray();

                            // категории из представления плюс вручную добавленные или другие присутствующие в карточке категории, кроме null
                            var mainCategories = viewCategories
                                .Union(categories)
                                .Where(x => x != null)
                                .ToArray();

                            // добавляем наверх "Без категории" и возвращаем результирующий список
                            return new List<IFileCategory> { null }
                                .Union(mainCategories);
                        }
                    };
                    await fileControl.InitializeAsync(fileContainer.Files, cancellationToken: ct);
                    model.Info[viewControl.Name] = fileControl;
                    FormCreationContext.Current.Register(fileControl);
                }
            });
        }


        /// <summary>
        /// Свзяывает представление с файловым API через FileControl, созданный в InitializeFileControlAsync.
        /// </summary>
        /// <param name="cardModel"> Модель карточки </param>
        /// <param name="viewControlName"> Алаиса представления </param>
        /// <param name="createRowFunc"> Функция создающая строку представления</param>
        /// <param name="initializationStrategy"> Стратегия инициализации представления </param>
        /// <param name="viewModifierAction"> Функция для небольших модификации представления, например, заданию дефолного столбца для сортировки</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Файловый контрол <see cref="ViewFileControl"/>.</returns>
        protected async ValueTask<ViewFileControl> AttachViewToFileControlAsync(
            ICardModel cardModel,
            string viewControlName,
            Func<TableRowCreationOptions, ViewControlRowViewModel> createRowFunc,
            IViewCardControlInitializationStrategy initializationStrategy = null,
            Action<CardViewControlViewModel> viewModifierAction = null,
            CancellationToken cancellationToken = default)
        {   
            if (!cardModel.Controls.TryGet(viewControlName, out var controlViewModel))
            {
                throw new ArgumentException($"Control ViewModel with Name='{viewControlName}' not found.", nameof(viewControlName));
            }

            var viewControlViewModel = controlViewModel as CardViewControlViewModel
                ?? throw new ArgumentException(
                    $"Control ViewModel with Name='{viewControlName}' can`t be casted to type '{nameof(CardViewControlViewModel)}'.",
                    nameof(viewControlName));

            viewControlViewModel.CreateRowFunc = createRowFunc;
            var fileControl = TryGetFileControl(cardModel.Info, viewControlViewModel.Name);
            this.InitializeContextMenu(viewControlViewModel, fileControl);
            if (initializationStrategy != null)
            {
                await viewControlViewModel.InitializeStrategyAsync(initializationStrategy, true, cancellationToken);
                viewModifierAction?.Invoke(viewControlViewModel);
                await viewControlViewModel.InitializeOnTabAsync();
            }

            AttachToFileControl(viewControlViewModel, fileControl);
            InitializeGrouping(viewControlViewModel, fileControl);
            InitializeDragDrop(viewControlViewModel, cardModel);
            InitializeClickCommands(viewControlViewModel, fileControl);
            InitializeMenuButton(viewControlViewModel, fileControl);
            InitializeKeyDownHandlers(viewControlViewModel, fileControl);
            viewControlViewModel.Unloaded += fileControl.StopTimer;
            return fileControl;
        }

        #endregion

        #region Initialization

        /// <summary>
        /// Привязывает измнение коллекции файлов к представлению
        /// </summary>
        /// <param name="viewModel"> ViewModel контрола предсталвения </param>
        /// <param name="fileControl"> ViewModel Скрытого контрола с файлами</param>
        private static void AttachToFileControl(CardViewControlViewModel viewModel, IFileControl fileControl)
        {
            fileControl.Items.CollectionChanged += (sender, e) =>
            {
                viewModel.DelayedViewRefresh.RunAfterDelay(150);
            };
        }

        /// <summary>
        /// Добавляет Drag&amp;Drop
        /// </summary>
        /// <param name="viewModel"> ViewModel контрола предсталвения </param>
        /// <param name="cardModel"> Модель карточки </param>
        private static void InitializeDragDrop(CardViewControlViewModel viewModel, ICardModel cardModel)
        {
            viewModel.AllowDrop = true;
            viewModel.DragDrop = new FilesDragDrop(cardModel, viewModel.Name);
        }

        /// <summary>
        /// Синхронизация группировки в файловом контроле и предсталвении
        /// </summary>
        /// <param name="viewModel"> ViewModel контрола предсталвения </param>
        /// <param name="fileControl"> Контрол файлов </param>
        private static void InitializeGrouping(CardViewControlViewModel viewModel, IFileControl fileControl)
        {
            foreach(var column in viewModel.Table.Columns.Cast<TableColumnViewModel>())
            {
                column.ContextMenuGenerators.Clear();
            }
            var groupCaptionColumn = viewModel.Table.Columns.Cast<TableColumnViewModel>().FirstOrDefault(x => x.ColumnName == ColumnsConst.GroupCaption);
            if (groupCaptionColumn != null)
            {
                groupCaptionColumn.Visibility = false;
            }

            fileControl.PropertyChanged += (o, e) =>
            {
                if (e.PropertyName == nameof(FileControl.SelectedGrouping))
                {
                    if (fileControl.SelectedGrouping == null)
                    {
                        viewModel.Table.GroupingColumn = null;
                        var categoryColumn = viewModel.Table.Columns.Cast<TableColumnViewModel>().First(x => x.ColumnName == ColumnsConst.CategoryCaption);
                        categoryColumn.Visibility = true;
                    }
                    else
                    {
                        if(fileControl.SelectedGrouping.Name == "Category")
                        {
                            var categoryColumn = viewModel.Table.Columns.Cast<TableColumnViewModel>().First(x => x.ColumnName == ColumnsConst.CategoryCaption);
                            categoryColumn.Visibility = false;
                        }
                        viewModel.Table.GroupingColumn = null;
                        viewModel.Table.GroupingColumn = viewModel.Table.Columns.Cast<TableColumnViewModel>().FirstOrDefault(x => x.ColumnName == ColumnsConst.GroupCaption).Metadata;
                    }

                    var groupCaptionColumnValue = viewModel.Table.Columns.Cast<TableColumnViewModel>().FirstOrDefault(x => x.ColumnName == ColumnsConst.GroupCaption);
                    if (groupCaptionColumnValue != null)
                    {
                        groupCaptionColumnValue.Visibility = false;
                    }
                }
            };
        }

        /// <summary>
        /// Инициализирует обработчики нажатий на клавиатуру
        /// </summary>
        /// <param name="viewModel"> ViewModel контрола предсталвения </param>
        /// <param name="fileControl"> ViewModel Скрытого контрола с файлами</param>
        private static void InitializeKeyDownHandlers(CardViewControlViewModel viewModel, IFileControl fileControl)
        {
            viewModel.KeyDownHandlers.Add(async (item, data, e) =>
            {
                if (data is TableFileRowViewModel row)
                {
                    if (row.GridViewModel.SelectedItems.Count == 1 &&
                        row.GridViewModel.SelectedItem == row)
                    {
                        if (e.Key == Key.Enter && Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                        {
                            var file = row.FileViewModel.Model;
                            if (file == null)
                            {
                                TessaDialog.ShowMessage(string.Format(LocalizationManager.Localize("$UI_Common_FileNotFound"), ""));

                                return;
                            }

                            await FileControlHelper.OpenAsync(fileControl, new[] { file }, FileOpeningMode.ForEdit);
                        }
                    }
                }
            });
        }

        /// <summary>
        /// биндинг событий, возникающих при нажатии кнопок мыши
        /// </summary>
        /// <param name="viewModel"> ViewModel контрола предсталвения </param>
        /// <param name="fileControl"> ViewModel Скрытого контрола с файлами</param>
        private static void InitializeClickCommands(CardViewControlViewModel viewModel, ViewFileControl fileControl)
        {
            viewModel.Table.RowUnselected += async (o, e) =>
            {
                if (e.Row is null)
                {
                    return;
                }
                
                var gridViewModel = e.Row.GridViewModel;
                var row = (TableFileRowViewModel) e.Row;
                var fileViewModel = row.FileViewModel;
                var file = fileViewModel.Model.Versions.Last.File;

                //<- Обработка случая, когда строка выбрана, а файл еще прогружается.
                if (e.Row.IsSelected == fileViewModel.IsSelected)
                {
                    return;
                }

                if (fileControl.Manager.IsPreviewInProgress())
                {
                    fileViewModel.IsSelected = true;
                    e.Row.IsSelected = true;
                    return;
                }
                //->
                
                fileViewModel.IsSelected = false;
                
                // следующий код асинхронный, использовать аргументы "e" нельзя
                
                if (file.IsLocal && fileControl.Manager.IsInPreview(file.Content.GetLocalFilePath()))
                {
                    await fileControl.Manager.ResetPreviewAsync();
                }
                else if (gridViewModel.SelectedItems.Count == 1)
                {
                    await fileControl.Manager.ResetPreviewAsync();
                }

                if (Keyboard.Modifiers.Has(ModifierKeys.Control) || Keyboard.Modifiers.Has(ModifierKeys.Shift))
                {
                    var presenter = new FileControlPresenter(fileControl);
                    await presenter.ShowSelectedFilesMessageAsync(fileControl.SelectedItems.ToArray());
                }
            };

            viewModel.Table.RowSelected += async (o, e) =>
            {
                if (e.Row is null)
                {
                    return;
                }

                var gridViewModel = e.Row.GridViewModel;
                var row = (TableFileRowViewModel) e.Row;
                var fileViewModel = row.FileViewModel;

                //<- Обработка случая, когда строка выбрана, а файл еще прогружается.
                if (e.Row.IsSelected == fileViewModel.IsSelected)
                {
                    return;
                }

                if (fileControl.Manager.IsPreviewInProgress())
                {
                    fileViewModel.IsSelected = false;
                    e.Row.IsSelected = false;
                    return;
                }

                //->
                fileViewModel.IsSelected = true;
                
                // следующий код асинхронный, использовать аргументы "e" нельзя
                
                // если мы перевели фокус на это представление
                if (gridViewModel.SelectedItems.Count == 1)
                {
                    await fileControl.Manager.ClearSelectionAsync(fileControl);
                }

                if (Keyboard.Modifiers.HasNot(ModifierKeys.Control) && Keyboard.Modifiers.HasNot(ModifierKeys.Shift))
                {
                    fileControl.BeginShowPreview(fileViewModel);
                }

                if (Keyboard.Modifiers.Has(ModifierKeys.Control) || Keyboard.Modifiers.Has(ModifierKeys.Shift))
                {
                    var presenter = new FileControlPresenter(fileControl);
                    await presenter.ShowSelectedFilesMessageAsync(fileControl.SelectedItems.ToArray());
                }
            };

            viewModel.LeftButtonClickCommand = new DelegateCommand(async (o) =>
            {
                var clickInfo = (ViewMouseButtonClickInfo) o;
                var row = (TableFileRowViewModel) clickInfo.Row;
                var fileViewModel = row.FileViewModel;
                if (fileControl.Manager.IsPreviewInProgress())
                {
                    clickInfo.EventArgs.Handled = true;
                    return;
                }

                if (Keyboard.Modifiers.HasNot(ModifierKeys.Control) && Keyboard.Modifiers.HasNot(ModifierKeys.Shift) && row.IsSelected)
                {
                    fileControl.BeginShowPreview(fileViewModel);
                }
            });

            viewModel.DoubleClickCommand =
                new DelegateCommand(async o =>
                {
                    var clickInfo = (ViewMouseButtonClickInfo) o;
                    var row = (TableFileRowViewModel) clickInfo.Row;
                    var selectedFileID = row.FileID;
                    var file = fileControl.Files.FirstOrDefault(f => f.ID == selectedFileID);
                    if (file == null)
                    {
                        await TessaDialog.ShowMessageAsync(string.Format(LocalizationManager.Localize("$UI_Common_FileNotFound"), ""));
                        return;
                    }

                    if (Keyboard.Modifiers.HasFlag(ModifierKeys.Alt))
                    {
                        await FileControlHelper.OpenAsync(fileControl, new[] { file }, FileOpeningMode.ForEdit);
                    }
                    else
                    {
                        await FileControlHelper.OpenAsync(fileControl, new[] { file }, FileOpeningMode.ForRead);
                    }
                });

            viewModel.RightButtonClickCommand = new DelegateCommand(async o =>
            {
                var clickInfo = (ViewMouseButtonClickInfo) o;
                var presenter = new FileControlPresenter(fileControl);
                var row = (TableFileRowViewModel) clickInfo.Row;
                var fileID = row.FileID;
                var file = fileControl.Items.FirstOrDefault(f => f.Model.ID == fileID);
                await presenter.ShowFileMenuAsync(file, clickInfo.frameworkElement).ConfigureAwait(false);
            });
        }

        /// <summary>
        /// Создаем контекстное меню контрола
        /// </summary>
        /// <param name="viewModel"> ViewModel контрола предсталвения </param>
        /// <param name="fileControl"> ViewModel Скрытого контрола с файлами</param>
        private void InitializeContextMenu(CardViewControlViewModel viewModel, IFileControl fileControl)
        {
            viewModel.ContextMenuGenerators.Add(async context =>
            {
                (IMenuActionCollection actions, _, _) = await fileControl.GenerateControlMenuAsync();
                var toRemoveActions = new[] { FileMenuActionNames.Filterings, FileMenuActionNames.Sortings};
                context.MenuActions.AddRange(actions.Where(x => !toRemoveActions.Contains(x.Name)));
            });
        }

        /// <summary>
        /// Добавление кнопки Меню в контрол представления
        /// </summary>
        /// <param name="viewModel"> ViewModel контрола предсталвения </param>
        /// <param name="fileControl"> ViewModel Скрытого контрола с файлами</param>
        private static void InitializeMenuButton(CardViewControlViewModel viewModel, IFileControl fileControl)
        {
            var refreshButtonIndex = viewModel.TopItems.Items.IndexOf(i => i is RefreshButton);
            if (refreshButtonIndex != -1)
            {
                viewModel.TopItems.Items.RemoveAt(refreshButtonIndex);
            }

            var addMenuButton = new ShowContextMenuButtonViewModel { FileControl = fileControl, ViewModel = viewModel };
            viewModel.TopItems.Items.Insert(0, addMenuButton);
        }

        #endregion

        #region Drag&Drop

        private sealed class FilesDragDrop : DefaultDragDrop
        {
            private readonly ICardModel cardModel;
            private readonly string fileControlName;

            public FilesDragDrop(
                ICardModel cardModel,
                string fileControlName)
            {
                this.cardModel = cardModel ?? throw new ArgumentNullException(nameof(cardModel));
                this.fileControlName = fileControlName;
            }

            public override async void OnDrop(object sender, DragEventArgs e)
            {
                var fileControl = TryGetFileControl(this.cardModel.Info, this.fileControlName);
                fileControl?.DropAction(sender, e);
            }

            public override async void DragOver(object sender, DragEventArgs e)
            {
                var fileControl = TryGetFileControl(this.cardModel.Info, this.fileControlName);
                fileControl?.DragOverAction(sender, e);
            }
        }

        #endregion
    }
}