﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tessa.Extensions.Default.Client.UI.CardFiles
{
    public class FilesViewInitializationOptions
    {
        public FilesViewInitializationOptions(bool isCategoryFilterEnabled = false)
        {
            IsCategoryFilterEnabled = isCategoryFilterEnabled;
        }

        public bool IsCategoryFilterEnabled { get; set; }
    }
}
