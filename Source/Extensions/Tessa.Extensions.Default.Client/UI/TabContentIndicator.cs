﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tessa.Cards;
using Tessa.Localization;
using Tessa.Platform.Storage;
using Tessa.UI.Cards;
using Tessa.UI.Cards.Controls;

namespace Tessa.Extensions.Default.Client.UI
{
    /// <summary>
    /// Отображает знак наличия контента в ярлычке элемента управления кладками и, если разрешено, то и заголовке блока его содержащего, если поля элементов управления расположенных на вкладках контрола управления владками содержат значения.
    /// </summary>
    public sealed class TabContentIndicator
    {
        /// <summary>
        /// Выполняет обход дерева элементов управления в ширину.
        /// При выполнении обхода составляется массив содержащий связи между элементами управления и физическими полями.
        /// </summary>
        private sealed class Visitor : BreadthFirstControlVisitor
        {
            /// <summary>
            /// Словарь содержащий: ключ - имя поля; значение - индекс элемента управления содержащего это поле.
            /// </summary>
            private readonly Dictionary<string, int> fieldSectionMapping;

            /// <summary>
            /// Словарь содержащий: ключ - идентификатор поля в схеме; значение - имя поля в схеме.
            /// </summary>
            private readonly IDictionary<Guid, string> fieldIDs;

            /// <summary>
            /// Инициализирует новый экземпляр класса <see cref="Visitor"/>.
            /// </summary>
            /// <param name="fieldSectionMapping">Словарь содержащий: ключ - имя поля; значение - индекс элемента управления содержащего это поле.</param>
            /// <param name="fieldIDs">Словарь содержащий: ключ - идентификатор поля в схеме; значение - имя поля в схеме.</param>
            public Visitor(
                Dictionary<string, int> fieldSectionMapping,
                IDictionary<Guid, string> fieldIDs)
            {
                this.fieldIDs = fieldIDs;
                this.fieldSectionMapping = fieldSectionMapping;
            }
            
            /// <summary>
            /// Возвращает или задаёт порядковый номер элемента управления к которому относится поле.
            /// </summary>
            public int Index { get; set; }

            /// <inheritdoc />
            protected override void VisitControl(
                IControlViewModel controlViewModel)
            {
                if (controlViewModel.CardTypeControl is CardTypeEntryControl textBoxControl
                    && textBoxControl.Type == CardControlTypes.String)
                {
                    foreach (var colID in textBoxControl.PhysicalColumnIDList)
                    {
                        this.fieldSectionMapping[this.fieldIDs[colID]] = this.Index;
                    }
                }
            }

            /// <inheritdoc />
            protected override void VisitBlock(
                IBlockViewModel blockViewModel)
            {
            }
        }

        /// <summary>
        /// Словарь содержащий: ключ - имя поля; значение - индекс элемента управления содержащего это поле (элемент управления вкладками).
        /// </summary>
        private readonly Dictionary<string, int> fieldTabMapping = new Dictionary<string, int>(StringComparer.Ordinal);

        /// <summary>
        /// Список содержащий списки названий физических полей.
        /// Порядковый номер элемента списка соответсвует порядковому номеру элемента управления его содержащего.
        /// Размерность равна <see cref="tabs"/>.
        /// </summary>
        private readonly List<List<string>> tabFieldsMapping;

        /// <summary>
        /// Коллекция содержащая верхнеуровневые элементы управления - элементы управления вкладками.
        /// </summary>
        private readonly List<IFormViewModel> tabs = new List<IFormViewModel>();

        /// <summary>
        /// Модель представления блока содержащего элемент управления вкладками изменение дочерних элементов которого отслеживается.
        /// </summary>
        private readonly IBlockViewModel blockViewModel;

        /// <summary>
        /// Список содержащий значения заголовков вкладок до их изменения.
        /// Размерность равна <see cref="tabs"/>.
        /// </summary>
        private readonly List<string> originalTabNames = new List<string>();

        /// <summary>
        /// Текст отображаемый в заголовке блока до изменения.
        /// </summary>
        private readonly string originalBlockName;

        /// <summary>
        /// Словарь содержащий контролируемые поля. Значения данных полей проверяется на наличие данных.
        /// </summary>
        private readonly IDictionary<string, object> fieldsStorage;

        /// <summary>
        /// Список содержащий флаги, показывающие, что элемент управления содержит контент.
        /// Размерность равна <see cref="originalTabNames"/>.
        /// </summary>
        private readonly List<bool> hasContent;
        
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="TabContentIndicator"/>.
        /// </summary>
        /// <param name="tabControl">Элемент управления вкладками отслеживание полекй которого должно выполняться.</param>
        /// <param name="fieldsStorage">Словарь содержащий контролируемые поля. Значения данных полей проверяется на наличие данных.</param>
        /// <param name="fieldIDs">Словарь содержащий информацию о контролируемых полях содержащуются в метаданных секции.</param>
        /// <param name="updateBlockHeader">Значение <see langword="true"/>, если необходимо обновлять также заголовок блока содержащего <paramref name="tabControl"/>.</param>
        public TabContentIndicator(
            TabControlViewModel tabControl,
            IDictionary<string, object> fieldsStorage,
            IDictionary<Guid, string> fieldIDs,
            bool updateBlockHeader = false)
        {
            this.fieldsStorage = fieldsStorage;
            if (updateBlockHeader)
            {
                this.blockViewModel = tabControl.Block;
                this.originalBlockName = this.blockViewModel.Caption;
            }

            // Построение связей между физическими полями вложенных элементов управления и элементами управления их содержащими - элементом управления вкладками.
            var visitor = new Visitor(this.fieldTabMapping, fieldIDs);
            for (var i = 0; i < tabControl.Tabs.Count; i++)
            {
                var tab = tabControl.Tabs[i];
                this.tabs.Add(tab);
                this.originalTabNames.Add(tab.TabCaption);
                visitor.Index = i;
                visitor.Visit(tab);
            }

            this.tabFieldsMapping =
                this.fieldTabMapping
                    .GroupBy(p => p.Value)
                    .Select(p => new { tabOrder = p.Key, fields = p.Select(q => q.Key).ToList()})
                    .OrderBy(p => p.tabOrder)
                    .Select(p => p.fields)
                    .ToList();

            hasContent = this.originalTabNames.Select(_ => false).ToList();
        }

        /// <summary>
        /// Обновляет текст отображаемый на ярлычке вкладки и, если задан блок её содержащим, то и его заголовок.
        /// </summary>
        public void Update()
        {
            for (var i = 0; i < this.tabs.Count; i++)
            {
                hasContent[i] = this.UpdateTabName(i);
            }

            if (this.blockViewModel != null)
            {
                this.blockViewModel.Caption = this.hasContent.Any(p => p) 
                    ? LocalizationManager.Format("$KrProcess_TabContainsText", this.originalBlockName) 
                    : this.originalBlockName;
            }
        }

        /// <summary>
        /// Обработчик события изменения значения поля контролируемой строки.
        /// </summary>
        /// <param name="s">Источник события.</param>
        /// <param name="e">Информация о событии.</param>
        public void FieldChangedAction(object s, CardFieldChangedEventArgs e)
        {
            if (!this.fieldTabMapping.TryGetValue(e.FieldName, out var index))
            {
                return;
            }

            hasContent[index] = this.UpdateTabName(index);
            
            if (this.blockViewModel != null)
            {
                this.blockViewModel.Caption = this.hasContent.Any(p => p) 
                    ? LocalizationManager.Format("$KrProcess_TabContainsText", this.originalBlockName) 
                    : this.originalBlockName;
            }
        }
        
        private bool UpdateTabName(int index)
        {
            var tab = this.tabs[index];
            var fields = this.tabFieldsMapping[index];
            foreach (var field in fields)
            {
                if (!string.IsNullOrWhiteSpace(this.fieldsStorage.Get<string>(field)))
                {
                    tab.TabCaption = LocalizationManager.Format("$KrProcess_TabContainsText", this.originalTabNames[index]);
                    return true;
                }
            }

            tab.TabCaption = this.originalTabNames[index];
            return false;
        }
        
    }
}