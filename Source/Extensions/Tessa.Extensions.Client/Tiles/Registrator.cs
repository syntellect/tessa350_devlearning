﻿using Tessa.UI.Tiles.Extensions;
using Unity;
using Unity.Lifetime;

namespace Tessa.Extensions.Client.Tiles
{
    /// <summary>
    /// Регистрация расширений, управляющих плитками.
    /// </summary>
    [Registrator]
    public sealed class Registrator : RegistratorBase
    {
        public override void RegisterUnity()
        {
            this.UnityContainer
                .RegisterType<CreateIpTileExtension>(new ContainerControlledLifetimeManager())
                ;
        }

        public override void RegisterExtensions(IExtensionContainer extensionContainer)
        {
            // Global
            extensionContainer
                .RegisterExtension<ITileGlobalExtension, CreateIpTileExtension>(x => x
                    .WithOrder(ExtensionStage.AfterPlatform, 1)
                    .WithUnity(this.UnityContainer));
        }
    }
}
