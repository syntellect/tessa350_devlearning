﻿using System;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Extensions.Default.Shared;
using Tessa.Extensions.Shared.Info;
using Tessa.Extensions.Shared.Services;
using Tessa.Platform.Runtime;
using Tessa.UI;
using Tessa.UI.Tiles;
using Tessa.UI.Tiles.Extensions;

namespace Tessa.Extensions.Client.Tiles
{
    public sealed class CreateIpTileExtension : TileExtension
    {
        private readonly IUIHost host;
        private readonly ICardRepository cardRepository;
        private readonly IDemoService demoService;

        public CreateIpTileExtension(IUIHost host, ICardRepository cardRepository, IDemoService demoService, ISession session)
        {
            this.host = host;
            this.cardRepository = cardRepository;
            this.demoService = demoService;
        }

        public override async Task InitializingGlobal(ITileGlobalExtensionContext context)
        {
            var tile = new Tile("CreateIp",
                TileHelper.SplitCaption("$Edu_Tiles_CreateIp"),
                context.Icons.Get("Thin83"),
                context.Workspace.LeftPanel,
                new DelegateCommand(async p => await CreateIpAsync(p)),
                TileGroups.Top,
                order: 0,
                evaluating: this.Evaluate);
            context.Workspace.RightPanel.Tiles.Add(tile);
            
            var getDataTile = new Tile("GetData",
                TileHelper.SplitCaption("Получить данные"),
                context.Icons.Get("Thin83"),
                context.Workspace.LeftPanel,
                new DelegateCommand(async p => await GetData(p)),
                TileGroups.Top,
                order: 0,
                evaluating: this.Evaluate);
            context.Workspace.RightPanel.Tiles.Add(getDataTile);
        }

        private async Task CreateIpAsync(object o)
        {
            var uiContext = await this.host.CreateCardAsync(DefaultCardTypes.PartnerTypeID);
            var card = uiContext.Context.CardEditor.CardModel.Card;
            card.Sections["Partners"].Fields["TypeID"] = 3;
            card.Sections["Partners"].Fields["TypeName"] = "$PartnerType_SoleTrader";

            var response = await this.cardRepository.RequestAsync(new CardRequest { RequestType = Requests.RequestComment });
            if (!response.ValidationResult.IsSuccessful())
            {
                await DispatcherHelper.InvokeInUIAsync(async () => await TessaDialog.ShowNotEmptyAsync(response.ValidationResult));
                return;
            }

            card.Sections["Partners"].Fields["Comment"] = response.Info["Comment"];
        }

        private async Task GetData(object o)
        {
            var totalCount = await this.demoService.GetDataAsync(string.Empty);
            var incomingCount = await this.demoService.GetDataAsync(string.Empty, 2);
            var outgoingCount = await this.demoService.GetDataAsync(string.Empty, 1);
            await DispatcherHelper.InvokeInUIAsync(() => TessaDialog.ShowMessage($"Текущее количество карточек - {totalCount}\r\nВходящих - {incomingCount}\r\nИсходящих - {outgoingCount}"));
        }

        private void Evaluate(object sender, TileEvaluationEventArgs e) => e.SetIsEnabledWithCollapsing(e.CurrentTile, true);
    }
}