﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Extensions.Shared.Services;
using Tessa.Platform.Runtime;

namespace Tessa.Extensions.Client.Services
{
    /// <summary>
    /// Прокси-класс для обращения к методам контроллера Tessa.Extensions.Server.Web/Controllers/ServiceController.
    /// Все методы в нём асинхронные, но аналогичны методам сервиса <see cref="IService"/>.
    /// </summary>
    public sealed class DemoServiceWebProxy :
        WebProxy
    {
        #region Constructors

        /*
         * Базовому конструктору передаётся путь к контроллеру, задаваемый в атрибуте [Route("...")] на классе контроллера.
         */
        /// <doc path='info[@type="class" and @item=".ctor"]'/>
        public DemoServiceWebProxy()
            : base("demo")
        {
        }

        #endregion

        #region Methods

        /*
         * Флаг RequestFlags.IgnoreSession запрещает передавать токен сессии в HTTP-заголовке "Tessa-Session".
         * Это актуально в методах логина, при передаче токена в параметре или при вызове методов, которым не требуется логин.
         */
        public Task<string> LoginAsync(IntegrationLoginParameters parameters, CancellationToken cancellationToken = default) =>
            this.SendAsync<string>(HttpMethod.Post, "Login", RequestFlags.IgnoreSession, cancellationToken,
                request => request.Content = TessaHttpContent.FromJsonObject(parameters));
        
        public Task<string> WinLoginAsync(CancellationToken cancellationToken = default) =>
            this.SendAsync<string>(HttpMethod.Post, "Winlogin", RequestFlags.IgnoreSession, cancellationToken);

        public Task LogoutAsync(string token, CancellationToken cancellationToken = default) =>
            this.PostWithFlagsAndCancellationAsync<Void>("Logout", RequestFlags.IgnoreSession, cancellationToken, token);

        public Task<int> GetDataAsync(string token, int type = 0, CancellationToken cancellationToken = default) =>
            this.PostWithCancellationAsync<int>($"GetData?type={type}", cancellationToken, token);

        #endregion
    }
}