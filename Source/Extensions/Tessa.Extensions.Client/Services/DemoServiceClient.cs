﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Extensions.Shared.Services;
using Tessa.Platform.Runtime;

namespace Tessa.Extensions.Client.Services
{
    public sealed class DemoServiceClient :
        IDemoService
    {
        #region Constructors

        /// <summary>
        /// Создаёт экземпляр класса с указанием его зависимостей.
        /// </summary>
        /// <param name="proxies">Фабрики прокси-объектов для обращения к веб-сервису.</param>
        public DemoServiceClient(IWebProxyFactory proxies) =>
            this.proxies = proxies ?? throw new ArgumentNullException(nameof(proxies));

        #endregion

        #region Fields

        private readonly IWebProxyFactory proxies;

        #endregion

        #region IService Members

        public async Task<string> LoginAsync(IntegrationLoginParameters parameters, CancellationToken cancellationToken = default)
        {
            await using var proxy = await this.proxies.UseProxyAsync<DemoServiceWebProxy>().ConfigureAwait(false);
            return await proxy.LoginAsync(parameters, cancellationToken).ConfigureAwait(false);
        }

        
        public async Task<string> WinLoginAsync(CancellationToken cancellationToken = default)
        {
            await using var proxy = await this.proxies.UseProxyAsync<DemoServiceWebProxy>().ConfigureAwait(false);
            return await proxy.WinLoginAsync(cancellationToken).ConfigureAwait(false);
        }
        

        public async Task LogoutAsync(string token, CancellationToken cancellationToken = default)
        {
            await using var proxy = await this.proxies.UseProxyAsync<DemoServiceWebProxy>().ConfigureAwait(false);
            await proxy.LogoutAsync(token, cancellationToken).ConfigureAwait(false);
        }


        public async Task<int> GetDataAsync(string token, int type = 0, CancellationToken cancellationToken = default)
        {
            await using var proxy = await this.proxies.UseProxyAsync<DemoServiceWebProxy>().ConfigureAwait(false);
            return await proxy.GetDataAsync(token, type, cancellationToken).ConfigureAwait(false);
        }

        #endregion
    }
}
