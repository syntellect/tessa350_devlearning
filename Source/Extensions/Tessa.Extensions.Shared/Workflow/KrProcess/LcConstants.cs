﻿using Tessa.Extensions.Default.Shared.Workflow.KrProcess;

namespace Tessa.Extensions.Shared.Workflow.KrProcess
{
    /// <summary>
    /// Предоставляет константы используемые в новых типах этапов.
    /// </summary>
    public static class LcConstants
    {
        public static class KrExampleStageSettingsVirtual
        {
            public const string Name = nameof(KrExampleStageSettingsVirtual);

            public static readonly string Digest = StageTypeSettingsNaming.PlainColumnName(Name, nameof(Digest));

            public static readonly string Script = StageTypeSettingsNaming.PlainColumnName(Name, nameof(Script));
        }
    }
}
