﻿using Tessa.Extensions.Default.Shared.Workflow.KrProcess.Formatters;
using Tessa.Extensions.Shared.Workflow.KrProcess.Workflow;
using Tessa.Platform;
using Unity;
using Unity.Lifetime;

namespace Tessa.Extensions.Shared.Workflow.KrProcess.Formatters
{
    /// <summary>
    /// Регистратор новых обработчиков типов этапов.
    /// </summary>
    [Registrator]
    public class Registrator : RegistratorBase
    {
        /// <inheritdoc/>
        public override void RegisterUnity()
        {
            this.UnityContainer
                .RegisterType<ExampleStageTypeFormatter>(new ContainerControlledLifetimeManager());
        }

        /// <inheritdoc/>
        public override void FinalizeRegistration()
        {
            this.UnityContainer
                .TryResolve<IStageTypeFormatterContainer>()
                ?
                .RegisterFormatter<ExampleStageTypeFormatter>(CustomStageTypeDescriptors.ExampleStageDescriptor);
        }
    }
}
