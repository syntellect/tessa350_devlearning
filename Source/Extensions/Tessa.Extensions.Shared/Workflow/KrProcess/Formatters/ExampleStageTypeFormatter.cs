﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess.Formatters;
using Tessa.Extensions.Shared.Workflow.KrProcess.Workflow;
using Tessa.Platform.Storage;

namespace Tessa.Extensions.Shared.Workflow.KrProcess.Formatters
{
    /// <summary>
    /// Форматер типа этапа <see cref="CustomStageTypeDescriptors.ExampleStageDescriptor"/>.
    /// </summary>
    public class ExampleStageTypeFormatter : StageTypeFormatterBase
    {
        /// <inheritdoc/>
        public override async ValueTask FormatClientAsync(IStageTypeFormatterContext context)
        {
            await base.FormatClientAsync(context);

            FormatInternal(context, context.StageRow);
        }

        /// <inheritdoc/>
        public override async ValueTask FormatServerAsync(IStageTypeFormatterContext context)
        {
            await base.FormatServerAsync(context);

            FormatInternal(context, context.Settings);
        }

        private static void FormatInternal(
            IStageTypeFormatterContext context,
            IDictionary<string, object> settings)
        {
            var digest = settings.TryGet<string>(LcConstants.KrExampleStageSettingsVirtual.Digest);

            context.DisplaySettings = "Дайджест: " + digest;
        }
    }
}
