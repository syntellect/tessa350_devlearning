﻿using System;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;

namespace Tessa.Extensions.Shared.Workflow.KrProcess.Workflow
{
    /// <summary>
    /// Содержит пользовательские дескриптоы типов этапов.
    /// </summary>
    public static class CustomStageTypeDescriptors
    {
        /// <summary>
        /// Пример дескриптора этап.
        /// </summary>
        public static readonly StageTypeDescriptor ExampleStageDescriptor = 
            StageTypeDescriptor.Create(b =>
            {
                b.ID = new Guid(0x60d2c9f4, 0x3411, 0x4dd0, 0x84, 0xf1, 0xb6, 0x40, 0xcb, 0x34, 0x15, 0x16);
                b.Caption = "$KrStages_ExampleStage";
                b.DefaultStageName = "$KrStages_ExampleStage";
                b.SettingsCardTypeID = new Guid(0x655e95f7, 0x1730, 0x4c6b, 0xb4, 0x2d, 0xb2, 0xca, 0xc3, 0x12, 0x94, 0x6e);
                b.SupportedModes.AddRange(new[] { KrProcessRunnerMode.Sync, KrProcessRunnerMode.Async });
                b.CanBeHidden = true;
                b.CanBeSkipped = true;
                b.PerformerCaption = "$UI_KrPerformersSettings_Performer";
                b.PerformerUsageMode = Default.Shared.Workflow.PerformerUsageMode.Single;
                b.PerformerIsRequired = true;
            });
    }
}
