﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;

namespace Tessa.Extensions.Shared.Services
{
    /// <summary>
    /// Веб-сервис для выполнения произвольных действий.
    /// Интерфейс зарегистрирован на клиенте.
    /// </summary>
    public interface IDemoService
    {
        /// <summary>
        /// Выполняет вход в систему для интеграционного взаимодействия с веб-сервисом.
        /// Возвращает строку с токеном сессии, которую можно использовать для передачи в другие методы для авторизации.
        /// </summary>
        /// <param name="parameters">Параметры входа.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Токен сессии.</returns>
        Task<string> LoginAsync(IntegrationLoginParameters parameters, CancellationToken cancellationToken = default);
        
        /// <summary>
        /// Выполняет вход в систему для интеграционного взаимодействия с веб-сервисом.
        /// Возвращает строку с токеном сессии, которую можно использовать для передачи в другие методы для авторизации.
        /// </summary>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Токен сессии.</returns>
        Task<string> WinLoginAsync(CancellationToken cancellationToken = default);

        /// <summary>
        /// Выходим из системы, закрывая сессию, которая описывается указанным токеном <paramref name="token"/>.
        /// </summary>
        /// <param name="token">Токен сессии. Возвращается в методах <c>LoginAsync</c>.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Асинхронная задача.</returns>
        Task LogoutAsync(string token, CancellationToken cancellationToken = default);

        /// <summary>
        /// Метод сервиса. Может принимать и возвращать произвольные данные.
        /// </summary>
        /// <param name="token">Токен сессии. Возвращается в методах <c>LoginAsync</c>.</param>
        /// <param name="type">Тип карточек.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Данные, возвращаемые методом.</returns>
        Task<int> GetDataAsync(string token, int type = 0, CancellationToken cancellationToken = default);
    }
}
