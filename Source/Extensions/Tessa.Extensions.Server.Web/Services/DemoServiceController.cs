﻿using System;
using System.Net.Mime;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Tessa.Extensions.Shared.Services;
using Tessa.Platform;
using Tessa.Platform.Data;
using Tessa.Platform.Runtime;
using Tessa.Web;
using Tessa.Web.Client;
using Tessa.Web.Services;
using Unity;

namespace Tessa.Extensions.Server.Web.Services
{
    /// <summary>
    ///     Контроллер, для которого задан базовый путь "demo". Является примером реализации сервисов в рамках приложения
    ///     TESSA.
    /// </summary>
    [Route("demo")]
    [AllowAnonymous]
    public sealed class DemoServiceController :
        TessaControllerBase
    {
        #region Constructors

        /// <summary>
        ///     Конструктор запрашивает зависимости от контейнера ASP.NET Core, у которого они отличаются от зависимостей в
        ///     UnityContainer.
        /// </summary>
        /// <param name="scope">Объект, посредством которого можно получить доступ к API Tessa, в т.ч. к UnityContainer.</param>
        /// <param name="options">Настройки веб-сервиса.</param>
        public DemoServiceController(
            ITessaWebScope scope,
            IOptions<WebOptions> options)
        {
            this.scope = scope;
            this.options = options;
        }

        #endregion

        #region Fields

        private readonly ITessaWebScope scope;

        private readonly IOptions<WebOptions> options;

        #endregion

        #region Private Methods

        /// <summary>
        ///     Запрашивает сервисы из контейнера Unity. Такие запросы нельзя выполнять в конструкторе.
        /// </summary>
        /// <typeparam name="T">Тип запрашиваемого сервиса.</typeparam>
        /// <returns>Запрошенный сервис.</returns>
        private T Resolve<T>(string name = null) => this.scope.UnityContainer.Resolve<T>(name);

        private Guid? GetTypeIDFromType(int type)
        {
            switch (type)
            {
                case 0: // общее количество
                    return Guid.Empty;
                case 1: // исходящие
                    return new Guid("c59b76d9-c0db-01cd-a3fb-b339740f0620");
                case 2: // входящие
                    return new Guid("001f99fd-5bf3-0679-9b6f-455767af72b5");
                default: // все остальное
                    return null;
            }
        }

        #endregion

        #region Controller Methods

        /*
         * Метод доступен по базовому адресу контроллера, не требует авторизации и не обращается к сессии.
         * Для проверки функционирования сервиса перейдите по URL вида: https://localhost/tessa/web/demo
         */
        /// <summary>
        ///     Возвращает текстовое описание для конфигурации веб-сервиса, если в конфигурации
        ///     установлена настройка <c>HealthCheckIsEnabled</c> равной <c>true</c>.
        /// </summary>
        /// <returns>Текстовое описание для конфигурации веб-сервиса.</returns>

        // GET demo
        [HttpGet]
        [Produces(MediaTypeNames.Text.Plain)]
        public string Get() => 
            $"Syntellect TESSA, build {BuildInfo.Version} of {FormattingHelper.FormatDate(BuildInfo.Date, false)}{Environment.NewLine}" +
            $"Demo service base page.";

        /*
         * Метод для входа по паре логин/пароль. Здесь используется клиентская информация по умолчанию и идентификатор неизвестного приложения;
         * обычно вместо этого задают конкретное приложение и передают параметры.
         *
         * Метод возвращает строку с токеном, которую надо передавать в следующие методы
         * или же использоваться API Tessa для проброса токена в HTTP-заголовок "Tessa-Session".
         */
        /// <summary>
        ///     Открывает сессию для входа пользователя по паре логин/пароль. Возвращает строку, содержащую токен сессии,
        ///     который должен передаваться во все другие запросы к веб-сервисам.
        /// </summary>
        /// <param name="parameters">Параметры входа.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Строка, содержащая токен сессии.</returns>

        // POST service/Login
        [HttpPost("Login")]
        [Consumes(MediaTypes.JsonName, TessaBson)]
        [Produces(MediaTypeNames.Text.Plain)]
        public async Task<string> PostLogin(
            [FromBody] IntegrationLoginParameters parameters,
            CancellationToken cancellationToken = default)
        {
            var token = await this.Resolve<ISessionServer>()
                .OpenSessionAsync(
                    parameters.Login,
                    parameters.Password,
                    ApplicationIdentifiers.Other,
                    ApplicationLicenseType.Web,
                    SessionServiceType.WebClient,
                    SessionClientParameters.CreateCurrent(),
                    cancellationToken: cancellationToken);

            return token.SerializeToXml(new SessionSerializationOptions { Mode = SessionSerializationMode.Auth });
        }

        /*
         * Метод для входа, используя windows-аутентификацию. Здесь используется клиентская информация по умолчанию и идентификатор неизвестного приложения;
         * обычно вместо этого задают конкретное приложение и передают параметры.
         *
         * Метод возвращает строку с токеном, которую надо передавать в следующие методы
         * или же использоваться API Tessa для проброса токена в HTTP-заголовок "Tessa-Session".
         */
        /// <summary>
        ///     Открывает сессию для входа пользователя, используя windows-аутентификацию. Возвращает строку, содержащую токен
        ///     сессии,
        ///     который должен передаваться во все другие запросы к веб-сервисам.
        /// </summary>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Строка, содержащая токен сессии.</returns>

        // POST service/WinLogin
        [HttpPost("winlogin")]
        public async Task<string> PostWinLogin(CancellationToken cancellationToken = default)
        {
            var identityName = this.HttpContext.User.Identity.Name;

            if (string.IsNullOrEmpty(identityName))
            {
                this.Response.StatusCode = 401;

                if (this.options.Value.KerberosIsEnabled)
                {
                    this.Response.Headers.Add("WWW-Authenticate", new[] { "Negotiate" });
                }

                return "Identity name is not defined";
            }

            var token = await this.Resolve<ISessionServer>()
                .OpenSessionAsync(
                    identityName,
                    null,
                    ApplicationIdentifiers.Other,
                    ApplicationLicenseType.Web,
                    SessionServiceType.WebClient,
                    WebClientHelper.GetSessionClientParameters(this.HttpContext),
                    UserLoginType.Windows,
                    true,
                    cancellationToken);

            return token.SerializeToXml(new SessionSerializationOptions { Mode = SessionSerializationMode.Auth });
        }

        /*
         * Метод для закрытия сессии. Здесь используется клиентская информация по умолчанию и идентификатор неизвестного приложения;
         * обычно вместо этого задают конкретное приложение и передают параметры.
         */
        /// <summary>
        ///     Закрывает сессию с указанием строки с токеном сессии. Токен возвращается методом открытия сессии
        ///     <see cref="PostLogin" />.
        ///     Методу не требуется наличие информации по сессии в HTTP-заголовке.
        /// </summary>
        /// <param name="token">Токен закрываемой сессии.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Асинхронная задача.</returns>

        // POST service/Logout
        [HttpPost("Logout")]
        [Consumes(MediaTypeNames.Text.Plain, TessaBson)]
        [SessionMethod]
        public Task PostLogout(
            [FromBody] [SessionToken] string token,
            CancellationToken cancellationToken = default) =>
            this.Resolve<ISessionService>().CloseSessionWithTokenAsync(token, cancellationToken);

        /*
         * Атрибут SessionMethod нужен для того, чтобы выполнять действия в пределах сессии.
         * Если в атрибуте указать UserAccessLevel.Administrator, то метод сможет вызвать только администратор Tessa.
         *
         * Если убрать атрибут, то любая внешняя система может вызвать метод от любого пользователя,
         * а также не будет доступна информация по текущей сессии. См. метод GetDataWithoutCheckingToken.
         */
        /// <summary>
        ///     Выполняет запрос на получение информации о количестве пользователей в системе.
        /// </summary>
        /// <param name="token">Токен.</param>
        /// <param name="type">Тип карточек.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Результат запроса.</returns>
        /// <remarks>
        ///     Информация по HTTP-заголовкам, используемым платформой, доступна в методе <see cref="SessionHttpRequestHeader" />.
        /// </remarks>

        // POST service/GetData
        [HttpPost("GetData")]
        [Consumes(MediaTypeNames.Text.Plain, TessaBson)]
        [Produces(MediaTypes.JsonName)]
        [SessionMethod]
        public async Task<int> PostGetData(
            [FromBody] [SessionToken] string token,
            int type = 0,
            CancellationToken cancellationToken = default)
        {
            var typeID = GetTypeIDFromType(type);
            if (typeID == null)
            {
                return -1;
            }
            
            var dbScope = this.Resolve<IDbScope>();
            await using (dbScope.Create())
            {
                var db = dbScope.Db;
                var query =
                    dbScope.BuilderFactory
                        .Select()
                        .Count()
                        .From("Instances", "i");
                if (typeID != Guid.Empty)
                {
                    query.Where().C("i", "TypeID").Equals().P("TypeID");
                }

                return await db
                    .SetCommand(query.Build(), db.Parameter("TypeID", typeID))
                    .LogCommand()
                    .ExecuteAsync<int>(cancellationToken);
            }
        }

        #endregion
    }
}