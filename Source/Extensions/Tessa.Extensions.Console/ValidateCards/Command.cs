﻿using System;
using System.IO;
using System.Threading.Tasks;
using Tessa.Localization;
using Tessa.Platform.CommandLine;
using Tessa.Platform.ConsoleApps;
using Unity;

namespace Tessa.Extensions.Console.ValidateCards
{
    public static class Command
    {
        [Verb("ValidateCards")]
        [LocalizableDescription("Common_CLI_ValidateCards")]
        public static async Task ValidateCardsAsync(
            [Output] TextWriter stdOut,
            [Error] TextWriter stdErr,
            [Argument] [LocalizableDescription("Common_CLI_SourceCards")] string source,
            [Argument("a")] [LocalizableDescription("Common_CLI_Address")] string address = null,
            [Argument("i")] [LocalizableDescription("Common_CLI_Instance")] string instanceName = null,
            [Argument("u")] [LocalizableDescription("Common_CLI_UserName")] string userName = null,
            [Argument("p")] [LocalizableDescription("Common_CLI_Password")] string password = null,
            [Argument("t")] [LocalizableDescription("Common_CLI_SourceTypes")] string sourceTypes = null,
            [Argument("sc"), LocalizableDescription("Common_CLI_SortCards")] bool sortCards = false,
            [Argument("rd"), LocalizableDescription("Common_CLI_RemoveDuplicates")] bool removeDuplicates = false,
            [Argument("gc"), LocalizableDescription("Common_CLI_GenerateCardLib")] bool generateCardlib = false,
            [Argument("q"), LocalizableDescription("Common_CLI_Quiet")] bool quiet = false,
            [Argument("nologo")] [LocalizableDescription("CLI_NoLogo")] bool nologo = false)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source is null");
            }

            if (!nologo && !quiet)
            {
                ConsoleAppHelper.WriteLogo(stdOut);
            }

            IUnityContainer container = new UnityContainer().ConfigureConsoleForClient(stdOut, stdErr, quiet, instanceName, address);

            int result;
            await using(var operation = container.Resolve<Operation>())
            {
                var context = new OperationContext
                {
                    Source = source,
                    SourceTypes = sourceTypes,
                    SortCards = sortCards,
                    RemoveDuplicates = removeDuplicates,
                    GenerateCardlib = generateCardlib
                };

                if (!await operation.LoginAsync(userName, password))
                {
                    ConsoleAppHelper.EnvironmentExit(ConsoleAppHelper.FailedLoginExitCode);
                }


                result = await operation.ExecuteAsync(context);
                await operation.CloseAsync();
            }

            ConsoleAppHelper.EnvironmentExit(result);
        }
    }
}
