﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using Tessa.Cards;
using Tessa.Cards.ComponentModel;
using Tessa.Extensions.Default.Console;
using Tessa.Json;
using Tessa.Localization;
using Tessa.Platform;
using Tessa.Platform.ConsoleApps;
using Tessa.Platform.IO;
using Tessa.Platform.Json;
using Tessa.Roles;
using Tessa.Workflow.Helpful;
using Unity;

namespace Tessa.Extensions.Console.ValidateCards
{
    public sealed class Operation : ConsoleOperation<OperationContext>
    {
        public struct CardInfo
        {
            public Guid ID;
            public string Path;
            public Card Card;
            public bool ReplacebleCard;
        }

        public struct CardTypeInfo
        {
            public Guid ID;
            public string Path;
            public CardType Card;
        }

        public Operation(
            ConsoleSessionManager sessionManager,
            IConsoleLogger logger,
            ICardRepository cardRepository,
            [Dependency(nameof(LocalizationServiceClient))]
            ILocalizationService localization)
            : base(logger, sessionManager, true)
        {
            this.cardRepository = cardRepository;
            this.localization = localization;
        }

        private readonly ICardRepository cardRepository;
        private readonly ILocalizationService localization;
        private const string DuplicateMessage = "Duplicate card:\r\nNewer: {0}\r\n       {1}";

        /// <inheritdoc />
        public override async Task<int> ExecuteAsync(OperationContext context, CancellationToken cancellationToken = default)
        {
            if (!this.SessionManager.IsOpened)
            {
                return -1;
            }

            try
            {
                await LocalizationManager.InitializeAsync(this.localization);
                LocalizationManager.SetEnglishLocalization();

                context.CardPaths = await this.LoadCards(context.Source);
                context.Types = await this.LoadTypes(context.SourceTypes);
                if (context.CardPaths.Count == 0)
                {
                    throw new FileNotFoundException("Nothing to validate in \"{0}\"", context.Source);
                }

                await this.Logger.InfoAsync("Validating {0} cards...", context.CardPaths.Count);
                await this.ValidateCards(context, cancellationToken);
                if (context.SortCards && context.Types != null)
                {
                    await this.Logger.InfoAsync("Sorting cards...");
                    var tempPath = FileHelper.CreateSubFolderPath(FileSpecialFolder.Temp);
                    await this.CreateCards(context.Cards, context.Types, tempPath, cancellationToken);
                    await this.CreateCards(context.CardsPg, context.Types, Path.Combine(tempPath, "PostgreSql"), cancellationToken);
                    await SaveMiscFiles(context.Source, tempPath, cancellationToken);
                    Directory.Delete(context.Source, true);
                    if (string.Equals(Path.GetPathRoot(context.Source), Path.GetPathRoot(context.Source), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (Directory.Exists(context.Source))
                        {
                            try
                            {
                                Directory.Delete(tempPath, true);
                            }
                            catch (Exception e)
                            {
                                await this.Logger.LogExceptionAsync("Delete directory error", e);
                            }
                        }
                        Directory.Move(tempPath, context.Source);   
                    }
                    else
                    {
                        FileSystem.CopyDirectory(tempPath, context.Source);
                        Directory.Delete(tempPath, true);
                    }
                    if (context.GenerateCardlib)
                    {
                        await this.Logger.InfoAsync("Generating cardlibs...");
                        context.CardPaths = await this.LoadCards(context.Source);
                        await this.ValidateCards(context, cancellationToken);
                    }
                }
                else
                {
                    await this.Logger.InfoAsync("Removing {0} duplicate cards", context.CardsToRemove.Count);
                    foreach (var card in context.CardsToRemove)
                    {
                        File.Delete(card);
                    }

                    if (context.GenerateCardlib)
                    {
                        await this.Logger.InfoAsync("Generating cardlibs...");
                        context.CardPaths.RemoveAll(x => context.CardsToRemove.Contains(x));
                    }
                }

                if (context.GenerateCardlib)
                {
                    await this.Logger.InfoAsync("Exporting cardlibs...");
                    await this.ExportToCardLib(context, context.Cards, "Roles.ms.cardlib");
                    await this.ExportToCardLib(context, context.CardsPg, "Roles.pg.cardlib");
                }
            }
            catch (Exception e)
            {
                await this.Logger.LogExceptionAsync("Error validate cards", e);
                return -1;
            }

            await this.Logger.InfoAsync("Cards are validated successfully");
            return 0;
        }

        private async Task<List<string>> LoadCards(string source)
        {
            var cardPaths = new List<string>();
            var cardLibs = DefaultConsoleHelper.GetSourceFiles(source, "*.cardlib", throwIfNotFound: false, checkPatternMatch: true);
            foreach (string cardlib in cardLibs)
            {
                await this.Logger.InfoAsync("Reading card library from: \"{0}\"", cardlib);

                var library = new CardLibrary();
                await using (FileStream fileStream = File.OpenRead(cardlib))
                {
                    library.DeserializeFromXml(fileStream);
                }

                if (library.Items.Count == 0)
                {
                    await this.Logger.InfoAsync("There are no files in the card library");
                    continue;
                }

                string folder = Path.GetDirectoryName(cardlib);
                if (string.IsNullOrEmpty(folder))
                {
                    folder = Directory.GetCurrentDirectory();
                }

                var fileNames = library
                    .Items
                    .Select(relativePath => Path.Combine(folder, relativePath.Path).NormalizePathOnCurrentPlatform());

                cardPaths.AddRange(CheckCards(fileNames));
            }

            cardPaths.AddRange(CheckCards(DefaultConsoleHelper.GetSourceFiles(source, "*.*", throwIfNotFound: false)));
            cardPaths = cardPaths.Distinct().ToList();
            return cardPaths;
        }

        private async Task CreateCards(Dictionary<Guid, CardInfo> cards, Dictionary<Guid, CardTypeInfo> types, string tempPath, CancellationToken cancellationToken)
        {
            foreach (var card in cards)
            {
                var cardInfo = card.Value;
                var type = types[cardInfo.Card.TypeID].Card;
                var directory = Path.Join(tempPath,
                    string.Equals(type.Group, "Dictionaries", StringComparison.CurrentCultureIgnoreCase)
                        ? string.Empty
                        : LocalizationManager.Localize(type.Group),
                    string.Equals(type.Group, "Settings", StringComparison.CurrentCultureIgnoreCase) && type.Flags.HasFlag(CardTypeFlags.Singleton)
                        ? string.Empty
                        : GetGroup(LocalizationManager.Localize(cardInfo.Card.TypeCaption)));
                Directory.CreateDirectory(directory);
                string cardDigest = await this.cardRepository
                    .GetDigestAsync(cardInfo.Card, cancellationToken: cancellationToken)
                    .ConfigureAwait(false);
                string fileNameWithoutExt = GetExportedCardDesiredFileNameWithoutExtension(type, cardDigest ?? type.Caption, type.Caption);
                string fileName = ResolveFilePath(directory, fileNameWithoutExt, Path.GetExtension(cardInfo.Path));
                File.Copy(cardInfo.Path, fileName);
            }
        }
        
        private static async Task SaveMiscFiles(string source, string tempPath, CancellationToken cancellationToken)
        {
            var files = DefaultConsoleHelper.GetSourceFiles(source, "*.*", throwIfNotFound: false);
            foreach (var file in files)
            {
                switch (Path.GetExtension(file).ToLowerInvariant())
                {
                    case ".cardlib":
                    case ".card":
                    case ".jcard":
                        continue;
                    default:
                        File.Copy(file, Path.Combine(tempPath, file.Replace(source, string.Empty).Trim('\\')));
                        break;
                }
            }
        }

        private static string GetGroup(string typeGroup)
        {
            if (typeGroup.EndsWith("e"))
            {
                return typeGroup + "s";
            }

            if (typeGroup.EndsWith("y"))
            {
                return typeGroup.Substring(0, typeGroup.Length - 1) + "ies";
            }

            return typeGroup;
        }

        private async Task ValidateCards(OperationContext context, CancellationToken cancellationToken)
        {
            context.Cards = new Dictionary<Guid, CardInfo>();
            context.CardsPg = new Dictionary<Guid, CardInfo>();
            context.CardsToRemove = new List<string>();
            foreach (var cardPath in context.CardPaths)
            {
                string extension = Path.GetExtension(cardPath);
                CardFileFormat format = CardHelper.TryParseCardFileFormatFromExtension(extension) ?? CardFileFormat.Json;
                await using FileStream sourceStream = File.OpenRead(cardPath);
                bool isPgCard = cardPath.StartsWith(Path.Combine(context.Source, "PostgreSql"));
                var request = await this.GetCard(format, sourceStream, cancellationToken);
                var card = request.TryGetCard();
                if (context.Types != null)
                {
                    if (!context.Types.ContainsKey(card.TypeID))
                    {
                        await this.Logger.ErrorAsync("Card {0} 's type {1} ({2}) not found in metadata", cardPath, card.TypeName, card.TypeID);
                        if (context.RemoveDuplicates)
                        {
                            context.CardsToRemove.Add(cardPath);
                        }
                        continue;
                    }
                }

                var validatePath = await this.ValidateCard(context, isPgCard ? context.CardsPg : context.Cards, card, cardPath);
                if (!string.IsNullOrWhiteSpace(validatePath) && context.RemoveDuplicates)
                {
                    context.CardsToRemove.Add(validatePath);
                }
            }
        }

        private async Task<string> ValidateCard(OperationContext context, Dictionary<Guid, CardInfo> cards, Card card, string cardPath)
        {
            var isReplacebleCard = IsReplacebleCard(context, card);
            if (cards.ContainsKey(card.ID))
            {
                var existedCard = cards[card.ID];
                if (card.Modified > existedCard.Card.Modified)
                {
                    cards[card.ID] = new CardInfo { Card = card, ID = card.ID, Path = cardPath, ReplacebleCard = isReplacebleCard };
                    await this.Logger.ErrorAsync(DuplicateMessage, existedCard.Path, cardPath, card.TypeID);
                    return existedCard.Path;
                }

                await this.Logger.ErrorAsync(DuplicateMessage, cardPath, existedCard.Path, card.TypeID);
                return cardPath;
            }

            cards.Add(card.ID, new CardInfo { Card = card, ID = card.ID, Path = cardPath, ReplacebleCard = isReplacebleCard });
            return null;
        }

        private static bool IsReplacebleCard(OperationContext context, Card card)
        {
            switch (card.TypeName)
            {
                case "ReportPermissions":
                case RoleHelper.StaticRoleTypeName:
                    return false;
                case "Calendar":
                case "TimeZones":
                case "KrSettings":
                case "WorkflowEngineSettings":
                    return true;
                default:
                    var type = context.Types[card.TypeID];
                    return type.Card.Flags.HasNot(CardTypeFlags.Singleton) || !string.Equals(type.Card.Group, "Settings", StringComparison.CurrentCultureIgnoreCase);
            }
        }

        private async Task<CardStoreRequest> GetCard(CardFileFormat format, Stream sourceStream, CancellationToken cancellationToken)
        {
            switch (format)
            {
                case CardFileFormat.Binary:
                    var cardReader = new CardReader(sourceStream);
                    await cardReader.ReadHeaderAsync(cancellationToken).ConfigureAwait(false);
                    return await cardReader.ReadCardStoreRequestAsync(cancellationToken).ConfigureAwait(false);
                case CardFileFormat.Json:
                    using (var reader = new StreamReader(sourceStream, Encoding.UTF8, true, FileHelper.DefaultBufferSize, leaveOpen: true))
                    {
                        using var jsonReader = new JsonTextReader(reader);
                        var storage = TessaSerializer.JsonTyped.Deserialize<List<object>>(jsonReader);
                        Dictionary<string, object> requestStorage;
                        if (storage.Count == 0
                            || (requestStorage = storage[0] as Dictionary<string, object>) is null)
                        {
                            // это такая же критичная ошибка, как невозможность прочитать из стрима, поэтому кидаем исключение
                            throw new InvalidOperationException($"Can't read card as {CardFileFormat.Json} format from specified stream.");
                        }

                        return new CardStoreRequest(requestStorage);
                    }
                default:
                    throw new ArgumentOutOfRangeException(nameof(format));
            }
        }

        private async Task<Dictionary<Guid, CardTypeInfo>> LoadTypes(string sourceType)
        {
            if (string.IsNullOrWhiteSpace(sourceType) || !Directory.Exists(sourceType))
            {
                return null;
            }
            await this.Logger.InfoAsync("Importing types from: \"{0}\"", sourceType);

            var types = new Dictionary<Guid, CardTypeInfo>();

            foreach (string typeFile in DefaultConsoleHelper.GetSourceFiles(sourceType, "*.jtype", throwIfNotFound: false))
            {
                await this.Logger.InfoAsync("Reading type from: \"{0}\"", typeFile);

                string text = await File.ReadAllTextAsync(typeFile);

                var cardType = new CardType();
                cardType.DeserializeFromJson(text);

                if (types.ContainsKey(cardType.ID))
                {
                    await this.Logger.ErrorAsync("Card type {0} already duplicated. Added path: {1}", cardType.ID, types[cardType.ID].Path);
                    continue;
                }

                types.Add(cardType.ID, new CardTypeInfo { Card = cardType, ID = cardType.ID, Path = typeFile });
            }

            foreach (string typeFile in DefaultConsoleHelper.GetSourceFiles(sourceType, "*.tct", throwIfNotFound: false))
            {
                await this.Logger.InfoAsync("Reading type from: \"{0}\"", typeFile);

                await using FileStream fileStream = File.OpenRead(typeFile);
                var cardType = new CardType();
                cardType.DeserializeFromXml(fileStream);

                if (types.ContainsKey(cardType.ID))
                {
                    await this.Logger.ErrorAsync("Card type {0} already duplicated. Added path: {1}", cardType.ID, types[cardType.ID].Path);
                    continue;
                }

                types.Add(cardType.ID, new CardTypeInfo { Card = cardType, ID = cardType.ID, Path = typeFile });
            }

            if (types.ContainsKey(CardHelper.BusinessProcessTemplateTypeID))
            {
                var type = types[CardHelper.BusinessProcessTemplateTypeID];
                var extendedCardType = type.Card.DeepClone();
                extendedCardType.ID = WorkflowEngineHelper.ExtendedBusinessProcessTemplateTypeID;
                extendedCardType.Name += "Extended";
                extendedCardType.Flags |= CardTypeFlags.Hidden;
                types.Add(extendedCardType.ID, new CardTypeInfo { Card = extendedCardType, ID = extendedCardType.ID, Path = type.Path });
            }

            return types;
        }

        private static List<string> CheckCards(IEnumerable<string> files)
        {
            var cards = new List<string>();
            foreach (string source in files)
            {
                string extension = Path.GetExtension(source);
                CardFileFormat? format = CardHelper.TryParseCardFileFormatFromExtension(extension);

                if (format.HasValue && File.Exists(source))
                {
                    cards.Add(source);
                }
            }

            return cards;
        }

        /// <summary>
        /// Возращает предпочитаемое имя файла, в которую экспортируется карточка, без расширения.
        /// </summary>
        /// <param name="cardType">Тип карточки.</param>
        /// <param name="displayValue">
        /// Отображаемое имя карточки или Digest. Может быть строкой локализации.
        /// </param>
        /// <param name="desiredTypeCaption">
        /// Предпочитаемое имя типа карточки или <c>null</c>, если используется имя типа без изменений.
        /// Может быть строкой локализации.
        /// </param>
        /// <returns>
        /// Предпочитаемое имя файла, в которую экспортируется карточка, без расширения.
        /// </returns>
        private static string GetExportedCardDesiredFileNameWithoutExtension(
            CardType cardType,
            string displayValue,
            string desiredTypeCaption = null)
        {
            Check.ArgumentNotNull(cardType, "cardType");

            return cardType.Flags.HasNot(CardTypeFlags.Singleton)
                ? LocalizationManager.Format(displayValue)
                : string.Equals(cardType.Group, "Settings", StringComparison.InvariantCulture)
                    ? string.Format(
                        LocalizationManager.GetString("UI_Cards_SettingsExportTemplate"),
                        LocalizationManager.Format(desiredTypeCaption ?? cardType.Caption))
                    : LocalizationManager.Format(desiredTypeCaption ?? cardType.Caption);
        }

        private static string ResolveFilePath(string folderName, string cardName, string extension)
        {
            string validCardName = CardHelper.GetCardFileNameWithoutExtension(cardName);
            string filePath = Path.Combine(folderName, validCardName + extension);

            if (!File.Exists(filePath))
            {
                return filePath;
            }

            int counter = 1;
            while (File.Exists(filePath = Path.Combine(folderName, validCardName + " (" + counter + ")" + extension)))
            {
                counter++;
            }

            return filePath;
        }

        private async Task ExportToCardLib(OperationContext context, Dictionary<Guid, CardInfo> cards, string libraryFileName)
        {
            var pathStaticRole = Path.Combine(context.Source, "Static" + libraryFileName);
            var pathNonStaticRole = Path.Combine(context.Source, "NonStatic" + libraryFileName);
            await this.ExportToCardLibCore(context, cards.Values.Where(x => !x.ReplacebleCard).Select(x => x.Path).ToList(), pathStaticRole);
            await this.ExportToCardLibCore(context, cards.Values.Where(x => x.ReplacebleCard).Select(x => x.Path).ToList(), pathNonStaticRole);
        }

        private async Task ExportToCardLibCore(OperationContext context, List<string> cards, string libraryFilePath)
        {
            if (cards.Count > 0 && !string.IsNullOrEmpty(libraryFilePath))
            {
                var library = new CardLibrary();

                if (File.Exists(libraryFilePath))
                {
                    await this.Logger.InfoAsync("Appending to existent card library: \"{0}\"", libraryFilePath);

                    await using FileStream fileStream = File.OpenRead(libraryFilePath);
                    library.DeserializeFromXml(fileStream);
                }
                else
                {
                    await this.Logger.InfoAsync("Creating card library: \"{0}\"", libraryFilePath);
                }

                bool hasChanges = false;
                foreach (string card in cards)
                {
                    string filePath = card.Replace(context.Source, string.Empty).TrimStart('\\');
                    if (library.Items.Any(x => x.Path == filePath))
                    {
                        await this.Logger.InfoAsync("Skipping card: \"{0}\"", filePath);
                    }
                    else
                    {
                        await this.Logger.InfoAsync("Adding card: \"{0}\"", filePath);
                        library.Items.Add(new CardLibraryItem(filePath));
                        hasChanges = true;
                    }
                }

                if (hasChanges)
                {
                    await this.Logger.InfoAsync("Saving card library");

                    // чтобы при ошибке сериализации случайно не перезаписать файл с библиотекой - сериализуем в памяти
                    string xmlText = library.SerializeToXml();

                    await File.WriteAllTextAsync(libraryFilePath, xmlText, Encoding.UTF8);
                }
                else
                {
                    await this.Logger.InfoAsync("Card library isn't changed");
                }
            }
        }
    }
}
