﻿using System;
using System.Collections.Generic;
using Tessa.Properties.Resharper;

namespace Tessa.Extensions.Console.ValidateCards
{
    public class OperationContext
    {
        [CanBeNull]
        public string SourceTypes { get; set; }

        public bool SortCards { get; set; }

        public bool RemoveDuplicates { get; set; }

        public bool GenerateCardlib { get; set; }

        public string Source { get; set; }

        public Dictionary<Guid, Operation.CardInfo> Cards { get; set; }

        public Dictionary<Guid, Operation.CardInfo> CardsPg { get; set; }

        public Dictionary<Guid, Operation.CardTypeInfo> Types { get; set; }

        public List<string> CardPaths { get; set; }

        public List<string> CardsToRemove { get; set; }
    }
}
