﻿using Tessa.Views;
using Unity;
using Unity.Lifetime;

namespace Tessa.Extensions.Server.View
{
    [Registrator]
    public sealed class Registrator : RegistratorBase
    {
        public override void RegisterUnity()
        {
            this.UnityContainer
                .RegisterType<IExtraViewProvider, DemoViewExtraProvider>(nameof(DemoViewExtraProvider),
                    new ContainerControlledLifetimeManager())
                ;
        }
    }
}