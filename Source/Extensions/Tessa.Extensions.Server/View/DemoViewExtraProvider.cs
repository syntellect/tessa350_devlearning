﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Metadata;
using Tessa.Platform;
using Tessa.Platform.Data;
using Tessa.Roles;
using Tessa.Scheme;
using Tessa.Views;
using Tessa.Views.Metadata;
using Tessa.Views.Metadata.Criteria;
using Tessa.Views.Metadata.Types;
using Tessa.Views.Parser;

namespace Tessa.Extensions.Server.View
{
    public sealed class DemoViewExtraProvider : IExtraViewProvider
    {
        #region Constructor

        public DemoViewExtraProvider(
            ISchemeTypeConverter schemeTypeConverter,
            IDbScope dbScope,
            ICardMetadata cardMetadata,
            ISchemeService schemeService)
            => this.extraView = new DemoView(schemeTypeConverter, dbScope, cardMetadata, schemeService);

        #endregion

        #region Fields

        /// <summary>
        ///     The extra view.
        /// </summary>
        private readonly DemoView extraView;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Возвращает программное представление.
        /// </summary>
        public ITessaView GetExtraView() => this.extraView;

        #endregion

        /// <summary>
        ///     Представление по секциям
        /// </summary>
        private sealed class DemoView : ITessaView, ITessaViewAccess
        {
            #region Fields

            private readonly ISchemeTypeConverter schemeTypeConverter;

            private readonly IDbScope dbScope;

            private readonly ICardMetadata cardMetadata;
            private readonly ISchemeService schemeService;

            private IList<object> resultColumns;
            private IList<SchemeType> schemeTypes;
            private IList<object> dataTypes;

            #endregion

            #region Constructor

            public DemoView(
                ISchemeTypeConverter schemeTypeConverter,
                IDbScope dbScope,
                ICardMetadata cardMetadata,
                ISchemeService schemeService)
            {
                this.schemeTypeConverter = schemeTypeConverter;
                this.dbScope = dbScope;
                this.cardMetadata = cardMetadata;
                this.schemeService = schemeService;

                this.Metadata = new ViewMetadata { Alias = "Demo", Caption = "Демо", QuickSearchParam = "Name", MultiSelect = true };
                
                this.Metadata.Columns.Add(new ViewColumnMetadata { Alias = "DemoID", Caption = "ID", Hidden = true, SchemeType = SchemeType.Guid });
                this.Metadata.Columns.Add(new ViewColumnMetadata { Alias = "DemoName", Caption = "$Views_Sections_Name", SchemeType = SchemeType.String });
                this.Metadata.Columns.Add(new ViewColumnMetadata { Alias = "DemoTypeID", Hidden = true, Caption = "$Views_Sections_SectionType", SchemeType = SchemeType.Int32 });
                this.Metadata.References.Add(new ViewReferenceMetadata { RefSection = new[] { "Demo" }, IsCard = false, OpenOnDoubleClick = false, ColPrefix = "Demo" });
                this.Metadata.Parameters.Add(new ViewParameterMetadata { Alias = "Name", Caption = "$Views_Sections_Name_Param", SchemeType = SchemeType.String });
                this.Metadata.Parameters.Add(new ViewParameterMetadata { Alias = "IsVirtual", Caption = "$Views_Sections_IsVirtual_Param", SchemeType = SchemeType.Boolean });
                this.Metadata.Parameters.Add(new ViewParameterMetadata { Alias = "InstanceType", Hidden = true, SchemeType = SchemeType.Int32 });
            }

            #endregion

            #region Public Properties

            /// <summary>
            ///     Gets метаданные представления
            /// </summary>
            public IViewMetadata Metadata { get; }

            /// <summary>
            ///     Возвращает список ролей которые необходимы для доступа к представлению
            ///     реализующему данный интерфейс
            /// </summary>
            /// <returns>
            ///     Список ролей
            /// </returns>
            public IEnumerable<Role> GetRoles() => new[] { new Role { ID = new Guid(0x7ff52dc0, 0xff6a, 0x4c9d, 0xba, 0x25, 0xb5, 0x62, 0xc3, 0x70, 0x00, 0x4d), Name = "Все сотрудники" } };

            #endregion

            #region Public Methods and Operators

            /// <summary>
            ///     Выполняет получение данных из представления
            ///     на основании полученного <see cref="ITessaViewRequest">запроса</see>
            /// </summary>
            /// <param name="request">
            ///     Запрос к представлению
            /// </param>
            /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
            /// <returns>
            ///     <see cref="ITessaViewResult">Результат</see> выполнения запроса
            /// </returns>
            public async Task<ITessaViewResult> GetDataAsync(ITessaViewRequest request, CancellationToken cancellationToken = default)
            {
                var dbms = await this.dbScope.GetDbmsAsync(cancellationToken);
                var rows = await this.GetRowsAsync(request, cancellationToken);

                return new TessaViewResult
                {
                    Columns = this.resultColumns ??= this.Metadata.Columns.Select(x => (object) x.Alias).ToList(),
                    Rows = rows,
                    SchemeTypes = this.schemeTypes ??= this.Metadata.Columns.Select(x => x.SchemeType).ToList(),
                    DataTypes = this.dataTypes ??= this.Metadata.Columns.Select(x => (object) this.schemeTypeConverter.TryGetSqlTypeName(x.SchemeType, dbms)).ToList()
                };
            }

            #endregion

            #region Methods

            /// <summary>
            ///     The get section parameter value.
            /// </summary>
            /// <param name="request">
            ///     The request.
            /// </param>
            /// <param name="alias"></param>
            /// <returns>
            ///     The <see cref="string" />.
            /// </returns>
            private static T GetParameterValue<T>(ITessaViewRequest request, string alias)
            {
                var param = GetParameterFirstValue(request, alias);
                if (param == null)
                {
                    return default;
                }

                var criteriaOperator = CriteriaHelper.GetCriteria(param.Item1);

                if (criteriaOperator is IsTrueCriteriaOperator)
                {
                    return (T) BooleanBoxes.True;
                }

                if (criteriaOperator is IsFalseCriteriaOperator)
                {
                    return (T) BooleanBoxes.False;
                }

                if (param.Item2 == null || param.Item2.IsNull())
                {
                    return default;
                }

                return (T) param.Item2.Value;
            }

            /// <summary>
            ///     The get parameter first value.
            /// </summary>
            /// <param name="request">
            ///     The request.
            /// </param>
            /// <param name="alias">
            ///     The alias.
            /// </param>
            /// <returns>
            ///     The <see cref="Tuple" />.
            /// </returns>
            private static Tuple<string, CriteriaValue> GetParameterFirstValue(ITessaViewRequest request, string alias)
            {
                var param = request.Values?.FirstOrDefault(p => ParserNames.IsEquals(alias, p.Name));
                if (param == null)
                {
                    return null;
                }

                var criteria = param.CriteriaValues.FirstOrDefault();
                return criteria == null
                    ? null
                    : new Tuple<string, CriteriaValue>(criteria.CriteriaName, criteria.Values.FirstOrDefault());
            }

            /// <summary>
            ///     The get rows.
            /// </summary>
            /// <param name="request">
            ///     The request.
            /// </param>
            /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
            /// <returns>
            ///     The <see cref="IList{T}" />.
            /// </returns>
            private async ValueTask<IList<object>> GetRowsAsync(ITessaViewRequest request, CancellationToken cancellationToken = default)
            {
                var result = new List<object>();
                var section = GetParameterValue<string>(request, "Name")?.ToLowerInvariant();
                var isVirtual = GetParameterValue<bool?>(request, "IsVirtual");
                var instanceType = GetParameterValue<int?>(request, "InstanceType");

                var sections = await this.cardMetadata.GetSectionsAsync(cancellationToken);
                var enumerations = await this.cardMetadata.GetEnumerationsAsync(cancellationToken);

                foreach (var table in sections.Cast<CardSerializableEntry>().Concat(enumerations).OrderBy(x => x.Name))
                {
                    if (!string.IsNullOrEmpty(section)
                        && !table.Name.ToLowerInvariant().Contains(section))
                    {
                        continue;
                    }

                    if (isVirtual.HasValue)
                    {
                        var tableIsVirtual = table is CardMetadataSection s && s.IsVirtual;
                        if (tableIsVirtual != isVirtual.Value)
                        {
                            continue;
                        }
                    }

                    if (instanceType.HasValue)
                    {
                        if (!(table is CardMetadataSection s)
                            || (await this.schemeService.GetTableAsync(s.ID, cancellationToken))?.InstanceType.GetHashCode() != instanceType.Value)
                        {
                            continue;
                        }
                    }

                    var sectionType = table is CardMetadataSection sect
                        ? sect.TableType
                        : SchemeTableContentType.Enumeration;

                    result.Add(
                        new List<object>
                        {
                            table.ID,
                            table.Name,
                            sectionType.GetHashCode()
                        });
                }

                return result;
            }

            #endregion
        }
    }
}