﻿using System;
using System.Threading.Tasks;
using Tessa.Cards.Extensions;

namespace Tessa.Extensions.Server.Cards
{
    public sealed class CommentRequestExtension : CardRequestExtension
    {
        public override async Task AfterRequest(ICardRequestExtensionContext context)
        {
            context.Response.Info["Comment"] = "Наш тестовый комментарий: " + DateTime.Now;
        }
    }
}