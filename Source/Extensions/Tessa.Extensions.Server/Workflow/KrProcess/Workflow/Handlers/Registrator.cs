﻿using Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow;
using Tessa.Extensions.Shared.Workflow.KrProcess.Workflow;
using Tessa.Platform;
using Unity;
using Unity.Lifetime;

namespace Tessa.Extensions.Server.Workflow.KrProcess.Workflow.Handlers
{
    /// <summary>
    /// Регистратор новых обработчиков типов этапов.
    /// </summary>
    [Registrator]
    public sealed class Registrator : RegistratorBase
    {
        /// <inheritdoc/>
        public override void RegisterUnity()
        {
            this.UnityContainer
                .RegisterType<ExampleStageHandler>(new ContainerControlledLifetimeManager());
        }

        /// <inheritdoc/>
        public override void FinalizeRegistration()
        {
            this.UnityContainer
                .TryResolve<IKrProcessContainer>()
                ?
                .RegisterHandler<ExampleStageHandler>(CustomStageTypeDescriptors.ExampleStageDescriptor);
        }
    }
}
