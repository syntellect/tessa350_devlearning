﻿using System;
using System.Threading.Tasks;
using Tessa.Extensions.Default.Server.Workflow.KrCompilers;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.Handlers;
using Tessa.Extensions.Shared.Workflow.KrProcess;
using Tessa.Extensions.Shared.Workflow.KrProcess.Workflow;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Unity;

namespace Tessa.Extensions.Server.Workflow.KrProcess.Workflow.Handlers
{
    /// <summary>
    /// Обработчик типа этапа <see cref="CustomStageTypeDescriptors.ExampleStageDescriptor"/>.
    /// </summary>
    public class ExampleStageHandler : StageTypeHandlerBase
    {
        #region Nested Types

        /// <summary>
        /// Предоставляет дополнительные данные доступные в методе <see cref="MethodName"/>.
        /// </summary>
        public class ScriptContext
        {
            /// <summary>
            /// Возвращает значение параметра этапа "Дайджест".
            /// </summary>
            public string Digest { get; }

            /// <summary>
            /// Инициализирует новый экземпляр класса <see cref="ScriptContext"/>.
            /// </summary>
            /// <param name="digest">Значение параметра этапа "Дайджест".</param>
            public ScriptContext(
                string digest)
            {
                this.Digest = digest;
            }
        }

        #endregion

        #region Constants And Static Fields

        /// <summary>
        /// Имя метода.
        /// </summary>
        public static readonly string MethodName = "TestMethod";

        /// <summary>
        /// Отображаемое имя метода.
        /// </summary>
        public static readonly string MethodDisplayName = "$Edu_KrStages_ExampleStage_TestMethod";

        /// <summary>
        /// Имя параметра метода <see cref="MethodName"/>.
        /// </summary>
        public static readonly string MethodParametherName = "context";

        /// <summary>
        /// Тип параметра <see cref="MethodParametherName"/>.
        /// </summary>
        public static readonly string MethodParametherType = typeof(ExampleStageHandler).FullName + "." + nameof(ScriptContext);

        /// <summary>
        /// Тип значения, возвращаемого методом <see cref="MethodName"/> .
        /// </summary>
        public static readonly string MethodReturnType = nameof(Boolean);

        #endregion

        #region Fields

        private readonly IKrCompilationCache krCompilationCache;
        private readonly IUnityContainer unityContainer;

        #endregion

        #region Constructors

        public ExampleStageHandler(
            IKrCompilationCache krCompilationCache,
            IUnityContainer unityContainer)
        {
            this.krCompilationCache = krCompilationCache;
            this.unityContainer = unityContainer;
        }

        #endregion

        #region Base Overrides

        /// <inheritdoc/>
        public override async Task<StageHandlerResult> HandleStageStartAsync(IStageTypeHandlerContext context)
        {
            var stage = context.Stage;

            var performer = stage.Performer;

            if (performer is null)
            {
                context.ValidationResult.AddInfo(this, "Исполнитель не задан.");
                return StageHandlerResult.EmptyResult;
            }

            var digest = stage.SettingsStorage.TryGet<string>(LcConstants.KrExampleStageSettingsVirtual.Digest);

            context.ValidationResult.AddInfo(this, $"{performer}{Environment.NewLine}Дайджест: {digest}");

            var scriptContext = new ScriptContext(digest);
            var result = await this.RunScriptAsync(context, scriptContext);

            context.ValidationResult.AddInfo(this, $"Результат выполнения сценария: {result}");

            return StageHandlerResult.CompleteResult;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Выполняет дополнительный сценарий этапа (<see cref="MethodName"/>).
        /// </summary>
        /// <param name="context">Контекст обработчика этапа.</param>
        /// <param name="scriptContext">Параметр метода.</param>
        /// <returns>Результат выполнения сценария.</returns>
        public async Task<bool> RunScriptAsync(
            IStageTypeHandlerContext context,
            ScriptContext scriptContext)
        {
            var inst = await HandlerHelper.CreateScriptInstanceAsync(
                this.krCompilationCache,
                context.Stage.ID,
                context.ValidationResult,
                context.CancellationToken);

            await HandlerHelper.InitScriptContextAsync(this.unityContainer, inst, context);

            return await inst.InvokeExtraAsync<bool>(MethodName, scriptContext);
        }

        #endregion
    }
}
