﻿using Tessa.Extensions.Default.Server.Workflow.KrProcess.Serialization;
using Unity;
using Unity.Lifetime;

namespace Tessa.Extensions.Server.Workflow.KrProcess.Serialization
{
    [Registrator]
    public sealed class Registrator : RegistratorBase
    {
        /// <inheritdoc/>
        public override void RegisterUnity()
        {
            this.UnityContainer
                .RegisterType<ExtraSourcesCustomStageRowExtension>(new ContainerControlledLifetimeManager());
        }

        /// <inheritdoc/>
        public override void RegisterExtensions(IExtensionContainer extensionContainer)
        {
            extensionContainer
                .RegisterExtension<IKrStageRowExtension, ExtraSourcesCustomStageRowExtension>(x => x
                    .WithOrder(ExtensionStage.AfterPlatform)
                    .WithUnity(this.UnityContainer)
                    .WhenRouteCardTypes(RouteCardType.Template))
                ;
        }
    }
}