﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Extensions.Default.Server.Workflow.KrCompilers;
using Tessa.Extensions.Default.Server.Workflow.KrCompilers.SourceBuilders;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Serialization;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Extensions.Server.Workflow.KrProcess.Workflow.Handlers;
using Tessa.Extensions.Shared.Workflow.KrProcess;
using Tessa.Extensions.Shared.Workflow.KrProcess.Workflow;
using Tessa.Platform.Storage;

namespace Tessa.Extensions.Server.Workflow.KrProcess.Serialization
{
    /// <summary>
    /// Расширение на сериализацию параметров этапов.
    /// </summary>
    public sealed class ExtraSourcesCustomStageRowExtension : KrStageRowExtension
    {
        #region Fields

        private readonly IExtraSourceSerializer extraSourceSerializer;

        #endregion

        #region Constructors

        public ExtraSourcesCustomStageRowExtension(
            IExtraSourceSerializer extraSourceSerializer)
        {
            this.extraSourceSerializer = extraSourceSerializer;
        }

        #endregion

        #region Base Overrides

        /// <inheritdoc/>
        public override async Task BeforeSerialization(IKrStageRowExtensionContext context)
        {
            var rows = context.InnerCard.GetStagesSection().Rows;

            foreach (var row in rows)
            {
                var stageTypeID = row.TryGet<Guid?>(KrConstants.KrStages.StageTypeID);

                if (stageTypeID == CustomStageTypeDescriptors.ExampleStageDescriptor.ID)
                {
                    this.SetSingleSource(
                        context,
                        row,
                        LcConstants.KrExampleStageSettingsVirtual.Script,
                        ExampleStageHandler.MethodDisplayName,
                        ExampleStageHandler.MethodName,
                        ExampleStageHandler.MethodParametherType,
                        ExampleStageHandler.MethodParametherName,
                        ExampleStageHandler.MethodReturnType);
                }
            }
        }

        /// <inheritdoc/>
        public override async Task DeserializationBeforeRepair(IKrStageRowExtensionContext context)
        {
            var rows = context.CardToRepair.Sections[KrConstants.KrStages.Virtual].Rows;

            foreach (var row in rows)
            {
                var stageTypeID = row.TryGet<Guid?>(KrConstants.KrStages.StageTypeID);

                if (stageTypeID == CustomStageTypeDescriptors.ExampleStageDescriptor.ID)
                {
                    this.MoveSourceToSettings(row, LcConstants.KrExampleStageSettingsVirtual.Script);
                }
            }
        }

        #endregion

        #region Private Methods

        private void SetSingleSource(
            IKrStageRowExtensionContext context,
            CardRow row,
            string scriptField,
            string displayName,
            string name,
            string paremeterType,
            string parameterName,
            string returnType = SourceIdentifiers.Void)
        {
            if (context.StageStorages.TryGetValue(row.RowID, out var settings)
                && SourceChanged(context, row, scriptField))
            {
                var newSrc = settings.TryGet<string>(scriptField);
                var extraSources = new List<IExtraSource>
                {
                    new ExtraSource
                    {
                        DisplayName = displayName,
                        Name = name,
                        ReturnType = returnType,
                        ParameterType = paremeterType,
                        ParameterName = parameterName,
                        Source = newSrc,
                    }
                };

                settings[scriptField] = null;
                this.SetSourcesToSettings(row, extraSources);
            }
        }

        private void SetSourcesToSettings(
            CardRow row,
            IList<IExtraSource> extraSources)
        {
            row.Fields[KrConstants.KrStages.ExtraSources] = this.extraSourceSerializer.Serialize(extraSources);
        }

        private static bool SourceChanged(
            IKrStageRowExtensionContext context,
            CardRow row,
            string key)
        {
            return context.OuterCard.StoreMode == CardStoreMode.Insert
                || context.OuterCard.TryGetStagesSection(out var krStages, preferVirtual: true)
                && krStages.Rows.Any(p => p.RowID == row.RowID && p.ContainsKey(key));
        }

        private void MoveSourceToSettings(CardRow row, string key, string sourceName = null)
        {
            var extraSources = this.extraSourceSerializer.Deserialize(row.Fields[KrConstants.KrStages.ExtraSources] as string);

            IExtraSource source = null;
            if (sourceName is null
                && extraSources.Count > 0)
            {
                source = extraSources[0];
            }
            else if (sourceName != null)
            {
                foreach (var s in extraSources)
                {
                    if (s.Name == sourceName)
                    {
                        source = s;
                        break;
                    }
                }
            }

            if (source != null)
            {
                row[key] = source.Source;
            }
        }

        #endregion
    }
}
