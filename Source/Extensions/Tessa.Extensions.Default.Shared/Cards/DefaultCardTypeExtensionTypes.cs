﻿using System;
using System.Diagnostics.Contracts;
using Tessa.Cards;

namespace Tessa.Extensions.Default.Shared.Cards
{
    public static class DefaultCardTypeExtensionTypes
    {
        #region Static Fields

        /// <summary>
        /// Расширение, которое устанавливает поле в состояние "только для чтения" после первого сохранения карточки
        /// </summary>
        public static readonly CardTypeExtensionType InitializeFilesView =
            new CardTypeExtensionType(
                new Guid(0x5e2f5766, 0xb107, 0x4dd1, 0xa7, 0x41, 0x65, 0x5e, 0x83, 0x91, 0xfa, 0xe5),
                nameof(InitializeFilesView),
                new[] { CardInstanceType.Card });

        #endregion

        #region RegisterInternal Method

        /// <summary>
        /// Регистрирует все стандартные типы посредством заданного метода.
        /// </summary>
        /// <param name="registerAction">Метод, выполняющий регистрацию типа.</param>
        public static void Register(Action<CardTypeExtensionType> registerAction)
        {
            Contract.Requires(registerAction != null);

            foreach (CardTypeExtensionType extensionType
                in new[]
                {
                    InitializeFilesView
                })
            {
                registerAction(extensionType);
            }
        }

        #endregion
    }
}
