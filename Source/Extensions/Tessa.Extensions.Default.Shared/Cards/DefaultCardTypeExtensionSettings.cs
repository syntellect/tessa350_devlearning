﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tessa.Extensions.Default.Shared.Cards
{
    public static class DefaultCardTypeExtensionSettings
    {
        public static readonly string FilesViewAlias = nameof(FilesViewAlias);

        public static readonly string CategoriesViewAlias = nameof(CategoriesViewAlias);

        public static readonly string PreviewControlName = nameof(PreviewControlName);

        public static readonly string IsCategoriesEnabled = nameof(IsCategoriesEnabled);

        public static readonly string IsManualCategoriesCreationDisabled = nameof(IsManualCategoriesCreationDisabled);

        public static readonly string IsNullCategoryCreationDisabled = nameof(IsNullCategoryCreationDisabled);

        public static readonly string IsIgnoreExistingCategories = nameof(IsIgnoreExistingCategories);

        public static readonly string IsCategoryFilterEnabled = nameof(IsCategoryFilterEnabled);

        public static readonly string DefaultGroup = nameof(DefaultGroup);
    }
}
