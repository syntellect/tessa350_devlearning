﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Extensions.Templates;
using Tessa.Extensions.Default.Shared.Workflow.KrCompilers;
using Tessa.Platform;
using Tessa.Platform.Data;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using static Tessa.Extensions.Default.Shared.Workflow.KrProcess.KrConstants;


namespace Tessa.Extensions.Default.Shared.Workflow.KrProcess
{
    public static class KrProcessSharedHelper
    {

        /// <summary>
        /// Возвращает значение, показывающее, может ли указанный тип карточки содержать шаблоны этапов.
        /// </summary>
        /// <param name="typeID">Идентификатор типа карточки.</param>
        /// <returns>Значение true, если указанный тип карточки может содержать шаблоны этапов, иначе - false.</returns>
        public static bool DesignTimeCard(Guid typeID) =>
            typeID == DefaultCardTypes.KrStageTemplateTypeID
            || typeID == DefaultCardTypes.KrSecondaryProcessTypeID;

        /// <summary>
        /// Возвращает значение, показывающее, является ли указанный тип карточки типом карточки в котором выполняется маршрут.
        /// </summary>
        /// <param name="typeID">Идентификатор типа карточки.</param>
        /// <returns>Значение true, если указанный тип карточки может содержать выполняющийся маршрут, иначе - false.</returns>
        public static bool RuntimeCard(Guid typeID) => !DesignTimeCard(typeID);

        /// <summary>
        /// Асинхронно возвращает идентификатор типа документа для карточки с указанным идентификатором.
        /// </summary>
        /// <param name="cardID">Идентификатор карточки для которой требуется получить идентификатор типа документа.</param>
        /// <param name="dbScope">Объект для взаимодействия с базой данных.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Идентификатор типа документа или значение по умолчанию для типа, если не удалось получить идентификатор типа документа для указанной карточки.</returns>
        public async static Task<Guid?> GetDocTypeIDAsync(
            Guid cardID,
            IDbScope dbScope,
            CancellationToken cancellationToken = default)
        {
            Check.ArgumentNotNull(dbScope, nameof(dbScope));

            await using (dbScope.Create())
            {
                var query = dbScope.BuilderFactory
                    .Select()
                    .C(DocumentCommonInfo.DocTypeID)
                    .From(DocumentCommonInfo.Name).NoLock()
                    .Where().C(ID).Equals().P("CardID")
                    .Build();
                return await dbScope.Db.SetCommand(
                        query,
                        dbScope.Db.Parameter("CardID", cardID))
                    .LogCommand()
                    .ExecuteAsync<Guid?>(cancellationToken);
            }
        }

        /// <summary>
        /// Асинхронно возвращает идентификатор типа документа для карточки с указанным идентификатором.
        /// Метод кэширует тип документа в Card.Info.
        /// </summary>
        /// <param name="card">Карточка для которой требуется получить идентияикатор типа документа.</param>
        /// <param name="dbScope">Объект для взаимодействия с базой данных.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Идентификатор типа документа или значение по умолчанию для типа, если не удалось получить идентификатор типа документа для указанной карточки.</returns>
        public static async ValueTask<Guid?> GetDocTypeIDAsync(
            Card card,
            IDbScope dbScope = null,
            CancellationToken cancellationToken = default)
        {
            Check.ArgumentNotNull(card, nameof(card));

            // Тип лежит в секции DocumentCommonInfo.DocTypeID.
            if (card.Sections.TryGetValue(DocumentCommonInfo.Name, out var sec)
                && sec.Fields.TryGetValue(DocumentCommonInfo.DocTypeID, out object docTypeIDObj))
            {
                return docTypeIDObj as Guid?;
            }

            // Тип закэширован в Card.Info.
            if (card.Info.TryGetValue(Keys.DocTypeID, out docTypeIDObj))
            {
                return docTypeIDObj as Guid?;
            }

            // В карточке ничего нет, придется лезть в базу.
            Guid? docTypeFromDb = null;
            if(dbScope != null)
            {
                docTypeFromDb = await GetDocTypeIDAsync(card.ID, dbScope, cancellationToken);
                card.Info[Keys.DocTypeID] = docTypeFromDb;
            }

            return docTypeFromDb;
        }

        /// <summary>
        /// Асинхронно возвращает базы состояние согласования для карточки с указанным идентификатором.
        /// </summary>
        /// <param name="dbScope">Объект для взаимодействия с базой данных.</param>
        /// <param name="cardID">Идентификатор основной карточки.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Состояние карточки указанное в сателлите (или Draft если сателлита нет).</returns>
        public static async Task<KrState?> GetKrStateAsync(
            Guid cardID,
            IDbScope dbScope,
            CancellationToken cancellationToken = default)
        {
            Check.ArgumentNotNull(dbScope, nameof(dbScope));

            await using (dbScope.Create())
            {
                //Получаем состояние карточки
                var db = dbScope.Db;
                var builderFactory = dbScope.BuilderFactory;
                var cardState = await db
                    .SetCommand(
                        builderFactory
                            .Select().Coalesce(b => b.C(StateID).V(0))
                            .From(KrApprovalCommonInfo.Name).NoLock()
                            .Where().C(KrProcessCommonInfo.MainCardID).Equals().P("CardID")
                            .Build(),
                        db.Parameter("CardID", cardID))
                    .LogCommand()
                    .ExecuteAsync<int?>(cancellationToken).ConfigureAwait(false);
                return cardState.HasValue
                    ? (KrState?)new KrState(cardState.Value)
                    : null;
            }
        }

        /// <summary>
        /// Асинхронно возвращает состояние карточки из возможных источников:<para/>
        /// Секция KrApprovalCommonInfoVirtual;<para/>
        /// Сателлит из Info карточки;<para/>
        /// БД (опционально).
        /// </summary>
        /// <param name="card">Карточка для которой требуется получить состояние.</param>
        /// <param name="dbScope">Объект для взаимодействия с базой данных.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Состояние карточки.</returns>
        public static async ValueTask<KrState?> GetKrStateAsync(
            Card card,
            IDbScope dbScope = null,
            CancellationToken cancellationToken = default)
        {
            Check.ArgumentNotNull(card, nameof(card));

            KrState? result = null;

            if (card.Sections.TryGetValue(KrApprovalCommonInfo.Virtual, out var section))
            {
                result = (KrState?)section.RawFields.TryGet<int?>(StateID);
            }

            if (!result.HasValue)
            {
                // возможно удалённая карточка
                Card satelliteCard = CardSatelliteHelper.TryGetSingleSatelliteCardFromList(card, CardSatelliteHelper.SatellitesKey, DefaultCardTypes.KrSatelliteTypeID);
                if (satelliteCard != null)
                {
                    result = satelliteCard.Sections[KrApprovalCommonInfo.Name].RawFields.Get<KrState?>(StateID);
                }
            }

            if (!result.HasValue
                && dbScope != null)
            {
                result = await GetKrStateAsync(card.ID, dbScope, cancellationToken).ConfigureAwait(false);
            }

            return result;
        }

        /// <summary>
        /// Возвращает эффективные настройки для типа карточки или типа документа <see cref="IKrType"/>
        /// по карточке <paramref name="card"/>, которая загружена со всеми секциями, или <c>null</c>, если настройки нельзя получить.
        /// </summary>
        /// <param name="krTypesCache">Кэш типов карточек.</param>
        /// <param name="card">Карточка, загруженная со всеми секциями.</param>
        /// <param name="cardTypeID">Идентификатор типа карточки.</param>
        /// <param name="validationResult">
        /// Объект, в который записываются сообщения об ошибках, или <c>null</c>, если сообщения никуда не записываются.
        /// </param>
        /// <param name="validationObject">
        /// Объект, информация о котором записывается в сообщениях об ошибках в <paramref name="validationResult"/>,
        /// или <c>null</c>, если информация об объекте не будет указана.
        /// </param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>
        /// Возвращает эффективные настройки для типа карточки или типа документа
        /// или <c>null</c>, если настройки нельзя получить.
        /// </returns>
        public static async ValueTask<IKrType> TryGetKrTypeAsync(
            IKrTypesCache krTypesCache,
            Card card,
            Guid cardTypeID,
            IValidationResultBuilder validationResult = null,
            object validationObject = null,
            CancellationToken cancellationToken = default)
        {
            Check.ArgumentNotNull(krTypesCache, nameof(krTypesCache));
            Check.ArgumentNotNull(card, nameof(card));

            KrCardType krCardType = (await krTypesCache.GetCardTypesAsync(cancellationToken).ConfigureAwait(false))
                .FirstOrDefault(x => x.ID == cardTypeID);

            if (krCardType == null)
            {
                // карточка может не входить в типовое решение, тогда возвращается null
                // при этом нельзя кидать ошибку в ValidationResult, иначе любое действие с такой карточкой будет неудачным
                return null;
            }

            IKrType result = krCardType;
            if (krCardType.UseDocTypes)
            {
                if (card.Sections.TryGetValue(DocumentCommonInfo.Name, out CardSection section))
                {
                    if (section.RawFields.TryGetValue(DocumentCommonInfo.DocTypeID, out object value))
                    {
                        if (value is Guid docTypeID)
                        {
                            result = (await krTypesCache.GetDocTypesAsync(cancellationToken).ConfigureAwait(false))
                                .FirstOrDefault(x => x.ID == docTypeID);

                            if (result == null)
                            {
                                if (validationResult != null)
                                {
                                    validationResult.AddError(validationObject, "$KrMessages_UnableToFindTypeWithID", docTypeID);
                                }

                                return null;
                            }
                        }
                        else
                        {
                            if (validationResult != null)
                            {
                                validationResult.AddError(validationObject, "$KrMessages_DocTypeNotSpecified");
                            }

                            return null;
                        }
                    }
                }
            }

            return result;
        }


        /// <summary>
        /// Восстанавливает порядок сортировки для списка строк настроек.
        /// </summary>
        /// <param name="rows">Список сортируемых строк.</param>
        /// <param name="orderField">Имя поля содержащего порядок сортировки.</param>
        public static void RepairStorageRowsOrders(
            IList<object> rows,
            string orderField)
        {
            Check.ArgumentNotNull(rows, nameof(rows));

            if (rows.Count == 0)
            {
                return;
            }

            if (rows.Count == 1)
            {
                // Если строка одна, то можно сэкономить
                var singleRow = (IDictionary<string, object>)rows[0];
                singleRow[orderField] = 0;
                return;
            }

            // Полученные из словаря ордеры кэшируются, чтобы не вычислять позицию в хэш-таблице несколько раз.
            // 0 - значение в кэше не заполнено. Признаком заполненности является значение старшего бита, равное 1.
            // Ордеры здесь и в словаре могут расходится. Только в этом массиве поддерживается отношение порядка
            // В словарях проводится восстановление последовательности ордеров 0..n-1
            var ordersCache = new int[rows.Count];

            // Выполняем сортировку вставками, т.к. в большинстве случаев считаем последовательность упорядоченной
            // В случае, когда последовательность упорядочена, сортировка выполняется за O(n).
            for (var i = 1; i < rows.Count; i++)
            {
                var iRow = (IDictionary<string, object>)rows[i];
                var iOrder = GetOrder(iRow, orderField, ordersCache, i);

                int j = i - 1;
                for (; j >= 0; j--)
                {
                    var jRow = (IDictionary<string, object>)rows[j];
                    int jOrder = GetOrder(jRow, orderField, ordersCache, j);
                    if (jOrder <= iOrder)
                    {
                        break;
                    }
                    // Сдвигаем на место перемещаемой назад.
                    rows[j + 1] = rows[j];
                    ordersCache[j + 1] = ordersCache[j];
                    // Фикс ордеров для сдвигаемых строк.
                    ((IDictionary<string, object>)rows[j + 1])[orderField] = j + 1;
                }

                // Элемент необходимо переместить назад.
                if (j + 1 != i)
                {
                    rows[j + 1] = iRow;
                    ordersCache[j + 1] = SetCachedMask(iOrder);
                }

                // Если ордер в строке несоответствует порядковому ордеру,
                // то в i-тую строку ставим j+1 ордер, на который i-тая строка перемещается.
                if (iOrder != j + 1)
                {
                    iRow[Order] = j + 1;
                }
            }
        }

        /// <summary>
        /// Определяет порядок добавленного в ручную этапа при вставке в маршрут.
        /// </summary>
        /// <param name="groupID">Идентификатор группы этапов.</param>
        /// <param name="groupOrder">Порядок группы этапов.</param>
        /// <param name="rows">Список строк, в который выполяется вставка нового этапа.</param>
        /// <returns>Порядок добавленного в ручную этапа при вставке в маршрут.</returns>
        public static int ComputeStageOrder(
            Guid groupID,
            int groupOrder,
            IReadOnlyList<CardRow> rows)
        {
            Check.ArgumentNotNull(rows, nameof(rows));

            if (rows.Count == 0)
            {
                return 0;
            }

            var rowIndex = 0;

            Guid GetID() => rows[rowIndex].TryGet(StageGroupID, Guid.Empty);
            int GetOrder() => rows[rowIndex].TryGet(StageGroupOrder, int.MaxValue);
            bool NestedStage() => rows[rowIndex].TryGet<bool>(Keys.NestedStage);

            var cnt = rows.Count;

            // Достигаем начало требуемой группы
            while (rowIndex < cnt
                && (GetID() != groupID
                    && GetOrder() < groupOrder
                    || NestedStage()))
            {
                rowIndex++;
            }

            // На нестеде тут мы быть не можем, т.к. пропустили возможные нестеды выше
            // Проверим, что мы в конце или на другой группе
            if (rows.Count == rowIndex
                || (GetID() != groupID
                    && GetOrder() != groupOrder))
            {
                // Текущая группа последняя
                // В текущей группе нет этапов, просто добавляем на нужное место.
                return rowIndex;
            }

            var firstIndexInGroup = rowIndex;
            rowIndex++;

            // Спускаемся до конца группы
            while (rowIndex < cnt
                && (GetID() == groupID
                    && GetOrder() == groupOrder
                    || NestedStage()))
            {
                rowIndex++;
            }

            // Поднимаемся вверх до возможного места добавления
            var position = rowIndex;
            var sortedRows = rows.OrderBy(p => (int) p[Order]).ToArray();
            for (int i = rowIndex - 1; i >= firstIndexInGroup; i--)
            {
                var row = sortedRows[i];
                if (row.TryGet<bool>(Keys.NestedStage))
                {
                    continue;
                }

                if (row.Fields.TryGetValue(KrStages.BasedOnStageTemplateGroupPositionID, out var gpObj)
                    && GroupPosition.GetByID(gpObj) == GroupPosition.AtLast
                    && row.Fields.TryGetValue(KrStages.OrderChanged, out var orderChangedObj)
                    && orderChangedObj is bool orderChanged
                    && !orderChanged)
                {
                    position = i;
                }
            }

            return position;
        }

        #region private

        /// <summary>
        /// Маска, позволяющая установить в старшем бите Int32 признак того, что значение является инициализированным.
        /// </summary>
        private const int CachedMark = 0x40000000;

        /// <summary>
        /// Маска, позволяющая снять <see cref="CachedMark"/> для получения значения.
        /// </summary>
        private const int InvertCachedMark = 0x3FFFFFFF;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int SetCachedMask(
            int value) => CachedMark | value;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int UnsetCachedMask(
            int value) => value & InvertCachedMark;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool IsCached(
            int value) => (CachedMark & value) != 0;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int GetOrder(
            IDictionary<string, object> rowStorage,
            string orderField,
            int[] cache,
            int idx)
        {
            var cachedOrder = cache[idx];
            if (IsCached(cachedOrder))
            {
                return UnsetCachedMask(cachedOrder);
            }
            var order = rowStorage.TryGet<int?>(orderField) ?? int.MaxValue;
            cache[idx] = SetCachedMask(order);
            return order;
        }

        #endregion

    }
}