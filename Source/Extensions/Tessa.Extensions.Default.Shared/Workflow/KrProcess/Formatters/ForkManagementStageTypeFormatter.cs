﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Platform.Storage;

namespace Tessa.Extensions.Default.Shared.Workflow.KrProcess.Formatters
{
    /// <summary>
    /// Форматтер типа этапа <see cref="StageTypeDescriptors.ForkManagementDescriptor"/>.
    /// </summary>
    public sealed class ForkManagementStageTypeFormatter : StageTypeFormatterBase
    {
        /// <inheritdoc />
        public override ValueTask FormatClientAsync(IStageTypeFormatterContext context)
        {
            var rows = context.Card.Sections[KrConstants.KrForkSecondaryProcessesSettingsVirtual.Synthetic].Rows;
            var modeName = context.StageRow.Fields[KrConstants.KrForkManagementSettingsVirtual.ModeName] as string;
            modeName = modeName != null 
                ? "{" + modeName + "}"
                : string.Empty;
            context.DisplaySettings = 
                modeName
                + Environment.NewLine
                + string.Join(Environment.NewLine, ExtractRows(context.StageRow.RowID, rows));

            return new ValueTask();
        }

        /// <inheritdoc />
        public override ValueTask FormatServerAsync(IStageTypeFormatterContext context)
        {
            var rows = context.Settings.TryGet<IList<object>>(KrConstants.KrForkSecondaryProcessesSettingsVirtual.Synthetic);
            var modeName = context.Settings.TryGet<string>(KrConstants.KrForkManagementSettingsVirtual.ModeName);
            modeName = modeName != null 
                ? "{" + modeName + "}"
                : string.Empty;
            context.DisplaySettings = 
                modeName
                + Environment.NewLine
                + string.Join(Environment.NewLine, ExtractRows(context.StageRow.RowID, rows.Cast<IDictionary<string, object>>()));

            return new ValueTask();
        }

        private static IEnumerable<string> ExtractRows(
            Guid stageRowID,
            IEnumerable<IDictionary<string, object>> rows)
        {
            foreach (var row in rows)
            {
                if (row.TryGet<Guid>(KrConstants.StageRowID) == stageRowID)
                {
                    var state = (CardRowState)row.TryGet<int>(CardRow.SystemStateKey);
                    if (state != CardRowState.Deleted)
                    {
                        var name = row.TryGet<string>(KrConstants.KrForkSecondaryProcessesSettingsVirtual.SecondaryProcessName);
                        yield return name.StartsWith("$", StringComparison.Ordinal) ? "{" + name + "}" : name;
                    }
                }
            }
        }
    }
}