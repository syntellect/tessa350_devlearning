﻿using System;
using System.Threading.Tasks;
using Tessa.Platform.Storage;

namespace Tessa.Extensions.Default.Shared.Workflow.KrProcess.Formatters
{
    /// <summary>
    /// Форматтер типа этапа <see cref="StageTypeDescriptors.AddFromTemplateDescriptor"/>.
    /// </summary>
    public sealed class AddFileFromTemplateStageTypeFormatter : StageTypeFormatterBase
    {
        /// <inheritdoc/>
        public override ValueTask FormatClientAsync(IStageTypeFormatterContext context)
        {
            context.DisplayParticipants = string.Empty;
            context.DisplayTimeLimit = string.Empty;
            context.DisplaySettings = string.Join(
                Environment.NewLine,
                context.StageRow.Fields.Get<string>(KrConstants.KrAddFromTemplateSettingsVirtual.FileTemplateName),
                context.StageRow.Fields.Get<string>(KrConstants.KrAddFromTemplateSettingsVirtual.Name)
            ).Trim();

            return new ValueTask();
        }

        /// <inheritdoc/>
        public override ValueTask FormatServerAsync(IStageTypeFormatterContext context)
        {
            context.DisplayParticipants = string.Empty;
            context.DisplayTimeLimit = string.Empty;
            context.DisplaySettings = string.Join(
                Environment.NewLine,
                context.Settings.TryGet<string>(KrConstants.KrAddFromTemplateSettingsVirtual.FileTemplateName),
                context.Settings.TryGet<string>(KrConstants.KrAddFromTemplateSettingsVirtual.Name)
            ).Trim();

            return new ValueTask();
        }
    }
}