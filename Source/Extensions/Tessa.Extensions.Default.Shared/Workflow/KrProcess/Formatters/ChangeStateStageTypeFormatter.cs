﻿using System.Threading.Tasks;
using Tessa.Platform.Storage;

namespace Tessa.Extensions.Default.Shared.Workflow.KrProcess.Formatters
{
    /// <summary>
    /// Форматтер типа этапа <see cref="StageTypeDescriptors.ChangesStateDescriptor"/>.
    /// </summary>
    public sealed class ChangeStateStageTypeFormatter : StageTypeFormatterBase
    {
        private const string KrChangeStateSettingsVirtual = nameof(KrChangeStateSettingsVirtual);
        private static readonly string SettingsStateName = StageTypeSettingsNaming.PlainColumnName(KrChangeStateSettingsVirtual, "StateName");

        /// <inheritdoc/>
        public override ValueTask FormatClientAsync(IStageTypeFormatterContext context)
        {
            // На клиенте доступны виртуальные секции.
            var state = context.StageRow.Fields.TryGet<string>(SettingsStateName);
            SetState(state, context);

            return new ValueTask();
        }

        /// <inheritdoc/>
        public override ValueTask FormatServerAsync(
            IStageTypeFormatterContext context)
        {
            // На сервере доступен словарь с настройками.
            var state = context.Settings.TryGet<string>(SettingsStateName);
            SetState(state, context);

            return new ValueTask();
        }

        private static void SetState(
            string state,
            IStageTypeFormatterContext context)
        {
            context.DisplaySettings = string.IsNullOrEmpty(state)
                ? string.Empty : state[0] == '$'
                ? string.Concat("{$UI_KrChangeState_State}: ", "{", state, "}")
                : string.Concat("{$UI_KrChangeState_State}: ", state);
        }

    }
}
