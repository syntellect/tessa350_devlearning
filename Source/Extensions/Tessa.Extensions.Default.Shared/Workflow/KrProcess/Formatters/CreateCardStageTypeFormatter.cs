﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tessa.Localization;
using Tessa.Platform.Storage;
using static Tessa.Extensions.Default.Shared.Workflow.KrProcess.KrConstants.KrCreateCardStageSettingsVirtual;

namespace Tessa.Extensions.Default.Shared.Workflow.KrProcess.Formatters
{
    /// <summary>
    /// Форматтер типа этапа <see cref="StageTypeDescriptors.CreateCardDescriptor"/>.
    /// </summary>
    public sealed class CreateCardStageTypeFormatter : StageTypeFormatterBase
    {
        /// <inheritdoc />
        public override ValueTask FormatClientAsync(
            IStageTypeFormatterContext context)
        {
            FormatInternal(context, context.StageRow.Fields);

            return new ValueTask();
        }

        /// <inheritdoc />
        public override ValueTask FormatServerAsync(
            IStageTypeFormatterContext context)
        {
            FormatInternal(context, context.Settings);

            return new ValueTask();
        }


        private static void FormatInternal(
            IStageTypeFormatterContext context,
            IDictionary<string, object> storage)
        {
            var display = new List<string>
            {
                $"{LocalizationManager.Localize(storage.TryGet(TypeCaption, string.Empty))}{storage.TryGet(TemplateCaption, string.Empty)}",
                $"{LocalizationManager.Localize(storage.TryGet(ModeName, string.Empty))}"
            };

            context.DisplaySettings = string.Join(Environment.NewLine, display.Where(x => !string.IsNullOrWhiteSpace(x)));

        }
    }
}