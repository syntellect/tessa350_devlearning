﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tessa.Platform;
using Tessa.Platform.Storage;

namespace Tessa.Extensions.Default.Shared.Workflow.KrProcess.Formatters
{
    /// <summary>
    /// Форматтер типа этапа <see cref="StageTypeDescriptors.ResolutionDescriptor"/>.
    /// </summary>
    public sealed class ResolutionStageTypeFormatter : StageTypeFormatterBase
    {
        /// <inheritdoc />
        public override async ValueTask FormatClientAsync(IStageTypeFormatterContext context)
        {
            await base.FormatClientAsync(context);

            var settings = context.StageRow.Fields;
            FormatInternal(context, settings);
        }

        /// <inheritdoc />
        public override async ValueTask FormatServerAsync(IStageTypeFormatterContext context)
        {
            await base.FormatServerAsync(context);

            var settings = context.Settings;
            FormatInternal(context, settings);
        }


        private static void FormatInternal(
            IStageTypeFormatterContext context,
            IDictionary<string, object> settings)
        {
            var builder = StringBuilderHelper.Acquire();

            AppendString(builder, settings, KrConstants.KrResolutionSettingsVirtual.KindCaption, "{$CardTypes_Controls_Kind}", true);
            AppendString(builder, settings, KrConstants.KrAuthorSettingsVirtual.AuthorName, "{$CardTypes_Controls_From}");
            AppendString(builder, settings, KrConstants.KrResolutionSettingsVirtual.ControllerName, "{$CardTypes_Controls_Controller}", canBeWithoutValue: true);

            context.DisplaySettings = builder.ToStringAndRelease();

            DateTime? planned = settings.TryGet<DateTime?>(KrConstants.KrResolutionSettingsVirtual.Planned);
            double? timeLimit = settings.TryGet<double?>(KrConstants.KrResolutionSettingsVirtual.DurationInDays);
            DefaultDateFormatting(planned, timeLimit, context);
        }
    }
}
