﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tessa.Platform;

namespace Tessa.Extensions.Default.Shared.Workflow.KrProcess.Formatters
{
    /// <summary>
    /// Форматтер типа этапа <see cref="StageTypeDescriptors.UniversalTaskDescriptor"/>.
    /// </summary>
    public sealed class UniversalTaskStageTypeFormatter : StageTypeFormatterBase
    {
        /// <inheritdoc />
        public override async ValueTask FormatClientAsync(IStageTypeFormatterContext context)
        {
            await base.FormatClientAsync(context);

            var settings = context.StageRow.Fields;
            FormatInternal(context, settings);
        }

        /// <inheritdoc />
        public override async ValueTask FormatServerAsync(IStageTypeFormatterContext context)
        {
            await base.FormatServerAsync(context);

            var settings = context.Settings;
            FormatInternal(context, settings);
        }


        private static void FormatInternal(
            IStageTypeFormatterContext context,
            IDictionary<string, object> settings)
        {
            var builder = StringBuilderHelper.Acquire();

            AppendString(builder, settings, KrConstants.KrUniversalTaskSettingsVirtual.KindCaption, string.Empty, true);
            AppendString(builder, settings, KrConstants.KrUniversalTaskSettingsVirtual.Digest, string.Empty, true, limit: 30);

            context.DisplaySettings = builder.ToStringAndRelease();
        }
    }
}