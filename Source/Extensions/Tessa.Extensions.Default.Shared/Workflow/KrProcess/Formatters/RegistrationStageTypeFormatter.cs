﻿using System;
using System.Threading.Tasks;
using Tessa.Platform.Storage;

namespace Tessa.Extensions.Default.Shared.Workflow.KrProcess.Formatters
{
    /// <summary>
    /// Форматтер типа этапа <see cref="StageTypeDescriptors.RegistrationDescriptor"/>.
    /// </summary>
    public sealed class RegistrationStageTypeFormatter: StageTypeFormatterBase
    {
        /// <inheritdoc/>
        public override async ValueTask FormatClientAsync(
            IStageTypeFormatterContext context)
        {
            await base.FormatClientAsync(context);
            
            context.DisplaySettings = string.Join(
                Environment.NewLine, 
                context.StageRow.TryGet<string>(KrConstants.KrRegistrationStageSettingsVirtual.Comment) ?? string.Empty, 
                context.StageRow.TryGet<bool?>(KrConstants.KrRegistrationStageSettingsVirtual.WithoutTask) == true
                    ? "{$CardTypes_Controls_WithoutTask}"
                    : string.Empty)
                .Trim();
        }

        /// <inheritdoc/>
        public override async ValueTask FormatServerAsync(
            IStageTypeFormatterContext context)
        {
            await base.FormatServerAsync(context);

            context.DisplaySettings = string.Join(
                Environment.NewLine, 
                context.Settings.TryGet<string>(KrConstants.KrRegistrationStageSettingsVirtual.Comment) ?? string.Empty, 
                context.Settings.TryGet<bool?>(KrConstants.KrRegistrationStageSettingsVirtual.WithoutTask) == true
                    ? "{$CardTypes_Controls_WithoutTask}"
                    : string.Empty)
                .Trim();
        }
    }
}