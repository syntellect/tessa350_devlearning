﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Extensions.Default.Shared.Workflow.KrCompilers;
using Tessa.Platform;
using Tessa.Platform.Initialization;
using Tessa.Platform.Storage;
using static Tessa.Extensions.Default.Shared.Workflow.KrProcess.KrConstants;

namespace Tessa.Extensions.Default.Shared.Workflow.KrProcess
{
    /// <summary>
    /// Методы-расширения для пространства имён <c>Tessa.Extensions.Default.Shared.Workflow.KrProcess</c>.
    /// </summary>
    public static class KrProcessSharedExtensions
    {
        #region Flags Extensions

        /// <doc path='info[@type="flags" and @item="Has"]'/>
        public static bool Has(this KrComponents flags, KrComponents flag)
        {
            return (flags & flag) == flag;
        }

        /// <doc path='info[@type="flags" and @item="HasAny"]'/>
        public static bool HasAny(this KrComponents flags, KrComponents flag)
        {
            return (flags & flag) != 0;
        }

        /// <doc path='info[@type="flags" and @item="HasNot"]'/>
        public static bool HasNot(this KrComponents flags, KrComponents flag)
        {
            return (flags & flag) == 0;
        }

        /// <doc path='info[@type="flags" and @item="Has"]'/>
        public static bool Has(this InfoAboutChanges flags, InfoAboutChanges flag)
        {
            return (flags & flag) == flag;
        }

        /// <doc path='info[@type="flags" and @item="HasAny"]'/>
        public static bool HasAny(this InfoAboutChanges flags, InfoAboutChanges flag)
        {
            return (flags & flag) != 0;
        }

        /// <doc path='info[@type="flags" and @item="HasNot"]'/>
        public static bool HasNot(this InfoAboutChanges flags, InfoAboutChanges flag)
        {
            return (flags & flag) == 0;
        }

        #endregion

        #region Card Extensions

        public static CardSection GetApprovalInfoSection(this Card card)
        {
            Check.ArgumentNotNull(card, nameof(card));

            return card.Sections[GetApprovalInfoAlias(card)];
        }

        public static bool TryGetKrApprovalCommonInfoSection(this Card card, out CardSection section)
        {
            Check.ArgumentNotNull(card, nameof(card));

            return card.Sections.TryGetValue(GetApprovalInfoAlias(card), out section);
        }

        public static CardSection GetStagesSection(this Card card, bool preferVirtual = false)
        {
            Check.ArgumentNotNull(card, nameof(card));

            return card.Sections[GetStagesAlias(card, preferVirtual)];
        }

        public static bool TryGetStagesSection(this Card card, out CardSection section, bool preferVirtual = false)
        {
            Check.ArgumentNotNull(card, nameof(card));

            return card.Sections.TryGetValue(GetStagesAlias(card, preferVirtual), out section);
        }

        public static CardSection GetPerformersSection(this Card card)
        {
            Check.ArgumentNotNull(card, nameof(card));

            return card.TypeID == DefaultCardTypes.KrApprovalStageTypeSettingsTypeID
                ? card.Sections[KrPerformersVirtual.Name]
                : card.Sections[KrPerformersVirtual.Synthetic];
        }

        public static bool TryGetPerformersSection(this Card card, out CardSection section)
        {
            Check.ArgumentNotNull(card, nameof(card));

            var secAlias = card.TypeID == DefaultCardTypes.KrApprovalStageTypeSettingsTypeID
                ? KrPerformersVirtual.Name
                : KrPerformersVirtual.Synthetic;

            return card.Sections.TryGetValue(secAlias, out section);
        }

        public static CardSection GetActiveTasksSection(this Card card)
        {
            Check.ArgumentNotNull(card, nameof(card));

            return card.TypeID == DefaultCardTypes.KrSatelliteTypeID
                ? card.Sections[KrActiveTasks.Name]
                : card.Sections[KrActiveTasks.Virtual];
        }

        public static bool TryGetActiveTasksSection(this Card card, out CardSection section)
        {
            Check.ArgumentNotNull(card, nameof(card));

            var secAlias = card.TypeID == DefaultCardTypes.KrSatelliteTypeID
                ? KrActiveTasks.Name
                : KrActiveTasks.Virtual;

            return card.Sections.TryGetValue(secAlias, out section);
        }

        public static CardSection GetKrApprovalHistorySection(this Card card)
        {
            Check.ArgumentNotNull(card, nameof(card));

            return card.TypeID == DefaultCardTypes.KrSatelliteTypeID
                ? card.Sections[KrApprovalHistory.Name]
                : card.Sections[KrApprovalHistory.Virtual];
        }

        public static bool TryGetKrApprovalHistorySection(this Card card, out CardSection section)
        {
            Check.ArgumentNotNull(card, nameof(card));

            var secAlias = card.TypeID == DefaultCardTypes.KrSatelliteTypeID
                ? KrApprovalHistory.Name
                : KrApprovalHistory.Virtual;

            return card.Sections.TryGetValue(secAlias, out section);
        }

        private static string GetApprovalInfoAlias(Card card)
        {
            string secAlias;
            if (card.TypeID == DefaultCardTypes.KrSatelliteTypeID)
            {
                secAlias = KrApprovalCommonInfo.Name;
            }
            else if (card.TypeID == DefaultCardTypes.KrSecondarySatelliteTypeID)
            {
                secAlias = KrSecondaryProcessCommonInfo.Name;
            }
            else
            {
                secAlias = KrApprovalCommonInfo.Virtual;
            }

            return secAlias;
        }

        private static string GetStagesAlias(
            Card card,
            bool preferVirtual)
        {
            string secAlias;
            if (card.TypeID == DefaultCardTypes.KrSatelliteTypeID
                || card.TypeID == DefaultCardTypes.KrSecondarySatelliteTypeID
                || (card.TypeID == DefaultCardTypes.KrStageTemplateTypeID && !preferVirtual)
                || (card.TypeID == DefaultCardTypes.KrSecondaryProcessTypeID && !preferVirtual))
            {
                secAlias = KrStages.Name;
            }
            else
            {
                secAlias = KrStages.Virtual;
            }

            return secAlias;
        }

        #endregion

        #region ICardFieldContainer

        /// <summary>
        /// Подставить новое значение в поле, если изменено
        /// </summary>
        /// <param name="aci"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool SetIfDiffer<T>(this ICardFieldContainer aci, string key, T value)
        {
            Check.ArgumentNotNull(aci, nameof(aci));

            if (aci.Fields.TryGetValue(key, out var oldValue)
                && Equals(value, oldValue))
            {
                return false;
            }

            aci.Fields[key] = value;
            return true;
        }

        #endregion

        #region CardInfoStorageObject

        internal const string SecondaryProcessInfoMark = nameof(SecondaryProcessInfoMark);

        public static void SetStartingSecondaryProcess(
            this CardInfoStorageObject request,
            StartingSecondaryProcessInfo info)
        {
            Check.ArgumentNotNull(request, nameof(request));
            Check.ArgumentNotNull(info, nameof(info));

            request.Info[SecondaryProcessInfoMark] = info.GetStorage();
        }

        public static StartingSecondaryProcessInfo GetStartingSecondaryProcess(
            this CardInfoStorageObject request)
        {
            Check.ArgumentNotNull(request, nameof(request));

            if (request.Info.TryGetValue(SecondaryProcessInfoMark, out var infoObj)
                && infoObj is Dictionary<string, object> info)
            {
                return new StartingSecondaryProcessInfo(info);
            }

            return null;
        }

        public static void RemoveSecondaryProcess(this CardInfoStorageObject request)
        {
            Check.ArgumentNotNull(request, nameof(request));

            request.Info.Remove(SecondaryProcessInfoMark);
        }

        public const string KrProcessClientCommandInfoMark = nameof(KrProcessClientCommandInfoMark);

        public static void AddKrProcessClientCommands(
            this CardInfoStorageObject storage,
            List<KrProcessClientCommand> commands)
        {
            Check.ArgumentNotNull(storage, nameof(storage));
            Check.ArgumentNotNull(commands, nameof(commands));

            if (!storage.Info.TryGetValue(KrProcessClientCommandInfoMark, out var clientCommandsObj)
                || !(clientCommandsObj is List<object> clientCommands))
            {
                clientCommands = new List<object>();
            }
            clientCommands.AddRange(commands.Select(p => p.GetStorage()));
            storage.Info[KrProcessClientCommandInfoMark] = clientCommands;
        }

        public static List<KrProcessClientCommand> GetKrProcessClientCommands(
            this CardInfoStorageObject storage)
        {
            Check.ArgumentNotNull(storage, nameof(storage));

            if (storage.Info.TryGetValue(KrProcessClientCommandInfoMark, out var commandObj))
            {
                switch (commandObj)
                {
                    case List<object> groupsObjList:
                        return groupsObjList
                            .Cast<Dictionary<string, object>>()
                            .Select(p => new KrProcessClientCommand(p))
                            .ToList();

                    case List<Dictionary<string, object>> groupsStorage:
                        return groupsStorage
                            .Select(p => new KrProcessClientCommand(p))
                            .ToList();
                }
            }

            return null;
        }

        public static bool TryGetKrProcessClientCommands(
            this CardInfoStorageObject storage,
            out List<KrProcessClientCommand> clientCommands)
        {
            // storage будет проверен в GetKrProcessClientCommands.

            var value = GetKrProcessClientCommands(storage);
            clientCommands = value;
            return value != null;
        }

        internal const string IgnoreButtonsInfoMark = nameof(IgnoreButtonsInfoMark);

        public static void IgnoreButtons(
            this CardInfoStorageObject request,
            bool value = true)
        {
            Check.ArgumentNotNull(request, nameof(request));

            request.Info[IgnoreButtonsInfoMark] = BooleanBoxes.Box(value);
        }

        public static bool AreButtonsIgnored(
            this CardInfoStorageObject request)
        {
            Check.ArgumentNotNull(request, nameof(request));

            return request.Info.TryGet(IgnoreButtonsInfoMark, false);
        }

        internal const string DontHideStagesInfoMark = nameof(DontHideStagesInfoMark);

        public static void DontHideStages(
            this Dictionary<string, object> storage,
            bool value = true)
        {
            Check.ArgumentNotNull(storage, nameof(storage));

            storage[DontHideStagesInfoMark] = BooleanBoxes.Box(value);
        }

        public static void DontHideStages(
            this CardInfoStorageObject request,
            bool value = true)
        {
            Check.ArgumentNotNull(request, nameof(request));

            request.Info.DontHideStages(value);
        }

        public static bool ConsiderHiddenStages(
            this CardInfoStorageObject request)
        {
            Check.ArgumentNotNull(request, nameof(request));

            return !request.Info.TryGet(DontHideStagesInfoMark, false);
        }


        internal const string IgnoreKrSatelliteInfoMark = nameof(IgnoreKrSatelliteInfoMark);

        public static void IgnoreKrSatellite(
            this CardInfoStorageObject request,
            bool value = true)
        {
            Check.ArgumentNotNull(request, nameof(request));

            request.Info[IgnoreKrSatelliteInfoMark] = BooleanBoxes.Box(value);
        }

        public static bool IsKrSatelliteIgnored(
            this CardInfoStorageObject request)
        {
            Check.ArgumentNotNull(request, nameof(request));

            return request.Info.TryGet(IgnoreKrSatelliteInfoMark, false);
        }


        internal const string GlobalTilesInfoMark = nameof(GlobalTilesInfoMark);

        public static void SetGlobalTiles(
            this InitializationResponse response,
            List<KrTileInfo> tileInfos)
        {
            Check.ArgumentNotNull(response, nameof(response));
            Check.ArgumentNotNull(tileInfos, nameof(tileInfos));

            response.Info[GlobalTilesInfoMark] = tileInfos
                .Select(p => p.GetStorage())
                .ToList();
        }

        public static List<KrTileInfo> GetGlobalTiles(
            this InitializationResponse response)
        {
            Check.ArgumentNotNull(response, nameof(response));

            if (response.Info.TryGetValue(GlobalTilesInfoMark, out var groupsObj))
            {
                switch (groupsObj)
                {
                    case List<object> groupsObjList:
                        return groupsObjList
                            .Cast<Dictionary<string, object>>()
                            .Select(p => new KrTileInfo(p))
                            .ToList();
                    case List<Dictionary<string, object>> groupsStorage:
                        return groupsStorage
                            .Select(p => new KrTileInfo(p))
                            .ToList();
                }
            }

            return null;
        }

        public static bool TryGetGlobalTiles(
            this InitializationResponse response,
            out List<KrTileInfo> globalTiles)
        {
            // response будет проверен в GetGlobalTiles.

            var value = GetGlobalTiles(response);
            globalTiles = value;
            return value != null;
        }

        internal const string LocalTilesInfoMark = nameof(LocalTilesInfoMark);

        public static void SetLocalTiles(
            this CardInfoStorageObject request,
            List<KrTileInfo> tileInfos)
        {
            Check.ArgumentNotNull(request, nameof(request));

            request.Info[LocalTilesInfoMark] = tileInfos
                .Select(p => p.GetStorage())
                .ToList();
        }

        public static List<KrTileInfo> GetLocalTiles(
            this CardInfoStorageObject storage)
        {
            Check.ArgumentNotNull(storage, nameof(storage));

            if (storage.Info.TryGetValue(LocalTilesInfoMark, out var groupsObj))
            {
                switch (groupsObj)
                {
                    case List<object> groupsObjList:
                        return groupsObjList
                            .Cast<Dictionary<string, object>>()
                            .Select(p => new KrTileInfo(p))
                            .ToList();
                    case List<Dictionary<string, object>> groupsStorage:
                        return groupsStorage
                            .Select(p => new KrTileInfo(p))
                            .ToList();
                }
            }

            return null;
        }

        public static bool TryGetLocalTiles(
            this CardInfoStorageObject storage,
            out List<KrTileInfo> globalTiles)
        {
            // storage будет проверен в GetLocalTiles.

            var value = GetLocalTiles(storage);
            globalTiles = value;
            return value != null;
        }

        public static void RemoveLocalTiles(
            this CardInfoStorageObject storage)
        {
            storage?.Info?.Remove(LocalTilesInfoMark);
        }


        internal const string StartKrProcessInstance = nameof(StartKrProcessInstance);

        public static void SetKrProcessInstance(
            this CardInfoStorageObject storage,
            KrProcessInstance krProcess)
        {
            Check.ArgumentNotNull(storage, nameof(storage));

            storage.Info.SetKrProcessInstance(krProcess);
        }

        public static void SetKrProcessInstance(
            this Dictionary<string, object> storage,
            KrProcessInstance krProcess)
        {
            Check.ArgumentNotNull(storage, nameof(storage));
            Check.ArgumentNotNull(krProcess, nameof(krProcess));

            storage[StartKrProcessInstance] = krProcess.GetStorage();
        }

        public static KrProcessInstance GetKrProcessInstance(
            this CardInfoStorageObject storage)
        {
            Check.ArgumentNotNull(storage, nameof(storage));

            if (storage.Info.TryGetValue(StartKrProcessInstance, out var processObj)
                && processObj is Dictionary<string, object> processDict)
            {
                return new KrProcessInstance(processDict);
            }

            return null;
        }

        public static bool TryGetKrProcessInstance(
            this CardInfoStorageObject storage,
            out KrProcessInstance pID)
        {
            // storage будет проверен в GetKrProcessInstance.

            var value = GetKrProcessInstance(storage);
            pID = value;
            return value != null;
        }

        internal const string KrProcessLaunchResult = nameof(KrProcessLaunchResult);

        public static void SetKrProcessLaunchResult(
            this CardInfoStorageObject storage,
            KrProcessLaunchResult launchResult)
        {
            Check.ArgumentNotNull(storage, nameof(storage));
            Check.ArgumentNotNull(launchResult, nameof(launchResult));

            storage.Info[KrProcessLaunchResult] = launchResult.GetStorage();
        }

        public static KrProcessLaunchResult GetKrProcessLaunchResult(
            this CardInfoStorageObject storage)
        {
            Check.ArgumentNotNull(storage, nameof(storage));

            if (storage.Info.TryGetValue(KrProcessLaunchResult, out var processObj)
                && processObj is Dictionary<string, object> processDict)
            {
                return new KrProcessLaunchResult(processDict);
            }

            return null;
        }

        public static bool TryGetKrProcessLaunchResult(
            this CardInfoStorageObject storage,
            out KrProcessLaunchResult result)
        {
            // storage будет проверен в GetKrProcessLaunchResult.

            var value = GetKrProcessLaunchResult(storage);
            result = value;
            return value != null;
        }

        internal const string StagePositionsInfoMark = "StagePositions";

        public static void SetStagePositions(
            this Card card,
            List<object> stagePositions)
        {
            Check.ArgumentNotNull(card, nameof(card));

            card.Info[StagePositionsInfoMark] = stagePositions;
        }

        public static void SetStagePositions(
            this Card card,
            List<KrStagePositionInfo> stagePositions)
        {
            Check.ArgumentNotNull(stagePositions, nameof(stagePositions));

            card.SetStagePositions(stagePositions.Select(i => i.GetStorage()).Cast<object>().ToList());
        }

        public static List<KrStagePositionInfo> GetStagePositions(
            this Card card)
        {
            Check.ArgumentNotNull(card, nameof(card));

            if (card.Info.TryGetValue(StagePositionsInfoMark, out var stagePositions)
                && stagePositions is List<object> stagePositionsList)
            {
                return stagePositionsList
                    .Cast<Dictionary<string, object>>()
                    .Select(p => new KrStagePositionInfo(p))
                    .ToList();
            }

            return null;
        }

        public static bool TryGetStagePositions(
            this Card card,
            out List<KrStagePositionInfo> stagePositions)
        {
            // card будет проверен в GetStagePositions.

            var value = GetStagePositions(card);
            stagePositions = value;
            return value != null;
        }

        public static bool HasStagePositions(
            this Card card,
            bool atLeastOne)
        {
            Check.ArgumentNotNull(card, nameof(card));

            return card.Info.TryGetValue(StagePositionsInfoMark, out var stagePositions)
                && stagePositions is List<object> stagePositionsList
                && (!atLeastOne
                    || stagePositionsList.Count > 0);
        }

        public static bool HasHiddenStages(
            this Card card)
        {
            Check.ArgumentNotNull(card, nameof(card));

            return card.Info.TryGetValue(StagePositionsInfoMark, out var stagePositions)
                && stagePositions is List<object> stagePositionsList
                && stagePositionsList.Any(p => (p as Dictionary<string, object>)?.TryGet<bool?>(nameof(KrStagePositionInfo.Hidden)) == true);
        }

        public static void RemoveStagePositions(this Card card)
        {
            Check.ArgumentNotNull(card, nameof(card));

            card.Info.Remove(StagePositionsInfoMark);
        }

        internal const string AsyncProcessCompletedSimultaniosly = nameof(AsyncProcessCompletedSimultaniosly);

        public static void SetAsyncProcessCompletedSimultaniosly(
            this IDictionary<string, object> info,
            bool flag = true)
        {
            Check.ArgumentNotNull(info, nameof(info));

            info[AsyncProcessCompletedSimultaniosly] = BooleanBoxes.Box(flag);
        }

        public static bool GetAsyncProcessCompletedSimultaniosly(
            this IDictionary<string, object> info)
        {
            Check.ArgumentNotNull(info, nameof(info));

            return info.TryGet<bool?>(AsyncProcessCompletedSimultaniosly) == true;
        }

        internal const string ProcessInfoAtEnd = nameof(ProcessInfoAtEnd);

        public static void SetProcessInfoAtEnd(
            this IDictionary<string, object> info,
            IDictionary<string, object> processInfo)
        {
            Check.ArgumentNotNull(info, nameof(info));

            info[ProcessInfoAtEnd] = processInfo.ToDictionaryStorage();
        }

        public static IDictionary<string, object> GetProcessInfoAtEnd(
            this IDictionary<string, object> info)
        {
            Check.ArgumentNotNull(info, nameof(info));

            return info.TryGet<IDictionary<string, object>>(ProcessInfoAtEnd);
        }

        #endregion

        #region Recalc

        private const string RecalcInfoMark = CardHelper.SystemKeyPrefix + "Recalc";

        private const string InfoAboutChangesInfoMark = CardHelper.SystemKeyPrefix + "InfoAboutChanges";

        private const string HasChangesInRoute = CardHelper.SystemKeyPrefix + "HasChangesInRoute";

        private const string RouteChanges = CardHelper.SystemKeyPrefix + "RouteChanges";

        public static bool GetRecalcFlag(
            this IDictionary<string, object> info)
        {
            Check.ArgumentNotNull(info, nameof(info));

            return info.TryGetValue(RecalcInfoMark, out var flagObj)
                && flagObj is bool flag
                && flag;
        }

        public static void SetRecalcFlag(
            this IDictionary<string, object> info)
        {
            Check.ArgumentNotNull(info, nameof(info));

            info[RecalcInfoMark] = BooleanBoxes.True;
        }

        public static bool GetRecalcFlag(
            this CardInfoStorageObject request)
        {
            Check.ArgumentNotNull(request, nameof(request));

            return request.Info.GetRecalcFlag();
        }

        public static void SetRecalcFlag(
            this CardInfoStorageObject request)
        {
            Check.ArgumentNotNull(request, nameof(request));

            request.Info.SetRecalcFlag();
        }

        public static InfoAboutChanges? GetInfoAboutChanges(
            this IDictionary<string, object> info)
        {
            Check.ArgumentNotNull(info, nameof(info));

            return info.TryGetValue(InfoAboutChangesInfoMark, out var iacObj) && iacObj is int iac
                ? (InfoAboutChanges?)iac
                : null;
        }

        public static void SetInfoAboutChanges(
            this IDictionary<string, object> info,
            InfoAboutChanges infoAboutChanges)
        {
            Check.ArgumentNotNull(info, nameof(info));

            info[InfoAboutChangesInfoMark] = (int)infoAboutChanges;
        }

        public static InfoAboutChanges? GetInfoAboutChanges(
            this CardInfoStorageObject request) => request.Info.GetInfoAboutChanges();

        public static void SetInfoAboutChanges(
            this CardInfoStorageObject request,
            InfoAboutChanges infoAboutChanges)
        {
            Check.ArgumentNotNull(request, nameof(request));

            request.Info.SetInfoAboutChanges(infoAboutChanges);
        }

        public static bool? GetHasRecalcChanges(
            this CardInfoStorageObject obj)
        {
            Check.ArgumentNotNull(obj, nameof(obj));

            return obj.Info.TryGet<bool?>(HasChangesInRoute);
        }

        public static void SetHasRecalcChanges(
            this CardInfoStorageObject response,
            bool hasChanges)
        {
            Check.ArgumentNotNull(response, nameof(response));

            response.Info[HasChangesInRoute] = hasChanges;
        }

        public static List<RouteDiff> GetRecalcChanges(
            this CardInfoStorageObject obj)
        {
            Check.ArgumentNotNull(obj, nameof(obj));

            var info = obj.Info;
            if (info.TryGetValue(RouteChanges, out var diffsObj)
                && diffsObj is IEnumerable diffsEnum)
            {
                var diffs = new List<RouteDiff>();
                foreach (var diffObj in diffsEnum)
                {
                    if (diffObj is Dictionary<string, object> diffStorage)
                    {
                        diffs.Add(new RouteDiff(diffStorage));
                    }
                }

                return diffs;
            }

            return null;
        }

        public static void SetRecalcChanges(
            this CardInfoStorageObject obj,
            IEnumerable<RouteDiff> diffs)
        {
            Check.ArgumentNotNull(obj, nameof(obj));
            Check.ArgumentNotNull(diffs, nameof(diffs));

            obj.Info[RouteChanges] = diffs.Select(p => p.GetStorage()).ToList();
        }

        /// <summary>
        /// Возвращает значение, показывающее, наличие скрытых пропущенных этапов.
        /// </summary>
        /// <param name="card">Карточка в которой выполняется проверка.</param>
        /// <returns>Значение true, если скрытые этапы содержатся, иначе - false.</returns>
        public static bool HasSkipStages(
            this Card card)
        {
            Check.ArgumentNotNull(card, nameof(card));

            card.Sections.TryGetValue(KrConstants.KrStages.Virtual, out var krStagesVirtual);
            var krStagesVirtualRows = krStagesVirtual?.Rows;

            return card.Info.TryGetValue(StagePositionsInfoMark, out var stagePositions)
                && stagePositions is List<object> stagePositionsList
                && stagePositionsList.Any(p =>
                {
                    if (!(p is Dictionary<string, object> pTyped))
                    {
                        return false;
                    }

                    return pTyped.TryGet<bool?>(nameof(KrStagePositionInfo.Skip)) == true
                        && krStagesVirtualRows?.Any(i => i.RowID == pTyped.TryGet<Guid?>(nameof(KrStagePositionInfo.RowID))) == false;
                });
        }

        /// <summary>
        /// Ключ элемента в <see cref="Card"/>.<see cref="CardInfoStorageObject.Info"/> содержащий значение флага, показывающего, треюуется ли показать пропущенные этапы. Тип значения: <see cref="bool"/>.
        /// </summary>
        private const string DontHideSkippedStagesInfoMark = nameof(DontHideSkippedStagesInfoMark);

        public static void DontHideSkippedStages(
            this Dictionary<string, object> storage,
            bool value = true)
        {
            Check.ArgumentNotNull(storage, nameof(storage));

            storage[DontHideSkippedStagesInfoMark] = BooleanBoxes.Box(value);
        }

        public static bool ConsiderSkippedStages(
            this CardInfoStorageObject request)
        {
            Check.ArgumentNotNull(request, nameof(request));

            return request.Info.TryGet(DontHideSkippedStagesInfoMark, false);
        }

        #endregion

        #region ICardMetadata

        public static async ValueTask<string> GetDocumentStateNameAsync(this ICardMetadata metadata, KrState state, CancellationToken cancellationToken = default)
        {
            Check.ArgumentNotNull(metadata, nameof(metadata));

            if (state.IsDefault())
            {
                return state.TryGetDefaultName();
            }

            var intState = (int)state;

            var record = (await metadata
                .GetEnumerationsAsync(cancellationToken))[DefaultSchemeHelper.KrDocStateSectionID]
                .Records
                .FirstOrDefault(p => p[ID].Equals(intState));
            if (!(record?[Name] is string name))
            {
                throw new InvalidOperationException(
                    $"State with ID = {state.ID} doesn't exist in KrDocState enumeration table.");
            }

            return name;
        }

        public static async ValueTask<string> GetStageStateNameAsync(this ICardMetadata metadata, KrStageState state, CancellationToken cancellationToken = default)
        {
            Check.ArgumentNotNull(metadata, nameof(metadata));

            if (state.IsDefault())
            {
                return state.TryGetDefaultName();
            }
            var intState = (int)state;
            var record = (await metadata
                .GetEnumerationsAsync(cancellationToken))[DefaultSchemeHelper.KrStageStateSectionID]
                .Records
                .FirstOrDefault(p => p[ID].Equals(intState));

            if (!(record?[Name] is string name))
            {
                throw new InvalidOperationException(
                    $"State with ID = {state.ID} doesn't exist in KrStageState enumeration table.");
            }

            return name;
        }

        #endregion
    }
}