﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tessa.Cards;
using Tessa.Files;
using Tessa.Platform.EDS;
using Tessa.Platform.Storage;

namespace Tessa.Extensions.Default.Shared.EDS
{
    public static class CAdESSignatureHelper
    {
        #region Constants

        public const string SignatureKey = CardHelper.SystemKeyPrefix + "signature";
        public const string EDSActionKey = CardHelper.SystemKeyPrefix + "edsAction";
        public const string CertificateKey = CardHelper.SystemKeyPrefix + "certificate";
        public const string SignatureTypeKey = CardHelper.SystemKeyPrefix + "signatureType";
        public const string SignatureProfileKey = CardHelper.SystemKeyPrefix + "signatureProfile";
        public const string TargetSignatureTypeKey = CardHelper.SystemKeyPrefix + "targetSignatureType";
        public const string TargetSignatureProfileKey = CardHelper.SystemKeyPrefix + "targetSignatureProfile";
        public const string SigningTimeKey = CardHelper.SystemKeyPrefix + "signingTime";
        public const string FileKey = CardHelper.SystemKeyPrefix + "file";
        public const string DigestAlgorithmKey = CardHelper.SystemKeyPrefix + "digestAlgorithm";
        public const string EncriptionAlgorithmKey = CardHelper.SystemKeyPrefix + "encriptionAlgoritm";

        #region SignatureValidation

        public const string SignatureValidationKey = CardHelper.SystemKeyPrefix + "signatureValidation";
        public const string ValidationInfoStateKey = "state";
        public const string SigningCertificateValitityDescKey = "signingCertificateValitityDesc";
        public const string SigningCertificateKey = "signingCertificate";
        public const string SigningCertificateChainKey = "signingCertificateChain";
        public const string SigningDateKey = "signingDate";
        public const string ReachedLevelErrorDescKey = "reachedLevelErrorDesc";
        public const string TimestampsTKey = "timestampsT";
        public const string TimestampsXRefsKey = "timestampsXRefs";
        public const string TimestampsXSigAndRefsKey = "timestampsXSigAndRefs";
        public const string TimestampsAKey = "timestampsA";
        public const string LogKey = "log";
        public const string ValidationTargetSignatureTypeKey = "targetSignatureType";
        public const string ValidationTargetSignatureProfileKey = "targetSignatureProfile";
        public const string ReachedSignatureTypeKey = "reachedSignatureType";
        public const string ReachedSignatureProfileKey = "reachedSignatureProfile";
        public const string CertsDataKey = "certsData";


        #region Certificate

        public const string CertificateSubjectNameKey = "subjectName";
        public const string CertificateIssuerNameKey = "issuerName";
        public const string CertificateValidFromKey = "validFrom";
        public const string CertificateValidToKey = "validTo";
        public const string CertificateCompanyKey = "company";
        public const string CertificateSerialNumberKey = "serialNumber";

        #endregion

        #region Timestamp

        public const string TimestampSerialNumberKey = "serialNumber";
        public const string TimestampCreationTimeKey = "creationTime";
        public const string TimestampIssuerNameKey = "issuerName";
        public const string TimestampStatusKey = "status";
        public const string TimestampStatusDescriptionKey = "statusDescription";
        public const string TimestampCertStatusKey = "certStatusDescription";
        public const string TimestampCertsKey = "certs";

        #endregion

        #region LogEntry

        public const string LogLogLevelKey = "logLevel";
        public const string LogMessageKey = "message";

        #endregion

        #region CertData

        public const string CertDataKey = "data";
        public const string CertStatusKey = "status";
        public const string CertStatusDescriptionKey = "statusDescription";
        private const string OcspsKey = "ocsps";
        private const string CrlsKey = "crls";
        private const string CrlSigAlgNameKey = "sigAlgName";
        private const string CrlSigAlgOidKey = "sigAlgOid";
        private const string CrlIssuerDNKey = "issuerDN";
        private const string CrlNextUpdateKey = "nextUpdate";
        private const string CrlThisUpdateKey = "thisUpdate";
        private const string OcspProducedAtKey = "producedAt";
        private const string OcspSigAlgNameKey = "sigAlgName";
        private const string OcspSigAlgOidKey = "sigAlgOid";
        private const string OcspSubjectDNKey = "subjectDN";
        private const string CertificateSourceTypeKey = "certificateSource";


        #endregion

        #endregion

        #region Signature attributes

        public const string HashKey = "messageDigest";
        public const string HashOidKey = "digestOid";
        public const string SignedAttributesKey = "signedAttributes";

        #endregion

        #endregion

        #region Static methods

        public static void SetSigningInfo(
           CardRequest request,
           byte[] signature,
           byte[] certificate)
        {
            request.Info[SignatureKey] = Convert.ToBase64String(signature);
            request.Info[EDSActionKey] = (int)EDSAction.Sign;
            request.Info[CertificateKey] = Convert.ToBase64String(certificate);
        }

        public static void SetGetBESInfo(
           CardRequest request,
           byte[] signature)
        {
            request.Info[SignatureKey] = Convert.ToBase64String(signature);
            request.Info[EDSActionKey] = (int)EDSAction.GetBESFromExtended;
        }

        public static void SetGetSignedAttributesInfo(
           CardRequest request,
           byte[] signature)
        {
            request.Info[SignatureKey] = Convert.ToBase64String(signature);
            request.Info[EDSActionKey] = (int)EDSAction.GetSignatureAttributesFromSignature;
        }

        public static byte[] GetBESInfo(CardResponse response)
        {
            return Convert.FromBase64String((string)response.Info[SignatureKey]);
        }

        public static SignedData GetSignedData(CardResponse response)
        {
            return new SignedData
            {
                Signature = Convert.FromBase64String((string)response.Info[SignatureKey]),
                SignatureType = (SignatureType)response.Info[SignatureTypeKey],
                SignatureProfile = (SignatureProfile)response.Info[SignatureProfileKey],
            };
        }

        public static SignatureAttributes GetSignatureAttributes(CardResponse response)
        {
            return new SignatureAttributes
            {
                Hash = Convert.FromBase64String((string)response.Info[HashKey]),
                HashOid = (string)response.Info[HashOidKey],
                Certificate = Convert.FromBase64String((string)response.Info[CertificateKey]),
                Signature = Convert.FromBase64String((string)response.Info[SignatureKey]),
                SignedAttributes = Convert.FromBase64String((string)response.Info[SignedAttributesKey]),
            };
        }

        public static void SetValidationInfo(CardRequest request, SignedData signedData)
        {
            request.Info[SignatureKey] = Convert.ToBase64String(signedData.Signature);
            request.Info[EDSActionKey] = (int)EDSAction.Verify;
            request.Info[TargetSignatureTypeKey] = signedData.SignatureType;
            request.Info[TargetSignatureProfileKey] = signedData.SignatureProfile;
        }

        public static void SetToBeSignedInfo(CardRequest request, byte[] certificate, byte[] file, DateTime signingTime, string digestAlgorithmOid, string encriptionAlgorithmOid)
        {
            request.Info[SignatureKey] = Convert.ToBase64String(file);
            request.Info[EDSActionKey] = (int)EDSAction.GetToBeSigned;
            request.Info[CertificateKey] = Convert.ToBase64String(certificate);
            request.Info[SigningTimeKey] = signingTime;
            request.Info[DigestAlgorithmKey] = digestAlgorithmOid;
            request.Info[EncriptionAlgorithmKey] = encriptionAlgorithmOid;
        }

        public static byte[] GetToBeSignedInfo(CardResponse response)
        {
            return Convert.FromBase64String((string)response.Info[SignatureKey]);
        }

        public static IReadOnlyCollection<SignatureValidationInfo> GetValidationInfo(CardResponse response)
        {
            var result = new List<SignatureValidationInfo>();

            var rows = response.Info.TryGet<List<object>>(SignatureValidationKey);
            foreach (var rowObj in rows)
            {
                var row = rowObj as Dictionary<string, object>;
                var item = new SignatureValidationInfo();
                item.State = (FileSignatureState)row.TryGet<int>(ValidationInfoStateKey);
                item.SigningCertificateValitityDesc = row.TryGet<string>(SigningCertificateValitityDescKey);
                item.SigningCertificate = GetCertificateObject(row.TryGet<Dictionary<string, object>>(SigningCertificateKey));
                item.SigningDate = row.TryGet<DateTime?>(SigningDateKey);
                item.ReachedLevelErrorDesc = row.TryGet<List<object>>(ReachedLevelErrorDescKey).Select(x => (x as List<object>).Cast<string>().ToArray()).ToArray();
                item.TimestampsT = row.TryGet<List<object>>(TimestampsTKey).Select(x => GetTimestampObject(x as Dictionary<string, object>));
                item.TimestampsXRefs = row.TryGet<List<object>>(TimestampsXRefsKey).Select(x => GetTimestampObject(x as Dictionary<string, object>));
                item.TimestampsXSigAndRefs = row.TryGet<List<object>>(TimestampsXSigAndRefsKey).Select(x => GetTimestampObject(x as Dictionary<string, object>));
                item.TimestampsA = row.TryGet<List<object>>(TimestampsAKey).Select(x => GetTimestampObject(x as Dictionary<string, object>));
                item.Log = row.TryGet<List<object>>(LogKey).Select(x => GetLogObject(x as Dictionary<string, object>));
                item.ReachedSignatureType = (SignatureType)row.TryGet<int>(ReachedSignatureTypeKey);
                item.ReachedSignatureProfile = (SignatureProfile)row.TryGet<int>(ReachedSignatureProfileKey);
                item.TargetSignatureType = (SignatureType)row.TryGet<int>(ValidationTargetSignatureTypeKey);
                item.TargetSignatureProfile = (SignatureProfile)row.TryGet<int>(ValidationTargetSignatureProfileKey);
                item.CertsData = GetCertsData(row.TryGet<Dictionary<string, object>>(CertsDataKey));

                result.Add(item);
            }

            return result;
        }

        public static byte[] GetBesSignaturePkcs7Info(CardResponse response)
        {
            return Convert.FromBase64String((string)response.Info[SignatureKey]);
        }

        public static void SetBesSignaturePkcs7Info(CardRequest request, byte[] certificate, byte[] file, DateTime signingTime, byte[] signature, string digestAlgorithmOid, string encriptionAlgorithmOid)
        {
            request.Info[FileKey] = Convert.ToBase64String(file);
            request.Info[EDSActionKey] = (int)EDSAction.GetBesSignature;
            request.Info[CertificateKey] = Convert.ToBase64String(certificate);
            request.Info[SigningTimeKey] = signingTime;
            request.Info[SignatureKey] = Convert.ToBase64String(signature);
            request.Info[DigestAlgorithmKey] = digestAlgorithmOid;
            request.Info[EncriptionAlgorithmKey] = encriptionAlgorithmOid;
        }

        #endregion

        #region Private static

        private static IEDSCertificate GetCertificateObject(Dictionary<string, object> obj)
        {
            return new EDSCertificate(
                null,
                obj.TryGet<string>(CertificateCompanyKey),
                obj.TryGet<string>(CertificateSubjectNameKey),
                obj.TryGet<string>(CertificateIssuerNameKey),
                obj.TryGet<string>(CertificateSerialNumberKey),
                obj.TryGet<DateTime?>(CertificateValidFromKey),
                obj.TryGet<DateTime?>(CertificateValidToKey)
                );
        }

        private static TimestampInfo GetTimestampObject(Dictionary<string, object> obj)
        {
            return new TimestampInfo
            {
                SerialNumber = obj.TryGet<string>(TimestampSerialNumberKey),
                CreationTime = obj.TryGet<DateTime>(TimestampCreationTimeKey),
                IssuerName = obj.TryGet<string>(TimestampIssuerNameKey),
                Status = obj.TryGet<string>(TimestampStatusKey),
                StatusDescription = obj.TryGet<string>(TimestampStatusDescriptionKey),
                CertStatus = obj.TryGet<string>(TimestampCertStatusKey),
                CertStatusDescription = obj.TryGet<string>(TimestampCertStatusKey),
                State = (FileSignatureState)obj.TryGet<int>(ValidationInfoStateKey)
            };
        }

        private static ICAdESLoggerEntry GetLogObject(Dictionary<string, object> obj)
        {
            return new CAdESLoggerEntry((CAdESLogLevel)obj.TryGet<int>(LogLogLevelKey), obj.TryGet<string>(LogMessageKey));
        }

        private static Dictionary<string, CertDataAndVerification> GetCertsData(Dictionary<string, object> obj)
        {
            var result = new Dictionary<string, CertDataAndVerification>();

            foreach (var pair in obj)
            {
                result.Add(pair.Key, GetCertData(pair.Value as Dictionary<string, object>));
            }

            return result;
        }

        private static CertDataAndVerification GetCertData(Dictionary<string, object> obj)
        {
            return new CertDataAndVerification
            {
                Company = obj.TryGet<string>(CertificateCompanyKey),
                SubjectName = obj.TryGet<string>(CertificateSubjectNameKey),
                IssuerName = obj.TryGet<string>(CertificateIssuerNameKey),
                SerialNumber = obj.TryGet<string>(CertificateSerialNumberKey),
                ValidFrom = obj.TryGet<DateTime?>(CertificateValidFromKey),
                ValidTo = obj.TryGet<DateTime?>(CertificateValidToKey),
                Data = obj.TryGet<byte[]>(CertDataKey),
                Status = obj.TryGet<string>(CertStatusKey),
                StatusDescription = obj.TryGet<string>(CertStatusDescriptionKey),
                CertificateSourceType = (CertificateSourceType)obj.TryGet<int>(CertificateSourceTypeKey),
                OcspInfos = obj.TryGet<List<object>>(OcspsKey).Select(x => GetOcspObject(x as Dictionary<string, object>)),
                CrlInfos = obj.TryGet<List<object>>(CrlsKey).Select(x => GetCrlObject(x as Dictionary<string, object>))
            };
        }

        private static CrlInfo GetCrlObject(Dictionary<string, object> dictionary)
        {
            return new CrlInfo
            {

                NextUpdate = dictionary.TryGet<DateTime?>(CrlNextUpdateKey),
                ThisUpdate = dictionary.TryGet<DateTime?>(CrlThisUpdateKey),
                SigAlgName = dictionary.TryGet<string>(CrlSigAlgNameKey),
                SigAlgOid = dictionary.TryGet<string>(CrlSigAlgOidKey),
                IssuerDN = dictionary.TryGet<string>(CrlIssuerDNKey),
            };
        }

        private static OcspInfo GetOcspObject(Dictionary<string, object> dictionary)
        {
            return new OcspInfo
            {
                ProducedAt = dictionary.TryGet<DateTime?>(OcspProducedAtKey),
                SigAlgName = dictionary.TryGet<string>(OcspSigAlgNameKey),
                SigAlgOid = dictionary.TryGet<string>(OcspSigAlgOidKey),
                SubjectDN = dictionary.TryGet<string>(OcspSubjectDNKey),
            };
        }

        #endregion
    }
}
