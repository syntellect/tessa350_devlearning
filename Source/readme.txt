Для компиляции расширений требуется:
1. Установленная Visual Studio 2019 версии 16.5 или позже. Возможна установка бесплатной Community Edition.
2. Если используется другая IDE, помимо Visual Studio, то потребуется также установить .NET Core SDK 3.1.x последней доступной версии: https://dotnet.microsoft.com/download/dotnet-core/3.1

Для развёртывания веб-сервиса на IIS требуется установить .NET Core Runtime & Hosting Bundle версии 3.1.4 или старше:
https://dotnet.microsoft.com/download/dotnet-core/3.1
